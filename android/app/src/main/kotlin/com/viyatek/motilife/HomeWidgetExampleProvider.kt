package com.viyatek.motilife

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.RemoteViews
import es.antonborri.home_widget.HomeWidgetProvider

import android.net.Uri
import es.antonborri.home_widget.HomeWidgetBackgroundIntent
import es.antonborri.home_widget.HomeWidgetLaunchIntent


class HomeWidgetExampleProvider : HomeWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray, widgetData: SharedPreferences) {
        appWidgetIds.forEach { widgetId ->
            val views = RemoteViews(context.packageName, R.layout.example_layout).apply {
                //Open App on Widget Click
                val pendingIntent = HomeWidgetLaunchIntent.getActivity(
                        context,
                        MainActivity::class.java)
                setOnClickPendingIntent(R.id.widget_container, pendingIntent)
/*                setOnClickPendingIntent(R.id.widget_container,
                        PendingIntent.getActivity(context, 0, Intent(context, MainActivity::class.java), 0))*/
                setTextViewText(R.id.widget_title, widgetData.getString("title", null)
                        ?: "41")
                setTextViewText(R.id.widget_message, widgetData.getString("message", null)
                        ?: "Happiness is not an ideal of reason, but of imagination.")
                setTextViewText(R.id.widget_author, widgetData.getString("author", null)
                        ?: "Immanuel Kant")
                /*setTextViewText(R.id.widget_theme, widgetData.getString("themeName", null)
                        ?: "No Theme Set")*/
                setImageViewResource(R.id.widget_theme,
                        context.getResources().getIdentifier(
                                widgetData.getString("themeName", null),
                                "drawable",
                                context.getPackageName()));

                val backgroundIntent = HomeWidgetBackgroundIntent.getBroadcast(
                        context,
                        Uri.parse("homeWidgetExample://titleClicked")
                )
                setOnClickPendingIntent(R.id.widget_title, backgroundIntent)
                val message = widgetData.getString("message", null)
                setTextViewText(R.id.widget_message, message
                        ?: "No Message Set")
                // Detect App opened via Click inside Flutter
                val pendingIntentWithData = HomeWidgetLaunchIntent.getActivity(
                        context,
                        MainActivity::class.java,
                        Uri.parse("homeWidgetExample://message?message=$message"))
                setOnClickPendingIntent(R.id.widget_message, pendingIntentWithData)

            }

            appWidgetManager.updateAppWidget(widgetId, views)
        }
    }
}