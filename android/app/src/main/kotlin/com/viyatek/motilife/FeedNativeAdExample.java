/*
package com.viyatek.motilife;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.widget.TextView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import io.flutter.plugins.googlemobileads.GoogleMobileAdsPlugin.NativeAdFactory;
import java.util.Map;
import com.google.android.gms.ads.nativead.MediaView;
import com.viyatek.ads.admob.BindAdViewsToLayout;
import com.viyatek.ads.admob.FeedNativeAdFiller;
import com.viyatek.ads.databinding.ThirdVersionAdmobStandloneBinding;
import com.viyatek.ads.databinding.SecondVersionAdmobStandloneFeedFlutterBinding;

import androidx.constraintlayout.widget.ConstraintLayout;
import android.widget.RatingBar;
import android.widget.ImageView;
import android.view.View;


class FeedNativeAdExample implements NativeAdFactory {
    private final LayoutInflater layoutInflater;
    private final BindAdViewsToLayout adBinder = new BindAdViewsToLayout();
    private final FeedNativeAdFiller nativeAdFiller = new FeedNativeAdFiller();
    FeedNativeAdExample(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    @Override
    public NativeAdView createNativeAd(NativeAd nativeAd, Map<String, Object> customOptions) {
        //View theAdContainer = layoutInflater.inflate(R.layout.my_native_ad, null);

        SecondVersionAdmobStandloneFeedFlutterBinding binding = SecondVersionAdmobStandloneFeedFlutterBinding.inflate(layoutInflater);//ThirdVersionAdmobStandloneBinding.inflate(layoutInflater);
        NativeAdView theAdContainer = (NativeAdView) binding.getRoot();

        adBinder.bindView(binding);
        nativeAdFiller.handle(nativeAd, theAdContainer);
        return theAdContainer;
    }
}
*/
