import 'dart:convert';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:motilife/ExtractedWidgets/first_review_message.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:motilife/prefs.dart';
import 'dart:io' show Platform;

import 'notifier_provider.dart';

class DatabaseProvider {
  static final _databaseName = "motivation.db";
  static final dbVersion = 34;

  static final userTopicTable = 'user_topic_table';
  static final userTopicId = 'userTopicId';
  static final isSelectedTopic = 'topicSelectedStatus';

  static final userSectionTable = 'user_section_table';
  static final userSectionId = 'userSectionId';
  static final isSelectedSection = 'sectionSelectedStatus';
  static final isUnlockedSection = 'sectionIsUnlockedStatus';

  static final userQuotesTable = 'user_quote_table';
  static final userQuotesId = 'userQuoteId';
  static final isSeen = 'isSeenStatus';
  static final isBookmarked = 'quoteBookmarkedStatus';
  static final isCollected = 'quoteCollectedStatus';


  static final userThemesTable = 'userThemesTable';
  static final userLiveThemesTable = 'userLiveThemesTable';
  static final userThemeName = 'userThemeName';
  static final isUnlockedTheme = 'isUnlockedTheme';

  static final musicName = "musicName";
  static final musicId = "musicId";
  static final musicUrl = "musicUrl";
  static final isPremiumMusic = "isPremiumMusic";

  static final userMusicTable = "myUserMusicTable";
  static final userMusicId = "userMusicId";
  static final isUnlockedMusic = "isUnlockedMusic";

  static final userFontTable = "userFontTable";
  static final userFontId = "userFontId";
  static final isUnlockedFont = "isUnlockedFont";


  static final tableMyMusic = 'my_music_table';
  static final tableThemes = 'my_theme_table';
  static final tableLiveThemes = 'my_live_theme_table';
  static final tableQuotes = 'quotes_table';
  static final tableSec = 'section_table';
  static final tableTopic = 'topic_table';
  static final tableFont = 'font_table';


  DatabaseProvider._privateConstructor();
  static final DatabaseProvider instance =
      DatabaseProvider._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    //print('Getting database...');
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  initDatabase() async {
//    print('This text comes from initDatabase()');
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(
      path,
      version: dbVersion,
      onCreate: _onCreate,
      onUpgrade: onUpgrade,
      readOnly: false,
      singleInstance: true,
    );
  }


  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    //int dbLockValue = 2;
    String languageCode = await SharedPreferencesHelper.getLocalLanguageCode();
/*    var connectivityResult;
    connectivityResult = await (Connectivity().checkConnectivity());
    if(connectivityResult != ConnectivityResult.none) {
      RemoteConfig remoteConfig = await functions.setupRemoteConfig();
      try {
        await remoteConfig.fetch(expiration: const Duration(seconds: 3));
        await remoteConfig.activateFetched();
        dbLockValue = remoteConfig.getInt('db_lock_value');
        print("Db Lock value: $dbLockValue");
        await SharedPreferencesHelper.setDbLockValue(dbLockValue);
      } on FetchThrottledException catch (exception) {
        print(exception);
        await SharedPreferencesHelper.setDbLockValue(dbLockValue);
      } catch (exception) {
        print(exception);
        await SharedPreferencesHelper.setDbLockValue(dbLockValue);
      }
    }*/
    print('DB version number $version is ON CREATE...');
    //await SharedPreferencesHelper.setCurrentAppVersion(upToDateDbVersion);
    String initCreateScript =
        await rootBundle.loadString("assets/script_create.sql");
    List<String> scripts = initCreateScript.split(";");
    if(Platform.isAndroid) db.execute("BEGIN TRANSACTION;");
    scripts.forEach((v) {
      if (v.isNotEmpty) {
        db.execute(v.trim());
      } else {
        print('$v is empty');
      }
    });
    print("Local lang code: $languageCode");
    String initInsertScript = await rootBundle.loadString("assets/languages/$languageCode/insert_general_data.sql");
    Iterable<String> list = LineSplitter.split(initInsertScript);
    list.forEach((e) async {
      if (e.isNotEmpty) {
        await db.transaction((txn) async {
          await txn.rawInsert(e.trim());
        });
        //db.execute(e.trim());
        //print("> $e");
      } else {
        print('$e is empty');
      }
    });
    String initInsertOtherData = await rootBundle.loadString("assets/languages/$languageCode/script_insert2.sql");
    Iterable<String> list1 = LineSplitter.split(initInsertOtherData);
    list1.forEach((e) async {
      if (e.isNotEmpty) {
        await db.transaction((txn) async {
          await txn.rawInsert(e.trim());
        });
        //db.execute(e.trim());
        //print("> $e");
      } else {
        print('$e is empty');
      }
    });
    await db.transaction((txn) async {
      await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userTopicTable (
            $userTopicId INTEGER PRIMARY KEY,
            $isSelectedTopic INTEGER
          )
          ''');
      await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userSectionTable (
            $userSectionId INTEGER PRIMARY KEY,
            $isUnlockedSection INTEGER,
            $isSelectedSection INTEGER
            )
    ''');
      await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userMusicTable (
            $userMusicId INTEGER PRIMARY KEY,
            $isUnlockedMusic INTEGER
            )
    ''');
      await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userThemesTable (
            $userThemeName TEXT PRIMARY KEY,
            $isUnlockedTheme INTEGER
          )
          ''');
      await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userLiveThemesTable (
            $userThemeName TEXT PRIMARY KEY,
            $isUnlockedTheme INTEGER
          )
          ''');
      await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userFontTable (
            $userFontId INTEGER PRIMARY KEY,
            $isUnlockedFont INTEGER
          )
          ''');
      _insertDefaultUserDataOnCreate(); //After tables had been created, user tables' data will be added row by row
    });
    if(Platform.isAndroid) db.execute("COMMIT;");

    print('Created');
  }

  _insertDefaultUserDataOnCreate() async {
    final topics = await queryAllTopics();
    topics.forEach((topicRow) async {
      await insertDefaultUserTopicData(topicRow['topicid']);
    });
    final sections = await queryAllSections();
    sections.forEach((sectionRow) async {
      await insertDefaultUserSectionData(
          sectionRow['sectionid'], sectionRow['isPremium']);
    });
    final themes = await queryAllThemes();
    themes.forEach((theme) async {
      await insertDefaultUserThemeData(theme['themeName']);
    });
    final liveThemes = await queryAllLiveThemes();
    liveThemes.forEach((liveTheme) async {
      await insertDefaultUserLiveThemeData(liveTheme['themeName']);
    });
    final musics = await queryAllMusics();
    musics.forEach((music) async {
      print(music['musicName']);
      await insertDefaultUserMusicData(music['musicId']);
    });
    final fonts = await queryAllFonts();
    fonts.forEach((font) async {
      await insertDefaultUserFontData(font['fontId']);
    });
  }

  _insertDefaultUserDataOnUpgrade() async {
    final topics = await queryAllTopics();
    topics.forEach((topicRow) async {
      await insertDefaultUserTopicData(topicRow['topicid']);
    });
    final sections = await queryAllSections();
    sections.forEach((sectionRow) async {
      await insertDefaultUserSectionData(
          sectionRow['sectionid'], sectionRow['isPremium']);
    });
    final quotes = await queryAllQuotes();
    quotes.forEach((quoteRow) async {
      await insertDefaultUserQuotesData(quoteRow['quoteid']);
    });
    final themes = await queryAllThemes();
    themes.forEach((theme) async {
      await insertDefaultUserThemeData(theme['themeName']);
    });
    final liveThemes = await queryAllLiveThemes();
    liveThemes.forEach((theme) async {
      await insertDefaultUserLiveThemeData(theme['themeName']);
    });
    final musics = await queryAllMusics();
    musics.forEach((music) async {
      print(music['musicName']);
      await insertDefaultUserMusicData(music['musicId']);
    });
    final fonts = await queryAllFonts();
    fonts.forEach((font) async {
      await insertDefaultUserFontData(font['fontId']);
    });
  }

  writeDefaultQuotesData() async {
    final quotes = await queryAllQuotes();
    quotes.forEach((quoteRow) async {
      print('INSERT INTO "$userQuotesTable" ("$userQuotesId","$isSeen","$isCollected","$isBookmarked") VALUES (${quoteRow["quoteid"]}, 0, 0, 0);');
      await insertDefaultUserQuotesData(quoteRow['quoteid']);
    });
  }


  Future onUpgrade(Database db, int oldVersion, int newVersion) async {
    String languageCode = await SharedPreferencesHelper.getLocalLanguageCode();
    /*if (oldVersion < 6) {
      db.execute(
          "CREATE TEMPORARY TABLE user_quote_backup($userQuotesId, $isSeen, $isCollected, $isBookmarked);");
      db.execute(
          "INSERT INTO user_quote_backup SELECT $userQuotesId, $isSeen, $isCollected, $isBookmarked FROM $userQuotesTable;");
      db.execute("DROP TABLE $userQuotesTable;");
      db.execute(
          "CREATE TABLE $userQuotesTable($userQuotesId, $isSeen, $isCollected, $isBookmarked);");
      db.execute(
          "INSERT INTO $userQuotesTable SELECT $userQuotesId, $isSeen, $isCollected, $isBookmarked FROM user_quote_backup;");
      db.execute("DROP TABLE user_quote_backup;");
      if (Platform.isAndroid) db.execute("COMMIT;");
    }*/
    //int dbLockValue = await SharedPreferencesHelper.getDbLockValue();
    if(oldVersion<15){
      if(Platform.isAndroid) db.execute("BEGIN TRANSACTION;");
      db.execute("DROP TABLE IF EXISTS musics_list_table;");
      await db.transaction((txn) async {
        await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userMusicTable (
            $userMusicId INTEGER PRIMARY KEY,
            $isUnlockedMusic INTEGER
            )
    ''');
      });
      if (Platform.isAndroid) db.execute("COMMIT;");
    }
    if(oldVersion<17) {
      if(Platform.isAndroid) db.execute("BEGIN TRANSACTION;");
      await db.transaction((txn) async {
        await txn.execute('''
          CREATE TABLE IF NOT EXISTS $userFontTable (
            $userFontId INTEGER PRIMARY KEY,
            $isUnlockedFont INTEGER
          )
          ''');
      });
      if (Platform.isAndroid) db.execute("COMMIT;");
    }
    if(oldVersion<19) {
      if(Platform.isAndroid) db.execute("BEGIN TRANSACTION;");
      db.execute("DROP TABLE IF EXISTS themes_table;");
      if (Platform.isAndroid) db.execute("COMMIT;");
    }
    if(oldVersion<27) {
      if(Platform.isAndroid) db.execute("BEGIN TRANSACTION;");
      await db.execute('''
          CREATE TABLE IF NOT EXISTS $userLiveThemesTable (
            $userThemeName TEXT PRIMARY KEY,
            $isUnlockedTheme INTEGER
          )
          ''');
      if (Platform.isAndroid) db.execute("COMMIT;");
    }
    print('DB is ON UPGRADE... old version: $oldVersion, new version: $newVersion');
    //wait SharedPreferencesHelper.setCurrentAppVersion(upToDateDbVersion);
    String createScript = await rootBundle.loadString("assets/script_create.sql"); //add new data tables after dropping existing ones
    List<String> scripts = createScript.split(";");
    if(Platform.isAndroid) db.execute("BEGIN TRANSACTION;");
    scripts.forEach((v) async {
      if (v.isNotEmpty) {
        await db.transaction((txn) async {
          txn.execute(v.trim());
        });
      }
    });
    String insertScript = await rootBundle.loadString("assets/languages/$languageCode/insert_general_data_on_upgrade.sql");
    Iterable<String> list = LineSplitter.split(insertScript);
    list.forEach((e) async {
      if (e.isNotEmpty) {
        await db.transaction((txn) async {
          txn.execute(e.trim());
        });
      }
    });
    String initInsertOtherData = await rootBundle.loadString("assets/languages/$languageCode/script_insert2.sql");
    Iterable<String> list1 = LineSplitter.split(initInsertOtherData);
    list1.forEach((e) async {
      if (e.isNotEmpty) {
        await db.transaction((txn) async {
          await txn.rawInsert(e.trim());
        });
        //db.execute(e.trim());
        //print("> $e");
      } else {
        print('$e is empty');
      }
    });
    _insertDefaultUserDataOnUpgrade();
    if(Platform.isAndroid) db.execute("COMMIT;");
  }

  Future<List<Map<String, dynamic>>> querySelectedUserSection(
      int sectionId) async {
    final db = await database;
    print(await db.rawQuery(
        "SELECT * FROM $userSectionTable WHERE userSectionId=$sectionId"));
    return await db.rawQuery(
        "SELECT * FROM $userSectionTable WHERE userSectionId=$sectionId");
  }

  Future<List<Map<String, dynamic>>> queryAllTrendingQuotes(int topicId) async {
    final db = await database;
    //print(await db.query('SELECT s.section.id FROM $tableSec s INNER JOIN $userSectionTable u ON (s.sectionid = u.userSectionId) WHERE (s.topicid1=$topicId OR s.topicid2=$topicId OR s.topicid3=$topicId)'));
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus, s.topicid1, s.sectionimagename, s.sectionname FROM quotes_table q LEFT JOIN section_table s ON (q.sectionid1=s.sectionid OR q.sectionid2=s.sectionid OR q.sectionid3=s.sectionid) LEFT JOIN user_quote_table u ON (q.quoteid=u.userQuoteId) WHERE (s.topicid1=$topicId OR s.topicid2=$topicId OR s.topicid3=$topicId)');
  }


  Future<List<Map<String, dynamic>>> queryAllMusics() async {
    final db = await database;
    return await db.query(tableMyMusic);
  }

  Future<List<Map<String, dynamic>>> queryAllMusicsWithUserData() async {
    final db = await database;
    return await db.rawQuery("SELECT * FROM $tableMyMusic m INNER JOIN $userMusicTable u ON m.$musicId = u.$userMusicId");
  }

  Future<List<Map<String, dynamic>>> queryAllFontsWithUserData() async {
    final db = await database;
    return await db.rawQuery("SELECT * FROM $tableFont f INNER JOIN $userFontTable u ON f.fontId = u.userFontId");
  }

  Future<List<Map<String, dynamic>>> queryAllUserThemes() async {
    final db = await database;
    return await db.query(userThemesTable);
  }

  Future<List<Map<String, dynamic>>> queryAllUserLiveThemes() async {
    final db = await database;
    return await db.query(userLiveThemesTable);
  }

  Future<List<Map<String, dynamic>>> querySelectedUserTheme(
      String themeName) async {
    final db = await database;
    //Database db = await instance.database;
    return await db.rawQuery(
        "SELECT * FROM userThemesTable WHERE userThemeName='$themeName'");
  }

  Future<List<Map<String, dynamic>>> queryAllSectionsWithUserData() async {
    final db = await database;
    return await db.rawQuery("SELECT * FROM section_table s INNER JOIN $userSectionTable u ON s.sectionid = u.userSectionId");
  }

  Future<List<Map<String, dynamic>>> queryAllThemesWithUserData() async {
    final db = await database;
    return await db.rawQuery(
        "SELECT * FROM $tableThemes t INNER JOIN $userThemesTable u ON t.themeName = u.userThemeName order by RANDOM()");
  }

  Future<List<Map<String, dynamic>>> queryAllLiveThemesWithUserData() async {
    final db = await database;
    return await db.rawQuery(
        "SELECT * FROM $tableLiveThemes t INNER JOIN $userLiveThemesTable u ON t.themeName = u.userThemeName order by RANDOM()");
  }

  Future<List<Map<String, dynamic>>> queryAllThemes() async {
    final db = await database;
    return await db.query(tableThemes);
  }

  Future<List<Map<String, dynamic>>> queryAllLiveThemes() async {
    final db = await database;
    return await db.query(tableLiveThemes);
  }

  Future<List<Map<String, dynamic>>> queryAllFonts() async {
    final db = await database;
    return await db.query(tableFont);
  }


  Future<List<Map<String, dynamic>>> querySelectedTimeRange() async {
    final db = await database;
    //Database db = await instance.database;
    return await db.rawQuery(
        'SELECT startHour FROM user_Notification_TimeRange WHERE isChosenStatus=1');
  }

  Future<List<Map<String, dynamic>>> queryAllTopicsWithUserData() async {
    final db = await database;
    //Database db = await instance.database;
    return await db.rawQuery(
        'SELECT t.topicid, t.topicname, t.topicimagename, u.topicSelectedStatus FROM topic_table t INNER JOIN user_topic_table u ON t.topicid=u.userTopicId');
  }

  Future<List<Map<String, dynamic>>> queryAllSectionsOfAnyTopicWithUserData(
      int topicId) async {
    final db = await database;
    return await db.rawQuery(
        'SELECT s.sectionid, u.sectionSelectedStatus, u.sectionIsUnlockedStatus FROM section_table s INNER JOIN user_section_table u ON s.sectionid=u.userSectionId WHERE s.topicid1=$topicId OR s.topicid2=$topicId OR s.topicid3=$topicId');
  }

  Future<List<Map<String, dynamic>>> queryAllQuotesOfAnySectionWithUserData(
      int sectionId) async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus FROM quotes_table q INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId WHERE (q.sectionid1=$sectionId OR q.sectionid2=$sectionId OR q.sectionid3=$sectionId) order by RANDOM()');
  }

  Future<List<Map<String, dynamic>>> queryAllQuotesOfAnySectionWithIsSeenData(
      int sectionId) async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, q.priority, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus FROM quotes_table q INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId WHERE (q.sectionid1=$sectionId OR q.sectionid2=$sectionId OR q.sectionid3=$sectionId) AND u.isSeenStatus=0 order by RANDOM()');
  }

  Future<List<Map<String, dynamic>>>
      queryAllQuotesOfAnySectionWithUserAndSectionData(int sectionId) async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus, s.sectionid, s.sectionimagename, s.sectionname FROM quotes_table q LEFT JOIN user_quote_table u ON q.quoteid=u.userQuoteId LEFT JOIN $tableSec s ON (q.sectionid1=s.sectionid) WHERE (q.sectionid1=$sectionId OR q.sectionid2=$sectionId OR q.sectionid3=$sectionId) order by RANDOM()');
  }

  Future<int> queryCountAllQuotesOfAnySectionWithUserData(int sectionId) async {
    final db = await database;
    final result = await db.rawQuery(
        'SELECT COUNT(*) FROM quotes_table q INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId WHERE (q.sectionid1=$sectionId OR q.sectionid2=$sectionId OR q.sectionid3=$sectionId)');
    final count = Sqflite.firstIntValue(result);
    return count;
  }

  Future<List<Map<String, dynamic>>> queryAllTopics() async {
    final db = await database;
    return await db.query(tableTopic);
  }

  Future<List<Map<String, dynamic>>> queryAllSections() async {
    final db = await database;
    print(tableSec.length);
    return await db.query(tableSec);
  }

  Future<List<Map<String, dynamic>>> queryAllQuotes() async {
    final db = await database;
    //print(await db.query(tableQuotes));
    return await db.query(tableQuotes);
  }

  Future<List<Map<String, dynamic>>> queryAllUnlockedQuotes() async {
    final db = await database;
    return await db.rawQuery(
        "SELECT * FROM quotes_table q INNER JOIN user_section_table u ON (q.sectionid1=u.userSectionId) WHERE u.sectionIsUnlockedStatus=1");
  }

  Future<List<Map<String, dynamic>>> queryAllUnlockedAndSelectedSections() async {
    final db = await database;
    return await db.rawQuery(
        "SELECT s.sectionid FROM $tableSec s INNER JOIN $userSectionTable u ON (s.sectionid=u.userSectionId) WHERE u.sectionIsUnlockedStatus=1 AND u.sectionSelectedStatus=1 order by RANDOM()");
  }

  Future<List<Map<String, dynamic>>> queryAllUserTopics() async {
    final db = await database;
    return await db.query(userTopicTable);
  }

  Future<List<Map<String, dynamic>>> queryAllUserSections() async {
    final db = await database;
    return await db.query(userSectionTable);
  }

  Future<List<Map<String, dynamic>>> queryAllUserQuotes() async {
    final db = await database;
    return await db.query(userQuotesTable);
  }

  Future<List<Map<String, dynamic>>> queryAllSectionsOfAnyTopic(
      int topicId) async {
    final db = await database;
    return await db.rawQuery(
        'SELECT * FROM section_table s INNER JOIN $userSectionTable u ON s.sectionid=u.userSectionId WHERE s.topicid1=$topicId OR s.topicid2=$topicId OR s.topicid3=$topicId');
  }

  //Show Quotes on HomePage
  Future<List<Map<String, dynamic>>> queryAllQuotesPreferencesOfUser() async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus FROM $tableQuotes q LEFT JOIN $userQuotesTable u ON q.quoteid=u.userQuoteId LEFT JOIN $userSectionTable s ON (q.sectionid1=s.userSectionId) WHERE u.isSeenStatus=0 AND s.sectionIsUnlockedStatus=1 order by RANDOM()');
  }

  Future<List<Map<String, dynamic>>>
      queryAllQuotesPreferencesOfUserWithSeen() async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus FROM quotes_table q LEFT JOIN user_quote_table u ON q.quoteid=u.userQuoteId LEFT JOIN $userSectionTable s ON (q.sectionid1=s.userSectionId) WHERE s.sectionIsUnlockedStatus=1 order by RANDOM()');
  }

  Future<int> queryCountUserPrefs() async {
    final db = await database;
    final result = await db.rawQuery(
        'SELECT COUNT(*) FROM quotes_table q INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId WHERE u.isSeenStatus=0');
    final count = Sqflite.firstIntValue(result);
    return count;
  }

//Show Bookmarked Quote on Bookmarked Tab
  Future<List<Map<String, dynamic>>> queryAllBookmarkedQuotesOfUser() async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus, s.sectionimagename FROM quotes_table q, section_table s INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId AND (q.sectionid1=s.sectionid) WHERE u.quoteBookmarkedStatus=1');
  }

  Future<List<Map<String, dynamic>>> queryAllSeenQuotesOfUser() async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus FROM quotes_table q INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId WHERE u.isSeenStatus=1');
  }

  Future<int> queryCountAllSeenQuotes() async {
    final db = await database;
    final result = await db.rawQuery(
        'SELECT COUNT(*) FROM quotes_table q INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId WHERE u.isSeenStatus=1');
    final count = Sqflite.firstIntValue(result);
    return count;
  }

  Future<List<Map<String, dynamic>>> queryCollectedQuotesOfAnyFile(
      int fileId) async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus, s.sectionimagename FROM quotes_table q, section_table s INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId AND (q.sectionid1=s.sectionid) WHERE u.quoteCollectedStatus=$fileId');
  }

  //query Single Quotes
  Future<List<Map<String, dynamic>>> querySingleQuote(int quoteId) async {
    final db = await database;
    return await db.rawQuery(
        'SELECT q.quoteid, q.quote, q.authorname, u.quoteBookmarkedStatus, u.quoteCollectedStatus, u.isSeenStatus FROM quotes_table q INNER JOIN user_quote_table u ON q.quoteid=u.userQuoteId WHERE q.quoteid=$quoteId');
  }


  Future<void> updateUserTopicTable(UserTopic selectedTopic) async {
    // Get a reference to the database.
    //final db = await database;
    await _database.transaction((txn) async {
      txn.update(
        userTopicTable,
        selectedTopic.toMap(),
        // Ensure that the Topic has a matching id.
        where: "userTopicId = ?",
        // Pass the Topic's id as a whereArg to prevent SQL injection.
        whereArgs: [selectedTopic.userTopicId],
      );
    });
  }

  Future<void> updateUserSectionTable(UserSection selectedSection) async {
    //final db = await database;
    await _database.transaction((txn) async {
      txn.update(
        userSectionTable,
        selectedSection.toMap(),
        where: "userSectionId = ?",
        whereArgs: [selectedSection.userSectionId],
      );
    });
  }

  Future<void> updateUserQuoteTable(UserQuote selectedQuote) async {
    //final db = await database;
    await _database.transaction((txn) async {
      txn.update(
        userQuotesTable,
        selectedQuote.toMap(),
        where: "userQuoteId = ?",
        whereArgs: [selectedQuote.userQuoteId],
      );
    });
  }


  Future<void> updateThemeTable(ThemeDb theme) async {
    //TODO will be deleted
    final db = await database;
    await db.update(
      tableThemes,
      theme.toMap(),
      where: "themeName = ?",
      whereArgs: [theme.themeName],
    );
  }


  Future<void> updateUserThemeTable(UserTheme userTheme) async {
    final db = await database;
    await db.update(
      userThemesTable,
      userTheme.toMap(),
      where: "userThemeName = ?",
      whereArgs: [userTheme.userThemeName],
    );
  }

  Future<void> updateUserLiveThemeTable(UserTheme userTheme) async {
    final db = await database;
    await db.update(
      userLiveThemesTable,
      userTheme.toMap(),
      where: "userThemeName = ?",
      whereArgs: [userTheme.userThemeName],
    );
  }

  Future<void> updateUserMusicTable(UserMusic userMusic) async {
    final db = await database;
    await db.update(
      userMusicTable,
      userMusic.toMap(),
      where: "userMusicId = ?",
      whereArgs: [userMusic.userMusicId],
    );
  }


  Future<void> updateUserFontTable(UserFont userFont) async {
    final db = await database;
    await db.update(
      userFontTable,
      userFont.toMap(),
      where: "userFontId = ?",
      whereArgs: [userFont.userFontId],
    );
  }

  Future<int> insertDefaultUserTopicData(int topicid) async {
    final db = await database;
    Map<String, dynamic> row = {
      DatabaseProvider.userTopicId: topicid,
      DatabaseProvider.isSelectedTopic: topicid == 0 ? 0 : 1,
    };
    return await db.insert(userTopicTable, row,
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }


  Future<int> insertDefaultUserSectionData(int sectionid, int isPremium) async {
    final db = await database;
    Map<String, dynamic> row = {
      DatabaseProvider.userSectionId: sectionid,
      DatabaseProvider.isUnlockedSection: isPremium == 1 ? 0 : 1,
      DatabaseProvider.isSelectedSection: 1,
    };
    return await db.insert(userSectionTable, row,
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future<int> insertDefaultUserQuotesData(int quoteid) async {
    final db = await database;
    Map<String, dynamic> row = {
      DatabaseProvider.userQuotesId: quoteid,
      DatabaseProvider.isSeen: 0,
      DatabaseProvider.isBookmarked: 0,
      DatabaseProvider.isCollected: 0,
    };
    return await db.insert(userQuotesTable, row,
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }


  Future<int> insertDefaultUserThemeData(String themeName) async {
    final db = await database;
    Map<String, dynamic> row = {
      DatabaseProvider.userThemeName: themeName,
      DatabaseProvider.isUnlockedTheme: 0,
    };
    return await db.insert(userThemesTable, row,
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future<int> insertDefaultUserLiveThemeData(String themeName) async {
    final db = await database;
    Map<String, dynamic> row = {
      DatabaseProvider.userThemeName: themeName,
      DatabaseProvider.isUnlockedTheme: 0,
    };
    return await db.insert(userLiveThemesTable, row,
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }


  Future<int> insertDefaultUserMusicData(int myMusicId) async {
    final db = await database;
    Map<String, dynamic> row = {
      DatabaseProvider.userMusicId: myMusicId,
      DatabaseProvider.isUnlockedMusic: 0,
    };
    return await db.insert(userMusicTable, row,
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future<int> insertDefaultUserFontData(int myFontId) async {
    final db = await database;
    Map<String, dynamic> row = {
      DatabaseProvider.userFontId: myFontId,
      DatabaseProvider.isUnlockedFont: 0,
    };
    return await db.insert(userFontTable, row,
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

}

//Create class and structured data of user tables.
class UserTopic {
  final int userTopicId;
  final int isSelectedTopic;

  UserTopic({this.userTopicId, this.isSelectedTopic});

  Map<String, dynamic> toMap() {
    return {
      'userTopicId': userTopicId,
      'topicSelectedStatus': isSelectedTopic,
    };
  }
}

class Topic {
  final int topicId;
  final String topicName;

  Topic({this.topicId, this.topicName});

  Map<String, dynamic> toMap() {
    return {
      'topicId': topicId,
      'topicName': topicName,
    };
  }
}

class UserSection {
  final int userSectionId;
  final int isSelectedSection;
  final int isUnlockedSection;

  UserSection(
      {this.userSectionId, this.isSelectedSection, this.isUnlockedSection});

  Map<String, dynamic> toMap() {
    return {
      'userSectionId': userSectionId,
      'sectionSelectedStatus': isSelectedSection,
      'sectionIsUnlockedStatus': isUnlockedSection,
    };
  }
}

class UserQuote {
  final int userQuoteId;
  final int isSeen;
  final int isBookmarked;
  final int isCollected;

  UserQuote(
      {this.userQuoteId,
      this.isSeen,
      this.isBookmarked,
      this.isCollected});

  Map<String, dynamic> toMap() {
    return {
      'userQuoteId': userQuoteId,
      'quoteBookmarkedStatus': isBookmarked,
      'quoteCollectedStatus': isCollected,
      'isSeenStatus': isSeen,
    };
  }
}

class Quote {
  final int quoteid;
  final String quote;
  final String authorname;
  final int sectionid1;
  final int sectionid2;
  final int sectionid3;
  Quote(
      {this.quoteid,
      this.quote,
      this.authorname,
      this.sectionid1,
      this.sectionid2,
      this.sectionid3});
  Map<String, dynamic> toMap() {
    return {
      'quoteid': quoteid,
      'quote': quote,
      'authorname': authorname,
      'sectionid1': sectionid1,
      'sectionid2': sectionid2,
      'sectionid3': sectionid3,
    };
  }
}


class UserTheme {
  final String userThemeName;
  final int isUnlockedTheme;

  UserTheme({this.userThemeName, this.isUnlockedTheme});

  Map<String, dynamic> toMap() {
    return {
      'userThemeName': userThemeName,
      'isUnlockedTheme': isUnlockedTheme,
    };
  }
}

class UserMusic {
  final int userMusicId;
  final int isUnlockedMusic;

  UserMusic({this.userMusicId, this.isUnlockedMusic});

  Map<String, dynamic> toMap() {
    return {
      'userMusicId': userMusicId,
      'isUnlockedMusic': isUnlockedMusic,
    };
  }
}

class QuoteQuery {
  final String quote;
  final String authername;
  final int userQuoteId;
  final int isSeen;
  final int isBookmarked;
  final int isCollected;

  QuoteQuery(
      {this.quote,
      this.authername,
      this.userQuoteId,
      this.isSeen,
      this.isBookmarked,
      this.isCollected});

  Map<String, dynamic> toMap() {
    return {
      'quote': quote,
      'authorname': authername,
      'userQuoteId': userQuoteId,
      'quoteBookmarkedStatus': isBookmarked,
      'quoteCollectedStatus': isCollected,
      'isSeenStatus': isSeen,
    };
  }
}

class TrendingQuoteQuery {
  final String quote;
  final String authername;
  final int userQuoteId;
  final int isSeen;
  final int isBookmarked;
  final int isCollected;
  final int sectionid;
  final String sectionname;
  final String sectionimagename;

  TrendingQuoteQuery(
      {this.quote,
      this.authername,
      this.userQuoteId,
      this.isSeen,
      this.isBookmarked,
      this.isCollected,
      this.sectionid,
      this.sectionname,
      this.sectionimagename});

  Map<String, dynamic> toMap() {
    return {
      'quote': quote,
      'authorname': authername,
      'userQuoteId': userQuoteId,
      'quoteBookmarkedStatus': isBookmarked,
      'quoteCollectedStatus': isCollected,
      'isSeenStatus': isSeen,
      'sectionid1': sectionid,
      'sectionname': sectionname,
      'sectionimagename': sectionimagename,
    };
  }
}

class ThemeDb {
  final String themeName;
  final String fontName;
  final double fontSize;
  final int fontWeight;
  final int isPremiumTheme;
  final int a;
  final int r;
  final int g;
  final int b;
  ThemeDb(
      {this.themeName,
      this.fontName,
      this.fontSize,
      this.fontWeight,
      this.isPremiumTheme,
      this.a,
      this.r,
      this.g,
      this.b});
  Map<String, dynamic> toMap() {
    return {
      'themeName': themeName,
      'fontName': fontName,
      'fontSize': fontSize,
      'fontWeight': fontWeight,
      'isPremiumTheme': isPremiumTheme,
      'a': a,
      'r': r,
      'g': g,
      'b': b
    };
  }
}

class ThemeUserData {
  final String themeName;
  final int isPremiumTheme;
  final int a;
  final int r;
  final int g;
  final int b;
  final double xOffset1;
  final double yOffset1;
  final double blurRadius1;
  final int aShadow1;
  final int rShadow1;
  final int gShadow1;
  final int bShadow1;
  final int isUnlockedTheme;
  ThemeUserData({
    this.themeName,
    this.isPremiumTheme,
    this.a,
    this.r,
    this.g,
    this.b,
    this.aShadow1,
    this.rShadow1,
    this.bShadow1,
    this.gShadow1,
    this.xOffset1,
    this.yOffset1,
    this.blurRadius1,
    this.isUnlockedTheme,
  });
  Map<String, dynamic> toMap() {
    return {
      'themeName': themeName,
      'isPremiumTheme': isPremiumTheme,
      'a': a,
      'r': r,
      'g': g,
      'b': b,
      'aShadow1': aShadow1,
      'rShadow1': rShadow1,
      'gShadow1': gShadow1,
      'bShadow1': bShadow1,
      'blurRadius1': blurRadius1,
      'xOffset1': xOffset1,
      'yOffset1': yOffset1,
      'isUnlockedTheme': isUnlockedTheme,
    };
  }
}



class FontData {
  final int fontId;
  final String fontName;
  final double fontSize;
  final int fontWeight;
  final int isPremiumFont;
  final double height;
  final double wordSpacing;
  final double letterSpacing;
  FontData({
    this.fontId,
    this.fontName,
    this.fontSize,
    this.fontWeight,
    this.isPremiumFont,
    this.height,
    this.letterSpacing,
    this.wordSpacing,
  });
  Map<String, dynamic> toMap() {
    return {
      'fontId': fontId,
      'fontName': fontName,
      'fontSize': fontSize,
      'fontWeight': fontWeight,
      'isPremiumFont': isPremiumFont,
      'height': height,
      'letterSpacing': letterSpacing,
      'wordSpacing': wordSpacing,
    };
  }
}

class UserFont {
  final int userFontId;
  final int isUnlockedFont;

  UserFont({this.userFontId, this.isUnlockedFont});

  Map<String, dynamic> toMap() {
    return {
      'userFontId': userFontId,
      'isUnlockedFont': isUnlockedFont,
    };
  }
}
