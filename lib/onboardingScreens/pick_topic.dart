import 'dart:ui';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/ExtractedWidgets/my_grad_button.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/localizations.dart';
import 'package:motilife/screens/feed_quotes_page.dart';
import 'package:motilife/screens/quotes_bubble.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';

import '../prefs.dart';

bool isAnySelected = false;
int selectedCount = 0;
final dbProvider = DatabaseProvider.instance;
final dbFunctions = DbFunctions();
final functions = AllFunctions();

class PickTopicScreen extends StatelessWidget {
  static String id = 'topics';
  final String comingPage;
  List<String> themeProperties;

  PickTopicScreen({this.comingPage});

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    ThemeChanger themesChanger = Provider.of<ThemeChanger>(context);
    themeProperties = themesChanger.getThemeValues();
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: Platform.isAndroid
          ? AppBar(
        title: Text(
          AppLocalizations.of(context).translate('contentTypes'),
          style: TextStyle(
            fontFamily: kDefaultFontName,
            color: Colors.white,
            fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
            fontWeight: FontWeight.w500,
          ),
        ),
        toolbarHeight: heightScreen * 0.07,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      )
          : CupertinoNavigationBar(
        middle: Text(
          AppLocalizations.of(context).translate('contentTypes'),
          style: TextStyle(
            fontFamily: kDefaultFontName,
            color: Colors.white,
            fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
          onPressed: () async {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        height: heightScreen * 1,
        width: widthScreen * 1,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: kSigmaX, sigmaY: kSigmaY),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(height: heightScreen * 0.09),
              Container(
                height: heightScreen * 0.1,
                child: Center(
                  child: Text(
                    AppLocalizations.of(context).translate('pick_topic'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Nunito',
                      fontSize: MediaQuery.of(context).size.width * 0.05,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                height: heightScreen * 0.73,
                child: MyGridView(comingPage: comingPage,),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MyGridView extends StatefulWidget {
  final String comingPage;
  MyGridView({this.comingPage});
  @override
  _MyGridViewState createState() => _MyGridViewState();
}

class _MyGridViewState extends State<MyGridView> {
  List<int> selectedTopicIdsAtStart = [];
  List<int> selectedTopicIdsAtLast = [];
  _countSelectedTopic() async {
    selectedCount = 0;
    final allRows = await dbProvider.queryAllUserTopics();
    allRows.forEach((topicRow) async {
      if (topicRow['topicSelectedStatus'] == 1) {
        selectedCount++;
        selectedTopicIdsAtStart.add(topicRow['userTopicId']);
        selectedTopicIdsAtLast.add(topicRow['userTopicId']);
        print(selectedCount);
        print(selectedTopicIdsAtStart);
        //if(await SharedPreferencesHelper.getIntroSeenStatus() == 0) dbFunctions.updateUserSectionsSelectedStatus(topicRow['userTopicId'], 1);
        await dbProvider.queryAllSections();
      }
    });
    setState(() {
      if (selectedCount > 0) {
        // print('Bigger than 0: $selectedCount');
        isAnySelected = true;
      } else {
        // print('Equals to 000: $selectedCount');
        isAnySelected = false;
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _countSelectedTopic();
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final orientation = MediaQuery.of(context).orientation;
    final dayNightChanger = Provider.of<DayNightChanger>(context);
    RemoteNotifyChanger remoteNotifyChanger = Provider.of<RemoteNotifyChanger>(context);
    return Column(
      children: [
        Container(
          height: heightScreen*0.6,
          margin: EdgeInsets.symmetric(horizontal: widthScreen*0.03),
          child: FutureBuilder(
            future: dbProvider.queryAllTopicsWithUserData(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return GridView.builder(
                  padding: EdgeInsets.all(0.0),
                  itemCount: snapshot.data.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: (orientation == Orientation.portrait) ? 2 : 2,
                    childAspectRatio: 1.8,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        HapticFeedback.lightImpact();
                        setState(() {
                          if (snapshot.data[index]['topicSelectedStatus'] == 0) {
                            dbFunctions.updateUserTopicAsTrue(snapshot.data[index]['topicid']);
                            dbFunctions.updateUserSectionsSelectedStatus(snapshot.data[index]['topicid'], 1);
                            selectedCount++;
                            selectedTopicIdsAtLast.add(snapshot.data[index]['topicid']);
                            print(selectedTopicIdsAtLast);
                            print('At start: $selectedTopicIdsAtStart');
                          } else {
                            dbFunctions.updateUserTopicAsFalse(snapshot.data[index]['topicid']);
                            dbFunctions.updateUserSectionsSelectedStatus(snapshot.data[index]['topicid'], 0);
                            selectedCount--;
                            selectedTopicIdsAtLast.remove(snapshot.data[index]['topicid']);
                            print(selectedTopicIdsAtLast);
                            print('At start: $selectedTopicIdsAtStart');
                          }
                          if (selectedCount > 0) {
                            // print('Bigger than 0: $selectedCount');
                            isAnySelected = true;
                          } else {
                            // print('Equals to 000: $selectedCount');
                            isAnySelected = false;
                          }
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.07),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        margin: EdgeInsets.all(widthScreen*0.02),
                        child: GridTile(
                          header: Padding(
                            padding: EdgeInsets.all(widthScreen*0.02),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: snapshot.data[index]['topicSelectedStatus'] == 1 ? ImageIcon(
                                  AssetImage('assets/signs2/check.png'),
                                color: Colors.white,
                                size: heightScreen/35,
                              ) : Container(),
                            ),
                          ),
                          child: Center(
                            child: Text(
                                snapshot.data[index]['topicname'],
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: kDefaultFontName,
                                fontSize: MediaQuery.of(context).size.width * 0.045,
                              ),
                            ),
                          ),
                        ),
                      ),

                      /*CardSquare(
                        cardDecoration: snapshot.data[index]['topicSelectedStatus'] == 1 ? kGradDecoration : dayNightChanger.getDecorationCard(), imageName: 'topics/${snapshot.data[index]['topicimagename']}',
                        textCard: snapshot.data[index]['topicname'],
                        textColor: snapshot.data[index]['topicSelectedStatus'] == 1 ? kWhiteText : dayNightChanger.getTextColor(),
                        imageScale: 2.9,
                        topicFontSize: MediaQuery.of(context).size.width * 0.045,
                      ),*/


                    );
                  },
                );
              } else {
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              }
            },
          ),
        ),
        Center(
          child: Container(
            height: heightScreen * 0.11,
            margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07, vertical: heightScreen*0.01),
            child: MyGradButton(
              buttonText:
              AppLocalizations.of(context).translate('continue_button'),
              decoration: isAnySelected
                  ? kGradButtonDecoration
                  : dayNightChanger.getDecorationButton(),
              buttonTextColor:
              isAnySelected ? kWhiteText : dayNightChanger.getTextColor(),
              callBack: () {
                if(isAnySelected) {
                  HapticFeedback.lightImpact();
                  print('At start: $selectedTopicIdsAtStart');
                  print('At last: $selectedTopicIdsAtLast');
                  /*selectedTopicIdsAtLast.forEach((topicId) {
                    if(!selectedTopicIdsAtStart.contains(topicId)){
                      print('CHANGING TO TRUE + $topicId');
                      dbFunctions.updateUserSectionsSelectedStatus(topicId, 1);
                    }
                  });
                  selectedTopicIdsAtStart.forEach((topicId) {
                    if(!selectedTopicIdsAtLast.contains(topicId)){
                      print('CHANGING TO FALSE - $topicId');
                      dbFunctions.updateUserSectionsSelectedStatus(topicId, 0);
                    }
                  });*/
                } else {
                  HapticFeedback.vibrate();
                  _toastInfo(AppLocalizations.of(context).translate('topicToastWarning'));
                }
              },
              nextScreen: isAnySelected
                  ? remoteNotifyChanger.getShowFeed() ? FeedQuotesPage() : NewHomePage()
                  : null,
              //pageId: QuotesCountDaily.id,
            ),
          ),
        ),
      ],
    );
  }
  _toastInfo(String info) {
    Fluttertoast.showToast(msg: info, toastLength: Toast.LENGTH_LONG);
  }
}
