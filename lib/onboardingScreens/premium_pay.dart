import 'dart:convert';
import 'dart:ui';

import 'package:advertising_id/advertising_id.dart';
import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
//import 'package:flutter_facebook_sdk/flutter_facebook_sdk.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:http/http.dart';
import 'package:motilife/ExtractedWidgets/my_checklist_tile.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/fetch_cache.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/popups/warning_popup.dart';
import 'package:motilife/screens/feed_quotes_page.dart';
import 'package:motilife/screens/quotes_bubble.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import '../localizations.dart';
import '../notifier_provider.dart';
import '../prefs.dart';

final dbFunctions = DbFunctions();
final functions = AllFunctions();

class PremiumPayPage extends StatelessWidget {
  final String directedPage;
  final String liveThemeName;
  PremiumPayPage({this.directedPage, this.liveThemeName});
  @override
  Widget build(BuildContext context) {
    return InApp(directedPage: directedPage, liveThemeName: liveThemeName);
  }
}

class InApp extends StatefulWidget {
  final String directedPage;
  final String liveThemeName;
  InApp({this.directedPage, this.liveThemeName});
  @override
  _InAppState createState() => new _InAppState();
}

class _InAppState extends State<InApp> with SingleTickerProviderStateMixin {
  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;
  StreamSubscription _connectionSubscription;
  List<String> _productLists = kProductIds;
  List<Container> productListAsWidget = <Container>[];
  String _queryProductError;
  int _radioValue = 0;
  bool isPurchasing = false;
  bool _purchasePending = false;
  IAPItem purchaseParam;
  FirebaseAnalytics analytics = FirebaseAnalytics();
  List<String> themeProperties;
  bool isSeenOtherSubsPlan = false;
  String currentMessage = "Trying to fetch data...";
  bool _isPlaying = true;
  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];

  AnimationController controller;
  Animation growAnimation;
  bool isSeenExitIcon = false;
  AppsflyerSdk _appsflyerSdk;
  String _advertisingId = '';
  bool _isLimitAdTrackingEnabled;

  @override
  void initState() {
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    themeProperties = themesChanger.getThemeValues();
    _productLists = remoteChanger.getFreeTrialOpen() == 1 ? kProductIdsFreeTrial : kProductIds;
    print("Product list: $_productLists");
    initPlatformState();
    //set appsflyer sdk value
    Map appsFlyerOptions = { "afDevKey": kDEV_KEY_ApsFlyer,
      "afAppId": Platform.isAndroid ? kAppId_Android : kAppId_iOS,
      "isDebug": true};

    _appsflyerSdk = AppsflyerSdk(appsFlyerOptions);
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        isSeenExitIcon = true;
      });
      controller = AnimationController(vsync: this, duration: Duration(seconds: 2))
        ..addListener(() {
          //print('GrowAnimation value: ${growAnimation.value}');
          setState(() {
            growAnimation.value;
          });
        });
      growAnimation = Tween<double>(begin: 0, end: 1).animate(controller);
      controller.forward();
    });
    super.initState();
  }

  Future<void> _sendAnalyticsPurchaseSuccess(String directedPage, String purchaseId) async {
    await analytics.logEvent(
      name: 'purchase_success',
      parameters: {'directed_page': directedPage.replaceAll(' ', ''), 'purchase_type': purchaseId.replaceAll(' ', '')},
    );
  }


  @override
  void dispose() {
    if (_connectionSubscription != null) {
      _connectionSubscription.cancel();
      _connectionSubscription = null;
    }
    controller.dispose();
    super.dispose();
  }



  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    await FlutterInappPurchase.instance.initConnection;
    print("In init platform state");
    // prepare
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;


    // refresh items for android
    try {
      String msg = await FlutterInappPurchase.instance.consumeAllItems;
      print('consumeAllItems: $msg');
    } catch (err) {
      print('consumeAllItems error: $err');
    }

    _getProduct();

    _connectionSubscription = FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });

    _purchaseUpdatedSubscription = FlutterInappPurchase.purchaseUpdated.listen((productItem) async {
      print('purchase-updated: $productItem');
      //print('purchase-updated: ${productItem.originalTransactionIdentifierIOS}');
      if(Platform.isAndroid) {
        print("check purchase");
        if(await _verifyPurchase(productItem)) {
          print("After verify: ${productItem.isAcknowledgedAndroid}");

/*          double purchaseRevenue = double.parse(purchaseParam.price)*0.66;
          final Map eventValues = {
            "af_content_id": "${productItem.productId}",
            "af_currency": "${purchaseParam.currency}",
            "af_revenue": "${purchaseRevenue.toStringAsFixed(2)}",
            "token": "${productItem.purchaseToken}"
          };
          functions.logAppsFlyerEvent(_appsflyerSdk, "af_purchase", eventValues);*/
          FlutterInappPurchase.instance.acknowledgePurchaseAndroid(productItem.purchaseToken);//Added for ack error
          if(productItem.isAcknowledgedAndroid){
            FlutterInappPurchase.instance.finishTransaction(productItem);
            _sendAnalyticsPurchaseSuccess(widget.directedPage, productItem.productId);
            deliverProduct(productItem);
          } else {
            print("Purchase didn't ack");
            FlutterInappPurchase.instance.finishTransaction(productItem);
            _sendAnalyticsPurchaseSuccess(widget.directedPage, productItem.productId);
            deliverProduct(productItem);
          }
          //facebookAppEvents.logPurchase(amount: double.parse(purchaseParam.price), currency: purchaseParam.currency);
/*          await facebookDeepLinks.logPurhcase(amount: double.parse(purchaseParam.price), currency: purchaseParam.currency, params: {
            'content-type': productItem.productId.replaceAll(' ', '')
          });*/
        } else {
          print('Sth happened while verifying purchase from our DB...');
          _handleInvalidPurchase(productItem);
        }
      } else {
        print('Transaction state: ${productItem.transactionStateIOS}');
        if(productItem.transactionStateIOS == TransactionState.purchased) {
          FlutterInappPurchase.instance.finishTransactionIOS(productItem.transactionId).catchError((onError){
            print("ERROR...Cannot finish transaction: $onError...");
            _handleInvalidPurchase(productItem);
          },
          ).whenComplete(() async {
            _sendAnalyticsPurchaseSuccess(widget.directedPage, productItem.productId);
            deliverProduct(productItem);
            //facebookAppEvents.logPurchase(amount: double.parse(purchaseParam.price), currency: purchaseParam.currency);
/*            await facebookDeepLinks.logPurhcase(amount: double.parse(purchaseParam.price), currency: purchaseParam.currency, params: {
              'content-type': productItem.productId.replaceAll(' ', '')
            });*/
          });
        } else {
          FlutterInappPurchase.instance.finishTransactionIOS(productItem.transactionId).catchError((onError){
            print("ERROR...Cannot finish transaction: $onError...");
            _handleInvalidPurchase(productItem);
          },
          ).whenComplete(() async {
            _sendAnalyticsPurchaseSuccess(widget.directedPage, productItem.productId);
            deliverProduct(productItem);
            //facebookAppEvents.logPurchase(amount: double.parse(purchaseParam.price), currency: purchaseParam.currency);
/*            await facebookDeepLinks.logPurhcase(amount: double.parse(purchaseParam.price), currency: purchaseParam.currency, params: {
              'content-type': productItem.productId.replaceAll(' ', '')
            });*/
          });
        }
      }

    });

    _purchaseErrorSubscription = FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error is: $purchaseError');
      //if(purchaseError!=null) isPurchasing = false;
      setState(() {
        isPurchasing = false;
      });
    });
  }

  void _requestPurchase(IAPItem item) async {
    String advertisingId;
    if(Platform.isAndroid) {
      try {
        advertisingId = await AdvertisingId.id(true);
      } on PlatformException {
        advertisingId = 'Failed to get Advertising id.';
      }
      print("Request uid: ${await _appsflyerSdk.getAppsFlyerUID()}, gaid: $advertisingId");
      //FlutterInappPurchase.instance.requestPurchase(item.productId, obfuscatedAccountIdAndroid: "${await _appsflyerSdk.getAppsFlyerUID()}", obfuscatedProfileIdAndroid: advertisingId);
      FlutterInappPurchase.instance.requestPurchase(item.productId, obfuscatedAccountId: "${await _appsflyerSdk.getAppsFlyerUID()}", obfuscatedProfileIdAndroid: advertisingId);
    } else {
      FlutterInappPurchase.instance.requestPurchase(item.productId);
    }
    Future.delayed(const Duration(milliseconds: 1500), () {
      setState(() {
        isPurchasing = true;
      });
    });
  }

  Future _getProduct() async {
    List<IAPItem> items = await FlutterInappPurchase.instance.getProducts(_productLists);
    for (var item in items) {
      print('${item.productId}');
      setState(() {
        _items.add(item);
      });
    }
    if(Platform.isAndroid) {
      List<IAPItem> subscriptionItems = await FlutterInappPurchase.instance.getSubscriptions(_productLists);
      for (var item in subscriptionItems) {
        print('${item.productId}');
        setState(() {
          _items.add(item);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final generalChanger = Provider.of<GeneralChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    List<Widget> stack = [];
    List<Widget> otherSubsStack = [];
    if (_queryProductError == null) {
      otherSubsStack.add(
        ListView(
          shrinkWrap: true,
          children: [
            GestureDetector(
              onTap: () {
                //functions.sendAnalyticsWithOwnName('Exit_Premium_Click');
                if(widget.directedPage == "On_First_Open") {
                  if(remoteChanger.getShowFeed()) {
                    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: FeedQuotesPage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                  } else {
                    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: NewHomePage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                  }
                } else {
                  Navigator.pop(context);
                }
              },
              child: Card(
                elevation: 0,
                color: Colors.transparent,
                child: Column(
                  children: [
                    Container(height: heightScreen*0.04),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                          height: heightScreen*0.02,
                          margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
                          child: Image.asset('assets/signs2/exit.png')
                      ),
                    ),
                    Container(height: heightScreen*0.06),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                  height: heightScreen*0.09,
                  child: Image.asset('assets/signs2/icon_onboarding.png')
              ),
            ),
            Container(height: heightScreen*0.02),
            Container(
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
              child: Text(
                AppLocalizations.of(context).translate('unlockPremium'),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.07,
                    fontWeight: FontWeight.w700,
                    color: Colors.white
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
              child: Divider(
                color: Colors.white,
                thickness: 1.7,
                indent: 50,
                endIndent: 50,
              ),
            ),
            SizedBox(height: heightScreen*0.02),
            Container(
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
              child: Text(
                AppLocalizations.of(context).translate('unlimitedAccessToAllFeatures'),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.05,
                    fontWeight: FontWeight.w600,
                    color: Colors.white
                ),
              ),
            ),
            SizedBox(height: heightScreen*0.03),
            isPurchasing ? Container(
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.2),
                  border: Border.all(
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07, vertical: heightScreen*0.03),
                padding:  EdgeInsets.symmetric(horizontal: widthScreen*0.03, vertical: 15),
                child: Column(
                  children: [
                    if(Platform.isAndroid) Text(AppLocalizations.of(context).translate('purchaseDialog'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: kDefaultFontName,
                        color: Colors.white,
                        fontSize: MediaQuery.of(context).size.width * 0.045,
                        //fontWeight: FontWeight.w600,
                      ),
                    ),
                    if(Platform.isAndroid) Container(
                      height: heightScreen*0.05,
                    ),
                    Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.white),
                        backgroundColor: Color(0xFF1f1f1f),
                      ),
                    ),
                  ],
                )) : _items.length != 0 ? _buildProductList() : Container(
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    border: Border.all(
                      color: Colors.white,
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07, vertical: heightScreen*0.03),
                  padding:  EdgeInsets.symmetric(horizontal: widthScreen*0.05, vertical: heightScreen*0.03),
                  child: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
                backgroundColor: Color(0xFF1f1f1f),
              ),
            ),
                ),
            SizedBox(height: heightScreen*0.03),
            if(_items.length != 0) Container(
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.03),
              child: Text(
                purchaseParam == null ? currentMessage : purchaseParam.productId == _productLists[1] ? '${remoteChanger.getFreeTrialOpen() == 1 ? AppLocalizations.of(context).translate('3daysFree'): AppLocalizations.of(context).translate('just')} ${purchaseParam.price} ${purchaseParam.currency} ${AppLocalizations.of(context).translate('gopremium6')}' :
                '${AppLocalizations.of(context).translate('just')} ${purchaseParam.price} ${purchaseParam.currency} ${AppLocalizations.of(context).translate('gopremium6a')}',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.046,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                    height: 1.2
                ),
              ),
            ),
            SizedBox(height: heightScreen*0.01),
            GestureDetector(
              onTap: () {
                Future.delayed(Duration(milliseconds: 10), () {
                  if(_items.length != 0) purchaseParam != null ? _requestPurchase(purchaseParam) : _requestPurchase(_items[1]);
                });
              },
              child: AnimatedCircularGlow(
                endRadius: 120.0,
                duration: Duration(milliseconds: 2000),
                repeat: true,
                repeatPause: Duration(milliseconds: 100),
                child: Material(
                  elevation: 8.0,
                  shape: CircleBorder(),
                  color: Colors.transparent,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 30.0, vertical: heightScreen*0.03),
                    height: heightScreen * 0.08,
                    decoration: kGradButtonDecorationGrey,
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context).translate('subscribeNow'),
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          color: Colors.black,
                          fontSize: MediaQuery.of(context).size.width * 0.058,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: widthScreen*0.02, horizontal: heightScreen*0.04),
              child: RichText(
                text: TextSpan(
                    children: [
                      TextSpan(
                        text: Platform.isAndroid
                            ? AppLocalizations.of(context).translate('premiumLongtextAndroid')
                            : AppLocalizations.of(context).translate('premiumLongtextIOS'),
                        //textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          fontSize: MediaQuery.of(context).size.width * 0.03,
                          fontWeight: FontWeight.values[4],
                          color: Colors.white,
                        ),
                      ),
                      Platform.isIOS
                          ? TextSpan(
                          text: AppLocalizations.of(context).translate('privacy'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.03,
                            fontWeight: FontWeight.values[4],
                            decoration: TextDecoration.underline,
                            color: Colors.white,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () { launch('https://viyatek.io/privacy/');}
                      ) : TextSpan(),
                      TextSpan(
                        text: Platform.isIOS
                            ? AppLocalizations.of(context).translate('and')
                            : '',
                        //textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          fontSize: MediaQuery.of(context).size.width * 0.03,
                          fontWeight: FontWeight.values[4],
                          color: Colors.white,
                        ),
                      ),
                      TextSpan(
                          text: AppLocalizations.of(context).translate('termsofuse'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.03,
                            fontWeight: FontWeight.values[4],
                            decoration: TextDecoration.underline,
                            color: Colors.white,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () { launch('https://viyatek.io/terms/');}
                      ),
                    ]
                ),
              ),
            ),
          ],
        ),
      );
      stack.add(
        ListView(
          shrinkWrap: true,
          children: [
            GestureDetector(
              onTap: () {
                //functions.sendAnalyticsWithOwnName('Exit_Premium_Click');
                if(widget.directedPage == "On_First_Open") {
                  if(remoteChanger.getShowFeed()) {
                    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: FeedQuotesPage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                  } else {
                    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: NewHomePage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                  }
                } else {
                  Navigator.pop(context);
                }
              },
              child: Card(
                elevation: 0,
                color: Colors.transparent,
                child: Column(
                  children: [
                    Container(height: heightScreen*0.04),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                          height: heightScreen*0.02,
                          margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
                          child: isSeenExitIcon ? Opacity(
                            opacity: growAnimation.value,
                              child: Image.asset('assets/signs2/exit.png')
                          ) : Container(),
                      ),
                    ),
                    Container(height: heightScreen*0.07),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
                  height: heightScreen*0.07,
                  child: Image.asset('assets/signs2/icon_onboarding.png')
              ),
            ),
            Container(height: heightScreen*0.01),
            Container(
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
              child: Text(
                AppLocalizations.of(context).translate('unlockPremium'),
                style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.07,
                    fontWeight: FontWeight.w700,
                    color: Colors.white
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: widthScreen*0.07),
              child: Divider(
                color: Colors.white,
                thickness: 1.7,
                endIndent: 128,
              ),
            ),
            Container(height: heightScreen*0.04),
            if(remoteChanger.getFreeTrialOpen() == 1) MyCheckListTile(
              explanation: AppLocalizations.of(context).translate('goPremiumFreeTrial'),
              colour: Colors.white,
              marginLeft: widthScreen*0.07,
            ),
            SizedBox(height: heightScreen*0.01),
            MyCheckListTile(
              explanation: AppLocalizations.of(context).translate('gopremium1'),
              colour: Colors.white,
              marginLeft: widthScreen*0.07,
            ),
            SizedBox(height: heightScreen*0.01),
            MyCheckListTile(
              explanation: AppLocalizations.of(context).translate('gopremium2'),
              colour: Colors.white,
              marginLeft: widthScreen*0.07,
            ),
            SizedBox(height: heightScreen*0.01),
            MyCheckListTile(
              explanation: AppLocalizations.of(context).translate('gopremium3'),
              colour: Colors.white,
              marginLeft: widthScreen*0.07,
            ),
            SizedBox(height: heightScreen*0.01),
            MyCheckListTile(
              explanation: AppLocalizations.of(context).translate('gopremium4'),
              colour: Colors.white,
              marginLeft: widthScreen*0.07,
            ),
            SizedBox(height: heightScreen*0.035),

            isPurchasing ? Container(
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.2),
                  border: Border.all(
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07, vertical: 15),
                padding:  EdgeInsets.symmetric(horizontal: widthScreen*0.07, vertical: 15),
                child: Column(
                  children: [
                    if(Platform.isAndroid) Text(AppLocalizations.of(context).translate('purchaseDialog'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: kDefaultFontName,
                        color: Colors.white,
                        fontSize: MediaQuery.of(context).size.width * 0.045,
                        //fontWeight: FontWeight.w600,
                      ),
                    ),
                    if(Platform.isAndroid) Container(
                      height: heightScreen*0.05,
                    ),
                    Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.white),
                        backgroundColor: Color(0xFF1f1f1f),
                      ),
                    ),
                  ],
                )) : _items.length != 0 ? Container(
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.03),
              child: Text(
                currentMessage,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.046,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  height: 1.1,
                ),
              ),
            ) : Container(
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.2),
                  border: Border.all(
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07, vertical: 15),
                padding:  EdgeInsets.symmetric(horizontal: widthScreen*0.07, vertical: 15),
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                    backgroundColor: Color(0xFF1f1f1f),
                  ),
                )),
            SizedBox(height: heightScreen*0.01),
            GestureDetector(
              onTap: () {
                Future.delayed(Duration(milliseconds: 10), () {
                  if(_items.length != 0) purchaseParam != null ? _requestPurchase(purchaseParam) : _requestPurchase(_items[1]);
                });
              },
              child: AnimatedCircularGlow(
                endRadius: 120.0,
                duration: Duration(milliseconds: 2000),
                repeat: true,
                repeatPause: Duration(milliseconds: 100),
                child: Material(
                  elevation: 8.0,
                  shape: CircleBorder(),
                  color: Colors.transparent,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: widthScreen*0.1, vertical: heightScreen*0.03),
                    height: heightScreen * 0.08,
                    decoration: kGradButtonDecorationGrey,
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context).translate('subscribeNow'),
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          color: Colors.black,
                          fontSize: MediaQuery.of(context).size.width * 0.056,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  isSeenOtherSubsPlan = true;
                });
                functions.sendAnalyticsWithOwnName('See_Other_Plans_Clicked');
              },
              child: Card(
                elevation: 0,
                color: Colors.transparent,
                child: Text(
                  AppLocalizations.of(context).translate('seeOtherSubsPlan'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    fontWeight: FontWeight.values[5],
                    color: Colors.white
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: heightScreen*0.03, horizontal: widthScreen*0.07),
              child: RichText(
                text: TextSpan(
                    children: [
                      TextSpan(
                        text: Platform.isAndroid
                            ? AppLocalizations.of(context).translate('premiumLongtextAndroid')
                            : AppLocalizations.of(context).translate('premiumLongtextIOS'),
                        //textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          fontSize: MediaQuery.of(context).size.width * 0.03,
                          fontWeight: FontWeight.values[4],
                          color: Colors.white,
                        ),
                      ),
                      Platform.isIOS
                          ? TextSpan(
                          text: AppLocalizations.of(context).translate('privacy'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.03,
                            fontWeight: FontWeight.values[4],
                            decoration: TextDecoration.underline,
                            color: Colors.white,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () { launch('https://viyatek.io/privacy/');}
                      ) : TextSpan(),
                      TextSpan(
                        text: Platform.isIOS
                            ? AppLocalizations.of(context).translate('and')
                            : '',
                        //textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          fontSize: MediaQuery.of(context).size.width * 0.03,
                          fontWeight: FontWeight.values[4],
                          color: Colors.white,
                        ),
                      ),
                      TextSpan(
                          text: AppLocalizations.of(context).translate('termsofuse'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.03,
                            fontWeight: FontWeight.values[4],
                            decoration: TextDecoration.underline,
                            color: Colors.white,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () { launch('https://viyatek.io/terms/');}
                      ),
                    ]
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      otherSubsStack.add(Center(
        child: Text(_queryProductError),
      ));
      stack.add(Center(
        child: Text(_queryProductError),
      ));
    }
    if (_purchasePending) {
      stack.add(
        Stack(
          children: [
            Text('Purchase Pending...'),
            Opacity(
              opacity: 0.3,
              child: const ModalBarrier(dismissible: false, color: Colors.grey),
            ),
            Center(
              child: CircularProgressIndicator(),
            ),
          ],
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        body: Container(
          height: heightScreen * 1,
          width: widthScreen * 1,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(widget.liveThemeName != null ? 'assets/themes/${widget.liveThemeName}.jpg' : 'assets/realImages/splash.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
              FetchCache(url: MyStatics.premiumPageURL),
              Stack(
                children: isSeenOtherSubsPlan ? otherSubsStack : stack,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container _buildProductList() {
    double widthScreen = MediaQuery.of(context).size.width;
    double heightScreen = MediaQuery.of(context).size.height;
    bool isSelected = false;
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    List<Widget> widgets = this._items.map((item) {
      if(item.productId == _productLists[1]) {
        setState(() {
          currentMessage = '${remoteChanger.getFreeTrialOpen() == 1 ? AppLocalizations.of(context).translate('3daysFree'): AppLocalizations.of(context).translate('just')} ${item.price} ${item.currency} ${AppLocalizations.of(context).translate('gopremium6')}';
        });
      }
      return item.productId != _productLists[0] ? GestureDetector(
        onTap: () {
          //this._requestPurchase(item);
          _handleRadioValueChange(item.productId == _productLists[1] ? 0 : 1);
          setState(() {
            purchaseParam = item;
            item.productId == _productLists[1] ? currentMessage = '${remoteChanger.getFreeTrialOpen() == 1 ? AppLocalizations.of(context).translate('3daysFree'): AppLocalizations.of(context).translate('just')} ${item.price} ${item.currency} ${AppLocalizations.of(context).translate('gopremium6')}' : currentMessage = '${AppLocalizations.of(context).translate('just')} ${item.price} ${item.currency} ${AppLocalizations.of(context).translate('gopremium6a')}';
          });
        },
        child: Card(
          color: Colors.transparent,
          elevation: 0,
          child: Container(
          height: item.productId == _productLists[2] ? heightScreen * 0.086 : heightScreen * 0.105,
          decoration: BoxDecoration(
            //color: Colors.white.withOpacity(0.2),
            border: BorderDirectional(
              bottom: BorderSide(
                  color: item.productId == _productLists[1] ? Colors.white : Colors.transparent,
                  width: item.productId == _productLists[1] ? 1.0 : 0,
                  style: BorderStyle.solid
              ),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Theme(
                        data: ThemeData.dark(),
                        child: Radio(
                            value: item.productId ==
                                _productLists[1]
                                ? 0
                                : 1,
                            groupValue: _radioValue,
                            activeColor: Colors.white,
                            focusColor: Colors.white,
                            hoverColor: Colors.white,
                            onChanged: (int value) {
                              _handleRadioValueChange(value);
                              setState(() {
                                purchaseParam = item;
                              });
                            }),
                      ),
                      SizedBox(
                        width: widthScreen * 0.02,
                      ),
                      Container(
                        width: widthScreen * 0.3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              item.productId == _productLists[2] ? AppLocalizations.of(context).translate('monthlyplan') : AppLocalizations.of(context).translate('yearlyplan'),
                              style: TextStyle(
                                fontSize: MediaQuery.of(context).size.width * 0.05,
                                fontFamily: kDefaultFontName,
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              'Just ${item.price}', //previousPurchase == null ? 'Just ${productDetails.price}' : 'Ur plan ${previousPurchase.productID}'
                              style: TextStyle(
                                fontSize: MediaQuery.of(context).size.width * 0.038,
                                fontFamily: kDefaultFontName,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  item.productId == _productLists[1] ? Container(
                    decoration: kGradPremiumSmallButtonDecoration,
                    height: MediaQuery.of(context).size.height*0.05,
                    width: MediaQuery.of(context).size.width*0.20,
                    margin: EdgeInsets.only(right: widthScreen*.03),
                    //padding: EdgeInsets.symmetric(vertical: 1, horizontal: 0.5),
                    alignment: Alignment.center,
                    child: AutoSizeText(
                      AppLocalizations.of(context).translate('save'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: kDefaultFontName,
                          fontSize: MediaQuery.of(context).size.width * 0.035,
                          fontWeight: FontWeight.w700,
                          color: Colors.black
                      ),
                    ),
                  ) : Container(),
                ],
              ),
            ],
          ),
    ),
        ),
      ) : Container();
    })
        .toList();
    return Container(
        decoration: BoxDecoration(
          //color: Colors.white.withOpacity(0.2),
          border: Border.all(
            color: Colors.white,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        margin: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
        child: Column(
          children: widgets,
        ));
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          break;
        case 1:
          break;
      }
    });
  }

  Future<bool> _verifyPurchase(PurchasedItem purchaseDetails) async {
    print('ON VERIFY. is purchasing: $isPurchasing');
    if (purchaseDetails.productId == _productLists[1] ||
        purchaseDetails.productId == _productLists[2]) {
      var endpoint = 'https://0tg0ktrj7h.execute-api.us-west-2.amazonaws.com/live/subscriptions-validatetoken-v2?subscriptionId=${purchaseDetails.productId}&packageName=com.viyatek.motilife&token=${purchaseDetails.purchaseToken}';
      var url = Uri.parse(endpoint);
      var response = await get(url);
      var jsonResponse = jsonDecode(response.body);
      var expiryTime = int.parse(jsonResponse['expiryTimeMillis']);
      print("Response code: ${response.statusCode}");
      if (response.statusCode == 200) {
        await SharedPreferencesHelper.setExpiryTime(expiryTime);
        return Future<bool>.value(true);
      } else {
        return Future<bool>.value(false);
      }
    } else {
      await SharedPreferencesHelper.setIsPremiumStatus(true);
      return Future<bool>.value(true);
    }
  }

  void deliverProduct(PurchasedItem purchaseDetails) {
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    print("Now delivering product");
    GeneralChanger generalChanger =
    Provider.of<GeneralChanger>(context, listen: false);
    generalChanger.setIsPremiumOrSubscribed(true);
    SharedPreferencesHelper.setIsPremiumOrSubscribedStatus(true);
    if(Platform.isIOS){
      if(purchaseDetails.productId == _productLists[0]){
        SharedPreferencesHelper.setIsPremiumStatus(true);//this is set for Android in _verifyPurchase func.
      } else {
        SharedPreferencesHelper.setExpiryTime(DateTime.now().day+2);//Temporary expiry time for ios.
      }
    }
    setState(() {
      //_purchases.add(purchaseDetails);
      _purchasePending = false;
      //isPurchasing = false;
    });
    dbFunctions.updateThemesUnlockedStatus(1);
    dbFunctions.updateLiveThemesUnlockedStatus(1);
    dbFunctions.updateSectionsUnlockedStatus(1);
    dbFunctions.updateMusicsUnlockedStatus(1);
    dbFunctions.updateFontsUnlockedStatus(1);
    if(widget.directedPage == "On_First_Open") {
      if(remoteChanger.getShowFeed()) {
        Navigator.push(
          context,
          SlideLeftRoute(
              page: FeedQuotesPage(isComeFromReminderPage: true)),
        );
/*        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            FeedQuotesPage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);*/
      } else {
        Navigator.push(
          context,
          SlideLeftRoute(
              page: NewHomePage(isComeFromReminderPage: true)),
        );
/*        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            NewHomePage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);//if purchase was made on first open, set reminders. to set reminders on home page, this value should be True...*/
      }
    } else {
      if(remoteChanger.getShowFeed()) {
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            FeedQuotesPage()), (Route<dynamic> route) => false);
      } else {
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            NewHomePage()), (Route<dynamic> route) => false);
      }
    }
  }

  void _handleInvalidPurchase(PurchasedItem purchaseDetails) {
    // handle invalid purchase here if  _verifyPurchase` failed.
    setState(() {
      isPurchasing = false;
    });
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WarningPopUp(
              text:
              'Invalid purchase. ERROR:${purchaseDetails.productId}.',);
        });
  }


  Future<bool> logEventAfPurchase(String eventName, Map eventValues) {
    print("$eventName log was sent");
    return _appsflyerSdk.logEvent(eventName, eventValues);
  }

}

class AnimatedCircularGlow extends StatefulWidget {
  final bool repeat;
  final Duration duration;
  final double endRadius;

  final Duration repeatPause;

  final Widget child;

  AnimatedCircularGlow(
      {this.endRadius,
        this.duration,
        this.repeat,
        this.repeatPause,
        @required this.child});

  @override
  _AnimatedCircularGlowState createState() => _AnimatedCircularGlowState();
}

class _AnimatedCircularGlowState extends State<AnimatedCircularGlow>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  Animation<double> animation2;
  Animation<double> alphaAnimation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: widget.duration, vsync: this);
    final Animation curve =
    CurvedAnimation(parent: controller, curve: Curves.decelerate);
    animation = Tween(
        begin: (widget.endRadius * 2) / 3,
        end: (widget.endRadius * 2))
        .animate(curve)
      ..addListener(() {
        setState(() {});
      });
    animation2 = Tween(begin: 0.0, end: (widget.endRadius * 2)).animate(curve)
      ..addListener(() {
        setState(() {});
      });

    alphaAnimation = Tween(begin: 0.30, end: 0.0).animate(controller);
    controller.addStatusListener((_) async {
      if (controller.status == AnimationStatus.completed) {
        await Future.delayed(widget.repeatPause);
        controller.reset();
        controller.forward();
      }
    });
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.endRadius/1.3,
      width: widget.endRadius * 2,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            height: animation2.value/2.6,
            width: animation2.value*1.6,
            child: SizedBox(),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white.withOpacity(alphaAnimation.value),
              borderRadius: BorderRadius.circular(60),
            ),
          ),
          Container(
            height: animation.value/2.6,
            width: animation.value*1.6,
            child: SizedBox(),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white.withOpacity(alphaAnimation.value),
              borderRadius: BorderRadius.circular(60),
            ),
          ),
          widget.child,
        ],
      ),
    );
  }
}