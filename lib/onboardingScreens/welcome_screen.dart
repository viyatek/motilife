import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:motilife/ExtractedWidgets/my_grad_button.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/Utilities/notification_functions.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/localizations.dart';
import 'package:motilife/onboardingScreens/premium_pay.dart';
import 'package:motilife/prefs.dart';
import 'package:motilife/screens/feed_quotes_page.dart';
import 'package:motilife/screens/quotes_bubble.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'dart:io';

class WelcomeScreen extends StatefulWidget {
  static String id = 'welcome';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final functions = AllFunctions();
  final dbProvider = DatabaseProvider.instance;
  int startHour = 8;
  int startMinute = 0;
  int endHour = 22;
  int endMinute = 0;
  int reminderCount = 5;
  String startTime = "09:00";
  String endTime = "21:00";
  int showPremiumPage = 0;
  int _openCount = 0;

  _getReminderData() async {
    _openCount = await SharedPreferencesHelper.getAppOpenCount();//get open count to direct premium or home page
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    startHour = await SharedPreferencesHelper.getReminderStartHour();
    startMinute = await SharedPreferencesHelper.getReminderStartMinute();
    endHour = await SharedPreferencesHelper.getReminderEndHour();
    endMinute = await SharedPreferencesHelper.getReminderEndMinute();
    setState(() {
      reminderCount = generalChanger.getNotificationCount();
      startMinute;
      startHour;
      endMinute;
      endHour;
      if(startHour>9) {
        if(startMinute != 0) {
          startTime = "$startHour:$startMinute";
        } else {
          startTime = "$startHour:0$startMinute";
        }
      } else {
        if(startMinute != 0) {
          startTime = "0$startHour:$startMinute";
        } else {
          startTime = "0$startHour:0$startMinute";
        }
      }
      if(endHour>9) {
        if(endMinute != 0) {
          endTime = "$endHour:$endMinute";
        } else {
          endTime = "$endHour:0$endMinute";
        }
      } else {
        if(endMinute != 0) {
          endTime = "0$endHour:$endMinute";
        } else {
          endTime = "0$endHour:0$endMinute";
        }
      }
    });
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    functions.sendAnalyticsWithOwnName('Welcome_Page_Open');
    _initDB();
    _getReminderData();
/*    _getRemoteShowPremiumPageValue();
    print("Func begins");
    SchedulerBinding.instance.addPostFrameCallback((_) => {
      if(showPremiumPage == 1) {
        Navigator.push( context, MaterialPageRoute( builder: (context) =>
            PremiumPayPage(directedPage: 'On First Open', )))
      }
    });*/
  }

  _initDB() async {
    return await dbProvider.initDatabase();
  }

/*  _getRemoteShowPremiumPageValue () {
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    showPremiumPage = remoteChanger.getShowPremiumPage();
    print("show_premium_page: $showPremiumPage");
  }*/

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final generalChanger = Provider.of<GeneralChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/realImages/onboarding.png"),
            fit: BoxFit.cover,
          ),
        ),
        //margin: EdgeInsets.symmetric(horizontal: 25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            GestureDetector(
              onTap: () async {
                if(await SharedPreferencesHelper.getIntroSeenStatus() == 1) {
                  Navigator.pop(context);
                } else {
                  generalChanger.setNotificationCount(reminderCount);
                  await SharedPreferencesHelper.setNotificationCount(reminderCount);
                  await SharedPreferencesHelper.setReminderStartHour(startHour);
                  await SharedPreferencesHelper.setReminderStartMinute(startMinute);
                  await SharedPreferencesHelper.setReminderEndHour(endHour);
                  await SharedPreferencesHelper.setReminderEndMinute(endMinute);
                  await SharedPreferencesHelper.setIntroSeenStatus(1);
                  if(Platform.isIOS) _requestPermissions();
                  //Show Premium Page
/*                  if (_openCount == 0 && !generalChanger.getWasDirectedToPremium() && remoteChanger.getShowPremiumPage() == 1) {
                    generalChanger.setWasDirectedToPremium(true);
                    SchedulerBinding.instance.addPostFrameCallback((_) => {
                      Navigator.of(context).pushAndRemoveUntil(SlideLeftRoute(page: PremiumPayPage(directedPage: "On_First_Open")), (Route<dynamic> route) => false)
                    });
                  } else {
                    if(remoteChanger.getShowFeed()) {
                      Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: FeedQuotesPage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                    } else {
                      Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: NewHomePage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                    }
                  }*/
                  if(remoteChanger.getShowFeed()) {
                    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: FeedQuotesPage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                  } else {
                    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: NewHomePage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                  }
                }
              },
              child: Container(
                height: heightScreen * 0.07,
                margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.06),
                child: Text(
                    AppLocalizations.of(context).translate('skip'),
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width*0.047,
                  ),
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.1,
              child: Center(
                child: Image.asset(
                  'assets/signs2/icon_onboarding.png',
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.02,
            ),
            Container(
              height: heightScreen * 0.085,
              margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.08),
              child: Center(
                child: AutoSizeText(
                  AppLocalizations.of(context)
                      .translate('setDailyQuoteReminders'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.063,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                  maxLines: 1,
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.06,
            ),
            Container(
              height: heightScreen * 0.5,
              margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.06),
              child: Column(
                children: [
                  buildOpaqueContainer(widthScreen, heightScreen, context, minusFuncReminderCount, sumFuncReminderCount, AppLocalizations.of(context).translate('howMany'), reminderCount.toString()),
                  Container(
                    height: heightScreen * 0.06,
                  ),
                  buildOpaqueContainer(widthScreen, heightScreen, context, minusFuncStartTime, sumFuncStartTime, AppLocalizations.of(context).translate('startAt'), startTime),
                  Container(
                    height: heightScreen * 0.02,
                  ),
                  buildOpaqueContainer(widthScreen, heightScreen, context, minusFuncEndTime, sumFuncEndTime, AppLocalizations.of(context).translate('endAt'), endTime),
                ],
              ),
            ),
            Center(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: widthScreen*0.045),
                height: heightScreen * 0.105,
                child: MyGradButton(
                  decoration: kGradButtonDecoration,
                  buttonTextColor: Colors.white,
                  buttonText:
                      AppLocalizations.of(context).translate('continue_button'),
                  callBack: () async {
                    HapticFeedback.lightImpact();
                    if(endHour != startHour) {
                      generalChanger.setNotificationCount(reminderCount);
                      if(Platform.isIOS) _requestPermissions();
                      await SharedPreferencesHelper.setNotificationCount(reminderCount);
                      await SharedPreferencesHelper.setReminderStartHour(startHour);
                      await SharedPreferencesHelper.setReminderStartMinute(startMinute);
                      await SharedPreferencesHelper.setReminderEndHour(endHour);
                      await SharedPreferencesHelper.setReminderEndMinute(endMinute);
                      await SharedPreferencesHelper.setIntroSeenStatus(1);
                      //functions.sendAnalyticsWithFunnelName('Welcome_Page');
                      generalChanger.setIsNotificationSet(false);
                      if(reminderCount != 0 && generalChanger.getIsNotifiable() == false) {
                        generalChanger.setIsNotifiable(true);
                        await SharedPreferencesHelper.setNotifiableStatus(true);
                      }
                      //Show Premium Page
/*                      print("Open count: $_openCount, direct premium: ${remoteChanger.getShowPremiumPage()}");
                      if (_openCount == 0 && !generalChanger.getWasDirectedToPremium() && remoteChanger.getShowPremiumPage() == 1) {
                        generalChanger.setWasDirectedToPremium(true);
                        SchedulerBinding.instance.addPostFrameCallback((_) => {
                          //Navigator.of(context).pushAndRemoveUntil(SlideLeftRoute(page: PremiumPayPage(directedPage: "On_First_Open")), (Route<dynamic> route) => false)
                          Navigator.push(
                              context,
                              SlideLeftRoute(
                                page: PremiumPayPage(directedPage: "On_First_Open")),
                              )
                        });
                      } else {
                        if(remoteChanger.getShowFeed()) {
                          Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: FeedQuotesPage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                        } else {
                          Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: NewHomePage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                        }
                      }*/
                      if(remoteChanger.getShowFeed()) {
                        Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: FeedQuotesPage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                      } else {
                        Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: NewHomePage(isComeFromReminderPage: true)), (Route<dynamic> route) => false);
                      }
                    } else {
                      _toastInfo(AppLocalizations.of(context).translate('reminderToastWarning'));
                    }
                  }, //null callback
                  //nextScreen: endHour != startHour ? NewHomePage(isComeFromReminderPage: true) : null, //PickTopicScreen(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container buildOpaqueContainer(double widthScreen, double heightScreen, BuildContext context, Function negativeFunc, Function positiveFunc, String leftText, String middleText) {
    return Container(
                  padding: EdgeInsets.symmetric(horizontal: widthScreen * 0.02),
                  height: heightScreen * 0.12,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: widthScreen*0.28,
                        margin: EdgeInsets.only(left: widthScreen*.04),
                        child: Text(
                          leftText,
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.05,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: negativeFunc,
                        child: Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: Container(
                            width: widthScreen*0.13,
                            //padding: EdgeInsets.symmetric(horizontal: widthScreen*0.03, vertical: heightScreen*.03),
                            padding: EdgeInsets.only(left: widthScreen*0.05, right: widthScreen*0.025, top: heightScreen*.03, bottom: heightScreen*.03),
                            child: Transform.scale(
                              scale: 1.35,
                              child: ImageIcon(
                                AssetImage('assets/signs2/onboarding_minus.png'),
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: widthScreen*0.17,
                        child: Text(
                          middleText,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.052,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: positiveFunc,
                        child: Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: Container(
                            width: widthScreen*0.13,
                            padding: EdgeInsets.only(left: widthScreen*0.025, right: widthScreen*0.05, top: heightScreen*.03, bottom: heightScreen*.03),
                            child: Transform.scale(
                              scale: 1.35,
                              child: ImageIcon(
                                AssetImage('assets/signs2/onboarding_sum.png'),
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
  }


  void _requestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }


  _toastInfo(String info) {
    Fluttertoast.showToast(msg: info, toastLength: Toast.LENGTH_LONG);
  }

  sumFuncReminderCount() {
    HapticFeedback.lightImpact();
    if(reminderCount<20) {
      setState(() {
        reminderCount++;
      });
    }
  }

  minusFuncReminderCount() {
    HapticFeedback.lightImpact();
    if(reminderCount>0) {
      setState(() {
        reminderCount--;
      });
    }
  }

  sumFuncStartTime() {
    HapticFeedback.lightImpact();
    setState(() {
      if(startHour<23) {
        if(startMinute == 0) {
          startMinute = startMinute + 30;
        } else {
          startMinute = startMinute - 30;
          startHour = startHour + 1;
        }
        if(startHour>9) {
          if(startMinute == 0) {
            startTime = "$startHour:00";
          } else {
            startTime = "$startHour:$startMinute";
          }
        } else {
          if(startMinute == 0) {
            startTime = "0$startHour:00";
          } else {
            startTime = "0$startHour:$startMinute";
          }
        }
      } else {
        if (startMinute == 0) {
          startMinute = startMinute + 30;
          startTime = "$startHour:$startMinute";
        } else {
          startHour = 0;
          startMinute = 0;
          startTime = "00:00";
        }
      }
    });
  }

  minusFuncStartTime() {
    HapticFeedback.lightImpact();
    setState(() {
      if(startHour>0) {
        if(startMinute == 0) {
          startMinute = 30;
          startHour = startHour - 1;
        } else {
          startMinute = 0;
        }
        if(startHour>9) {
          if(startMinute == 0) {
            startTime = "$startHour:00";
          } else {
            startTime = "$startHour:$startMinute";
          }
        } else {
          if(startMinute == 0) {
            startTime = "0$startHour:00";
          } else {
            startTime = "0$startHour:$startMinute";
          }
        }
      } else {
        if (startMinute == 30) {
          startMinute = 0;
          startTime = "0$startHour:0$startMinute";
        } else {
          startHour = 23;
          startMinute = 30;
          startTime = "$startHour:$startMinute";
        }
      }
    });
  }
  sumFuncEndTime() {
    HapticFeedback.lightImpact();
    setState(() {
      if(endHour<23) {
        if(endMinute == 0) {
          endMinute = endMinute + 30;
        } else {
          endMinute = endMinute - 30;
          endHour = endHour + 1;
        }
        if(endHour>9) {
          if(endMinute == 0) {
            endTime = "$endHour:00";
          } else {
            endTime = "$endHour:$endMinute";
          }
        } else {
          if(endMinute == 0) {
            endTime = "0$endHour:00";
          } else {
            endTime = "0$endHour:$endMinute";
          }
        }
      } else {
        if (endMinute == 0) {
          endMinute = endMinute + 30;
          endTime = "$endHour:$endMinute";
        } else {
          endHour = 0;
          endMinute = 0;
          endTime = "00:00";
        }
      }
    });
  }

  minusFuncEndTime() {
    HapticFeedback.lightImpact();
    setState(() {
      if(endHour>0) {
        if(endMinute == 0) {
          endMinute = 30;
          endHour = endHour - 1;
        } else {
          endMinute = 0;
        }
        if(endHour>9) {
          if(endMinute == 0) {
            endTime = "$endHour:00";
          } else {
            endTime = "$endHour:$endMinute";
          }
        } else {
          if(endMinute == 0) {
            endTime = "0$endHour:00";
          } else {
            endTime = "0$endHour:$endMinute";
          }
        }
      } else {
        if (endMinute == 30) {
          endMinute = 0;
          endTime = "0$endHour:0$endMinute";
        } else {
          endHour = 23;
          endMinute = 30;
          endTime = "$endHour:$endMinute";
        }
      }
    });
  }
}
