import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';


class FetchCache extends StatefulWidget {
  final String url;
  final Widget childWidget;

  FetchCache({this.url, this.childWidget});
  @override
  _FetchCacheState createState() => _FetchCacheState();
}

class _FetchCacheState extends State<FetchCache> {
  Stream<FileResponse> fileStream;

  void _downloadFile() {
    setState(() {
      fileStream = DefaultCacheManager().getFileStream(widget.url, withProgress: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (fileStream == null) {
      _downloadFile();
    }
    return StreamBuilder<FileResponse>(
      stream: fileStream,
      builder: (context, snapshot) {
        Widget body;

        var loading = !snapshot.hasData || snapshot.data is DownloadProgress;

        if (snapshot.hasError) {
          print(snapshot.error.toString());
          body = FileInfoWidget(
            //fileInfo: snapshot.data as FileInfo,
            assetImagePath: 'assets/default_theme/b7',
            childWidget: widget.childWidget,
          );
        } else if (loading) {
          DownloadProgress progress = snapshot.data as DownloadProgress;
          body = Container(
/*            child: CircularProgressIndicator(
              value: progress?.progress,
              backgroundColor: Color(0xFF8C6DFD),
              valueColor: AlwaysStoppedAnimation(Colors.white),
            ),*/
          );//ProgressIndicator(progress: snapshot.data as DownloadProgress);
        } else {
          body = FileInfoWidget(
            fileInfo: snapshot.data as FileInfo,
            childWidget: widget.childWidget,
          );
        }
        return body;
      },
    );
  }
}

class FileInfoWidget extends StatelessWidget {
  final FileInfo fileInfo;
  final Widget childWidget;
  final String assetImagePath;

  FileInfoWidget({this.fileInfo, this.childWidget, this.assetImagePath});

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Container(
          height: heightScreen*1,
          width: widthScreen*1,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: fileInfo == null ? AssetImage('$assetImagePath.png') :  FileImage(fileInfo.file),
          fit: BoxFit.cover,
        ),
      ),
          child: childWidget,
        );
  }
}