import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:facebook_audience_network/ad/ad_interstitial.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:http/http.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import '../notifier_provider.dart';
import '../prefs.dart';
import 'constants.dart';



class AllFunctions {
  FirebaseAnalytics analytics = FirebaseAnalytics();

  loadMyMopupRewardedAd(BuildContext context) {
    print("In load mopup rewarded ad");
/*    MoPubRewardedVideoAd videoAd;
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    videoAd = MoPubRewardedVideoAd(Platform.isAndroid ? kMopup_Rewarded_ad_unit_id_android : kMopup_Rewarded_ad_unit_id_ios,
            (result, args) {
          print('rewarded ad result: $result');
          if (result == RewardedVideoAdResult.GRANT_REWARD) {
            print('Grant reward: $args. Add stream to value 1');
            streamController.add(1);
          } else if (result == RewardedVideoAdResult.LOADED){
            print("Loaded: $result");
            newGeneralChanger.setIsRewardedReady(true);
            //controller.add(0);
          } else if (result == RewardedVideoAdResult.VIDEO_DISPLAYED) {
            streamController.add(0);
          }
        }, reloadOnClosed: true);
    videoAd.load();
    return videoAd;*/
  }

  Future<RemoteConfig> setupRemoteConfig() async {
    await Firebase.initializeApp();
    final RemoteConfig remoteConfig = RemoteConfig.instance;
    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(seconds: 10),
      minimumFetchInterval: const Duration(hours: 1),
    ));
    remoteConfig.setDefaults(<String, dynamic>{
/*      'section_click_count': 4,
      'music_click_count': 4,
      'theme_click_count': 4,
      'force_video_ads': 0,
      'android_admob_rewarded_ad_unit_id': kAdmobAndroidAdUnitId,
      'ios_admob_rewarded_ad_unit_id': kAdmobIosAdUnitId,
      'android_admob_app_id': kAdmobAndroidAppId,
      'ios_admob_app_id': kAdmobIosAppId,
      'db_lock_value': 2,
      'musicRewardedJson': {"rewarded1": "3", "rewarded2": "6", "rewarded3": "9", "rewarded4": "14", "rewarded5": "18", "rewarded6": "25"},
      'themeRewardedJson': {"rewarded1": "3", "rewarded2": "6", "rewarded3": "9", "rewarded4": "14", "rewarded5": "18", "rewarded6": "25"},
      'sectionRewardedJson': {"rewarded1": "3", "rewarded2": "6", "rewarded3": "9", "rewarded4": "14", "rewarded5": "18", "rewarded6": "25"},
      'native_ads_indexes': {"native1":"6","native2":"6","native3":"11","native4":"18","native5":"25","native6":"32"},
      'facebookNativeValue': 2,
      'rate_us_page_number': 4,
      'rate_us_page_number2': 2,
      'free_trial_open': 1,*/
    });
    RemoteConfigValue(null, ValueSource.valueStatic);
    return remoteConfig;
  }

  fetchClickCountOnFirebase(BuildContext context) async {
    RemoteNotifyChanger remoteNotifyChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    RemoteConfig remoteConfig = await setupRemoteConfig();
    try {
      // Using zero duration to force fetching from remote server.
      await remoteConfig.setConfigSettings(RemoteConfigSettings(
        fetchTimeout: const Duration(seconds: 3),
        minimumFetchInterval: Duration.zero,//const Duration(hours: 1),//Duration.zero,
      ));
      await remoteConfig.fetchAndActivate();
      print('android_admob_rewarded_ad_unit_id: ${remoteConfig.getString('android_admob_rewarded_ad_unit_id')} -> comes from firebase.');
      print('ios_admob_rewarded_ad_unit_id: ${remoteConfig.getString('ios_admob_rewarded_ad_unit_id')} -> comes from firebase.');
      var jsonResponseTheme = jsonDecode(remoteConfig.getString('themeRewardedJson'));
      var jsonResponseSection = jsonDecode(remoteConfig.getString('sectionRewardedJson'));
      var jsonResponseMusic = jsonDecode(remoteConfig.getString('musicRewardedJson'));
      var jsonResponseNativeAdsIndexes = jsonDecode(remoteConfig.getString('native_ad_indexes_2'));
      var jsonResponseRateUs = jsonDecode(remoteConfig.getString('rate_us_json'));
      var jsonShowProArray = jsonDecode(remoteConfig.getString('show_premium_on_launch_array'));

      print("json section: $jsonResponseSection");

      remoteNotifyChanger.setForceVideoAdsValue(remoteConfig.getInt('force_video_ads'));
      //Platform.isAndroid ? remoteNotifyChanger.setAdmobRewardedAdUnitId(remoteConfig.getString('android_admob_rewarded_ad_unit_id')) : remoteNotifyChanger.setAdmobRewardedAdUnitId(remoteConfig.getString('ios_admob_rewarded_ad_unit_id'));
      //Platform.isAndroid ? remoteNotifyChanger.setAdmobAppId(remoteConfig.getString('android_admob_app_id')) : remoteNotifyChanger.setAdmobAppId(remoteConfig.getString('ios_admob_app_id'));
      remoteNotifyChanger.setThemeRewardedJson([int.parse(jsonResponseTheme['rewarded1']), int.parse(jsonResponseTheme['rewarded2']), int.parse(jsonResponseTheme['rewarded3']), int.parse(jsonResponseTheme['rewarded4']), int.parse(jsonResponseTheme['rewarded5']), int.parse(jsonResponseTheme['rewarded6']), int.parse(jsonResponseTheme['rewarded7']), int.parse(jsonResponseTheme['rewarded8']), int.parse(jsonResponseTheme['rewarded9'])]);
      remoteNotifyChanger.setSectionRewardedJson([int.parse(jsonResponseSection['rewarded1']), int.parse(jsonResponseSection['rewarded2']), int.parse(jsonResponseSection['rewarded3']), int.parse(jsonResponseSection['rewarded4']), int.parse(jsonResponseSection['rewarded5']), int.parse(jsonResponseSection['rewarded6']), int.parse(jsonResponseTheme['rewarded7']), int.parse(jsonResponseTheme['rewarded8']), int.parse(jsonResponseTheme['rewarded9'])]);
      remoteNotifyChanger.setMusicRewardedJson([int.parse(jsonResponseMusic['rewarded1']), int.parse(jsonResponseMusic['rewarded2']), int.parse(jsonResponseMusic['rewarded3']), int.parse(jsonResponseMusic['rewarded4']), int.parse(jsonResponseMusic['rewarded5']), int.parse(jsonResponseMusic['rewarded6']), int.parse(jsonResponseTheme['rewarded7']), int.parse(jsonResponseTheme['rewarded8']), int.parse(jsonResponseTheme['rewarded9'])]);
      remoteNotifyChanger.setRateUsPageNumber(remoteConfig.getInt('rate_us_page_number_2'));
      remoteNotifyChanger.setNativeAdsIndexes([int.parse(jsonResponseNativeAdsIndexes['native1']), int.parse(jsonResponseNativeAdsIndexes['native2']), int.parse(jsonResponseNativeAdsIndexes['native3']), int.parse(jsonResponseNativeAdsIndexes['native4']), int.parse(jsonResponseNativeAdsIndexes['native5']), int.parse(jsonResponseNativeAdsIndexes['native6']), int.parse(jsonResponseNativeAdsIndexes['native7']), int.parse(jsonResponseNativeAdsIndexes['native8']), int.parse(jsonResponseNativeAdsIndexes['native9'])]);
      remoteNotifyChanger.setDefaultTheme(remoteConfig.getString('defaultTheme'));
      remoteNotifyChanger.setFreeTrialOpen(remoteConfig.getInt('free_trial_open'));
      remoteNotifyChanger.setShowPremiumPage(remoteConfig.getInt('show_premium_page'));
      remoteNotifyChanger.setRateUsJson([int.parse(jsonResponseRateUs['rate1']), int.parse(jsonResponseRateUs['rate2']), int.parse(jsonResponseRateUs['rate3']), int.parse(jsonResponseRateUs['rate4']), int.parse(jsonResponseRateUs['rate5']), int.parse(jsonResponseRateUs['rate6']), int.parse(jsonResponseRateUs['rate7']), int.parse(jsonResponseRateUs['rate8']), int.parse(jsonResponseRateUs['rate9'])]);
      remoteNotifyChanger.setShowLiveThemes(remoteConfig.getInt('show_live_themes'));
      remoteNotifyChanger.setShowFeed(remoteConfig.getBool('show_feed'));
      remoteNotifyChanger.setNativeAdsMode(remoteConfig.getInt('native_ads_mode_feed'));
      remoteNotifyChanger.setFbNativePlacementId(remoteConfig.getString(Platform.isAndroid ? 'fb_native_ad_placement_id' : 'fb_native_ad_placement_id_ios'));
      remoteNotifyChanger.setFbInterstitialPlacementId(remoteConfig.getString(Platform.isAndroid ? 'fb_interstitial_placement_id_android' : 'fb_interstitial_placement_id_ios'));
      remoteNotifyChanger.setFbNativeBannerPlacementId(remoteConfig.getString(Platform.isAndroid ? 'fb_banner_placement_id_android' : 'fb_banner_placement_id_ios'));
      remoteNotifyChanger.setFbBannerPlacementId(remoteConfig.getString(Platform.isAndroid ? 'fb_banner_original_placement_id_android' : 'fb_banner_original_placement_id_ios'));
      remoteNotifyChanger.setShowBannerAd(remoteConfig.getBool('show_banner_ad_2'));
      remoteNotifyChanger.setShowBannerAdForIOS(remoteConfig.getBool('show_banner_ad_for_ios_2'));
      remoteNotifyChanger.setShowPremiumOnLaunchArray([int.parse(jsonShowProArray['one']), int.parse(jsonShowProArray['two']), int.parse(jsonShowProArray['three']), int.parse(jsonShowProArray['four']), int.parse(jsonShowProArray['five']), int.parse(jsonShowProArray['six'])]);
      remoteNotifyChanger.setNativeBannerOrBanner(remoteConfig.getBool('nativeBanner_or_Banner'));

      MyStatics.premiumPageURL = remoteConfig.getString('premium_url');
      print("Show pro array: $jsonShowProArray");
    } on PlatformException catch (exception) {
      // Fetch throttled.
      print('CATCH ERROR FROM REMOTE CONFIG $exception');
      remoteNotifyChanger.setForceVideoAdsValue(1);
      //Platform.isAndroid ? remoteNotifyChanger.setAdmobRewardedAdUnitId(kAdmobAndroidAdUnitId) : remoteNotifyChanger.setAdmobRewardedAdUnitId(kAdmobIosAdUnitId);
      //Platform.isAndroid ? remoteNotifyChanger.setAdmobAppId(kAdmobAndroidAppId) : remoteNotifyChanger.setAdmobAppId(kAdmobIosAppId);
      remoteNotifyChanger.setThemeRewardedJson([1, 4, 8, 12, 16, 20]);
      remoteNotifyChanger.setMusicRewardedJson([1, 4, 8, 12, 16, 20]);
      remoteNotifyChanger.setSectionRewardedJson([1, 4, 8, 12, 16, 20]);
      remoteNotifyChanger.setNativeAdsIndexes([3, 8, 13, 18]);
      remoteNotifyChanger.setRateUsPageNumber(2);
      remoteNotifyChanger.setDefaultTheme('i1');
      remoteNotifyChanger.setFreeTrialOpen(1);
      remoteNotifyChanger.setShowPremiumPage(0);
      remoteNotifyChanger.setRateUsJson([2, 4, 5, 7, 9, 12]);
      remoteNotifyChanger.setShowLiveThemes(1);
      remoteNotifyChanger.setShowFeed(false);
      remoteNotifyChanger.setNativeAdsMode(5);
      remoteNotifyChanger.setFbNativePlacementId(Platform.isAndroid ? kFB_Native_Placement_id_android : kFB_Native_Placement_id_ios);
      remoteNotifyChanger.setFbInterstitialPlacementId(Platform.isAndroid ? kFB_interstitial_placement_id_android : kFB_interstitial_placement_id_ios);
      remoteNotifyChanger.setFbBannerPlacementId(Platform.isAndroid ? kFB_my_banner_placement_id_android : kFB_my_banner_placement_id_ios);
      remoteNotifyChanger.setFbNativeBannerPlacementId(Platform.isAndroid ? kFB_native_banner_placement_id_android : kFB_native_banner_placement_id_ios);
      remoteNotifyChanger.setShowBannerAd(true);
      remoteNotifyChanger.setShowBannerAdForIOS(true);
      remoteNotifyChanger.setShowPremiumOnLaunchArray([2,6,10, 14]);
      remoteNotifyChanger.setNativeBannerOrBanner(true);
    } catch (exception) {
      print(
          'Unable to fetch remote config. Cached or default values will be used: ERROR: $exception');
      remoteNotifyChanger.setForceVideoAdsValue(1);
      //Platform.isAndroid ? remoteNotifyChanger.setAdmobRewardedAdUnitId(kAdmobAndroidAdUnitId) : remoteNotifyChanger.setAdmobRewardedAdUnitId(kAdmobIosAdUnitId);
      //Platform.isAndroid ? remoteNotifyChanger.setAdmobAppId(kAdmobAndroidAppId) : remoteNotifyChanger.setAdmobAppId(kAdmobIosAppId);
      remoteNotifyChanger.setThemeRewardedJson([5, 10, 15, 20, 25, 30]);
      remoteNotifyChanger.setMusicRewardedJson([5, 10, 15, 20, 25, 30]);
      remoteNotifyChanger.setSectionRewardedJson([4, 8, 12, 16, 18, 20]);
      remoteNotifyChanger.setNativeAdsIndexes([3, 8, 13, 18]);
      remoteNotifyChanger.setRateUsPageNumber(2);
      remoteNotifyChanger.setDefaultTheme('i1');
      remoteNotifyChanger.setFreeTrialOpen(1);
      remoteNotifyChanger.setShowPremiumPage(0);
      remoteNotifyChanger.setRateUsJson([2, 4, 5, 7, 9, 12]);
      remoteNotifyChanger.setShowLiveThemes(1);
      remoteNotifyChanger.setShowFeed(false);
      remoteNotifyChanger.setNativeAdsMode(5);
      remoteNotifyChanger.setFbNativePlacementId(Platform.isAndroid ? kFB_Native_Placement_id_android : kFB_Native_Placement_id_ios);
      remoteNotifyChanger.setFbInterstitialPlacementId(Platform.isAndroid ? kFB_interstitial_placement_id_android : kFB_interstitial_placement_id_ios);
      remoteNotifyChanger.setFbBannerPlacementId(Platform.isAndroid ? kFB_my_banner_placement_id_android : kFB_my_banner_placement_id_ios);
      remoteNotifyChanger.setFbNativeBannerPlacementId(Platform.isAndroid ? kFB_native_banner_placement_id_android : kFB_native_banner_placement_id_ios);
      remoteNotifyChanger.setShowBannerAd(true);
      remoteNotifyChanger.setShowBannerAdForIOS(true);
      remoteNotifyChanger.setShowPremiumOnLaunchArray([2,6,10, 14]);
      remoteNotifyChanger.setNativeBannerOrBanner(true);
    }
  }

  Future<bool> verifyPurchaseWithDate(PurchasedItem purchaseDetails) async {
    var endpoint =
        "https://0tg0ktrj7h.execute-api.us-west-2.amazonaws.com/live/subscriptions-getbytoken-v2?token=${purchaseDetails.purchaseToken}&packageName=com.viyatek.motilife";
    var url = Uri.parse(endpoint);
    var response = await get(url);
    var jsonResponse = jsonDecode(response.body);
    var expiryTime = int.parse(jsonResponse['expiryTimeMillis']);
    if (response.statusCode == 200 && expiryTime > DateTime.now().millisecondsSinceEpoch) {
      print('$expiryTime > ${DateTime.now().millisecondsSinceEpoch}');
      await SharedPreferencesHelper.setExpiryTime(expiryTime);
      await SharedPreferencesHelper.setIsPremiumOrSubscribedStatus(true);
      return Future<bool>.value(true);
    } else {
      print(':((( $expiryTime < ${DateTime.now().millisecondsSinceEpoch}');
      await SharedPreferencesHelper.setIsPremiumOrSubscribedStatus(false);
      return Future<bool>.value(false);
    }
  }

  getExpiryDateForIOS() async {
    List<PurchasedItem> items = await FlutterInappPurchase.instance.getAvailablePurchases();
    print('Looking for expiry date from Apple');
    try{
      Map data = {
        "password": kAppleAppSecret,
        "receipt-data": items[0].transactionReceipt
      };
      //encode Map to JSON
      var body = json.encode(data);
      var url = Uri.parse(kAwsAppleEndpoint);
      var response = await post(url, body: body);
      var jsonResponse = jsonDecode(response.body);
      var lastReceiptInfo = jsonResponse['latest_receipt_info'];
      var expiryTimeMillis = int.parse(lastReceiptInfo[0]['expires_date_ms']);
      print('Expiry date from Apple: $expiryTimeMillis');
      await SharedPreferencesHelper.setExpiryTime(expiryTimeMillis);
      return expiryTimeMillis;
    } catch(e){
      print('sth went wrong: $e');
      return 1603107459537;
    }
  }

  Future<int> getExpiryDateForAndroid() async {
    List<PurchasedItem> items = await FlutterInappPurchase.instance.getAvailablePurchases();
    var expiryTime = 1603107459537;
    for(var item in items) {
      print('Item is: ${item.productId}');
      if(item.productId!=kLifetimeMemberShipId){
        //print('Android token: ${item.purchaseToken}');
        var endpoint = "https://0tg0ktrj7h.execute-api.us-west-2.amazonaws.com/live/subscriptions-getbytoken-v2?token=${item.purchaseToken}&packageName=com.viyatek.motilife";
        var url = Uri.parse(endpoint);
        var response = await get(url);
        var jsonResponse = jsonDecode(response.body);
        await SharedPreferencesHelper.setExpiryTime(int.parse(jsonResponse['expiryTimeMillis']));
        expiryTime = int.parse(jsonResponse['expiryTimeMillis']);
      }
    }
    print('Expiry date from Android: $expiryTime');
    return expiryTime;
  }

  Future<bool> verifyPurchaseFromPref(int expiry) async {
    print('Verify from PREF...');
    if (expiry > DateTime.now().millisecondsSinceEpoch) {
      print('Pref is bigger than now. OK. $expiry > ${DateTime.now().millisecondsSinceEpoch}');
      await SharedPreferencesHelper.setIsPremiumOrSubscribedStatus(true);
      return Future<bool>.value(true);
    } else {
      print(':((( Pref is smaller: $expiry < ${DateTime.now().millisecondsSinceEpoch}');
      await SharedPreferencesHelper.setIsPremiumOrSubscribedStatus(false);
      return Future<bool>.value(false);
    }
  }

  Future<void> sendAnalyticsEventFromSectionClick(
      String sectionName) async {
    await analytics.logEvent(
      name: 'section_click_event',
      parameters: {
        'section_name': sectionName
      },
    );
  }

  Future<void> sendAnalyticsEventFromThemeClick(
      String themeName) async {
    await analytics.logEvent(
      name: 'theme_click_event',
      parameters: {'theme_name': themeName},
    );
  }

  Future<void> sendAnalyticsEventFromMusicClick(
      String musicName) async {
    await analytics.logEvent(
      name: 'music_click_event',
      parameters: {'music_name': musicName},
    );
  }

  Future<void> sendAnalyticsEventFromFontClick(
      String fontName) async {
    await analytics.logEvent(
      name: 'font_click_event',
      parameters: {'font_name': fontName},
    );
  }

  Future<void> sendAnalyticsWithOwnName(String eventName) async {
    // print('Event send');
    await analytics.logEvent(
      name: '$eventName',
      parameters: {'clicked': true},
    );
  }

  Future<bool> logAppsFlyerEvent(AppsflyerSdk appsflyerSdk, String eventName, Map eventValues) async {
    print("$eventName log was sent");
    return appsflyerSdk.logEvent(eventName, eventValues);
  }

  setIntroSeen() async {
    await SharedPreferencesHelper.setIntroSeenStatus(1);
  }

  playMusic(MusicChanger musicChanger) async {
    if(await SharedPreferencesHelper.getMusicPreference() != '') {
      String musicName = await SharedPreferencesHelper.getMusicPreference();
      musicChanger.getAdvancedPlayer().setUrl('$kCloudFrontMusicUrl/$musicName.mp3');
      musicChanger.getAdvancedPlayer().resume();
      musicChanger.getAdvancedPlayer().setReleaseMode(ReleaseMode.LOOP);
      musicChanger.getAdvancedPlayer().setPlaybackRate(playbackRate: 1.0);
      musicChanger.setAdvancedPlayer(musicChanger.getAdvancedPlayer());
      musicChanger.setPlayingMusic(await SharedPreferencesHelper.getMusicPreference());
    }
  }

  resumeMusic(MusicChanger musicChanger) async {
    if(await SharedPreferencesHelper.getMusicPreference() != '') {
      musicChanger.getAdvancedPlayer().resume();
    }
  }

  void loadInterstitialAd(RemoteNotifyChanger remoteChanger, BuildContext context) {
    print("Trying to load interstitial in func");
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    //newGeneralChanger.setIsInterstitialAdLoaded(false);//to avoid null value
    FacebookInterstitialAd.loadInterstitialAd(
      placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2650502525028617",
      //placementId: remoteChanger.getFbInterstitialPlacementId(),
      listener: (result, value) {
        print(">> FAN > Interstitial Ad: $result --> $value");
        if (result == InterstitialAdResult.LOADED){
          newGeneralChanger.setIsInterstitialAdLoaded(true);
          //FacebookInterstitialAd.showInterstitialAd();
        }
        /// Once an Interstitial Ad has been dismissed and becomes invalidated,
        /// load a fresh Ad by calling this function.
        if (result == InterstitialAdResult.DISMISSED &&
            value["invalidated"] == true) {
          newGeneralChanger.setIsInterstitialAdLoaded(false);
          loadInterstitialAd(remoteChanger, context);
        }
        if(result == InterstitialAdResult.ERROR) {
          newGeneralChanger.setIsInterstitialAdLoaded(false);
        }
      },
    );
  }

/*  void createAdmobInterstitialAd(int attemptsCount) {
    if (attemptsCount <= MyStatics.maxFailedLoadAttempts) {
      InterstitialAd.load(
          adUnitId: Platform.isAndroid ? kAdmob_Interstitial_ads_ad_unit_id_android : kAdmob_Interstitial_ads_ad_unit_id_ios,//InterstitialAd.testAdUnitId,//
          request: MyStatics.request,
          adLoadCallback: InterstitialAdLoadCallback(
            onAdLoaded: (InterstitialAd ad) {
              print('$ad loaded');
              MyStatics.interstitialAd = ad;
              MyStatics.numInterstitialLoadAttempts = 0;
              MyStatics.interstitialAd.setImmersiveMode(true);
            },
            onAdFailedToLoad: (LoadAdError error) {
              print('InterstitialAd failed to load: $error.');
              print("Attempt count: $attemptsCount, max limit: ${MyStatics.maxFailedLoadAttempts}");
              MyStatics.numInterstitialLoadAttempts += 1;
              MyStatics.interstitialAd = null;
              if (attemptsCount <= MyStatics.maxFailedLoadAttempts) {
                createAdmobInterstitialAd(attemptsCount+1);
              }
            },
          ));
    } else {
      print("Max interstitial request count exceeded... $attemptsCount");
    }
  }*/

}