import 'package:flutter/cupertino.dart';
import 'package:motilife/Utilities/constants.dart';

class TopicModel {
  String textTopic;
  final String subTitle;
  final String imageName;
  final int quotesCount;
  final String hours;
  final Widget nextScreen;
  final int hour;
  Color textColor;
  BoxDecoration boxDecoration;
  bool isSelected;
  TopicModel(
      {this.textTopic,
      this.subTitle,
      this.imageName,
      this.quotesCount,
      this.hours,
      this.hour,
      this.textColor = kBlackText,
      this.boxDecoration = kGradDecorationGrey,
      this.isSelected = false,
      this.nextScreen});


}


class QuoteBookmarkedModel {
  final int quoteId;
  bool isBookmarked;
  QuoteBookmarkedModel({this.quoteId, this.isBookmarked = false});

  void changeIsBookmarked() {
    isBookmarked = !isBookmarked;
  }
}

@immutable
class Message {
  final String title;
  final String body;

  const Message({
    @required this.title,
    @required this.body,
  });
}
