import '../DbProvider.dart';
import '../prefs.dart';
import 'constants.dart';

final dbProvider = DatabaseProvider.instance;

class DbFunctions {


  updateThemesUnlockedStatus(int isUnlocked) async {
    final rows = await dbProvider.queryAllUserThemes();
    rows.forEach((theme) async {
      await dbProvider.updateUserThemeTable(UserTheme(
        userThemeName: theme['userThemeName'],
        isUnlockedTheme: isUnlocked,
      ));
    });
  }

  updateMusicsUnlockedStatus(int isUnlockedMusic) async {
    final rows = await dbProvider.queryAllMusicsWithUserData();
    rows.forEach((music) async {
      await dbProvider.updateUserMusicTable(UserMusic(
        userMusicId: music['musicId'],
        isUnlockedMusic: isUnlockedMusic,
      ));
    });
  }

  updateFontsUnlockedStatus(int isUnlockedFont) async {
    final rows = await dbProvider.queryAllFonts();
    rows.forEach((font) async {
      await dbProvider.updateUserFontTable(UserFont(
        userFontId: font['fontId'],
        isUnlockedFont: isUnlockedFont,
      ));
    });
  }

  updateLiveThemesUnlockedStatus(int isUnlocked) async {
    final rows = await dbProvider.queryAllUserLiveThemes();
    rows.forEach((theme) async {
      await dbProvider.updateUserLiveThemeTable(UserTheme(
        userThemeName: theme['userThemeName'],
        isUnlockedTheme: isUnlocked,
      ));
    });
  }


  updateSectionsUnlockedStatus(int isUnlocked) async {
    final allUserMusics = await dbProvider.queryAllUserSections();
    allUserMusics.forEach((section) async {
      await dbProvider.updateUserSectionTable(UserSection(
          userSectionId: section['userSectionId'],
          isUnlockedSection: isUnlocked,
          isSelectedSection: section['sectionSelectedStatus'],
      ));
    });
    print('All sections updated as $isUnlocked.');
  }

  updateUserQuoteStatus(int quoteid, int isCollected, int isSeen, int isBookmarked) async {
    //print("$quoteid updated as $isSeen");
    await dbProvider.updateUserQuoteTable(UserQuote(
        userQuoteId: quoteid,
        isBookmarked: isBookmarked,
        isCollected: isCollected,
        isSeen: isSeen));
    //print('bookmarked: $isBookmarked, Collected: $isCollected, id: $quoteid, seen: $isSeen from func');
  }



  updateUserThemeUnlockedStatus(String themename) async {
    final row = await dbProvider.querySelectedUserTheme(themename);
    row.forEach((theme) async {
      await dbProvider.updateUserThemeTable(UserTheme(
        userThemeName: theme['userThemeName'],
        isUnlockedTheme: theme['isUnlockedTheme'] == 1 ? 0 : 1,
      ));
    });
  }


  updateUserTopicAsTrue(int topicId) async {
    await dbProvider.updateUserTopicTable(
        UserTopic(userTopicId: topicId, isSelectedTopic: 1));
  }

  updateUserTopicAsFalse(int topicId) async {
    await dbProvider.updateUserTopicTable(
        UserTopic(userTopicId: topicId, isSelectedTopic: 0));
  }

  updateUserSectionsSelectedStatus(int topicId, int isSelected) async {
    final allRowsSections =
    await dbProvider.queryAllSectionsOfAnyTopicWithUserData(topicId);
    allRowsSections.forEach((rowSection) async {
      await dbProvider.updateUserSectionTable(UserSection(
          userSectionId: rowSection['sectionid'],
          isUnlockedSection: rowSection['sectionIsUnlockedStatus'],
          isSelectedSection: isSelected));
    });
  }


  queryUserSelectedTopics() async {
    final allRows = await dbProvider.queryAllUserTopics();
    print('Topic query all rows:');
    allRows.forEach((row) => print(row));
  }

  queryUserSelectedSections() async {
    final allRowsSections = await dbProvider.queryAllUserSections();
    print('All section user data:');
    allRowsSections.forEach((row) => print(row));
  }

  queryUserSelectedQuotes() async {
    final allRows = await dbProvider.queryAllUserQuotes();
    print('All quotes user data:');
    allRows.forEach((row) => print(row));
  }

  //LOCK-UNLOCK FUNCTIONS, AFTER SUBSCRIBE
  /*updateThemesUnlockedStatusAsZero(String themeName) async {
    final allThemes = await dbProvider.queryAllThemesWithUserData();
    List<String> themeProperties = await SharedPreferencesHelper.getThemeProperties(themeName);
    String currentThemeName = themeProperties[17];
    allThemes.forEach((theme) async {
      if (theme['themeName'] == currentThemeName) {
        if (theme['isPremiumTheme'] == 1) {
          await dbProvider.updateUserThemeTable(UserTheme(
            userThemeName: theme['themeName'],
            isUnlockedTheme: 0,
          ));
          await SharedPreferencesHelper.setThemeProperties(['210','1','38','26','1.5','1.5','3.0','1.1','3.5','1.5','255','255','255','255','5','0.11','Berlin Sans FB Regular','b7']);//set to default theme
        }
      } else {
        await dbProvider.updateUserThemeTable(UserTheme(
          userThemeName: theme['themeName'],
          isUnlockedTheme: 0,
        ));
      }
    });
  }*/


  updateSectionsUnlockedStatusAsZero() async {
    final allUserMusics = await dbProvider.queryAllSectionsWithUserData();
    allUserMusics.forEach((section) async {
      await dbProvider.updateUserSectionTable(UserSection(
        userSectionId: section['userSectionId'],
        isUnlockedSection: section['isPremium'] == 1 ? 0 : 1,
        isSelectedSection: section['sectionSelectedStatus'],
      ));
    });
    print('Sections updated as ZERO');
  }

  querySeenQuotesCount() async {
    int count = await dbProvider.queryCountAllSeenQuotes();
    print('SEEN QUOTES COUNT: $count');
  }

  queryAllSeenQuotes() async {
    final seenQuotes = await dbProvider.queryAllSeenQuotesOfUser();
    seenQuotes.forEach((quote) {
      print(quote['quote']);
    });
  }

  /*setDbCreatedEvent() async {
    dbProvider.querySingleQuote(1).catchError(
          (onError) {
        print("ERROR..On Single Quote query... $onError");
      },
    ).whenComplete(() async {
      print('Db created event TRUE...');
      await SharedPreferencesHelper.setIsRecordDbCreatedEvent(true);
    });
  }*/

}