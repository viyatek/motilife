/*
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:motilife/Utilities/models.dart';
import 'package:motilife/onboardingScreens/premium_pay.dart';

class MessagingWidget extends StatefulWidget {
  @override
  _MessagingWidgetState createState() => _MessagingWidgetState();
}

class _MessagingWidgetState extends State<MessagingWidget> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<Message> messages = [];
  void _navigateToItemDetail(Map<String, dynamic> message) {
    Navigator.push(
        context,
        MaterialPageRoute(
        builder: (context) => MessagingWidget()
    ));
  }

  void _navigateToPremium() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PremiumPayPage(directedPage: "Firebase_message",)
        ));
  }

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final notification = message['notification'];
        setState(() {
          messages.add(Message(
            title: notification['title'],
            body: notification['body'],
          ));
        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        //_navigateToPremium();
        _navigateToItemDetail(message);
        final notification = message['notification'];
        setState(() {
          messages.add(Message(
            title: '${notification['title']}',
            body: '${notification['body']}',
          ));
        });
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        _navigateToPremium();
        //_navigateToItemDetail(message);
        final notification = message['data'];
        setState(() {
          messages.add(Message(
            title: notification['title'],
            body: notification['body'],
          ));
        });
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Text('Messages'),
            ListView(
              shrinkWrap: true,
              children: messages.map(buildMessage).toList(),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMessage(Message message) => ListTile(
        title: Text(message.title),
        subtitle: Text(message.body),
      );
}
*/
