import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:flutter/material.dart';
import 'dart:io';

const kGradDecoration = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFF8C6DFD),
      Color(0xFF6737BF),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  ),
  borderRadius: BorderRadius.all(Radius.circular(8.0)),
//  boxShadow: [
//    BoxShadow(
//      color: Colors.black12,
//      offset: Offset(5, 5),
//      blurRadius: 10,
//    )
//  ],
);

const kGradDecorationGrey = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFFF2F2F2),
      Color(0xFFEDEDED),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  ),
  borderRadius: BorderRadius.all(Radius.circular(8.0)),
//  boxShadow: [
//    BoxShadow(
//      color: Colors.black12,
//      offset: Offset(5, 5),
//      blurRadius: 10,
//    )
//  ],
);

const kGradButtonDecoration = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFF1f1f1f),
      Color(0xFF1f1f1f),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  ),
  borderRadius: BorderRadius.all(Radius.circular(32.0)),
//  boxShadow: [
//    BoxShadow(
//      color: Colors.black12,
//      offset: Offset(5, 5),
//      blurRadius: 10,
//    )
//  ],
);

const kGradButtonDecorationGrey = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFFFFFFFF),
      Color(0xFFFFFFFF),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  ),
  borderRadius: BorderRadius.all(Radius.circular(32.0)),
//  boxShadow: [
//    BoxShadow(
//      color: Colors.black12,
//      offset: Offset(5, 5),
//      blurRadius: 10,
//    )
//  ],
);

const kGradDecorationGreyNight = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFF424242),
      Color(0xFF424242)
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  ),
  borderRadius: BorderRadius.all(Radius.circular(8.0)),
);

const kGradButtonDecorationGreyNight = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFF424242),
      Color(0xFF424242)
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  ),
  borderRadius: BorderRadius.all(Radius.circular(32.0)),
//  boxShadow: [
//    BoxShadow(
//      color: Colors.black12,
//      offset: Offset(5, 5),
//      blurRadius: 10,
//    )
//  ],
);

const kGradPremiumSmallButtonDecoration = BoxDecoration(
  gradient: LinearGradient(
    colors: [
      Color(0xFFffffff),
      Color(0xFFffffff),
    ],
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
  ),
  borderRadius: BorderRadius.all(Radius.circular(4.0)),
//  boxShadow: [
//    BoxShadow(
//      color: Colors.black12,
//      offset: Offset(5, 5),
//      blurRadius: 10,
//    )
//  ],
);

const kWhiteText = Colors.white;
const kBlackText = Colors.black;


//const String kMonthlySubscriptionId = 'zmotilife_monthly_subscription';
//const String kYearlySubscriptionId = 'yearly_subscription_motilife';
const String kLifetimeMemberShipId = 'motilife_lifetime_membership';

const String kYearlySubscriptionIdFreeTrial = 'yearly_subscription_motilife_free';

const List<String> kProductIds = <String>[
  'motilife_lifetime_membership',
  'yearly_subscription_motilife',
  'zmotilife_monthly_subscription',
];

const List<String> kProductIdsFreeTrial = <String>[
  'motilife_lifetime_membership',
  'yearly_subscription_motilife_free',
  'zmotilife_monthly_subscription',
];


const String kTestDeviceId = 'YOUR_DEVICE_ID';

const String kAdmobAndroidAdUnitId = 'ca-app-pub-8081984700756791/7899886809';
const String kAdmobIosAdUnitId = 'ca-app-pub-8081984700756791/9728151407';

const String kAdmobAndroidAppId = 'ca-app-pub-8081984700756791~8749934046';
const String kAdmobIosAppId = 'ca-app-pub-8081984700756791~5980478080';

const String kCloudFrontMusicUrl = 'https://d14pudncugfw7u.cloudfront.net';
const String kS3MusicUrl = 'https://apps-images.s3-us-west-2.amazonaws.com/motilifeaudios';
const String kCloudFrontMusicImageUrl = 'https://d20maep3v8vsj.cloudfront.net';
const String kCloudFrontThemeImageUrl = 'https://d1mtb1z76438du.cloudfront.net';

const String kUrlPlayStore = 'https://play.google.com/store/apps/details?id=com.viyatek.motilife';
const String kUrlAppStore = 'https://apps.apple.com/tr/app/motilife-motivational-quotes/id1531721462';
const String kUrlAppStoreReview = 'https://apps.apple.com/tr/app/motilife-motivational-quotes/id1531721462?action=write-review';
const String kAppleAppSecret = '95b1566c9b244afc830a910d57875d86';
const String kAwsAppleEndpoint = 'https://krtiuy73y0.execute-api.us-west-2.amazonaws.com/live/verify-receipt';
const double kSigmaX = 6.0;
const double kSigmaY = 6.0;
const double kAppBarFontSize = 0.05;

final Uri kEmailLaunchUri = Uri(
    scheme: 'mailto',
    path: 'hello@viyatek.io',
    queryParameters: {'subject': 'Inspirational Quotes'});

const kDefaultFontName = "ProximaNova Regular";


//ADMOB
const kAdmob_Native_ads_app_id_android = "ca-app-pub-5926654923760585~3126392722";
const kAdmob_Native_ads_ad_unit_id_android = "ca-app-pub-5926654923760585/1031277072";
const kAdmob_Interstitial_ads_ad_unit_id_android = "ca-app-pub-5926654923760585/6564828879";
const kAdmob_Banner_ads_ad_unit_id_android = "ca-app-pub-5926654923760585/8768390196";


const kAdmob_Native_ads_app_id_ios = "ca-app-pub-5926654923760585~4652201155";
const kAdmob_Native_ads_ad_unit_id_ios = "ca-app-pub-5926654923760585/4282350271";
const kAdmob_Interstitial_ads_ad_unit_id_ios = "ca-app-pub-5926654923760585/9806139690";
const kAdmob_Banner_ads_ad_unit_id_ios = "ca-app-pub-5926654923760585/1899729519";




const kMopup_Native_ad_unit_id_android = "fa0277cad04143949b7b3ddaf37fe819";
const kMopup_Native_ad_unit_id_ios = "111111";
const kMopup_Rewarded_ad_unit_id_android = "101daef1ac86404ea54358e3dd04c92b";
const kMopup_Rewarded_ad_unit_id_ios = "111111";
const kMopup_Publisher_id = "4b2fca4f74614cc4b25bcfc4c04119b6";

//Facebook Ad Units
const String kFB_Native_Placement_id_ios = "307474117387104_321665329301316";
const String kFB_Native_Placement_id_android = "307474117387104_321637939304055";

const kFB_interstitial_placement_id_android = "307474117387104_373393497461832";
const kFB_interstitial_placement_id_ios = "307474117387104_373394484128400";

const kFB_native_banner_placement_id_android = "307474117387104_375215803946268";
const kFB_native_banner_placement_id_ios = "307474117387104_375216387279543";

const kFB_my_banner_placement_id_android = "307474117387104_389528479181667";
const kFB_my_banner_placement_id_ios = "307474117387104_389520309182484";

const String kUFPlayStoreUrl = "https://play.google.com/store/apps/details?id=com.viyatek.ultimatefacts";
const String kUFAppStoreUrl = "https://apps.apple.com/tr/app/ultimate-facts-did-you-know/id1477824910?l=en";
const String kQuotePlayStoreUrl = "https://play.google.com/store/apps/details?id=com.viyatek.ultimatequotes";
const String kDynamicLinkIQ = "https://motilife.page.link/app";

const String kDEV_KEY_ApsFlyer = "saSk33o7FZjSjeoZGHz5i7";
const String kAppId_iOS = "1531721462";
const String kAppId_Android = "com.viyatek.motilife";

const String kPlaceholderText =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod'
    ' tempor incididunt ut labore et dolore magna aliqua. Faucibus purus in'
    ' massa tempor. Quis enim lobortis scelerisque fermentum dui faucibus'
    ' in. Nibh praesent tristique magna sit amet purus gravida quis.'
    ' Magna sit amet purus gravida quis blandit turpis cursus in. Sed'
    ' adipiscing diam donec adipiscing tristique. Urna porttitor rhoncus'
    ' dolor purus non enim praesent. Pellentesque habitant morbi tristique'
    ' senectus et netus. Risus ultricies tristique nulla aliquet enim tortor'
    ' at.';


class MyStatics {
  static bool isOpenCountIncreased = false;
  static final facebookAppEvents = FacebookAppEvents();
  static String premiumPageURL = "https://d2e1xfn23zsdmn.cloudfront.net/UF_premium2.jpg";
/*  static final AdRequest request = AdRequest(
    keywords: <String>['foo', 'bar'],
    contentUrl: 'http://foo.com/bar.html',
    nonPersonalizedAds: true,
  );
  static InterstitialAd interstitialAd;*/
  static int maxFailedLoadAttempts = 4;
  static int numInterstitialLoadAttempts = 0;
}

class Constants {
  /// Placeholder text.
  static const String placeholderText =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod'
      ' tempor incididunt ut labore et dolore magna aliqua. Faucibus purus in'
      ' massa tempor. Quis enim lobortis scelerisque fermentum dui faucibus'
      ' in. Nibh praesent tristique magna sit amet purus gravida quis.'
      ' Magna sit amet purus gravida quis blandit turpis cursus in. Sed'
      ' adipiscing diam donec adipiscing tristique. Urna porttitor rhoncus'
      ' dolor purus non enim praesent. Pellentesque habitant morbi tristique'
      ' senectus et netus. Risus ultricies tristique nulla aliquet enim tortor'
      ' at.';
}

