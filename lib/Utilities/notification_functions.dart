import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:motilife/screens/single_quote.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/DbProvider.dart';
import 'dart:io';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:rxdart/subjects.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:motilife/prefs.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

/// Streams are created so that app can respond to notification-related events
/// since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject = BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject = BehaviorSubject<String>();


const MethodChannel platform = MethodChannel('dexterx.dev/flutter_local_notifications_example');

class ReceivedNotification {
  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });

  final int id;
  final String title;
  final String body;
  final String payload;
}


final dbProvider = DatabaseProvider.instance;

tz.TZDateTime scheduledDate;
String _timezone;

class NotificationHandler {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  //InitializationSettings initializationSettings;
  var allQuotes;
  List<QuoteQuery> quoteQuery = [];
  List<QuoteQuery> extraQuotes = [];
  int setForHowManyDays;
  bool isPremiumOrSubscribed = false;
  bool secondNotificationSet = false;


  Future showDailyAtTime(int day, int hour, int minute, int notificationId, String quote, int quoteId) async {
    var platformChannelSpecifics = const NotificationDetails(
        android: AndroidNotificationDetails('daily notification channel id',
          'daily notification channel name', 'daily notification description',
          priority: Priority.high,
          importance: Importance.max,
          styleInformation: BigTextStyleInformation(''),
          //ongoing: true,
          enableVibration: false,
          ticker: 'ticker',
          visibility: NotificationVisibility.public
        ),
        iOS: IOSNotificationDetails(
            presentAlert: false,
            presentBadge: true,
            presentSound: false));
    try {
      await flutterLocalNotificationsPlugin.zonedSchedule(
          notificationId,
          null,
          isPremiumOrSubscribed ? quote : quote.length>81 ? '${quote.substring(0, 80)}...' : quote,
          nextInstanceTime(day, hour, minute),
          platformChannelSpecifics, //payload: 'item x',
          androidAllowWhileIdle: true,
          payload: '$quoteId',
          uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime
      );
      print('${nextInstanceTime(day, hour, minute)} id: $notificationId, $quote');
    } catch (exception) {
      print('$exception : ${nextInstanceTime(day+setForHowManyDays, hour, minute)} id: $notificationId, $quote');
      await flutterLocalNotificationsPlugin.zonedSchedule(
          notificationId,
          null,
          isPremiumOrSubscribed ? quote : quote.length>81 ? '${quote.substring(0, 80)}...' : quote,
          nextInstanceTime(day+setForHowManyDays, hour, minute),
          platformChannelSpecifics, //payload: 'item x',
          androidAllowWhileIdle: true,
          payload: '$quoteId',
          uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime
      );
    }
    final singleQuote = await dbProvider.querySingleQuote(quoteId);
    singleQuote.forEach((quote) {
      dbFunctions.updateUserQuoteStatus(quoteId, quote['quoteCollectedStatus'], 1, quote['quoteBookmarkedStatus']);
    });
  }


  nextInstanceTime(int day, int hour, int minute) {
    //final generalChanger = Provider.of<GeneralChanger>(context, listen: false);

    var local = tz.getLocation(_timezone);
    final tz.TZDateTime now = tz.TZDateTime.now(local);
    scheduledDate = tz.TZDateTime(local, now.year, now.month, now.day+day-1, hour, minute);
    if (scheduledDate.isBefore(now)) {
      //scheduledDate = scheduledDate.add(const Duration(days: 1));
      //print('$scheduledDate IS BEFORE NOW');
    }
    return scheduledDate;
  }


  handlePendingNotifications() async {
    var pendingNotificationRequests = await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    print('PENDING NOTIFICATION LENGTH IS: ${pendingNotificationRequests.length}');
    for(PendingNotificationRequest notification in pendingNotificationRequests){
      //print(notification.payload);
      final singleQuote = await dbProvider.querySingleQuote(int.parse(notification.payload));
      singleQuote.forEach((quote) {
        dbFunctions.updateUserQuoteStatus(quote['quoteid'], quote['quoteCollectedStatus'], 0, quote['quoteBookmarkedStatus']);
      });
    }
  }


  setNotificationTimePeriod() async {
    _queryAllSectionsAndQuotesOfUser();
    isPremiumOrSubscribed = await SharedPreferencesHelper.getIsPremiumOrSubscribedStatus();
    print("Is subscribed: $isPremiumOrSubscribed");
    _timezone = await SharedPreferencesHelper.getTimeZone();
    int notificationLimit = Platform.isAndroid ? 85 : 64;
    int notificationCount = await SharedPreferencesHelper.getNotificationCount();//generalChanger.getNotificationCount();
    int startHour = await SharedPreferencesHelper.getReminderStartHour();
    int startMinute = await SharedPreferencesHelper.getReminderStartMinute();
    int endHour = await SharedPreferencesHelper.getReminderEndHour();
    int endMinute = await SharedPreferencesHelper.getReminderEndMinute();
    int timeDifferencesAsMinuteBetweenEndHour = endHour > startHour ?
      (endHour - startHour) * 60 - startMinute + endMinute :
      (24 - startHour + endHour) * 60 - startMinute + endMinute;
    int timeIntervalOfEachNotification = timeDifferencesAsMinuteBetweenEndHour ~/ notificationCount;
    int notificationIdNumber = 0;
    int extraQuoteNumber = 0;
    int count = 0;
    setForHowManyDays = notificationLimit ~/ notificationCount;
    tz.initializeTimeZones();
    await flutterLocalNotificationsPlugin.cancelAll();

    Future.delayed(Duration(milliseconds: 2000), () async {
      for(int dayNumber = 1; dayNumber < setForHowManyDays+1; dayNumber++){
        int addedTime = timeIntervalOfEachNotification + startMinute;
        count = 0;
        while(count < notificationCount) {
          notificationIdNumber++;
          try{
            await showDailyAtTime(dayNumber, startHour, addedTime, notificationIdNumber, quoteQuery[quoteQuery.length-notificationIdNumber].quote, quoteQuery[quoteQuery.length-notificationIdNumber].userQuoteId);
          } on RangeError catch (exception) {
            extraQuoteNumber++;
            print('Range Error..... $exception');
            if(!secondNotificationSet) {
              secondNotificationSet = true;
              Future.delayed(Duration(milliseconds: 3000), () {
                setNotificationTimePeriod();
              });
            }
          } catch (error) {
            print('ERROR FROM setNotificationFunc $error');
          }
          addedTime = addedTime + timeIntervalOfEachNotification;
          count++;
        }
      }
    });
  }

  Future<void> cancelAllNotifications() async {
    //final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    //generalChanger.setIsProcessing(true);
    await flutterLocalNotificationsPlugin.cancelAll();
    print('All notification has been cancelled');
    //generalChanger.setIsProcessing(false);
  }

  void requestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  Future getRandomQuote() async {
    Random random = Random();
    int randomNumber = random.nextInt(allQuotes.length);
    return allQuotes[randomNumber];
  }

  countSelectedPeriodFromPref() async {
    List<int> timeRangeSelectedStatus = [];
    return timeRangeSelectedStatus;
  }




  _queryAllSectionsAndQuotesOfUser() async {
    final sections = await dbProvider.queryAllUnlockedAndSelectedSections();
    sections.forEach((section) async {
      final allQuotes = await dbProvider.queryAllQuotesOfAnySectionWithIsSeenData(section['sectionid']);
      allQuotes.forEach((quote) {
        var contain = quoteQuery.where((myQuote) => myQuote.userQuoteId == quote['quoteid']);
        if(contain.isEmpty){
          quoteQuery.add(QuoteQuery(
            quote: quote['quote'],
            userQuoteId: quote['quoteid'],
            authername: quote['authorname'],
            isCollected: quote['quoteCollectedStatus'],
            isBookmarked: quote['quoteBookmarkedStatus'],
            isSeen: quote['isSeenStatus'],
          ));
        } else {
          //print('It already exists ${_quoteQuery.contains(quote)}: $quote');
        }
      });
      shuffleMyArray(quoteQuery);
    });
    Future.delayed(const Duration(milliseconds: 1500), () {
      if(quoteQuery.length<110) _addExtraQuote();
    });
  }

  void shuffleMyArray(List list, [int start = 1, int end]) {
    var random = Random();
    end ??= list.length;
    var length = end - start;
    while (length > 1) {
      var pos = random.nextInt(length);
      length--;
      var tmp1 = list[start + pos];
      list[start + pos] = list[start + length];
      list[start + length] = tmp1;
    }
  }

  _addExtraQuote() async {
    final allQuotesWithSeen = await dbProvider.queryAllQuotesPreferencesOfUserWithSeen();
    for (int i = 0; i < 100; i++) {
      quoteQuery.add(QuoteQuery(
        quote: allQuotesWithSeen[i]['quote'],
        userQuoteId: allQuotesWithSeen[i]['quoteid'],
        authername: allQuotesWithSeen[i]['authorname'],
        isCollected: allQuotesWithSeen[i]['quoteCollectedStatus'],
        isBookmarked: allQuotesWithSeen[i]['quoteBookmarkedStatus'],
        isSeen: allQuotesWithSeen[i]['isSeenStatus'],
      ));
      print('Extra quote added from main: new item count: ${quoteQuery.length}');
    }
  }

}