/*import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:motilife/Utilities/constants.dart';

// You can also test with your own ad unit IDs by registering your device as a
// test device. Check the logs for your device's ID value.
const String testDevice = 'YOUR_DEVICE_ID';


class RewardedVideo extends StatefulWidget {

  @override
  _RewardedVideoState createState() => _RewardedVideoState();
}

class _RewardedVideoState extends State<RewardedVideo> {

  int _coins = 0;

  @override
  void initState() {
    super.initState();
    FirebaseAdMob.instance.initialize(appId: FirebaseAdMob.testAppId);
    RewardedVideoAd.instance.listener =
        (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
      print("RewardedVideoAd event $event");
      if (event == RewardedVideoAdEvent.rewarded) {
        setState(() {
          _coins += rewardAmount;
        });
      }
    };
  }

  @override
  void dispose() {
//    _bannerAd?.dispose();
//    _nativeAd?.dispose();
//    _interstitialAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RaisedButton(
                child: const Text('LOAD REWARDED VIDEO'),
                onPressed: () {
                  RewardedVideoAd.instance.load(
                      adUnitId: RewardedVideoAd.testAdUnitId,
                      targetingInfo: targetingInfo);
                },
              ),
              RaisedButton(
                child: const Text('SHOW REWARDED VIDEO'),
                onPressed: () {
                  RewardedVideoAd.instance.show();
                },
              ),
              Text("You have $_coins coins."),
            ].map((Widget button) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: button,
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}*/