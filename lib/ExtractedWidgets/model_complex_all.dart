import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:motilife/screens/themes_tab.dart';
import 'package:provider/provider.dart';

import '../notifier_provider.dart';

class ComplexModal extends StatelessWidget {
  ComplexModal({this.onClose, this.nextScreen});
  final Function onClose;
  final Widget nextScreen;

  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeChanger>(context);
    return Material(
      child: WillPopScope(
        onWillPop: () async {
          bool shouldClose = true;
          if(themeChanger.getIsThemeChanged()) {
            onClose();
            themeChanger.setIsThemeChanged(false);
          }
          return shouldClose;
        },
        child: Navigator(
          onGenerateRoute: (_) => MaterialPageRoute(
            builder: (context) => Builder(
              builder: (context) => CupertinoPageScaffold(
                child: nextScreen,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/*onTap: () {
  Navigator.of(context).push(MaterialPageRoute(
      builder: (context) =>
          CupertinoPageScaffold(
              navigationBar:
              CupertinoNavigationBar(
                middle: Text('New Page'),
              ),
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[],
              ))));
},*/
