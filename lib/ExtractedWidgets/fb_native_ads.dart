import 'package:facebook_audience_network/facebook_audience_network.dart';
import 'package:flutter/material.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'dart:io';

class FBNativeAds extends StatefulWidget {
  FBNativeAds({
    this.nativeAd,
    this.marginX,
    this.marginY});

  final FacebookNativeAd nativeAd;
  final double marginY;
  final double marginX;

  @override
  _FBNativeAdsState createState() => _FBNativeAdsState();
}

class _FBNativeAdsState extends State<FBNativeAds> with SingleTickerProviderStateMixin {
  final double paddingAll = 8;

  final dbFunctions = DbFunctions();
/*  AnimationController controller;
  Animation animation;*/
  bool isVisible = false;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //if(widget.replacedQuoteId != null) _changeReplacedQuoteSeenStatus();
/*    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 800))
      ..addListener(() {
        //print('GrowAnimation value: ${growAnimation.value}');
        setState(() {
          animation.value;
        });
      });
    animation = Tween<double>(begin: 0, end: 1).animate(controller);
    controller.forward();*/
    Future.delayed(Duration(milliseconds: 100),() {
      setState(() {
        isVisible = true;//To avoid unwanted appearance of native ad, esp on ios
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print("Disposed");
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: Platform.isAndroid ? true : isVisible,
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: widget.marginX, vertical: widget.marginY),
          child: widget.nativeAd,
      ),
    );
  }
}
