import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/prefs.dart';

import '../localizations.dart';

class BottomButton extends StatelessWidget {
  final String imageName;
  final String imageText;

  BottomButton({this.imageName, this.imageText});

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Container(
      height: heightScreen * 0.05,
      width: widthScreen * 0.24,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.07),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Row(
        mainAxisAlignment:
        MainAxisAlignment.center,
        children: [
          Transform.scale(
            scale: 0.75,
            child: ImageIcon(
              AssetImage('assets/signs2/$imageName.png'),
              color: Colors.white,
            ),
          ),
          Container(width: widthScreen*0.01),
          Text(
            imageText,
            style: TextStyle(
              fontFamily: kDefaultFontName,
              color: Colors.white,
              fontSize:
              MediaQuery.of(context).size.width *
                  0.03,
              fontWeight: FontWeight.w300,
            ),
          ),
        ],
      ),
    );
  }
}

class MyCupertinoPageTransition extends StatelessWidget {
  final functions = AllFunctions();
  final Widget nextScreen;
  final String imageName;
  final String imageText;
  final bool isVisibleRedAlert;
  final int buttonTag;
  final GeneralChanger generalChanger;
  final NewGeneralChanger newGeneralChanger;
  final Color backgroundColor;
  final double fontSize;

  MyCupertinoPageTransition({this.nextScreen, this.imageName, this.imageText, this.isVisibleRedAlert, this.buttonTag, this.generalChanger, this.newGeneralChanger, this.backgroundColor, this.fontSize});

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        if(isVisibleRedAlert) Positioned(
          top: -22,
          right: -30,
          child: Transform.scale(
            scale: 0.225,
              child: Image.asset('assets/signs2/alert.png')
          ),
        ),
        CupertinoPageScaffold(
          backgroundColor: Colors.transparent,
          child: GestureDetector(
              child: Card(
                elevation: 0,
                color: Colors.transparent,
                child: Container(
                  height: heightScreen * 0.1,
                  width: widthScreen * 0.24,
                  padding: EdgeInsets.symmetric(horizontal: widthScreen*.015),
                  margin: EdgeInsets.symmetric(vertical: heightScreen*.017),
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.circular(heightScreen*.0085),
                  ),
                  child: Row(
                    mainAxisAlignment:
                    MainAxisAlignment.center,
                    children: [
                      Transform.scale(
                        scale: 0.75,
                        child: ImageIcon(
                          AssetImage('assets/signs2/$imageName.png'),
                          color: Colors.white,
                        ),
                      ),
                      Container(width: widthScreen*0.01),
                      Text(
                        imageText,
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.width * fontSize,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              onTap: () {
                HapticFeedback.lightImpact();
                if(isVisibleRedAlert) {
                  generalChanger.setDidChangedIconStatus(true);
                  if(buttonTag == 0) {
                    SharedPreferencesHelper.setNewTopicsAlert(false);
                    Future.delayed(Duration(milliseconds: 100), (){
                      newGeneralChanger.setNewTopicsAlert(false);
                    });
                  } else if(buttonTag == 1) {
                    SharedPreferencesHelper.setNewThemesAlert(false);
                    Future.delayed(Duration(milliseconds: 100), (){
                      newGeneralChanger.setNewThemesAlert(false);
                    });
                  } else {
                    SharedPreferencesHelper.setNewSettingsAlert(false);
                    Future.delayed(Duration(milliseconds: 100), (){
                      newGeneralChanger.setNewSettingsAlert(false);
                    });
                  }
                }
                return Navigator.push(context, SlideLeftRoute(page: nextScreen));
              }),
        ),
      ],
    );
  }
}


class ModalInsideModal extends StatelessWidget {
  final bool reverse;

  const ModalInsideModal({Key key, this.reverse = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        child: CupertinoPageScaffold(
          navigationBar: CupertinoNavigationBar(
              leading: Container(), middle: Text('Modal Page')),
          child: SafeArea(
            bottom: false,
            child: ListView(
              reverse: reverse,
              shrinkWrap: true,
              controller: ModalScrollController.of(context),
              physics: ClampingScrollPhysics(),
              children: ListTile.divideTiles(
                  context: context,
                  tiles: List.generate(
                    100,
                        (index) => ListTile(
                        title: Text('Item $index'),
                        onTap: () => showCupertinoModalBottomSheet(
                          expand: true,
                          isDismissible: false,
                          context: context,
                          backgroundColor: Colors.transparent,
                          builder: (context) =>
                              ModalInsideModal(reverse: reverse),
                        )),
                  )).toList(),
            ),
          ),
        ));
  }
}