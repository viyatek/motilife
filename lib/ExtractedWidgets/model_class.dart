import 'dart:ui';

class WidgetModel{
  int widgetChangePeriod = 8;
  String themeName;
  Color textColor;
  WidgetModel({this.widgetChangePeriod, this.themeName, this.textColor});
}