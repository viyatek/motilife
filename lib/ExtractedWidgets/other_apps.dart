import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motilife/ExtractedWidgets/my_grad_button.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:url_launcher/url_launcher.dart';
import '../localizations.dart';
import '../notifier_provider.dart';
import 'dart:io';


class SuggestOtherApps extends StatefulWidget {
  const SuggestOtherApps({
    @required this.generalChanger,
    @required this.directedLink,
    @required this.appIconName,
    @required this.motto,
    @required this.marginX,
    this.marginY,
  });

  final GeneralChanger generalChanger;
  final String directedLink;
  final String appIconName;
  final String motto;
  final double marginY;
  final double marginX;

  @override
  _SuggestOtherAppsState createState() => _SuggestOtherAppsState();
}

class _SuggestOtherAppsState extends State<SuggestOtherApps> with SingleTickerProviderStateMixin {

  final functions = AllFunctions();
  final dbFunctions = DbFunctions();
/*  AnimationController controller;
  Animation animation;*/
  bool isVisible = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
/*    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 800))
      ..addListener(() {
        //print('GrowAnimation value: ${growAnimation.value}');
        setState(() {
          animation.value;
        });
      });
    animation = Tween<double>(begin: 0, end: 1).animate(controller);
    controller.forward();*/
    Future.delayed(Duration(milliseconds: 100),() {
      setState(() {
        isVisible = true;//To avoid unwanted appearance of native ad, esp on ios
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Visibility(
      visible: Platform.isAndroid ? true : isVisible,
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.04, vertical: widget.marginY),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(height: heightScreen*.01),
              Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: heightScreen * 0.08,
                    maxHeight: heightScreen * 0.08,
                  ),
                  child: AutoSizeText(
                    AppLocalizations.of(context).translate(widget.appIconName),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: kDefaultFontName,
                      fontSize: MediaQuery.of(context).size.width * 0.07,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: heightScreen * 0.08,
                    maxHeight: heightScreen * 0.08,
                  ),
                  child: AutoSizeText(
                    AppLocalizations.of(context).translate(widget.motto),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: kDefaultFontName,
                      fontSize: MediaQuery.of(context).size.width * 0.05,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(height: heightScreen*.01),
              Container(
                height: heightScreen * 0.16,
                width: widthScreen * 0.2,
                child: Transform.scale(
                  child: Image.asset('assets/general/${widget.appIconName}.png'),
                  scale: widthScreen*0.0036,
                ),
              ),
              Container(
                height: heightScreen * 0.01,
              ),
              Container(
                height: heightScreen * 0.1,
                margin: EdgeInsets.symmetric(horizontal: widthScreen*0.02, vertical: heightScreen*0.01),
                child: MyGradButton(
                  buttonText: AppLocalizations.of(context).translate('downloadNow'),
                  decoration: kGradButtonDecoration,
                  buttonTextColor: kWhiteText,
                  callBack: () {
                    functions.sendAnalyticsWithOwnName("download_${widget.appIconName}");
                    launch(widget.directedLink);
                  },
                  //pageId: QuotesCountDaily.id,
                ),
              ),
            ],
          )),
    );
  }
}



class SuggestOtherAppsForFeed extends StatelessWidget {
  SuggestOtherAppsForFeed({
    @required this.generalChanger,
    @required this.directedLink,
    @required this.appIconName,
    @required this.motto,
  });

  final GeneralChanger generalChanger;
  final String directedLink;
  final String appIconName;
  final String motto;

  final functions = AllFunctions();

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Center(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: heightScreen * 0.05,
              maxHeight: heightScreen * 0.07,
            ),
            child: AutoSizeText(
              AppLocalizations.of(context).translate(appIconName),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: kDefaultFontName,
                fontSize: MediaQuery.of(context).size.width * 0.065,
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ),
            ),
          ),
        ),
        Center(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: heightScreen * 0.05,
              maxHeight: heightScreen * 0.07,
            ),
            child: AutoSizeText(
              AppLocalizations.of(context).translate(motto),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: kDefaultFontName,
                fontSize: MediaQuery.of(context).size.width * 0.05,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
          ),
        ),
        Container(height: heightScreen*.005),
        Container(
          height: heightScreen * 0.1,
          width: widthScreen * 0.15,
          child: Transform.scale(
            child: Image.asset('assets/general/$appIconName.png'),
            scale: widthScreen*0.0036,
          ),
        ),
        Container(
          height: heightScreen * 0.005,
        ),
        Container(
          height: heightScreen * 0.1,
          margin: EdgeInsets.symmetric(horizontal: widthScreen*0.1, vertical: 0),
          child: MyGradButton(
            buttonText: AppLocalizations.of(context).translate('downloadNow'),
            decoration: kGradButtonDecoration,
            buttonTextColor: kWhiteText,
            callBack: () {
              functions.sendAnalyticsWithOwnName("download_$appIconName");
              launch(directedLink);
            },
            //pageId: QuotesCountDaily.id,
          ),
        ),
      ],
    );
  }
}