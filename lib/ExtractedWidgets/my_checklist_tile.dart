import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';

class MyCheckListTile extends StatelessWidget {
  final String explanation;
  final double height;
  final Color colour;
  final double marginLeft;
  final double fontSize;
  final double blankSize;
  MyCheckListTile({this.explanation, this.height, this.colour = Colors.black, this.marginLeft = 15, this.fontSize = 0.045, this.blankSize = 0.03});

  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    return Container(
      height: height,
      margin: EdgeInsets.only(left: marginLeft),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Transform.scale(
            scale: 0.9,
            child: ImageIcon(
              AssetImage('assets/signs2/check.png'),
              color: colour,
            ),
          ),
          SizedBox(
            width: widthScreen*blankSize,
          ),
          Text(
            explanation,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontFamily: kDefaultFontName,
              fontSize: MediaQuery.of(context).size.width * fontSize,
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}

//Container(
//height: height,
//child: ListTile(
//contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
//leading: ImageIcon(
//AssetImage('assets/signs/black_sign.png'),
//color: colour,
//),
//title: Transform(
//transform: Matrix4.translationValues(-16, 0.0, 0.0),
//child: Text(
//explanation,
//textAlign: TextAlign.left,
//style: TextStyle(
//fontFamily: 'Nunito',
//fontSize: MediaQuery.of(context).size.width * 0.045,
//fontWeight: FontWeight.w500,
//),
//),
//),
//),
//);
