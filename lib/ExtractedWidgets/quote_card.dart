import 'package:flutter/material.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:provider/provider.dart';

import '../DbProvider.dart';
import '../notifier_provider.dart';

final dbProvider = DatabaseProvider.instance;
final dbFunctions = DbFunctions();

class CardBookmarked extends StatefulWidget {
  CardBookmarked(
      {
        this.quote,
        this.authorName,
        this.imageScale,
        this.quoteid,
        this.isSeenStatus,
        this.quoteCollectedStatus,
        this.quoteSelectedStatus, this.refreshScreen, this.quoteBookmarkedStatus});
  final String quote;
  final String authorName;
  final double imageScale;
  final int quoteid;
  final int quoteSelectedStatus;
  final int isSeenStatus;
  final int quoteCollectedStatus;
  final int quoteBookmarkedStatus;
  final Widget refreshScreen;

  @override
  _CardBookmarkedState createState() => _CardBookmarkedState();
}

class _CardBookmarkedState extends State<CardBookmarked> {
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<DayNightChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.fromLTRB(5, 0, 5, 10),
      decoration: themeChanger.getDecorationCard(),
      child: GridTile(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: heightScreen * 0.14,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                child: Center(
                  child: Text(
                    widget.quote,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 4,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Nunito',
                      fontSize: MediaQuery.of(context).size.width * 0.037,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
            widget.authorName == null ? Container() : Container(
              height: heightScreen * 0.04,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                child: Text(
                  widget.authorName,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Nunito',
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
            Container(
                height: heightScreen * 0.05,
                width: widthScreen * 0.30,
                margin: EdgeInsets.fromLTRB(0, 3, 0, 6),
                child: FlatButton(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  onPressed: () {
                    setState(() {
                      dbFunctions.updateUserQuoteStatus(
                          widget.quoteid,
                          widget.quoteCollectedStatus,
                          widget.isSeenStatus,
                          widget.quoteBookmarkedStatus);
                      Navigator.pushReplacement(
                          context,
                          FadeRoute(page: widget.refreshScreen)); //refresh page after removing a quote
                    });
                  },
                  color: themeChanger.getAppBarColor(),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Image.asset(
                          'assets/signs/remove.png',
                          scale: 3.1,
                        ),
                      ),
                      SizedBox(
                        width: widthScreen * 0.02,
                      ),
                      Text(
                        'Remove',
                        style: TextStyle(
                          fontFamily: 'Nunito',
                          fontSize: MediaQuery.of(context).size.width * 0.04,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}