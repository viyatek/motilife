import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/popups/pop_up.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';
import '../localizations.dart';
import '../notifier_provider.dart';
import '../prefs.dart';


class RateUsWidget extends StatefulWidget {
  const RateUsWidget({
    Key key,
    @required this.generalChanger,
    @required Uri emailLaunchUri,
    @required this.dayNightChanger,
    @required this.replacedQuoteId,
    @required this.replacedIsCollected,
    @required this.replacedIsBookmarked,
    @required this.replacedIsSeen,
    this.invisibleNativeAdWidget,
  }) : _emailLaunchUri = emailLaunchUri, super(key: key);

  final GeneralChanger generalChanger;
  final Uri _emailLaunchUri;
  final DayNightChanger dayNightChanger;
  final int replacedQuoteId;
  final int replacedIsCollected;
  final int replacedIsBookmarked;
  final int replacedIsSeen;
  final Widget invisibleNativeAdWidget;

  @override
  _RateUsWidgetState createState() => _RateUsWidgetState();
}

class _RateUsWidgetState extends State<RateUsWidget> {

  final functions = AllFunctions();
  final dbFunctions = DbFunctions();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => widget.generalChanger.setIsSeenRateUs(true));
    functions.sendAnalyticsWithOwnName('Rate_us_seen');
    _changeReplacedQuoteSeenStatus();
  }

  _changeReplacedQuoteSeenStatus() {
    Future.delayed(Duration(milliseconds: 1500), (){
      dbFunctions.updateUserQuoteStatus(widget.replacedQuoteId, widget.replacedIsCollected, widget.replacedIsSeen, widget.replacedIsBookmarked);
    });
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Container(
        margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.08),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.invisibleNativeAdWidget,
            Center(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: heightScreen * 0.13,
                  maxHeight: heightScreen * 0.13,
                ),
                child: AutoSizeText(
                  AppLocalizations.of(context).translate('rateushome'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.05,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Container(height: heightScreen*.02),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () {
                    functions.sendAnalyticsWithOwnName('Terrible_Click');
                    SharedPreferencesHelper.setIsSeenRateUs(true);
                    widget.generalChanger.setIsSeenRateUs(true);
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return MyPopUp(
                            imageName: 'emoji2/bad',
                            imageScale: 2.0,
                            text: AppLocalizations.of(context)
                                .translate('terriblenotsosentence'),
                            textButton: AppLocalizations.of(context)
                                .translate('mailbutton'),
                            callback: () {
                              functions.sendAnalyticsWithOwnName('Terrible_Click_Mail');
                              launch(widget._emailLaunchUri.toString());
                              Navigator.pop(context);
                            },
                          );
                        });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.17),
                      borderRadius: BorderRadius.circular(heightScreen*0.01),
                    ),
                    height: heightScreen * 0.16,
                    width: widthScreen * 0.2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform.scale(
                          child: Image.asset('assets/emoji2/bad.png'),
                          scale: widthScreen*0.0015,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('terrible'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.04,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    functions.sendAnalyticsWithOwnName('NotSo_Click');
                    SharedPreferencesHelper.setIsSeenRateUs(true);
                    widget.generalChanger.setIsSeenRateUs(true);
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return MyPopUp(
                            imageName: 'emoji2/good',
                            imageScale: 2.0,
                            text: AppLocalizations.of(context)
                                .translate('terriblenotsosentence'),
                            textButton: AppLocalizations.of(context)
                                .translate('mailbutton'),
                            callback: () {
                              functions.sendAnalyticsWithOwnName('NotSo_Click_Mail');
                              launch(widget._emailLaunchUri.toString());
                              Navigator.pop(context);
                            },
                          );
                        });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.17),
                      borderRadius: BorderRadius.circular(heightScreen*0.01),
                    ),
                    height: heightScreen * 0.16,
                    width: widthScreen * 0.2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform.scale(
                          child: Image.asset('assets/emoji2/good.png'),
                          scale: widthScreen*0.0015,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('good'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.04,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    functions.sendAnalyticsWithOwnName('Great_Click');
                    SharedPreferencesHelper.setIsSeenRateUs(true);
                    widget.generalChanger.setIsSeenRateUs(true);
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return MyPopUp(
                            imageName: 'emoji2/great',
                            imageScale: 2.0,
                            text: AppLocalizations.of(context)
                                .translate('greatsentence'),
                            textButton: AppLocalizations.of(context)
                                .translate('rateusbutton'),
                            callback: () {
                              functions.sendAnalyticsWithOwnName('Great_Click_Store');
                              Platform.isAndroid ? launch(kUrlPlayStore) : launch(kUrlAppStore);
                              Navigator.pop(context);
                            },
                          );
                        });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.17),
                      borderRadius: BorderRadius.circular(heightScreen*0.01),
                    ),
                    height: heightScreen * 0.16,
                    width: widthScreen * 0.2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform.scale(
                          child: Image.asset('assets/emoji2/great.png'),
                          scale: widthScreen*0.0015,
                        ),
                        Text(
                          AppLocalizations.of(context).translate('great'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.04,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: heightScreen * 0.02,
            ),
          ],
        ));
  }
}