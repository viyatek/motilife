import 'package:flutter/material.dart';

class TopProgressBar extends StatelessWidget {
  final Color color1;
  final Color color2;
  final Color color3;
  final Color color4;
  final Color color5;
  TopProgressBar({this.color1, this.color2, this.color3, this.color4, this.color5});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 120),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
                margin: EdgeInsets.only(right: 5.0),
                child: Divider(
                  color: color1,
                  thickness: 3.0,
                )),
          ),
          Expanded(
            child: Container(
                margin: EdgeInsets.only(right: 5.0),
                child: Divider(
                  color: color2,
                  thickness: 3.0,
                )),
          ),
          Expanded(
            child: Container(
                margin: EdgeInsets.symmetric(horizontal: 2.5),
                child: Divider(
                  color: color3,
                  thickness: 3.0,
                )),
          ),
          Expanded(
            child: Container(
                margin: EdgeInsets.only(left: 5.0),
                child: Divider(
                  color: color4,
                  thickness: 3.0,
                )),
          ),
          Expanded(
            child: Container(
                margin: EdgeInsets.only(left: 5.0),
                child: Divider(
                  color: color5,
                  thickness: 3.0,
                )),
          ),
        ],
      ),
    );
  }
}
