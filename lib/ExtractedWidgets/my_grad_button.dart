import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/page_transitions.dart';

class MyGradButton extends StatefulWidget {
  //final String pageId;
  final String buttonText;
  final Widget nextScreen;
  final Decoration decoration;
  final Color buttonTextColor;
  final VoidCallback callBack;
  final double horizontalMargin;
  final double verticalMargin;

  MyGradButton(
      {this.buttonText,
      this.nextScreen,
      @required this.decoration,
      this.buttonTextColor,
      this.callBack,
      this.horizontalMargin = 20,
      this.verticalMargin= 10});

  @override
  _MyGradButtonState createState() => _MyGradButtonState();
}

class _MyGradButtonState extends State<MyGradButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.nextScreen == null ? print('No widget found') : widget.nextScreen.toStringShort() == 'NewHomePage' ? Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: widget.nextScreen), (Route<dynamic> route) => false) : Navigator.push(context,
            FadeRoute(page: widget.nextScreen));
        widget.callBack();
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: widget.horizontalMargin, vertical: widget.verticalMargin),
        decoration: widget.decoration,
        child: Center(
          child: Text(
            widget.buttonText,
            style: TextStyle(
              fontFamily: kDefaultFontName,
              color: widget.buttonTextColor,
              fontSize: MediaQuery.of(context).size.width * 0.05,
              //fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
