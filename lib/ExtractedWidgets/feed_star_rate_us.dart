import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/popups/pop_up.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';
import '../localizations.dart';
import '../notifier_provider.dart';
import '../prefs.dart';

class FeedStarRateUs extends StatefulWidget {
  const FeedStarRateUs({
    Key key,
    @required this.themeProperties,
    this.heightScreen,
    this.widthScreen,
    this.invisibleNativeAdWidget,
    this.generalChanger
  });

  final List<String> themeProperties;
  final double heightScreen;
  final double widthScreen;
  final Widget invisibleNativeAdWidget;
  final GeneralChanger generalChanger;

  @override
  _FeedStarRateUsState createState() => _FeedStarRateUsState();
}

class _FeedStarRateUsState extends State<FeedStarRateUs> {
  final _functions = AllFunctions();
  Uri _emailLaunchUri = kEmailLaunchUri;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => widget.generalChanger.setIsSeenRateUs(true));
    _functions.sendAnalyticsWithOwnName('Rate_us_seen');
  }

  @override
  Widget build(BuildContext context) {
    final fontChanger = Provider.of<FontChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: widthScreen*0.03, vertical: heightScreen*0.01),
      padding: EdgeInsets.symmetric(horizontal: widthScreen*0.06, vertical: heightScreen*0.01),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.07),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            AppLocalizations.of(context).translate('rateYourExperience'),
            style: TextStyle(
              fontFamily: 'Futura Book',
              fontSize: MediaQuery.of(context).size.width * 0.065,
              fontWeight: FontWeight.w500,
              color: Color.fromARGB(int.parse(widget.themeProperties[0]), int.parse(widget.themeProperties[1]), int.parse(widget.themeProperties[2]), int.parse(widget.themeProperties[3])),
              wordSpacing: double.parse(fontChanger.getFontValues()[5]),
              letterSpacing: double.parse(fontChanger.getFontValues()[6]),
              height: double.parse(fontChanger.getFontValues()[7]),
              shadows: [
                Shadow(
                  offset: Offset(double.parse(widget.themeProperties[8]), double.parse(widget.themeProperties[9])),
                  blurRadius: double.parse(widget.themeProperties[10]),
                  color: Color.fromARGB(int.parse(widget.themeProperties[4]), int.parse(widget.themeProperties[5]), int.parse(widget.themeProperties[6]), int.parse(widget.themeProperties[7])),
                ),
              ],
            ),
          ),
          SizedBox(
            height: widget.heightScreen*0.05,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 0 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _lowLevelStarClicked(1);
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 1 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _lowLevelStarClicked(2);
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 2 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _lowLevelStarClicked(3);
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 3 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _fourStarClicked();
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 4 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _fiveStarClicked();
                },
              ),
            ],
          )
        ],
      ),
    );
  }
  _lowLevelStarClicked(int star) {
    setState(() {
      widget.generalChanger.setStarRate(star);
    });
    _functions.sendAnalyticsWithOwnName('$star-Star');
    SharedPreferencesHelper.setIsSeenRateUs(true);
    SharedPreferencesHelper.setStarRate(star);
    widget.generalChanger.setIsSeenRateUs(true);
    Future.delayed(Duration(milliseconds: 100), (){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyPopUp(
              imageName: 'emoji2/bad',
              imageScale: 2.0,
              text: AppLocalizations.of(context)
                  .translate('terriblenotsosentence'),
              textButton: AppLocalizations.of(context)
                  .translate('mailbutton'),
              callback: () {
                _functions.sendAnalyticsWithOwnName('$star-Click_Mail');
                launch(_emailLaunchUri.toString());
                Navigator.pop(context);
              },
            );
          });
    });
  }
  _fourStarClicked() {
    setState(() {
      widget.generalChanger.setStarRate(4);
    });
    _functions.sendAnalyticsWithOwnName('4-Star');
    SharedPreferencesHelper.setIsSeenRateUs(true);
    SharedPreferencesHelper.setStarRate(4);
    widget.generalChanger.setIsSeenRateUs(true);
    Future.delayed(Duration(milliseconds: 100), (){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyPopUp(
              imageName: 'emoji2/good',
              imageScale: 2.0,
              text: AppLocalizations.of(context)
                  .translate('howCanWeGetBetter'),
              textButton: AppLocalizations.of(context)
                  .translate('mailbutton'),
              callback: () {
                _functions.sendAnalyticsWithOwnName('4star_Click_Mail');
                launch(_emailLaunchUri.toString());
                Navigator.pop(context);
              },
            );
          });
    });
  }
  _fiveStarClicked() {
    setState(() {
      widget.generalChanger.setStarRate(5);
    });
    _functions.sendAnalyticsWithOwnName('5-Star');
    SharedPreferencesHelper.setIsSeenRateUs(true);
    SharedPreferencesHelper.setStarRate(5);
    widget.generalChanger.setIsSeenRateUs(true);
    Future.delayed(Duration(milliseconds: 100), (){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyPopUp(
              imageName: 'emoji2/great',
              imageScale: 2.0,
              text: AppLocalizations.of(context)
                  .translate('greatsentence'),
              textButton: AppLocalizations.of(context)
                  .translate('rateusbutton'),
              callback: () {
                _functions.sendAnalyticsWithOwnName('5star_Click_Store');
                Platform.isAndroid ? launch(kUrlPlayStore) : launch(kUrlAppStore);
                Navigator.pop(context);
              },
            );
          });
    });
  }
}