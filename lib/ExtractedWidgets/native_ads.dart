/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:motilife/Utilities/constants.dart';
import 'dart:io';

import 'package:motilife/Utilities/db_functions.dart';



class MyNativeAds extends StatefulWidget {
  MyNativeAds({
    this.nativeAd,
    this.replacedQuoteId,
    this.replacedIsCollected,
    this.replacedIsBookmarked,
    this.replacedIsSeen,
    this.marginX,
    this.marginY,
  });

  final NativeAd nativeAd;
  final int replacedQuoteId;
  final int replacedIsCollected;
  final int replacedIsBookmarked;
  final int replacedIsSeen;
  final double marginY;
  final double marginX;

  @override
  _MyNativeAdsState createState() => _MyNativeAdsState();
}

class _MyNativeAdsState extends State<MyNativeAds> with SingleTickerProviderStateMixin {
  final double paddingAll = 8;

  final dbFunctions = DbFunctions();
  AnimationController controller;
  Animation animation;
  bool isVisible = false;

  _changeReplacedQuoteSeenStatus() {
    Future.delayed(Duration(milliseconds: 1500), (){
      dbFunctions.updateUserQuoteStatus(widget.replacedQuoteId, widget.replacedIsCollected, widget.replacedIsSeen, widget.replacedIsBookmarked);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.replacedQuoteId != null) _changeReplacedQuoteSeenStatus();
    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 800))
      ..addListener(() {
        //print('GrowAnimation value: ${growAnimation.value}');
        setState(() {
          animation.value;
        });
      });
    animation = Tween<double>(begin: 0, end: 1).animate(controller);
    controller.forward();
    Future.delayed(Duration(milliseconds: 200),() {
      isVisible = true;//To avoid unwanted appearance of native ad, esp on ios
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print("Disposed");
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Visibility(
      visible: isVisible,
      child: Opacity(
        opacity: animation.value,
        child: Container(
*/
/*          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.12),
            borderRadius: BorderRadius.circular(8.0),
          ),
          padding: EdgeInsets.all(paddingAll),*//*

          margin: EdgeInsets.symmetric(horizontal: widget.marginX, vertical: widget.marginY),
          child: AdWidget(ad: widget.nativeAd)
        ),
      ),
    );
  }
}
*/
