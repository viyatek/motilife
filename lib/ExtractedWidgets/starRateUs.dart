import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/popups/pop_up.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';
import '../localizations.dart';
import '../notifier_provider.dart';
import '../prefs.dart';

class StarRateUs extends StatefulWidget {
  const StarRateUs({
    Key key,
    @required this.themeProperties,
    this.heightScreen,
    this.widthScreen,
    @required this.replacedQuoteId,
    @required this.replacedIsCollected,
    @required this.replacedIsBookmarked,
    @required this.replacedIsSeen,
    this.invisibleNativeAdWidget,
    this.generalChanger
  });

  final List<String> themeProperties;
  final double heightScreen;
  final double widthScreen;
  final int replacedQuoteId;
  final int replacedIsCollected;
  final int replacedIsBookmarked;
  final int replacedIsSeen;
  final Widget invisibleNativeAdWidget;
  final GeneralChanger generalChanger;

  @override
  _StarRateUsState createState() => _StarRateUsState();
}

class _StarRateUsState extends State<StarRateUs> {
  final _functions = AllFunctions();
  final _dbFunctions = DbFunctions();
  //int _clickedStar = 0;
  Uri _emailLaunchUri = kEmailLaunchUri;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => widget.generalChanger.setIsSeenRateUs(true));
    _functions.sendAnalyticsWithOwnName('Rate_us_seen');
    _changeReplacedQuoteSeenStatus();
  }

  _changeReplacedQuoteSeenStatus() {
    Future.delayed(Duration(milliseconds: 1500), (){
      _dbFunctions.updateUserQuoteStatus(widget.replacedQuoteId, widget.replacedIsCollected, widget.replacedIsSeen, widget.replacedIsBookmarked);
    });
  }

  @override
  Widget build(BuildContext context) {
    final fontChanger = Provider.of<FontChanger>(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            AppLocalizations.of(context).translate('rateYourExperience'),
            style: TextStyle(
              fontFamily: 'Futura Book',
              fontSize: MediaQuery.of(context).size.width * 0.065,
              fontWeight: FontWeight.w500,
              color: Color.fromARGB(int.parse(widget.themeProperties[0]), int.parse(widget.themeProperties[1]), int.parse(widget.themeProperties[2]), int.parse(widget.themeProperties[3])),
              wordSpacing: double.parse(fontChanger.getFontValues()[5]),
              letterSpacing: double.parse(fontChanger.getFontValues()[6]),
              height: double.parse(fontChanger.getFontValues()[7]),
              shadows: [
                Shadow(
                  offset: Offset(double.parse(widget.themeProperties[8]), double.parse(widget.themeProperties[9])),
                  blurRadius: double.parse(widget.themeProperties[10]),
                  color: Color.fromARGB(int.parse(widget.themeProperties[4]), int.parse(widget.themeProperties[5]), int.parse(widget.themeProperties[6]), int.parse(widget.themeProperties[7])),
                ),
              ],
            ),
          ),
          SizedBox(
            height: widget.heightScreen*0.04,
            child: Container(
              child: Opacity(
                opacity: 0.0,
                child: widget.invisibleNativeAdWidget != null ? widget.invisibleNativeAdWidget : SizedBox(),
              ),
            ),
          ),
          Text(
            AppLocalizations.of(context).translate('rateushome'),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Futura Book',
              fontSize: MediaQuery.of(context).size.width * 0.053,
              fontWeight: FontWeight.w500,
              color: Color.fromARGB(int.parse(widget.themeProperties[0]), int.parse(widget.themeProperties[1]), int.parse(widget.themeProperties[2]), int.parse(widget.themeProperties[3])),
              wordSpacing: double.parse(fontChanger.getFontValues()[5]),
              letterSpacing: double.parse(fontChanger.getFontValues()[6]),
              height: double.parse(fontChanger.getFontValues()[7]),
              shadows: [
                Shadow(
                  offset: Offset(double.parse(widget.themeProperties[8]), double.parse(widget.themeProperties[9])),
                  blurRadius: double.parse(widget.themeProperties[10]),
                  color: Color.fromARGB(int.parse(widget.themeProperties[4]), int.parse(widget.themeProperties[5]), int.parse(widget.themeProperties[6]), int.parse(widget.themeProperties[7])),
                ),
              ],
            ),
          ),
          SizedBox(
            height: widget.heightScreen*0.05,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 0 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _lowLevelStarClicked(1);
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 1 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _lowLevelStarClicked(2);
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 2 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _lowLevelStarClicked(3);
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 3 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _fourStarClicked();
                },
              ),
              IconButton(
                icon: ImageIcon(
                  AssetImage(widget.generalChanger.getStarRate() > 4 ? 'assets/signs2/starFull.png' : 'assets/signs2/star.png'),
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _fiveStarClicked();
                },
              ),
            ],
          )
        ],
      ),
    );
  }
  _lowLevelStarClicked(int star) {
    setState(() {
      widget.generalChanger.setStarRate(star);
    });
    _functions.sendAnalyticsWithOwnName('low_star_click');
    SharedPreferencesHelper.setIsSeenRateUs(true);
    SharedPreferencesHelper.setStarRate(star);
    widget.generalChanger.setIsSeenRateUs(true);
    Future.delayed(Duration(milliseconds: 100), (){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyPopUp(
              imageName: 'emoji2/bad',
              imageScale: 2.0,
              text: AppLocalizations.of(context)
                  .translate('terriblenotsosentence'),
              textButton: AppLocalizations.of(context)
                  .translate('mailbutton'),
              callback: () {
                _functions.sendAnalyticsWithOwnName('${star.toString()}_star_click_mail');
                launch(_emailLaunchUri.toString());
                Navigator.pop(context);
              },
            );
          });
    });
  }
  _fourStarClicked() {
    setState(() {
      widget.generalChanger.setStarRate(4);
    });
    _functions.sendAnalyticsWithOwnName('four_star_clicked');
    SharedPreferencesHelper.setIsSeenRateUs(true);
    SharedPreferencesHelper.setStarRate(4);
    widget.generalChanger.setIsSeenRateUs(true);
    Future.delayed(Duration(milliseconds: 100), (){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyPopUp(
              imageName: 'emoji2/good',
              imageScale: 2.0,
              text: AppLocalizations.of(context)
                  .translate('howCanWeGetBetter'),
              textButton: AppLocalizations.of(context)
                  .translate('mailbutton'),
              callback: () {
                _functions.sendAnalyticsWithOwnName('four_star_click_mail');
                launch(_emailLaunchUri.toString());
                Navigator.pop(context);
              },
            );
          });
    });
  }
  _fiveStarClicked() {
    setState(() {
      widget.generalChanger.setStarRate(5);
    });
    _functions.sendAnalyticsWithOwnName('five_star_clicked');
    SharedPreferencesHelper.setIsSeenRateUs(true);
    SharedPreferencesHelper.setStarRate(5);
    widget.generalChanger.setIsSeenRateUs(true);
    Future.delayed(Duration(milliseconds: 100), (){
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyPopUp(
              imageName: 'emoji2/great',
              imageScale: 2.0,
              text: AppLocalizations.of(context)
                  .translate('greatsentence'),
              textButton: AppLocalizations.of(context)
                  .translate('rateusbutton'),
              callback: () {
                _functions.sendAnalyticsWithOwnName('five_star_click_store');
                Platform.isAndroid ? launch(kUrlPlayStore) : launch(kUrlAppStore);
                Navigator.pop(context);
              },
            );
          });
    });
  }
}