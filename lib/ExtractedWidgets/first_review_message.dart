import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mailto/mailto.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/popups/pop_up.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';
import '../localizations.dart';
import '../notifier_provider.dart';
import '../prefs.dart';
import 'my_grad_button.dart';

final functions = AllFunctions();

class FirstReviewDialog extends StatefulWidget {
  FirstReviewDialog({
    @required this.generalChanger,
    @required this.dayNightChanger,
    this.marginX,
  });

  final GeneralChanger generalChanger;
  final DayNightChanger dayNightChanger;
  final double marginX;

  @override
  _FirstReviewDialogState createState() => _FirstReviewDialogState();
}

class _FirstReviewDialogState extends State<FirstReviewDialog> {
  String message;
  /*Uri firstReviewLaunchUri = Uri(
      scheme: 'mailto',
      path: 'hello@viyatek.io',
      queryParameters: {'subject': 'Motilife Experience'});*/
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    functions.sendAnalyticsWithOwnName('First_Experience_Dialog_seen');
    _setIsExperienced();
  }

  _setIsExperienced() async {
    await SharedPreferencesHelper.setIsUserExperienceSeen(true);
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final dayNightChanger = Provider.of<DayNightChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode()); //to dismiss keyboard
      },
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: widget.marginX != null ? widget.marginX : widthScreen * 0.08),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
                elevation: 4,
                color: Colors.black,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        height: heightScreen * 0.035,
                      ),
                      Container(
                        //decoration: dayNightChanger.getDecorationCard(),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          /*border: Border.all(
                              color: Color(0xFFDEDEDE),
                              width: 10
                          ),*/
                          color: Color(0xFF1f1f1f),
                        ),
                        height: heightScreen * 0.2,
                        //alignment: Alignment.center,
                        margin: EdgeInsets.symmetric(horizontal: widthScreen*0.07),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 1.0, horizontal: 8.0),
                          child: TextField(
                            maxLines: null,
                            style: TextStyle(
                              color: Colors.white
                            ),
                            decoration: InputDecoration(
                              hintText: AppLocalizations.of(context).translate('your_experience'),
                              //hintMaxLines: 3,
                              fillColor: Color(0xFF1f1f1f),
                              border: InputBorder.none,
                              filled: true,
                              hintStyle: TextStyle(
                                color: Color(0xFF98969d),
                              ),
                              contentPadding: EdgeInsets.all(5),
                            ),
                            onChanged: (value) {
                              message = value;
                            },
                          ),
                        ),
                      ),
                      Container(
                        height: heightScreen * 0.02,
                      ),
                      Center(
                        child: AutoSizeText(
                          AppLocalizations.of(context).translate('userExperienceSentence'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.045,
                            fontWeight: FontWeight.w600,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        height: heightScreen * 0.015,
                      ),
                      Center(
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 25.0),
                          height: heightScreen * 0.09,
                          child: MyGradButton(
                            horizontalMargin: 5,
                            verticalMargin: 10,
                            decoration: kGradButtonDecoration,
                            buttonTextColor: Colors.white,
                            buttonText: AppLocalizations.of(context).translate('send'),
                            callBack: (){
                              launch(Mailto(
                                to: ['hello@viyatek.io'],
                                subject: AppLocalizations.of(context).translate('ask_for_experience'),
                                body: '$message').toString());
                              FocusScope.of(context).requestFocus(FocusNode());
                            },
                          ),
                        ),
                      ),
                      Container(
                        height: heightScreen * 0.02,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}