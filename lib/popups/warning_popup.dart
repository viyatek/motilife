import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';

class WarningPopUp extends StatelessWidget {
  final String text;
  final String textButton;
  final Widget nextScreen;
  WarningPopUp({this.text, this.textButton, this.nextScreen});

  @override
  Widget build(BuildContext context) {
//    final dayNightChanger = Provider.of<DayNightChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      backgroundColor: Color(0xFF1f1f1f),
      child: Container(
        height: heightScreen * 0.44,
        width: widthScreen * 0.78,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: heightScreen * 0.065,
              child: Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.close, color: Colors.white,),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
            Container(
            height: heightScreen * 0.01,
            ),
            Container(
              height: heightScreen * 0.11,
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.05),
              child: Image.asset('assets/signs2/icon_onboarding.png'),
            ),
            Center(
              child: Container(
                height: heightScreen*0.2,
                margin: EdgeInsets.symmetric(horizontal: widthScreen*0.05),
                alignment: Alignment.center,
                child: AutoSizeText(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

