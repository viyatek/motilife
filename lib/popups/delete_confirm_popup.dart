import 'package:flutter/material.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/Utilities/constants.dart';

class DeleteConfirm extends StatelessWidget {
  final String text;
  final String textButtonDelete;
  final String textButtonCancel;
  final String imageName;
  final Function deleteFunc;
  final Function cancelFunc;
  DeleteConfirm({this.text, this.textButtonDelete, this.textButtonCancel, this.imageName, this.deleteFunc, this.cancelFunc});

  @override
  Widget build(BuildContext context) {
//    final dayNightChanger = Provider.of<DayNightChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        height: heightScreen * 0.50,
        width: widthScreen * 0.78,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: heightScreen * 0.065,
              child: Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.17,
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Image.asset('assets/collections/$imageName.png'),
            ),
            Container(
              height: heightScreen * 0.02,
            ),
            Center(
              child: Container(
                height: heightScreen * 0.1,
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Nunito',
                    fontSize: MediaQuery.of(context).size.width * 0.050,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.015,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: deleteFunc,
                  child: Center(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 7.0),
                      height: heightScreen * 0.07,
                      width: widthScreen*0.3,
                      decoration: kGradButtonDecoration,
                      child: Center(
                        child: Text(
                          textButtonDelete,
                          style: TextStyle(
                            fontFamily: 'Nunito',
                            fontSize: MediaQuery.of(context).size.width * 0.05,
                            fontWeight: FontWeight.w600,
                            color: kWhiteText,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                //Container(width: widthScreen*0.01,),
                GestureDetector(
                  onTap: cancelFunc,
                  child: Center(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 7.0),
                      height: heightScreen * 0.07,
                      width: widthScreen*0.3,
                      decoration: kGradButtonDecoration,
                      child: Center(
                        child: Text(
                          textButtonCancel,
                          style: TextStyle(
                            fontFamily: 'Nunito',
                            fontSize: MediaQuery.of(context).size.width * 0.05,
                            fontWeight: FontWeight.w600,
                            color: kWhiteText,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: heightScreen * 0.03,
            ),
          ],
        ),
      ),
    );
  }
}

