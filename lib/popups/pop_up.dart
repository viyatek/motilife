import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';

class MyPopUp extends StatelessWidget {
  final String text;
  final double textSize;
  final String textButton;
  final String imageName;
  final Widget nextScreen;
  final Function callback;
  final double imageScale;
  final double minHeight;
  final double maxHeight;
  final double popUpHeight;
  final double textHorizontalMargin;
  final double textVerticalMargin;
  final String imageExtension;
  final bool closeButtonVisible;
  final double imageHeight;
  final double blankHeight1;
  final double blankHeight2;
  final double blankHeight3;
  MyPopUp({this.text, this.textSize=0.045, this.textButton, this.imageName, this.nextScreen, this.callback, this.imageScale=3, this.minHeight = 0.1, this.maxHeight=0.1, this.popUpHeight=0.47, this.textHorizontalMargin=5, this.textVerticalMargin=0, this.imageExtension='png', this.closeButtonVisible=true, this.imageHeight=0.17, this.blankHeight1=0.065, this.blankHeight2=0.02, this.blankHeight3=0.015});

  @override
  Widget build(BuildContext context) {
//    final dayNightChanger = Provider.of<DayNightChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0)),
      backgroundColor: Color(0xFF0A0A0A),
      child: Container(
        height: heightScreen * popUpHeight,
        width: widthScreen * 0.8,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            closeButtonVisible ? Container(
              height: heightScreen * 0.065,
              child: Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.close, color: Color(0xFFD0D0D0),),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ) : Container(
        height: heightScreen * blankHeight1,
      ),
            Container(
              height: heightScreen * imageHeight,
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.05),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.asset(
                  'assets/$imageName.$imageExtension',
                  scale: imageScale,
                ),
              ),
            ),
            Container(
              height: heightScreen * blankHeight2,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: textHorizontalMargin, vertical: textVerticalMargin),
              child: Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
//                                  minWidth: 300.0,
//                                  maxWidth: 300.0,
                    minHeight: heightScreen * minHeight,
                    maxHeight: heightScreen * maxHeight,
                  ),
                  child: AutoSizeText(
                    text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: kDefaultFontName,
                      fontSize: MediaQuery.of(context).size.width * textSize,
                      fontWeight: FontWeight.w600,
                      color: Colors.white
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: heightScreen * blankHeight3,
            ),
            GestureDetector(
              onTap: () {
                nextScreen != null ? Navigator.push(context, MaterialPageRoute(builder: (context) => nextScreen)) : callback();
              },
              child: Center(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 30.0),
                  height: heightScreen * 0.07,
                  decoration: kGradButtonDecoration,
                  child: Center(
                    child: Text(
                      textButton,
                      style: TextStyle(
                        fontFamily: 'Nunito',
                        fontSize: MediaQuery.of(context).size.width * 0.045,
                        fontWeight: FontWeight.w600,
                        color: kWhiteText,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.03,
            ),
          ],
        ),
      ),
    );
  }
}

