import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';
//import 'package:provider/provider.dart';
//import '../notifier_provider.dart';

class RewardedPopUp extends StatelessWidget {
  final String text;
  final String firstOptionButtonText;
  final String secondOptionButtonText;
  final String imageName;
  final Function firstOptionScreen;
  final Function secondOptionFunction;
  final Widget secondOptionScreen;
  final double fontSize;
  final double popupHeight;
  final double textHeight;
  final String imageExtension;
  RewardedPopUp({this.text, this.firstOptionButtonText, this.secondOptionButtonText, this.imageName, this.firstOptionScreen, this.secondOptionFunction, this.secondOptionScreen, this.fontSize = 0.050, this.popupHeight=0.55, this.textHeight=0.08, this.imageExtension = '.png'});

  @override
  Widget build(BuildContext context) {
//    final dayNightChanger = Provider.of<DayNightChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0)),
      backgroundColor: Colors.black,
      child: Container(
        height: heightScreen * popupHeight,
        width: widthScreen * 0.78,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: heightScreen * 0.065,
              child: Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: Icon(Icons.close, color: Colors.white,),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.20,
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: widthScreen*0.05),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(heightScreen*0.01),
                  child: Image.asset('assets/$imageName$imageExtension')
              ),
            ),
            Container(
              height: heightScreen * textHeight,
              child: Center(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * fontSize,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: firstOptionScreen,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 30.0),
                height: heightScreen * 0.07,
                decoration: kGradButtonDecoration,
                child: Center(
                  child: Text(
                    firstOptionButtonText,
                    style: TextStyle(
                      fontFamily: kDefaultFontName,
                      fontSize: MediaQuery.of(context).size.width * 0.047,
                      fontWeight: FontWeight.w400,
                      color: kWhiteText,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.03,
            ),
            GestureDetector(
              onTap: () {
                secondOptionScreen != null ? Navigator.push(context, MaterialPageRoute(builder: (context) => secondOptionScreen)) : secondOptionFunction();
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 30.0),
                height: heightScreen * 0.07,
                decoration: kGradButtonDecoration,
                child: Center(
                  child: Text(
                    secondOptionButtonText,
                    style: TextStyle(
                      fontFamily: kDefaultFontName,
                      fontSize: MediaQuery.of(context).size.width * 0.047,
                      fontWeight: FontWeight.w400,
                      color: kWhiteText,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.03,
            ),
          ],
        ),
      ),
    );
  }
}
