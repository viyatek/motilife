import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:math';
import 'dart:ui';
import 'package:after_layout/after_layout.dart';
import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:facebook_audience_network/facebook_audience_network.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_ios_app_tracking_permission/flutter_ios_app_tracking_permission.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
//import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:motilife/Utilities/fetch_cache.dart';
import 'package:motilife/onboardingScreens/welcome_screen.dart';
import 'package:motilife/prefs.dart';
import 'package:motilife/localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:motilife/screens/feed_quotes_page.dart';
import 'package:motilife/screens/quotes_bubble.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:rxdart/rxdart.dart';
import 'Utilities/functions.dart';
import 'Utilities/notification_functions.dart';
import 'package:connectivity/connectivity.dart';


//final dbFunctions = DbFunctions();
//final notificationFunctions = NotificationHandler();
final dbProvider = DatabaseProvider.instance;
StreamController<double> streamController = StreamController<double>.broadcast();
StreamSubscription<double> streamSubscription;


//StreamController<double> streamController = StreamController();
StreamController<double> controller = StreamController();


/// Streams are created so that app can respond to notification-related events
/// since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject =
BehaviorSubject<String>();

const MethodChannel platform =
MethodChannel('dexterx.dev/flutter_local_notifications_example');

const _widgetChannel = const MethodChannel('Widget/Dart');

class ReceivedNotification {
  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });

  final int id;
  final String title;
  final String body;
  final String payload;
}

String selectedNotificationPayload;



var isolate;
void isolateSendPort(SendPort sendPort) {
  int counter = 0;
  sendPort.send("Root Isolate:$counter");
  //isolate.kill(priority: Isolate.immediate);
}



/// Define a top-level named handler which background/terminated messages will
/// call.
///
/// To verify things are working, check out the native platform logs.
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}

/// Create a [AndroidNotificationChannel] for heads up notifications
const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);


Future<void> main() async {
  // Set `enableInDevMode` to true to see reports while in debug mode
  // This is only to be used for confirming that reports are being
  // submitted as expected. It is not intended to be used for everyday
  // development.


  WidgetsFlutterBinding.ensureInitialized();
  //MobileAds.instance.initialize();
  //Workmanager().initialize(callbackDispatcher, isInDebugMode: kDebugMode);
  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  final NotificationAppLaunchDetails notificationAppLaunchDetails =
  await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  //String initialRoute = MyApp.routeName;
  if (notificationAppLaunchDetails?.didNotificationLaunchApp ?? false) {
    selectedNotificationPayload = notificationAppLaunchDetails.payload;
    //initialRoute = SecondPage.routeName;
  }

  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('app_icon');

  /// Note: permissions aren't requested here just to demonstrate that can be
  /// done later
  final IOSInitializationSettings initializationSettingsIOS =
  IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {
        didReceiveLocalNotificationSubject.add(ReceivedNotification(
            id: id, title: title, body: body, payload: payload));
      });
  const MacOSInitializationSettings initializationSettingsMacOS =
  MacOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false);
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
        if (payload != null) {
          debugPrint('notification payload: $payload');
        }
        selectedNotificationPayload = payload;
        selectNotificationSubject.add(payload);
      });

  await Firebase.initializeApp();
  // Force enable crashlytics collection enabled if we're testing it.
  await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  // Pass all uncaught errors to Crashlytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  //InAppPurchaseConnection.enablePendingPurchases();
  Platform.isAndroid ? _versionControlAndroid() : _versionControlIos();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  // DotEnv dotenv = DotEnv() is automatically called during import.
  await dotenv.load(fileName: ".env");
  runZonedGuarded(() {
    runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider<DayNightChanger>(
          create: (_) => DayNightChanger(
            ThemeData.light(),
            Brightness.light,
            Colors.white,
            Colors.black,
            kGradDecorationGrey,
            kGradButtonDecorationGrey,
            Color(0xFFF2F2F2),
            Colors.black,
            false,
          ),
        ),
        ChangeNotifierProvider<ThemeChanger>(
          create: (_) => ThemeChanger(
            '',
            TextStyle(
              fontFamily: kDefaultFontName,
            ),
            TextStyle(
              fontFamily: kDefaultFontName,
            ),
            Color.fromARGB(255, 0, 0, 0),
            [],
            false,
          ),
        ),
        ChangeNotifierProvider<GeneralChanger>(
          create: (_) => GeneralChanger(true, true, 10, false, 1, 1, 1, false, true, 'America/Detroit', false, '29.99 USD', false, [], true, false, false, 0, false),
        ),
        ChangeNotifierProvider<MusicChanger>(
          create: (_) => MusicChanger(AudioPlayer(), ''),
        ),
        ChangeNotifierProvider<RemoteNotifyChanger>(
          create: (_) => RemoteNotifyChanger(0, [2, 5, 8, 12, 18, 24], [2, 5, 8, 12, 18, 24], [2, 5, 8, 12, 18, 24], 0, [2, 6, 10, 14], 'i1', 1, 0, [2, 3, 5, 7, 9, 11, 14, 17], 1, false, 5, kFB_my_banner_placement_id_android, kFB_Native_Placement_id_android, kFB_interstitial_placement_id_android, kFB_native_banner_placement_id_android, true, true, [2,6,10, 14], true),
        ),
        ChangeNotifierProvider<EventChanger>(
          create: (_) => EventChanger(false, false),
        ),
        ChangeNotifierProvider<FontChanger>(
          create: (_) => FontChanger(['1','Algerian Regular','0.09','5','0','4.0','2.0','1.35']),
        ),
        ChangeNotifierProvider<NewGeneralChanger>(
          create: (_) => NewGeneralChanger(false, false, false, false, false, true, false),
        ),
      ],
      child:  MyApp(notificationAppLaunchDetails: notificationAppLaunchDetails)
    ));
  }, (error, stackTrace) {
    print('runZonedGuarded: Caught error in my root zone.');
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}


class MyApp extends StatefulWidget {

  MyApp({this.notificationAppLaunchDetails});

  final NotificationAppLaunchDetails notificationAppLaunchDetails;

  bool get didNotificationLaunchApp =>
      notificationAppLaunchDetails?.didNotificationLaunchApp ?? false;

  static const String routeName = '/';
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);
  List<QuoteQuery> _quoteQuery = [];
  int _itemCount=0;
  final receiver = ReceivePort();
  int isSeen;
  String widgetQuoteId;
  AppsflyerSdk _appsflyerSdk;
  Map _deepLinkData;
  Map _gcd;
  final functions = AllFunctions();
  final notificationFunctions = NotificationHandler();

  //For Facebook SDK
  String _deepLinkUrl = 'Unknown';
  //FlutterFacebookSdk facebookDeepLinks;
  bool isAdvertisingTrackingEnabled = false;

  _getNotificationCount() async{
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    int notificationCount = await SharedPreferencesHelper.getNotificationCount();
    generalChanger.setNotificationCount(notificationCount);
    print('Notification Count Set: ${generalChanger.getNotificationCount()}');
  }


  getIntroStatus() async {
    isSeen = await SharedPreferencesHelper.getIntroSeenStatus();
    setState(() {
      isSeen;
    });
  }

  _initAppsFlyerSdk() {
    //ApsFlyerSdk
/*    final AppsFlyerOptions options = AppsFlyerOptions(
        afDevKey: env[kDEV_KEY_ApsFlyer],
        appId: env[Platform.isAndroid ? kAppId_Android : kAppId_iOS],
        showDebug: true);
    _appsflyerSdk = AppsflyerSdk(options);*/
    Map appsFlyerOptions = { "afDevKey": kDEV_KEY_ApsFlyer,
      "afAppId": Platform.isAndroid ? kAppId_Android : kAppId_iOS,
      "isDebug": true};

    _appsflyerSdk = AppsflyerSdk(appsFlyerOptions);

    _appsflyerSdk.onAppOpenAttribution((res) {
      print("App open res: " + res.toString());
      setState(() {
        _deepLinkData = res;
      });
    });
    _appsflyerSdk.onInstallConversionData((res) {
      print("Install Conversion: " + res.toString());
      setState(() {
        _gcd = res;
      });
    });
    _appsflyerSdk.onDeepLinking((res){
      print("Deep Linking: " + res.toString());
      setState(() {
        _deepLinkData = res;
      });
    });
    _appsflyerSdk.initSdk(
        registerConversionDataCallback: true,
        registerOnAppOpenAttributionCallback: true,
        registerOnDeepLinkingCallback: true);
  }

/*  initMopup() {
    try {
      MoPub.init(Platform.isAndroid ? kMopup_Rewarded_ad_unit_id_android : kMopup_Rewarded_ad_unit_id_ios, testMode: false).then((_) {//b195f8dd8ded45fe847ad89ed1d016da
        functions.loadMyMopupRewardedAd(context);
      });
    } on PlatformException {
      print("Error while initializing mopup");
    }
  }*/

  // Platform messages are asynchronous, so we initialize in an async method.
/*  Future<void> initFacebookPlatformState() async {
    String deepLinkUrl;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      facebookDeepLinks = FlutterFacebookSdk();
      facebookDeepLinks.onDeepLinkReceived.listen((event) {
        setState(() {
          _deepLinkUrl = event;
        });
      });
      deepLinkUrl = await facebookDeepLinks.getDeepLinkUrl;
      setState(() {
        _deepLinkUrl = deepLinkUrl;
      });
    } on PlatformException {}
    if (!mounted) return;
  }

  Future<void> logActivateApp() async {
    await facebookDeepLinks.logActivateApp();
    facebookDeepLinks.setAdvertiserTracking(isEnabled: true);
  }*/

  @override
  void initState() {
    // TODO: implement initState
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    _getNotificationCount();
    _initAppsFlyerSdk();
    FacebookAudienceNetwork.init(
      testingId: "b9f2908b-1a6b-4a5b-b862-ded7ce289e41",
    );
    //initMopup();
    getIntroStatus().whenComplete(() {
      Future.delayed(Duration(milliseconds: 1000), (){
        if(isSeen == 1) {
          notificationFunctions.handlePendingNotifications().catchError(
                (onError) {
              print("ERROR...Cannot handle Pending Notifications... $onError");
            },
          ).whenComplete(() async {
            print("Pending quotes have been updated on main.");
            print('Is notifiable: ${generalChanger.getIsNotifiable()}');
            if(await SharedPreferencesHelper.getNotifiableStatus()){
              notificationFunctions.cancelAllNotifications().catchError(
                    (onError){
                  print("ERROR...Cannot CANCEL Pending Notifications... $onError");
                },).whenComplete(() async {
                if(generalChanger.getNotificationCount() != 0) {
                  //notificationFunctions.setNotificationTimePeriod(context);
                  receiver.listen((message) {
                    notificationFunctions.setNotificationTimePeriod();
                  });
                  isolate = await Isolate.spawn(isolateSendPort, receiver.sendPort);
                }
              });
            }
          });
        }
      });
    });

    if(Platform.isAndroid) {
      functions.logAppsFlyerEvent(_appsflyerSdk, "facebook_ads_app_opened", {});
      //functions.logAppsFlyerEvent(_appsflyerSdk, "admob_ads_app_opened", {});
    }
    if(Platform.isIOS) {
      AppTrackingPermission.requestPermission();
    }
    MyStatics.facebookAppEvents.setAdvertiserTracking(enabled: true);
/*    initFacebookPlatformState();
    logActivateApp();*/
    super.initState();
  }

  @override
  void dispose() {
    didReceiveLocalNotificationSubject.close();
    selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("Did notification launch app ${widget.didNotificationLaunchApp}");
    final themeChanger = Provider.of<DayNightChanger>(context);
    //ScreenUtil.init(context, designSize: Size(750, 1334), allowFontScaling: false);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //theme: themeChanger.getTheme(),
      //theme: Styles.themeData(themeChangeProvider.darkTheme, context),
      supportedLocales: [
        Locale('en', ''), // English, no country code
        Locale('tr', 'TR')
      ],
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate, //LTR, RTL
        GlobalCupertinoLocalizations.delegate,
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        for (var supportedLocaleLanguage in supportedLocales) {
          print("Supported Language ${supportedLocaleLanguage.languageCode}");
          if (supportedLocaleLanguage.languageCode == locale.languageCode &&
              supportedLocaleLanguage.countryCode == locale.countryCode) {
            print("User language: ${supportedLocaleLanguage.languageCode}");
            SharedPreferencesHelper.setLocalLanguageCode(supportedLocaleLanguage.languageCode);
            return supportedLocaleLanguage;
          }
        }
        return supportedLocales.first;
      },
      navigatorObservers: <NavigatorObserver>[observer],
      home: widget.didNotificationLaunchApp ? Splash(myPayload: widget.notificationAppLaunchDetails.payload) : Splash(),
/*      routes: {
        '/': (context) => Application(),
        '/message': (context) => MessageView(),
      },*/
    );
  }
}

class Splash extends StatefulWidget {
  final String myPayload;
  Splash({this.myPayload = '0'});
  @override
  SplashState createState() => SplashState();
}

class SplashState extends State<Splash> with AfterLayoutMixin<Splash> {
  int seenIntro;
  final functions = AllFunctions();
  var connectivityResult;
  final dbProvider = DatabaseProvider.instance;
  final dbFunctions = DbFunctions();

  Future checkWhenFirstOpen() async {
    connectivityResult = await (Connectivity().checkConnectivity());
    seenIntro = await SharedPreferencesHelper.getIntroSeenStatus();
    bool firstOpenSendToFB = await SharedPreferencesHelper.getFirstOpenReportedToFB();
    if(seenIntro == 1) {
      await initStoreInfo();
    }
    if(await SharedPreferencesHelper.getAppOpenCount() != 0) {
      await SharedPreferencesHelper.setAppOpenCount(await SharedPreferencesHelper.getAppOpenCount()+1);
    }

    try {
      await SharedPreferencesHelper.setTimezone(await FlutterNativeTimezone.getLocalTimezone());
    } on PlatformException {
      //error on fetching timezone
    }
    List<String> fontProperties = await SharedPreferencesHelper.getFontProperties();
    //int notificationCount = await SharedPreferencesHelper.getNotificationCount();
    bool isNotifiable = await SharedPreferencesHelper.getNotifiableStatus();
    bool nightMode = await SharedPreferencesHelper.getNightMode();
    RemoteNotifyChanger remoteNotifyChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    if (connectivityResult == ConnectivityResult.none) {
      remoteNotifyChanger.setForceVideoAdsValue(1);
      //remoteNotifyChanger.setAdmobRewardedAdUnitId(Platform.isAndroid ? kAdmobAndroidAdUnitId : kAdmobIosAdUnitId);
      //remoteNotifyChanger.setAdmobAppId(Platform.isAndroid ? kAdmobAndroidAppId : kAdmobIosAppId);
      remoteNotifyChanger.setThemeRewardedJson([2, 5, 8, 12, 18, 24]);
      remoteNotifyChanger.setSectionRewardedJson([2, 5, 8, 12, 18, 24]);
      remoteNotifyChanger.setMusicRewardedJson([2, 5, 8, 12, 18, 24]);
      remoteNotifyChanger.setNativeAdsIndexes([2, 6, 10, 14]);
      remoteNotifyChanger.setRateUsPageNumber(0);
      remoteNotifyChanger.setDefaultTheme('i1');
      remoteNotifyChanger.setFreeTrialOpen(1);
      remoteNotifyChanger.setShowPremiumPage(0);
      remoteNotifyChanger.setRateUsJson([2, 3, 5, 7, 9, 11, 14, 17]);
      remoteNotifyChanger.setShowLiveThemes(1);
      remoteNotifyChanger.setShowFeed(false);
      remoteNotifyChanger.setNativeAdsMode(5);
      remoteNotifyChanger.setFbNativePlacementId(Platform.isAndroid ? kFB_Native_Placement_id_android : kFB_Native_Placement_id_ios);
      remoteNotifyChanger.setFbInterstitialPlacementId(Platform.isAndroid ? kFB_interstitial_placement_id_android : kFB_interstitial_placement_id_ios);
      remoteNotifyChanger.setFbBannerPlacementId(Platform.isAndroid ? kFB_my_banner_placement_id_android : kFB_my_banner_placement_id_ios);
      remoteNotifyChanger.setFbNativeBannerPlacementId(Platform.isAndroid ? kFB_native_banner_placement_id_android : kFB_native_banner_placement_id_ios);
      remoteNotifyChanger.setShowBannerAd(true);
      remoteNotifyChanger.setShowBannerAdForIOS(true);
      remoteNotifyChanger.setShowPremiumOnLaunchArray([2,6,10, 14]);
      remoteNotifyChanger.setNativeBannerOrBanner(true);
    } else {
      await functions.fetchClickCountOnFirebase(context);
    }
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    generalChanger.setIsNotifiable(isNotifiable);
    generalChanger.setTimeZone(await FlutterNativeTimezone.getLocalTimezone());
    //Set Theme parameter on launch
    generalChanger.setIsSeenRateUs(await SharedPreferencesHelper.getIsSeenRateUs());
    List<String> themeProperties = await SharedPreferencesHelper.getThemeProperties(remoteNotifyChanger.getDefaultTheme());
    ThemeChanger _themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    _themesChanger.setThemeValues(themeProperties);
    //Set Red Alert
   final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
   newGeneralChanger.setNewTopicsAlert(await SharedPreferencesHelper.getNewTopicsAlert());
   newGeneralChanger.setNewThemesAlert(await SharedPreferencesHelper.getNewThemesAlert());
   newGeneralChanger.setNewSettingsAlert(await SharedPreferencesHelper.getNewSettingsAlert());
   newGeneralChanger.setUsingLiveTheme(await SharedPreferencesHelper.getIsUsingLiveTheme());
    //Set Font parameter on launch
    final fontChanger = Provider.of<FontChanger>(context, listen: false);
    fontChanger.setFontValues(fontProperties);
    String themeName;
    String fontName;
    double fontSize;
    int fontWeight;
    Color textColor;
    Color shadowColor;
    //_themesChanger.setThemeName(themeName);
    try {
      themeName = themeProperties[11];
      fontName = fontProperties[1];
      fontSize = double.parse(fontProperties[2]);
      fontWeight = int.parse(fontProperties[3]);
      _themesChanger.setThemeName(themeName);
      textColor = Color.fromARGB(int.parse(themeProperties[0]), int.parse(themeProperties[1]), int.parse(themeProperties[2]), int.parse(themeProperties[3]));
      shadowColor = Color.fromARGB(int.parse(themeProperties[4]), int.parse(themeProperties[5]), int.parse(themeProperties[6]), int.parse(themeProperties[7]));

    } catch (exception) {
      print('ERROR FROM THEME: $exception');
    }
    _themesChanger.setTextStyle(
      TextStyle(
        fontFamily: fontName,
        fontSize: MediaQuery.of(context).size.width * fontSize,
        fontWeight: FontWeight.values[fontWeight],
        color: textColor,
        wordSpacing: double.parse(fontProperties[5]),
        letterSpacing: double.parse(fontProperties[6]),
        height: double.parse(fontProperties[7]),
        shadows: [
          Shadow(
            offset: Offset(double.parse(themeProperties[8]), double.parse(themeProperties[9])),
            blurRadius: double.parse(themeProperties[10]),
            color: shadowColor,
          ),
        ], //Color(buttonColor),
      ),
    );
    _themesChanger.setTextStyleAuthorName(
      TextStyle(
        fontFamily: fontName,
        fontSize: MediaQuery.of(context).size.width * fontSize * 0.5,
        fontWeight: FontWeight.values[fontWeight-1],
        color: textColor,
        wordSpacing: double.parse(fontProperties[5]) * 0.75,
        letterSpacing: double.parse(fontProperties[6]) * 0.75,
        height: double.parse(fontProperties[7]) * 0.75,
        shadows: [
          Shadow(
            offset: Offset(double.parse(themeProperties[8]), double.parse(themeProperties[9])),
            blurRadius: double.parse(themeProperties[10]),
            color: shadowColor,
          ),
        ], //Color(buttonColor),
      ),
    );
    _themesChanger.setButtonColor(textColor);
    if (seenIntro == 1) {
/*      print("Feed: ${remoteNotifyChanger.getShowFeed()}");
      if(remoteNotifyChanger.getShowFeed()) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => FeedQuotesPage()));
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => NewHomePage(notifiedQuoteId: int.parse(widget.myPayload))));
      }*/
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => NewHomePage(notifiedQuoteId: int.parse(widget.myPayload))));
    } else {
      if(!firstOpenSendToFB) {
        print("This is first open");
        SharedPreferencesHelper.setFirstOpenReportedToFB(true);
/*        MyStatics.facebookAppEvents.logEvent(
          name: 'my_first_open_event',
        );*/
      }
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => WelcomeScreen()));
    }
  }


  @override
  void afterFirstLayout(BuildContext context) => checkWhenFirstOpen();
  //bool _visible = false;

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    //_visible = true;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/realImages/splash.png"),
            fit: BoxFit.cover,
          ),
        ),
        //margin: EdgeInsets.symmetric(horizontal: 25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: heightScreen * 0.035,
            ),
            Container(
              height: heightScreen * 0.12,
              child: Center(
                child: Image.asset(
                  'assets/signs2/splash_icon.png',
                ),
              ),
            ),
            Container(
              height: heightScreen * 0.035,
            ),
            Container(
              height: heightScreen * 0.2,
              child: Center(
                child: Text(
                  AppLocalizations.of(context).translate('inspirationalQuotesSeparate'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.07,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                    height: 1.4,
                    letterSpacing: 1.1,
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;
  StreamSubscription _connectionSubscription;
  final List<String> _productLists = kProductIds;
  List<Container> productListAsWidget = <Container>[];
  String _queryProductError;
  int _radioValue = 0;
  bool isPurchasing = false;
  bool _purchasePending = false;
  //IAPItem purchaseParam;


  //List<IAPItem> _items = [];
  //List<PurchasedItem> _purchases = [];

  Future<void> initStoreInfo() async {
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    bool isPremium = await SharedPreferencesHelper.getIsPremiumStatus();
    int expiryTimeOnPref = await SharedPreferencesHelper.getExpiryTime();
    if (isPremium) {
      print('Congratulations. You are a premium member.');
      dbFunctions.updateThemesUnlockedStatus(1);
      dbFunctions.updateLiveThemesUnlockedStatus(1);
      dbFunctions.updateSectionsUnlockedStatus(1);
      dbFunctions.updateMusicsUnlockedStatus(1);
      dbFunctions.updateFontsUnlockedStatus(1);
      generalChanger.setIsPremiumOrSubscribed(true);
    } else {
      if (await functions.verifyPurchaseFromPref(expiryTimeOnPref)) {
        dbFunctions.updateThemesUnlockedStatus(1);
        dbFunctions.updateLiveThemesUnlockedStatus(1);
        dbFunctions.updateSectionsUnlockedStatus(1);
        dbFunctions.updateMusicsUnlockedStatus(1);
        dbFunctions.updateFontsUnlockedStatus(1);
        generalChanger.setIsPremiumOrSubscribed(true);
      } else {
        //dbFunctions.updateThemesUnlockedStatusAsZero();
        dbFunctions.updateThemesUnlockedStatus(0);
        dbFunctions.updateLiveThemesUnlockedStatus(0);
        dbFunctions.updateSectionsUnlockedStatusAsZero();
        dbFunctions.updateMusicsUnlockedStatus(0);
        dbFunctions.updateFontsUnlockedStatus(0);
        generalChanger.setIsPremiumOrSubscribed(false);
      }
    }
  }

}


_versionControlAndroid() async {
  String buildNumber = await SharedPreferencesHelper.getCurrentBuildNumber();
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  print('Build number from pref: $buildNumber, newest one: ${packageInfo.buildNumber}, last version: ${packageInfo.version}, pref version: ${await SharedPreferencesHelper.getCurrentAppVersion()}');
  if(int.parse(buildNumber)==1){
    await SharedPreferencesHelper.setCurrentBuildNumber(packageInfo.buildNumber);
    await SharedPreferencesHelper.setCurrentAppVersion(packageInfo.version);
  } else {
    print('Build number is not equal to 1');
    if (int.parse(buildNumber) < int.parse(packageInfo.buildNumber)) {
      if(int.parse(buildNumber)<41){
        await SharedPreferencesHelper.setIsUserExperienceSeen(false);
      }
      if(int.parse(buildNumber)<43){
        await SharedPreferencesHelper.setAppOpenCount(0);
      }
      if(int.parse(buildNumber)<46){
        await SharedPreferencesHelper.setReminderStartHour(9);
        await SharedPreferencesHelper.setReminderStartMinute(0);
        await SharedPreferencesHelper.setReminderEndHour(21);
        await SharedPreferencesHelper.setReminderEndMinute(0);
        await SharedPreferencesHelper.setMusicPreference('');
        //await SharedPreferencesHelper.setThemeProperties(['200','0','0','0','1.5','1.5','3.0','1.35','4.0','2.0','255','255','255','255','5','0.09','Algerian Regular','f5']);
      }
      if(int.parse(buildNumber)<52) {
        await SharedPreferencesHelper.setFontProperties(['1','Algerian Regular','0.09','5','0','4.0','2.0','1.35']);
        await SharedPreferencesHelper.setThemeProperties(['255','255','255','255','200','0','0','0','1.5','1.5','3.0','f5']);
      }
      if(int.parse(buildNumber)<56) {
        await SharedPreferencesHelper.setDbLockValue(2);
      }
      if(int.parse(buildNumber)<57) {
        await SharedPreferencesHelper.setTimezone('America/Detroit');
        await SharedPreferencesHelper.setIsPremiumOrSubscribedStatus(false);
      }
      if(int.parse(buildNumber)<67) {
        await SharedPreferencesHelper.setNewSettingsAlert(false);
        await SharedPreferencesHelper.setNewTopicsAlert(true);
        await SharedPreferencesHelper.setNewThemesAlert(true);
      }
      if(int.parse(buildNumber)<68) {
        await SharedPreferencesHelper.setStarRate(0);
      }
      if(int.parse(buildNumber)<71) {
        SharedPreferencesHelper.setLocalLanguageCode('en');
        SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(int.parse(buildNumber)<73) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
        SharedPreferencesHelper.setNewThemesAlert(true);
      }
      if(int.parse(buildNumber)<82) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(int.parse(buildNumber)<94) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(int.parse(buildNumber)<100) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
        SharedPreferencesHelper.setNewThemesAlert(true);
      }
      if(int.parse(buildNumber)<102) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(int.parse(buildNumber)<103) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(int.parse(buildNumber)<104) {
        SharedPreferencesHelper.setFirstOpenReportedToFB(false);
      }
      if(int.parse(buildNumber)<105) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(int.parse(buildNumber)<106) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
        SharedPreferencesHelper.setFirstOpenReportedToFB(false);
      }
      if(int.parse(buildNumber)<107) {
        if (await SharedPreferencesHelper.getIsUsingLiveTheme()) {
          SharedPreferencesHelper.setIsUsingLiveTheme(false);
          SharedPreferencesHelper.setThemeProperties(['255','255','255','255','220','0','0','0','1.5','1.5','3.0', 'f6']);
        }
      }
      SharedPreferencesHelper.setCurrentBuildNumber(packageInfo.buildNumber);
      SharedPreferencesHelper.setCurrentAppVersion(packageInfo.version);
      print('Changed Build number: ${await SharedPreferencesHelper.getCurrentBuildNumber()}, newest one: ${packageInfo.buildNumber}');
    }
  }
}

_versionControlIos() async {
  String buildNumber = await SharedPreferencesHelper.getCurrentBuildNumber();
  String version = await SharedPreferencesHelper.getCurrentAppVersion();
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  print('Package info from pref: $version, newest one: ${packageInfo.version}, version data type: ${packageInfo.version.runtimeType}, build number: $buildNumber from pref, newest one: ${packageInfo.buildNumber}');
  if(version == '1.0.0') {
    await SharedPreferencesHelper.setCurrentBuildNumber(packageInfo.buildNumber);
    await SharedPreferencesHelper.setCurrentAppVersion(packageInfo.version);
  } else {
    if(version != packageInfo.version) {
      print("version != packageInfo.version");
      if(version.compareTo('1.1.3').isNegative) {
        print("$version : version");
        await SharedPreferencesHelper.setReminderStartHour(9);
        await SharedPreferencesHelper.setReminderStartMinute(0);
        await SharedPreferencesHelper.setReminderEndHour(21);
        await SharedPreferencesHelper.setReminderEndMinute(0);
        await SharedPreferencesHelper.setMusicPreference('');
      }
      if(version.compareTo('1.2.1').isNegative) {
        print("$version : version");
        await SharedPreferencesHelper.setFontProperties(['1','Algerian Regular','0.09','5','0','4.0','2.0','1.35']);
        await SharedPreferencesHelper.setThemeProperties(['255','255','255','255','200','0','0','0','1.5','1.5','3.0','f5']);
      }
      if(version.compareTo('1.2.6').isNegative) {
        await SharedPreferencesHelper.setDbLockValue(2);
        await SharedPreferencesHelper.setTimezone('America/Detroit');
      }
      if(version.compareTo('1.3.6').isNegative) {
        print("old version: $version, smaller than 1.3.6");
        await SharedPreferencesHelper.setNewSettingsAlert(false);
        await SharedPreferencesHelper.setNewTopicsAlert(true);
        await SharedPreferencesHelper.setNewThemesAlert(true);
      }
      if(version.compareTo('1.3.7').isNegative) {
        await SharedPreferencesHelper.setStarRate(0);
        SharedPreferencesHelper.setLocalLanguageCode('en');
        SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(version.compareTo('1.4.2').isNegative) {
        await SharedPreferencesHelper.setNewTopicsAlert(true);
        await SharedPreferencesHelper.setNewThemesAlert(true);
      }
      if(version.compareTo('1.5.1').isNegative) {
        await SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(version.compareTo('1.5.3').isNegative) {
        await SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(version.compareTo('1.6.2').isNegative) {
        await SharedPreferencesHelper.setNewTopicsAlert(true);
      }
      if(version.compareTo('1.6.7').isNegative) {
        await SharedPreferencesHelper.setNewTopicsAlert(true);
        await SharedPreferencesHelper.setNewThemesAlert(true);
      }
      if(version.compareTo('1.7.1').isNegative) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
        SharedPreferencesHelper.setFirstOpenReportedToFB(false);
      }
      if(version.compareTo('1.7.3').isNegative) {
        SharedPreferencesHelper.setNewTopicsAlert(true);
        if (await SharedPreferencesHelper.getIsUsingLiveTheme()) {
          SharedPreferencesHelper.setIsUsingLiveTheme(false);
          SharedPreferencesHelper.setThemeProperties(['255','255','255','255','220','0','0','0','1.5','1.5','3.0', 'f6']);
        }
      }
      await SharedPreferencesHelper.setCurrentBuildNumber(packageInfo.buildNumber);
      await SharedPreferencesHelper.setCurrentAppVersion(packageInfo.version);
      print('Changed Build number: ${await SharedPreferencesHelper.getCurrentBuildNumber()}, newest one: ${packageInfo.buildNumber}');
    }
  }
}






