import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'package:facebook_audience_network/ad/ad_banner.dart';
import 'package:facebook_audience_network/ad/ad_interstitial.dart';
import 'package:facebook_audience_network/ad/ad_native.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart' as render;
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:home_widget/home_widget.dart';
import 'package:http/http.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:motilife/DbProvider.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:motilife/ExtractedWidgets/fb_native_ads.dart';
import 'package:motilife/ExtractedWidgets/other_apps.dart';
import 'package:motilife/ExtractedWidgets/rateus.dart';
import 'package:motilife/ExtractedWidgets/starRateUs.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/fetch_cache.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/Utilities/notification_functions.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/onboardingScreens/premium_pay.dart';
import 'package:motilife/prefs.dart';
import 'package:motilife/push_notification/message.dart';
import 'package:motilife/screens/setting_tab.dart';
import 'package:motilife/screens/single_quote.dart';
import 'package:motilife/screens/themes_tab.dart';
import 'package:motilife/screens/topic_tab.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:screenshot/screenshot.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:motilife/localizations.dart';
import 'package:motilife/ExtractedWidgets/cupertino_page_transition.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
//import 'package:workmanager/workmanager.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:device_info/device_info.dart';
import 'dart:ui' as ui;
import '../main.dart';


final notificationFunctions = NotificationHandler();
final dbProvider = DatabaseProvider.instance;
final dbFunctions = DbFunctions();
final functions = AllFunctions();
bool isBookMarked = false;
int itemCount = 1;
var query;
var isolate;


class NewHomePage extends StatefulWidget{
  final int notifiedQuoteId;
  final bool isComeFromReminderPage;//if true, set notification

  NewHomePage({this.notifiedQuoteId, this.isComeFromReminderPage = false});
  //static const String routeName = '/secondPage';
  @override
  _NewHomePageState createState() => _NewHomePageState();
}
String fbMessageBody;
String fbMessageTitle;

const String testDevice = 'YOUR_DEVICE_ID';
const int maxFailedLoadAttempts = 3;

class _NewHomePageState extends State<NewHomePage> with WidgetsBindingObserver{
  bool _openWithWidgetData = true;
  String text = 'Text Test';
  String subject = 'Subject';
  String imagePath = "";
  // File _imageFile;
  Uint8List _imageFile;
  Stream<FileResponse> fileStream;
  List<QuoteQuery> quoteQuery = [];
  List<QuoteQuery> priorityQuoteQuery = [];
  UserQuote onScreenQuote;
  bool isSeenRateUs = false;
  List<String> themeProperties;
  bool _isShuffled = false;
  var allQuotes;
  var connectivityResult;
  bool _isWidgetSet = false;
  int _sessionCountPerLaunch = 0;

  ScreenshotController screenshotController = ScreenshotController();
  //bool _isSeenGif;
  int _openCount;
  bool _isExperienced;

  PageController _controller = PageController(
    initialPage: 0,
    viewportFraction: 2,
  );
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  //AppLifecycleState _lastLifecyleState;
  final receiver = ReceivePort();
  int widgetCyclePeriodHour = 12;

  String widgetId;
  String widgetMessage;
  String widgetAuthorName;
  String widgetTheme;

  String _deviceId = 'N/A';
  String _version = 'N/A';
  int showPremiumPage = 0;
  bool notificationSet = false;

  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  AppsflyerSdk _appsflyerSdk;
  Map _deepLinkData;
  Map _gcd;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _nativeAdIsLoaded = true;
  bool _nativeAdIsLoadedAtLeastOne = false;
  Widget _currentNativeAd = SuggestOtherApps(
    appIconName: 'uf',
    motto: 'factsMotto1',
    directedLink: Platform.isAndroid ? kUFPlayStoreUrl : kUFAppStoreUrl,
    marginX: 25,
    marginY: 435,
  );
  Widget _currentBannerAd = Container();
  bool _hideBannerAd = false;
  bool _adsFirstLoaded = false;
/*  NativeAd _myNative;
  AdWidget nativeAdWidget = AdWidget();
  bool _isNativeAdLoaded = false;

  //Admob Banner
  BannerAd _anchoredBanner;
  bool _loadingAnchoredBanner = false;*/

  bool isScreenShotProcessing = false;
  bool _rateUsSeen = false;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final musicChanger = Provider.of<MusicChanger>(context, listen: false);
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    print("LifecycleWatcherState#didChangeAppLifecycleState state=${state.toString()}");
    setState(() {
      //_lastLifecyleState = state;
    });
    if(state == AppLifecycleState.paused) {
      _sessionCountPerLaunch ++;
      musicChanger.getAdvancedPlayer().pause();
    } else {
      functions.resumeMusic(musicChanger);
    }
  }

  _setWidgetData() {
    print("Widget data set");
    widgetMessage = quoteQuery[quoteQuery.length-1].quote;
    widgetId = "${quoteQuery[quoteQuery.length-1].userQuoteId}";
    widgetAuthorName = quoteQuery[quoteQuery.length-1].authername != null ? quoteQuery[quoteQuery.length-1].authername : "";
    print(widgetMessage);
    print(widgetId);
    _sendAndUpdate();
    setState(() {
      _isWidgetSet = true;
    });
  }

  @override
  void initState() {
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    _loadWidgetData();
    if(!generalChanger.getIsPremiumOrSubscribed()) {
      //_setAdmobNativeAdListener();
      //_loadAdmobNative(0);
      //functions.createAdmobInterstitialAd(0);
      //_loadInterstitialAd(remoteChanger);

    }
    HomeWidget.setAppGroupId('group.com.viyatek.motilife');
    Map appsFlyerOptions = { "afDevKey": kDEV_KEY_ApsFlyer,
      "afAppId": Platform.isAndroid ? kAppId_Android : kAppId_iOS,
      "isDebug": true};
    _appsflyerSdk = AppsflyerSdk(appsFlyerOptions);
    //_initAppsFlyerSdk();
    _getFirstOpenAfterNotification();
    WidgetsBinding.instance.addObserver(this);
    quoteQuery = [];
    itemCount = 0;
    _firebaseMessagingHandle();
    _queryAllSectionsAndQuotesOfUser();
    _getPrefData(remoteChanger, generalChanger);
    _sendFirstOpenEvent();
    themeProperties = themesChanger.getThemeValues();
    _rateUsSeen = generalChanger.getIsSeenRateUs();

    //startSdk();
    Future.delayed(const Duration(milliseconds: 1500), () {
      if (widget.isComeFromReminderPage && !notificationSet) {
        notificationSet = true;
        print("Setting Notifications on home page");
        notificationFunctions.handlePendingNotifications().catchError(
          (onError) {
            print("ERROR...Cannot handle Pending Notifications... $onError");
          },
        ).whenComplete(() {
          print("Pending quotes have been updated on Home Page.");
          notificationFunctions.cancelAllNotifications().catchError(
            (onError) {
              print("ERROR...Cannot CANCEL Pending Notifications... $onError");
            },
          ).whenComplete(() async {

            if (generalChanger.getNotificationCount() != 0 && generalChanger.getIsNotifiable()) {
                receiver.listen((message) {
                  notificationFunctions.setNotificationTimePeriod();
                });
                isolate = await Isolate.spawn(isolateSendPort, receiver.sendPort);
            }

          });
        });
      }
    });
    Future.delayed(const Duration(milliseconds: 300), () async {
      final musicChanger = Provider.of<MusicChanger>(context, listen: false);
      functions.playMusic(musicChanger);
    });

    Future.delayed(const Duration(milliseconds: 2000), () async {
      if (generalChanger.getOnLaunch()) _updateExpiryDateOnPref();
      //_configureLocalTimeZone();
    });
    Future.delayed(const Duration(milliseconds: 3000), () async {
      if(!_isWidgetSet && _isShuffled) {
        _setWidgetData();
      }
    });

/*    Future.delayed(const Duration(milliseconds: 15000), () async {
      await dbProvider.writeDefaultQuotesData();
    });*/

    print("_sessionCountPerLaunch: $_sessionCountPerLaunch");
/*    FirebaseMessaging.instance.getToken().then((value) {
      String token = value;
      print("Firebase token: $token");
    });*/
    super.initState();
  }

  _clearCache() async {
    print("Clearing app cache");
    var appDir = (await getTemporaryDirectory()).path;
    new Directory(appDir).delete(recursive: true);
  }


  @override
  void dispose() {
    _controller.dispose();
    WidgetsBinding.instance.removeObserver(this);
    //_anchoredBanner?.dispose();
    super.dispose();
  }

  Future<void> _sendData() async {
    try {
      return Future.wait([
        HomeWidget.saveWidgetData<String>('title', widgetId),
        HomeWidget.saveWidgetData<String>('message', widgetMessage),
        HomeWidget.saveWidgetData<String>('themeName', themeProperties[11]),
        HomeWidget.saveWidgetData<String>('author', widgetAuthorName),
      ]);
    } on PlatformException catch (exception) {
      debugPrint('Error Sending Data. $exception');
    }
  }

  Future<void> _updateWidget() async {
    try {
      return HomeWidget.updateWidget(
          name: 'HomeWidgetExampleProvider', iOSName: 'HomeWidgetExample');
    } on PlatformException catch (exception) {
      debugPrint('Error Updating Widget. $exception');
    }
  }

  Future<void> _sendAndUpdate() async {
    await _sendData();
    await _updateWidget();
    setState(() {
      _isWidgetSet = true;
    });
  }

  Future<void> _loadWidgetData() async {
    try {
      print("On load data");
      return Future.wait([
        quoteQuery.length >1 ? HomeWidget.getWidgetData<String>('title', defaultValue: '${quoteQuery[quoteQuery.length-1].userQuoteId}')
            .then((value) => widgetId = value) : HomeWidget.getWidgetData<String>('title', defaultValue: '2352')
            .then((value) => widgetId = value),
        quoteQuery.length >1 ? HomeWidget.getWidgetData<String>('message',
            defaultValue: '${quoteQuery[quoteQuery.length-1].quote}')
            .then((value) => widgetMessage = value) : HomeWidget.getWidgetData<String>('message',
            defaultValue: 'Live the way you want, not the way others want.')
            .then((value) => widgetMessage = value),
        quoteQuery.length >1 ? HomeWidget.getWidgetData<String>('author',
            defaultValue: '${quoteQuery[quoteQuery.length-1].authername}')
            .then((value) => widgetAuthorName = value) : HomeWidget.getWidgetData<String>('author',
            defaultValue: '')
            .then((value) => widgetAuthorName = value),
        HomeWidget.getWidgetData<String>('themeName',
            defaultValue: 'f4')
            .then((value) => widgetTheme = value),
      ]);
    } on PlatformException catch (exception) {
      debugPrint('Error Getting Data. $exception');
    }
  }

/*  void _startBackgroundWidgetUpdate() {
    setState(() {
      _isWidgetSet = true;
    });
    Workmanager().cancelAll();
    for(int i = 2; i<20; i++) {
      int id = i - 2;
      *//*Workmanager.registerPeriodicTask('$id', 'widgetBackgroundUpdate$id',
          frequency: Duration(hours: widgetCyclePeriodHour), inputData: {'taskUniqueName': '$id', 'title': '${quoteQuery[quoteQuery.length-i].userQuoteId}', 'message': quoteQuery[quoteQuery.length-i].quote, "themeName": themeProperties[11], "author": quoteQuery[quoteQuery.length-i].authername != null ? quoteQuery[quoteQuery.length-i].authername : ""}, initialDelay: Duration(hours: id * widgetCyclePeriodHour));*//*
      Workmanager().registerOneOffTask(
          '$id',
          'widgetBackgroundUpdate$id',
          inputData: {'taskUniqueName': '$id', 'title': '${quoteQuery[quoteQuery.length-i].userQuoteId}', 'message': quoteQuery[quoteQuery.length-i].quote, "themeName": themeProperties[11], "author": quoteQuery[quoteQuery.length-i].authername != null ? quoteQuery[quoteQuery.length-i].authername : ""}, initialDelay: Duration(hours: id * widgetCyclePeriodHour,),
        constraints: Constraints(
            //networkType: NetworkType.connected,
            requiresBatteryNotLow: true,
            //requiresCharging: true,
            //requiresDeviceIdle: true,
            //requiresStorageNotLow: true
        )
      );
    }
  }*/


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("In didChangeDependencies");
    _checkForWidgetLaunch();
    HomeWidget.widgetClicked.listen(_launchedFromWidget);
  }

  void _checkForWidgetLaunch() {
    HomeWidget.initiallyLaunchedFromHomeWidget().then(_launchedFromWidget);
  }

  void _launchedFromWidget(Uri uri) {
    if (uri != null) {
      // App started from HomeScreenWidget
      print("App started from HomeScreenWidget");
      _loadWidgetData();
      print(widgetId);
      if(_sessionCountPerLaunch>0) {
        print(_sessionCountPerLaunch);
        Future.delayed(Duration(milliseconds: 100), () {
          Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: NewHomePage(notifiedQuoteId: int.parse(widgetId))), (Route<dynamic> route) => false);
        });
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    final generalChanger = Provider.of<GeneralChanger>(context);
    final themesChanger = Provider.of<ThemeChanger>(context);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
/*    if (!_loadingAnchoredBanner && remoteChanger.getShowBannerAd() && !generalChanger.getIsPremiumOrSubscribed()) {
      _loadingAnchoredBanner = true;
      _createAdmobAnchoredBanner(context);
    }*/
    if(!_adsFirstLoaded && remoteChanger.getShowBannerAd() && !generalChanger.getIsPremiumOrSubscribed()) {
      _adsFirstLoaded = true;
      _nativeAdFB(remoteChanger, widthScreen, heightScreen, 0);
      functions.loadInterstitialAd(remoteChanger, context);
      if(remoteChanger.getShowBannerAd()) {
        if(remoteChanger.getNativeBannerOrBanner()) {
          Platform.isAndroid ? _showNativeBannerAd(remoteChanger) : _showBannerAd(remoteChanger);
        } else {
          _showBannerAd(remoteChanger);
        }
      }
    }
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        body: CupertinoScaffold(
          body: Builder(
            builder: (context) {
              return Screenshot(
                controller: screenshotController,
                child: GestureDetector(
                        onTap: () {
                          //print(_deviceData['version.sdkInt']);//version.sdkInt: 30, version.release: 11
                          //print(_deviceData['version.release']);
                          //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
/*                          if(!_isWidgetSet && Platform.isAndroid) _startBackgroundWidgetUpdate();
                          if(!_isWidgetSet && Platform.isIOS) _setWidgetData();*/
                          //FocusScope.of(context).requestFocus(FocusNode());//Dismisses keyboard
                          //logEvent("Home_Scroll", eventValues);
                          if(!_isWidgetSet && _isShuffled) {
                            _setWidgetData();
                          }
                        },
                        child: Container(
                          height: heightScreen * 1,
                          width: widthScreen * 1,
                          decoration: newGeneralChanger.getUsingLiveTheme() && !isScreenShotProcessing ? BoxDecoration() : BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Stack(
                            children: [
                              Container(
                                height: 0,
                                child: FetchCache(url: MyStatics.premiumPageURL),
                              ),
                              Positioned(
                                top: 0,//heightScreen*0.005,
                                right: 0,//widthScreen*0.004,
                                left: 0,//widthScreen*0.004,
                                //bottom: heightScreen-heightScreen*0.005-55,
                                  child: !_hideBannerAd ? Opacity(
                                      opacity: !isScreenShotProcessing ? 1 : 0,
                                      child: Container(
                                    color: Colors.transparent,
                                    child: _currentBannerAd,
                                  )) : Container(),
                              ),
                              !_isShuffled //quoteQuery.length == 0
                                  ? SpinKitPulse(
                                itemBuilder: (_, int index) {
                                  return DecoratedBox(
                                    decoration: BoxDecoration(
                                        color: index.isEven ? Colors.white24 : Colors.black54,
                                        shape: BoxShape.circle
                                    ),
                                  );
                                },
                                size: 120.0,
                              )
                                  : buildPageView(generalChanger, heightScreen, themesChanger, widthScreen),//ReusableInlineExample(),//
                              if(!isScreenShotProcessing) Positioned(
                                bottom: heightScreen*0.025,
                                right: widthScreen*0.004,
                                left: widthScreen*0.004,
                                child: Container(
                                  height: heightScreen * 0.1,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      MyCupertinoPageTransition(nextScreen: TopicsTab(), imageName: 'topics', imageText: AppLocalizations.of(context).translate('topics'), isVisibleRedAlert: newGeneralChanger.getNewTopicsAlert(), buttonTag: 0, generalChanger: generalChanger, newGeneralChanger: newGeneralChanger, backgroundColor: Colors.white.withOpacity(0.07), fontSize: 0.03),
                                      MyCupertinoPageTransition(nextScreen: ThemesTab(), imageName: "themes", imageText: AppLocalizations.of(context).translate('themes'), isVisibleRedAlert: newGeneralChanger.getNewThemesAlert(), buttonTag: 1, generalChanger: generalChanger, newGeneralChanger: newGeneralChanger, backgroundColor:  Colors.white.withOpacity(0.07), fontSize: 0.03),
                                      MyCupertinoPageTransition(nextScreen: SettingsTab(), imageName: 'settings', imageText: AppLocalizations.of(context).translate('settings'), isVisibleRedAlert: newGeneralChanger.getNewSettingsAlert(), buttonTag: 2, generalChanger: generalChanger, newGeneralChanger: newGeneralChanger, backgroundColor: Colors.white.withOpacity(0.07), fontSize: 0.03),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
              );
            },
          ),
        ),
      ),
    );
  }

  PageView buildPageView(GeneralChanger barStatusChanger, double heightScreen,
      ThemeChanger themesChanger, double widthScreen) {
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    final eventChanger = Provider.of<EventChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    final fontChanger = Provider.of<FontChanger>(context);
    print("Page builder");
    return PageView.builder(
      controller: _controller,
      scrollDirection: Axis.vertical,
      itemCount: itemCount,
      onPageChanged: (int page) async {
        if(remoteChanger.getNativeAdsIndexes().contains(page + 2) && !generalChanger.getIsPremiumOrSubscribed()) {
          print("Native ad reLOAD");
          //_loadAdmobNative(page);
          _nativeAdFB(remoteChanger, widthScreen, heightScreen, page+2);
        }
        var pendingNotificationRequests = await flutterLocalNotificationsPlugin.pendingNotificationRequests();
        int matchingNotificationId;
        print('LENGTH of query from provider (calculated in main): ${generalChanger.getQuoteQuery().length}');
        dbFunctions.updateUserQuoteStatus(
            onScreenQuote.userQuoteId,
            onScreenQuote.isCollected,
            onScreenQuote.isSeen,
            onScreenQuote.isBookmarked);
        //If quote on the screen was scheduled for reminders, then cancel that reminder...
        var contain = pendingNotificationRequests.where((pendingNotification) {
          setState(() {
            matchingNotificationId = pendingNotification.id;
          });
          return int.parse(pendingNotification.payload) == onScreenQuote.userQuoteId;
        });
        if(contain.isNotEmpty) {
          print("$matchingNotificationId was cancelled");
          await flutterLocalNotificationsPlugin.cancel(matchingNotificationId);
        }
        print("Current Page: $page");
        int previousPage = page;
        if (page != 0) {
          if (_controller.position.userScrollDirection ==
              render.ScrollDirection.reverse) {
            previousPage--;
            print("Previous page: $previousPage");
          }
          if(page == 1) {
/*            if(!_isWidgetSet && Platform.isAndroid) _startBackgroundWidgetUpdate();
            if(!_isWidgetSet && Platform.isIOS) _setWidgetData();*/
            if(!_isWidgetSet && _isShuffled) {
              _setWidgetData();
            }
          }
        } else {
          previousPage = 2;
        }
        if (!eventChanger.getIsScrolled() && page == 1) {
          functions.sendAnalyticsWithOwnName('Home_Scroll');
          eventChanger.setIsScrolledOnce(true);
          //print(env);
        }
        if (!eventChanger.getIsScrolledForth() && page == 4) {
          functions.sendAnalyticsWithOwnName('Home_Scroll_4');
          eventChanger.setIsScrolledForth(true);
        }

        if(remoteChanger.getNativeAdsIndexes().contains(page)) {
          print("Hide banner ad");
          Future.delayed(Duration(milliseconds: 500), () {
            setState(() {
              _hideBannerAd = true;
            });
          });
        } else {
          print("Show banner ad");
          _hideBannerAd = false;
        }
      },
      itemBuilder: (context, index) {
        onScreenQuote = UserQuote(
          userQuoteId: quoteQuery[index].userQuoteId,
          isCollected: quoteQuery[index].isCollected,
          isSeen: 1,
          isBookmarked: quoteQuery[index].isBookmarked,
        );
        if (index == remoteChanger.getRateUsPageNumber() && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount) && widget.notifiedQuoteId != 100000) {
          return GestureDetector(
              onTap: () {
                _controller.animateToPage(index + 1, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
              },
              child: StarRateUs(
                themeProperties: themeProperties,
                heightScreen: heightScreen,
                widthScreen: widthScreen,
                replacedQuoteId: quoteQuery[index].userQuoteId,
                replacedIsBookmarked: quoteQuery[index].isBookmarked,
                replacedIsCollected: quoteQuery[index].isCollected,
                replacedIsSeen: quoteQuery[index].isSeen,
                generalChanger: generalChanger,
              ),
            );
        } else if(remoteChanger.getNativeAdsIndexes().contains(index) && !generalChanger.getIsPremiumOrSubscribed()) {
          dbFunctions.updateUserQuoteStatus(quoteQuery[index].userQuoteId, quoteQuery[index].isCollected, quoteQuery[index].isSeen, quoteQuery[index].isBookmarked);
          return _currentNativeAd;
        } else {
          return GestureDetector(
              onTap: () {
                _controller.animateToPage(index + 1, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                height: heightScreen * 0.89,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: heightScreen * 0.1,
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: heightScreen * 0.20,
                        maxHeight: heightScreen * 0.38,
                      ),
                      child: GestureDetector(
                        onLongPress: () {
                          subject = quoteQuery[index].authername != null
                              ? "${quoteQuery[index].quote}\n\n${quoteQuery[index].authername}"
                              : "${quoteQuery[index].quote}";
                          Clipboard.setData(new ClipboardData(text: subject));
                          _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate('textCopied')),));
                        },
                        child: AutoSizeText(
                          quoteQuery[index].quote,
                          textAlign: TextAlign.center,
                          style: themesChanger.getTextStyle(),
                        ),
                      ),
                    ),
                    Container(
                      height: heightScreen * 0.03,
                    ),
                    quoteQuery[index].authername != null
                        ? Text(
                      quoteQuery[index].authername,
                      textAlign: TextAlign.center,
                      style: themesChanger.getTextStyleAuthorName(),
                    )
                        : Container(),
                    Container(
                      height: heightScreen * 0.1,
                    ),
                    isScreenShotProcessing
                        ? generalChanger.getIsPremiumOrSubscribed()
                        ? Row()
                        : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            'assets/signs2/watermark.png',
                            scale: 3,
                            color: themesChanger.getButtonColor(),
                          ),
                          Container(
                            width: widthScreen * 0.02,
                          ),
                          Text(
                            AppLocalizations.of(context).translate('inspirationalQuotes'),
                            style: TextStyle(
                              fontFamily: 'Futura Book',
                              fontSize: MediaQuery.of(context).size.width * 0.06,
                              fontWeight: FontWeight.w500,
                              color: Color.fromARGB(int.parse(themeProperties[0]), int.parse(themeProperties[1]), int.parse(themeProperties[2]), int.parse(themeProperties[3])),
                              wordSpacing: double.parse(fontChanger.getFontValues()[5]),
                              letterSpacing: double.parse(fontChanger.getFontValues()[6]),
                              height: double.parse(fontChanger.getFontValues()[7]),
                              shadows: [
                                Shadow(
                                  offset: Offset(double.parse(themeProperties[8]), double.parse(themeProperties[9])),
                                  blurRadius: double.parse(themeProperties[10]),
                                  color: Color.fromARGB(int.parse(themeProperties[4]), int.parse(themeProperties[5]), int.parse(themeProperties[6]), int.parse(themeProperties[7])),
                                ),
                              ],
                            ),
                          ),
                        ])
                        : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                            child: ImageIcon(
                              AssetImage('assets/signs2/share.png'),
                              size: widthScreen * 0.08,
                              color: themesChanger.getButtonColor(),
                            ),
                            onTap: () {
                              functions.sendAnalyticsWithOwnName("quoteShareClicked");
                              if(!isScreenShotProcessing) {
                                setState(() {
                                  isScreenShotProcessing = true;
                                });
                                subject = quoteQuery[index].authername != null
                                    ? "${quoteQuery[index].quote}\n\n${quoteQuery[index].authername}\n$kDynamicLinkIQ"
                                    : "${quoteQuery[index].quote}\n$kDynamicLinkIQ";
                                Future.delayed(
                                    const Duration(milliseconds: 100), () {
                                  _takeScreenshot();
                                });
                                Future.delayed(const Duration(milliseconds: 800), () {
                                  setState(() {
                                    isScreenShotProcessing = false;
                                  });
                                  print('Process after screenshot: $isScreenShotProcessing');
                                });
                              }
                            }
                        ),
                        Container(
                          width: widthScreen * 0.08,
                        ),
                        GestureDetector(
                          child: ImageIcon(
                            AssetImage('assets/signs2/save.png'),
                            size: widthScreen * 0.08,
                            color: themesChanger.getButtonColor(),
                          ),
                          onTap: () async {
                            _requestPermission().catchError(
                                  (onError) {
                                print("ERROR...Cannot CANCEL Pending Notifications... $onError");
                              },
                            ).whenComplete(() {
                              functions.sendAnalyticsWithOwnName("quoteSaveClicked");
                              setState(() {
                                isScreenShotProcessing = true;
                              });
                              Future.delayed(
                                  const Duration(milliseconds: 300), () {
                                _saveScreen();
                              });
                              Future.delayed(
                                  const Duration(milliseconds: 1000), () {
                                setState(() {
                                  isScreenShotProcessing = false;
                                });
                              });
                            });
                          },
                        ),
                        Container(
                          width: widthScreen * 0.08,
                        ),
                        GestureDetector(
                          onTap: () {
                            functions.sendAnalyticsWithOwnName("quoteBookmarkClicked");
                            HapticFeedback.lightImpact();
                            setState(() {
                              quoteQuery[index] = QuoteQuery(
                                quote: quoteQuery[index].quote,
                                authername: quoteQuery[index].authername,
                                userQuoteId: quoteQuery[index].userQuoteId,
                                isBookmarked: quoteQuery[index].isBookmarked == 1 ? 0 : 1,
                                isSeen: 1,
                                isCollected: quoteQuery[index].isCollected,
                              );
                              dbFunctions.updateUserQuoteStatus(
                                  quoteQuery[index].userQuoteId,
                                  quoteQuery[index].isCollected,
                                  quoteQuery[index].isSeen,
                                  quoteQuery[index].isBookmarked);
                            });
                          },
                          child: ImageIcon(
                            AssetImage(onScreenQuote.isBookmarked == 1
                                ? 'assets/signs2/bookmark_full.png'
                                : 'assets/signs2/bookmark.png'),
                            size: widthScreen * 0.08,
                            color: themesChanger.getButtonColor(),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
        }
      },
    );
  }


  Future _onSelectNotification(String payload) async {// for launching app on notification click
    print('onSelection payload:: $payload');
    setState(() {
      _openWithWidgetData = false;
      _isShuffled = false;
      quoteQuery = [];
    });
    if(payload == "") {
      print("Payload null but app is opened by fb notification");
      if(fbMessageTitle == null) {
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  NewHomePage(notifiedQuoteId: int.parse("100000"))
          ),
        );
      } else {
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  NewHomePage()
          ),
        );
      }
    } else {
      return await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                NewHomePage(notifiedQuoteId: int.parse(payload))
          //SingleQuotePage(quoteId: int.parse(payload), appBarTitle: AppLocalizations.of(context).translate('inspirationalQuotes'))
        ),
      );
    }
  }

  Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async {// for launching app on notification click (iOS<10)
    print('Is first OPEN onDidRECEIVE: payload: $payload');
    setState(() {
      _openWithWidgetData = false;
      _isShuffled = false;
      quoteQuery = [];
    });
    return await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              NewHomePage(notifiedQuoteId: int.parse(payload))
        //SingleQuotePage(quoteId: int.parse(payload), appBarTitle: AppLocalizations.of(context).translate('inspirationalQuotes'))
      ),
    );
  }

  _getFirstOpenAfterNotification() {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid = AndroidInitializationSettings('app_icon'); //@mipmap/ic_launcher
    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _onSelectNotification);
  }

  _getPrefData(RemoteNotifyChanger remoteChanger, GeneralChanger generalChanger) async {
    //_isSeenGif = await SharedPreferencesHelper.getIsSeenGif();
    _openCount = await SharedPreferencesHelper.getAppOpenCount();
    print("Open count isss: $_openCount");
    if(_openCount == 0) {
      print("Open count is: $_openCount");
      await SharedPreferencesHelper.setAppOpenCount(1);//when user come this page, increase open count, just for first open
      initPlatformState();//Set os version release
/*      Future.delayed(Duration(milliseconds: 2000), () async {
        SharedPreferencesHelper.setVersionRelease(int.parse(_deviceData['version.release']));
        print("Version release from pref: ${await SharedPreferencesHelper.getVersionRelease()}");
      });*/
      if (_openCount == 0 && !generalChanger.getWasDirectedToPremium() && remoteChanger.getShowPremiumPage() == 1) {
        generalChanger.setWasDirectedToPremium(true);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PremiumPayPage(
                    directedPage: 'First_Open_Direct')));
      }
    } else {
      print("Open count: $_openCount, isOpenCountIncreased: ${MyStatics.isOpenCountIncreased}");
      if(!MyStatics.isOpenCountIncreased) {
        MyStatics.isOpenCountIncreased = true;
        Future.delayed(Duration(milliseconds: 1000), (){
          if(remoteChanger.getShowPremiumOnLaunchArray().contains(_openCount) && !generalChanger.getIsPremiumOrSubscribed()) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PremiumPayPage(
                        directedPage: 'On_App_Launch')));
          }
        });
      }
    }
    _isExperienced = await SharedPreferencesHelper.getIsUserExperienceSeen();
/*    _isAnyNewSettingAvailable = await SharedPreferencesHelper.getNewSettingsAlert();
    _isAnyNewThemeAvailable = await SharedPreferencesHelper.getNewThemesAlert();
    _isAnyNewTopicAvailable = await SharedPreferencesHelper.getNewTopicsAlert();*/
  }

  _sendFirstOpenEvent() async {
    if (await SharedPreferencesHelper.getIsHomeScreenOpenBefore() == false) {
      print('Sending first home screen open event...');
      functions.sendAnalyticsWithOwnName('first_homeScreen_open');
      /*if(await SharedPreferencesHelper.getIsRecordDbCreatedEvent() == false) {
        functions.sendAnalyticsWithFunnelName('new_db_NOT_ready');
        print("DB_NOT_ready event has been sent");
      } else {
        functions.sendAnalyticsWithFunnelName('new_db_ready');
        print("DB_ready event has been sent");
      }*/
      await SharedPreferencesHelper.setIsHomeScreenFirstOpen(true);
    }
  }

  _updateExpiryDateOnPref() async {
    await FlutterInappPurchase.instance.initConnection;
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult != ConnectivityResult.none) {
      if (await SharedPreferencesHelper.getIsPremiumStatus() == false)
        Platform.isAndroid
            ? await functions.getExpiryDateForAndroid()
            : await functions.getExpiryDateForIOS();
      print('EXPIRY TIME ON PREF WAS UPDATED');
    }
    generalChanger.setOnLaunch(false);
  }

  _queryAllSectionsAndQuotesOfUser() async {
    final sections = await dbProvider.queryAllUnlockedAndSelectedSections();
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    print(sections.length);
    print('Open count: $_openCount');
    if(widget.notifiedQuoteId != null && widget.notifiedQuoteId != 0 && widget.notifiedQuoteId != 100000) {//id 100000 means comes from firebase messaging
      print("id: ${widget.notifiedQuoteId} was added to first line...");
      final singleQuote = await dbProvider.querySingleQuote(widget.notifiedQuoteId);
      quoteQuery.insert(0, QuoteQuery(
        quote: singleQuote[0]['quote'],
        userQuoteId: singleQuote[0]['quoteid'],
        authername: singleQuote[0]['authorname'],
        isCollected: singleQuote[0]['quoteCollectedStatus'],
        isBookmarked: singleQuote[0]['quoteBookmarkedStatus'],
        isSeen: singleQuote[0]['isSeenStatus'],
      ));
      dbFunctions.updateUserQuoteStatus(
          singleQuote[0]['quoteid'],
          singleQuote[0]['quoteCollectedStatus'],
          1,
          singleQuote[0]['quoteBookmarkedStatus']);
      if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount)) {
        quoteQuery.insert(1, QuoteQuery(
          quote: singleQuote[0]['quote'],
          userQuoteId: singleQuote[0]['quoteid'],
          authername: singleQuote[0]['authorname'],
          isCollected: singleQuote[0]['quoteCollectedStatus'],
          isBookmarked: singleQuote[0]['quoteBookmarkedStatus'],
          isSeen: singleQuote[0]['isSeenStatus'],
        ));
      }
    } else if(widget.notifiedQuoteId == 100000 && fbMessageTitle == null) {
      quoteQuery.add(QuoteQuery(
        quote: fbMessageBody,
        userQuoteId: 100000,
        authername: "",
        isCollected: 0,
        isBookmarked: 0,
        isSeen: 0,
      ));
    } else {
      print("This page is not being opened by notification...");
      try {
        print("id: $widgetId was added to first line...");
        if(widgetId != null && widgetId != '2352') {
          final singleWidgetQuote = await dbProvider.querySingleQuote(int.parse(widgetId));
          quoteQuery.insert(0, QuoteQuery(
            quote: singleWidgetQuote[0]['quote'],
            userQuoteId: singleWidgetQuote[0]['quoteid'],
            authername: singleWidgetQuote[0]['authorname'],
            isCollected: singleWidgetQuote[0]['quoteCollectedStatus'],
            isBookmarked: singleWidgetQuote[0]['quoteBookmarkedStatus'],
            isSeen: singleWidgetQuote[0]['isSeenStatus'],
          ));
          dbFunctions.updateUserQuoteStatus(
              singleWidgetQuote[0]['quoteid'],
              singleWidgetQuote[0]['quoteCollectedStatus'],
              1,
              singleWidgetQuote[0]['quoteBookmarkedStatus']);
          if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount) && widget.notifiedQuoteId != 100000) {//if rate_us is seen on page_open, alternatively, set the first quote on the second line
            quoteQuery.insert(1, QuoteQuery(
              quote: singleWidgetQuote[0]['quote'],
              userQuoteId: singleWidgetQuote[0]['quoteid'],
              authername: singleWidgetQuote[0]['authorname'],
              isCollected: singleWidgetQuote[0]['quoteCollectedStatus'],
              isBookmarked: singleWidgetQuote[0]['quoteBookmarkedStatus'],
              isSeen: singleWidgetQuote[0]['isSeenStatus'],
            ));
          }
        }
      } catch (e) {
        print("Error on adding widget to first line $e");
      }
    }
    sections.forEach((section) async {
      final allQuotes = await dbProvider.queryAllQuotesOfAnySectionWithIsSeenData(section['sectionid']);
      allQuotes.forEach((quote) {
        if(quote['priority'] == 1) {
          var contain = priorityQuoteQuery.where((myQuote) => myQuote.userQuoteId == quote['quoteid']);
          if (contain.isEmpty) {
            priorityQuoteQuery.add(
                QuoteQuery(
                  quote: quote['quote'],
                  userQuoteId: quote['quoteid'],
                  authername: quote['authorname'],
                  isCollected: quote['quoteCollectedStatus'],
                  isBookmarked: quote['quoteBookmarkedStatus'],
                  isSeen: quote['isSeenStatus'],
                )
            );
            //print("${quote['quoteid']} added, priority length: ${priorityQuoteQuery.length}");
          } else {
            //print("${quote['quoteid']} was added before");
          }
        } else {
          var contain2 = quoteQuery.where((myQuote) => myQuote.userQuoteId == quote['quoteid']);
          if (contain2.isEmpty){
            quoteQuery.add(QuoteQuery(
              quote: quote['quote'],
              userQuoteId: quote['quoteid'],
              authername: quote['authorname'],
              isCollected: quote['quoteCollectedStatus'],
              isBookmarked: quote['quoteBookmarkedStatus'],
              isSeen: quote['isSeenStatus'],
            ));
            setState(() {
              itemCount = itemCount + 1;
            });
          } else {
            //print("${quote['quoteid']} was added before");
          }
        }
      });
      print('Other quote count is: $itemCount');
      if(priorityQuoteQuery.length > 10) shuffleMyArrayStartingFrom1(priorityQuoteQuery);
      if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount)) {
        shuffleMyArrayStartingFrom2(quoteQuery);
      } else {
        shuffleMyArrayStartingFrom1(quoteQuery);
      }
    });
    Future.delayed(const Duration(milliseconds: 3000), () async {
      if(priorityQuoteQuery.length > 0) {
        if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount)) {
          quoteQuery.insertAll(2, priorityQuoteQuery);//This condition means, first page is rate us block, and second one is the notification or widget quote
        } else {
          quoteQuery.insertAll(1, priorityQuoteQuery);
        }
        print("PRIORITY INSERTED");
      }
    });
    Future.delayed(const Duration(milliseconds: 5000), () async {
      if (itemCount < 20) _addExtraQuote();
    });
  }

  void shuffleMyArrayStartingFrom1(List list, [int start = 1, int end]) async {
    var random = Random();
    end ??= list.length;
    var length = end - start;
    while (length > 1) {
      var pos = random.nextInt(length);
      length--;
      var tmp1 = list[start + pos];
      list[start + pos] = list[start + length];
      list[start + length] = tmp1;
    }
    _isShuffled = true;
  }

  void shuffleMyArrayStartingFrom2(List list, [int start = 2, int end]) async {
    var random = Random();
    end ??= list.length;
    var length = end - start;
    while (length > 1) {
      var pos = random.nextInt(length);
      length--;
      var tmp1 = list[start + pos];
      list[start + pos] = list[start + length];
      list[start + length] = tmp1;
    }
    _isShuffled = true;
  }


  _addExtraQuote() async {
    final allQuotesWithSeen =
    await dbProvider.queryAllQuotesPreferencesOfUserWithSeen();
    for (int i = 0; i < 100; i++) {
      quoteQuery.add(QuoteQuery(
        quote: allQuotesWithSeen[i]['quote'],
        userQuoteId: allQuotesWithSeen[i]['quoteid'],
        authername: allQuotesWithSeen[i]['authorname'],
        isCollected: allQuotesWithSeen[i]['quoteCollectedStatus'],
        isBookmarked: allQuotesWithSeen[i]['quoteBookmarkedStatus'],
        isSeen: allQuotesWithSeen[i]['isSeenStatus'],
      ));
      setState(() {
        itemCount = itemCount + 1;
        print('Extra quote added: new item count: $itemCount');
      });
    }
    shuffleMyArrayStartingFrom2(quoteQuery);
  }


  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();

    final info = statuses[Permission.storage].toString();
    print(info);
    //_toastInfo(info);
  }

  _saveScreen() async {
    final uint8List = await screenshotController.capture();
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/image.png');
    final result = await ImageGallerySaver.saveImage(uint8List);
    print("Save Success: ${result['isSuccess']}");
    if(result['isSuccess']) {
      _toastInfo(AppLocalizations.of(context).translate('imageSaved'));
    } else {
      _toastInfo(AppLocalizations.of(context).translate('imageNotSaved'));
    }
  }


  _toastInfo(String info) {
    Fluttertoast.showToast(msg: info, toastLength: Toast.LENGTH_LONG);
  }
  List<String> imagePaths = [];


  void _takeScreenshot() async {
    final uint8List = await screenshotController.capture();
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/image.png');
    await file.writeAsBytes(uint8List);
    await Share.shareFiles([file.path], text: subject);
  }



  recommendThisApp() {
    final RenderBox box = context.findRenderObject() as RenderBox;

    Share.share('${AppLocalizations.of(context)
        .translate('recommend')}$kDynamicLinkIQ',
        subject: AppLocalizations.of(context).translate("inspirationalQuotes"),
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  leaveUsAReview() {
    Platform.isAndroid
        ? launch(kUrlPlayStore)
        : launch(kUrlAppStore);
  }



  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData = <String, dynamic>{};

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
        print("_deviceData: $deviceData");
/*        SharedPreferencesHelper.setVersionRelease(deviceData['version.release']);
        print("Version release from pref: ${await SharedPreferencesHelper.getVersionRelease()}");*/
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    setState(() {
      _deviceData = deviceData;
    });
    SharedPreferencesHelper.setVersionRelease(int.parse(_deviceData['version.release']));
    print("Version release from pref: ${await SharedPreferencesHelper.getVersionRelease()}");
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    print("ssssssss");
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  _firebaseMessagingHandle() {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) async {
      print("Notification got...: $message");
      if (message != null) {
        print("Not Null");
        if(message.notification.title == null) {
          fbMessageBody = message.notification.body;
          return await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    NewHomePage(notifiedQuoteId: int.parse("100000"))
            ),
          );
        } else {
          return await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    NewHomePage()
            ),
          );
        }
      } else {
        print("Message is null");
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      print("Notification got");
      print(notification.title);
      print(notification.body);
      fbMessageBody = notification.body;
      fbMessageTitle = notification.title;

      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'app_icon',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      print('A new onMessageOpenedApp event was published!');

      if(message.notification.title != null) {
        fbMessageBody = message.notification.body;
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  NewHomePage(notifiedQuoteId: int.parse("100000"))
          ),
        );
      } else {
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  NewHomePage()
          ),
        );
      }
    });
  }

  _nativeAdFB(RemoteNotifyChanger remoteChanger, double widthScreen, double heightScreen, int index) {
    print("In native ad, index: $index");
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    FacebookNativeAd myAd = FacebookNativeAd(
      //placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2964952163583650",
      placementId: remoteChanger.getFbNativePlacementId(),
      adType: NativeAdType.NATIVE_AD_VERTICAL,
      width: double.infinity,
      //height: 300,
      backgroundColor: Colors.blue,
      titleColor: Colors.white,
      descriptionColor: Colors.white,
      buttonColor: Colors.deepPurple,
      buttonTitleColor: Colors.white,
      buttonBorderColor: Colors.white,
      listener: (result, value) {
        print("FB Native Ad: $result --> $value");
        if(result == NativeAdResult.ERROR) {
          setState(() {
            _nativeAdIsLoaded = false;
            if(index.isEven || index % 3 == 0) {
              print("Index is even or 3x");
              if(!_nativeAdIsLoadedAtLeastOne) {
                _currentNativeAd = SuggestOtherApps(
                  appIconName: Platform.isIOS ? 'uf' : index % 4 == 0 ? 'uq' : 'uf',
                  motto: Platform.isIOS ? 'factsMotto1' : index % 4 == 0 ? 'quoteMotto1' : 'factsMotto1',
                  directedLink: Platform.isIOS ? kUFAppStoreUrl : index % 4 == 0 ? kQuotePlayStoreUrl : kUFPlayStoreUrl,
                  marginX: widthScreen*0.07,
                  marginY: heightScreen*0.68,
                );
              }
            } else {
              if(!_nativeAdIsLoadedAtLeastOne) {
                _currentNativeAd = Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SettingCard(imageName: 'recommend', text: 'recommendThisApp', func: recommendThisApp),
                  ],
                );
              }
            }
          });
        } else if (result == NativeAdResult.LOADED) {
          setState(() {
            _nativeAdIsLoaded = true;
            _nativeAdIsLoadedAtLeastOne = true;
          });
        }
      },
      keepExpandedWhileLoading: true,
      expandAnimationDuraion: 100,
    );
    if(_nativeAdIsLoaded) {
      setState(() {
        _currentNativeAd = FBNativeAds(
          nativeAd: myAd,
          marginX: widthScreen*0.04,//widthScreen*0.07,
          marginY: heightScreen*0.67,//heightScreen*0.68,
        );
      });
    } else {
      if(!_nativeAdIsLoadedAtLeastOne) {
        setState(() {
          if(index.isEven || index % 3 == 0) {
            print("Index is even");
            _currentNativeAd = SuggestOtherApps(
              appIconName: Platform.isIOS ? 'uf' : index % 4 == 0 ? 'uq' : 'uf',
              motto: Platform.isIOS ? 'factsMotto1' : index % 4 == 0 ? 'quoteMotto1' : 'factsMotto1',
              directedLink: Platform.isIOS ? kUFAppStoreUrl : index % 4 == 0 ? kQuotePlayStoreUrl : kUFPlayStoreUrl,
              marginX: widthScreen*0.07,
              marginY: heightScreen*0.68,
            );
          } else {
            print("Index is odd");
            _currentNativeAd = Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SettingCard(imageName: 'recommend', text: 'recommendThisApp', func: recommendThisApp),
              ],
            );
          }
        });
      }
    }
  }


  _showNativeBannerAd(RemoteNotifyChanger remoteNotifyChanger) {
    print("Showing native banner ad..");
    setState(() {
      _currentBannerAd = FacebookNativeAd(
        //placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2964953543583512",
        placementId: remoteNotifyChanger.getFbNativeBannerPlacementId(),
        adType: NativeAdType.NATIVE_BANNER_AD,
        bannerAdSize: NativeBannerAdSize.HEIGHT_50,
        width: double.infinity,
        backgroundColor: Colors.blue,
        titleColor: Colors.white,
        descriptionColor: Colors.white,
        buttonColor: Colors.deepPurple,
        buttonTitleColor: Colors.white,
        buttonBorderColor: Colors.white,
        listener: (result, value) {
          print("Native Banner Ad: $result --> $value");
        },
      );
    });
  }

  _showBannerAd(RemoteNotifyChanger remoteNotifyChanger) {
    print("Showing banner add");
    setState(() {
      _currentBannerAd = FacebookBannerAd(
        //placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2964944860251047", //testid
        placementId: remoteNotifyChanger.getFbBannerPlacementId(),
        bannerSize: BannerSize.STANDARD,
        listener: (result, value) {
          switch (result) {
            case BannerAdResult.ERROR:
              print("Banner Error: $value");
              setState(() {
                _currentBannerAd = Container();
              });
              break;
            case BannerAdResult.LOADED:
              print("Banner Loaded: $value");
              break;
            case BannerAdResult.CLICKED:
              print("Clicked: $value");
              break;
            case BannerAdResult.LOGGING_IMPRESSION:
              print("Logging Impression: $value");
              break;
          }
        },
      );
    });
  }

/*  void _loadInterstitialAd(RemoteNotifyChanger remoteChanger) {
    print("Trying to load interstitial in home page");
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    //newGeneralChanger.setIsInterstitialAdLoaded(false);//to avoid null value
    FacebookInterstitialAd.loadInterstitialAd(
      placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2650502525028617",
      //placementId: remoteChanger.getFbInterstitialPlacementId(),
      listener: (result, value) {
        print(">> FAN > Interstitial Ad: $result --> $value");
        if (result == InterstitialAdResult.LOADED){
          newGeneralChanger.setIsInterstitialAdLoaded(true);
        }
        /// Once an Interstitial Ad has been dismissed and becomes invalidated,
        /// load a fresh Ad by calling this function.
        if (result == InterstitialAdResult.DISMISSED &&
            value["invalidated"] == true) {
          newGeneralChanger.setIsInterstitialAdLoaded(false);
          _loadInterstitialAd(remoteChanger);
        }
        if(result == InterstitialAdResult.ERROR) {
          newGeneralChanger.setIsInterstitialAdLoaded(false);
        }
      },
    );
  }*/



/*  _loadAdmobNative(int index) {
    print("Loading native ad");
    _myNative.dispose();
    _isNativeAdLoaded = false;
    _myNative.load();
    nativeAdWidget = AdWidget(ad: _myNative);
    if(index != 0 && !_isNativeAdLoaded) {
      _setNativeAdAlternative(index);
    }
  }

  _setNativeAdAlternative(int index) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    _currentNativeAd = SuggestOtherApps(
      appIconName: Platform.isIOS ? 'uf' : index % 4 == 0 ? 'uq' : 'uf',
      motto: Platform.isIOS ? 'factsMotto1' : index % 4 == 0 ? 'quoteMotto1' : 'factsMotto1',
      directedLink: Platform.isIOS ? kUFAppStoreUrl : index % 4 == 0 ? kQuotePlayStoreUrl : kUFPlayStoreUrl,
      marginX: widthScreen*0.07,
      marginY: heightScreen*0.66,
    );
  }

  _setAdmobNativeAdListener() {
    _myNative = NativeAd(
      adUnitId: Platform.isAndroid ? kAdmob_Native_ads_ad_unit_id_android : kAdmob_Native_ads_ad_unit_id_ios,//NativeAd.testAdUnitId,//
      factoryId: 'adFactoryExample',
      request: AdRequest(),
      listener: NativeAdListener(
        // Called when an ad is successfully received.
        onAdLoaded: (Ad ad) {
          print('Ad loaded.');
          setState(() {
            _isNativeAdLoaded = true;
          });
        },
        // Called when an ad request failed.
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          // Dispose the ad here to free resources.
          ad.dispose();
          print('Native Ad failed to load: $error');
        },
        // Called when an ad opens an overlay that covers the screen.
        onAdOpened: (Ad ad) => print('Ad opened.'),
        // Called when an ad removes an overlay that covers the screen.
        onAdClosed: (Ad ad) => print('Ad closed.'),
        // Called when an impression occurs on the ad.
        onAdImpression: (Ad ad) => print('Ad impression.'),
        // Called when a click is recorded for a NativeAd.
        onNativeAdClicked: (NativeAd ad) => print('Ad clicked.'),
      ),
    );
  }

  Future<void> _createAdmobAnchoredBanner(BuildContext context) async {
    print("In _createAdmobAnchoredBanner func...");
    final AnchoredAdaptiveBannerAdSize size =
    await AdSize.getAnchoredAdaptiveBannerAdSize(
      Orientation.portrait,
      MediaQuery.of(context).size.width.truncate(),
    );

    if (size == null) {
      print('Unable to get height of anchored banner.');
      return;
    }

    final BannerAd banner = BannerAd(
      size: size,
      request: MyStatics.request,
      adUnitId: Platform.isAndroid ? kAdmob_Banner_ads_ad_unit_id_android : kAdmob_Banner_ads_ad_unit_id_ios,//BannerAd.testAdUnitId,//
      listener: BannerAdListener(
        onAdLoaded: (Ad ad) {
          print('$BannerAd banner ad loaded.');
          setState(() {
            _anchoredBanner = ad as BannerAd;
          });
        },
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          print('$BannerAd failedToLoad: $error');
          ad.dispose();
        },
        onAdOpened: (Ad ad) => print('$BannerAd onAdOpened.'),
        onAdClosed: (Ad ad) => print('$BannerAd onAdClosed.'),
      ),
    );
    return banner.load();
  }*/


/*  _changeAdWidgetToNormalWidget(int index) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    _currentNativeAd = SuggestOtherApps(
      appIconName: Platform.isIOS ? 'uf' : index % 4 == 0 ? 'uq' : 'uf',
      motto: Platform.isIOS ? 'factsMotto1' : index % 4 == 0 ? 'quoteMotto1' : 'factsMotto1',
      directedLink: Platform.isIOS ? kUFAppStoreUrl : index % 4 == 0 ? kQuotePlayStoreUrl : kUFPlayStoreUrl,
      marginX: widthScreen*0.07,
      marginY: heightScreen*0.68,
    );
  }*/

}
