import 'dart:ui';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:connectivity/connectivity.dart';
import 'package:facebook_audience_network/ad/ad_interstitial.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/screens/quotes_bubble.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:motilife/prefs.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/popups/warning_popup.dart';
import 'package:motilife/onboardingScreens/premium_pay.dart';
import '../DbProvider.dart';
import '../localizations.dart';
import 'dart:io' show Directory, Platform;
import 'dart:async';

import '../main.dart';
import 'feed_quotes_page.dart';

final functions = AllFunctions();
final dbFunctions = DbFunctions();

class ThemesTab extends StatefulWidget {
  @override
  _ThemesTabState createState() => _ThemesTabState();
}



class _ThemesTabState extends State<ThemesTab> with SingleTickerProviderStateMixin {
  final dbProvider = DatabaseProvider.instance;
  FirebaseAnalytics analytics = FirebaseAnalytics();
  List<String> themeProperties;
  AudioPlayer advancedPlayer = AudioPlayer();
  String selectedTheme = 'a1';
  String selectedLiveTheme = "live1";
  var connectivityResult;
  List<ThemeUserData> themeQuery = [];
  List<ThemeUserData> themeQueryPremium = [];
  List<ThemeUserData> liveThemeQuery = [];
  List<ThemeUserData> liveThemeQueryPremium = [];
  //ChewieController _chewieController;
  Timer _timer;
  int _start = 6;
  bool _allowLiveThemeClick = true;
  bool _cantPlayThemeTriggered = false;
  ThemeUserData _theme;
  bool _comeFromDoubleClick;
  bool _comeFromLive;
  bool _isThemeReward = true;
  String _musicUrl;

  @override
  void initState() {
    super.initState();
    _getThemeData();
    _getLiveThemeData();
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    final musicChanger = Provider.of<MusicChanger>(context, listen: false);
    advancedPlayer = musicChanger.getAdvancedPlayer();
    _setThemeChanger();
  }

  _clearCache() async {
    print("Clearing app cache");
    var appDir = (await getTemporaryDirectory()).path;
    new Directory(appDir).delete(recursive: true);
  }

  _setThemeChanger() {
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    themeProperties = themesChanger.getThemeValues();
    newGeneralChanger.getUsingLiveTheme() ? selectedLiveTheme = themeProperties[11] :  selectedTheme = themeProperties[11];
  }

  @override
  void dispose() {
    // TODO: implement dispose
    if(_timer != null) _timer.cancel();
    streamSubscription.cancel();
    print("Theme page disposed");
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    ThemeChanger themesChanger = Provider.of<ThemeChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    final remoteNotifyChanger = Provider.of<RemoteNotifyChanger>(context);
    final musicChanger = Provider.of<MusicChanger>(context);
    final fontChanger = Provider.of<FontChanger>(context);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final orientation = MediaQuery.of(context).orientation;
    return WillPopScope(
      onWillPop: () async {
        themesChanger.getIsThemeChanged() ? Navigator.of(context).pushAndRemoveUntil(SlideRightRoute(page: FeedQuotesPage()), (Route<dynamic> route) => false) : Navigator.pop(context);
        themesChanger.setIsThemeChanged(false);//to decide back button's func, if changed, then reload the home page
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
                  title: Text(
                    AppLocalizations.of(context).translate('themes'),
                    style: TextStyle(
                      fontFamily: kDefaultFontName,
                      color: Colors.white,
                      fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  toolbarHeight: heightScreen * 0.07,
                  backgroundColor: Colors.black,
                  centerTitle: true,
                  elevation: 0.0,
                  leading: IconButton(
                    onPressed: () {
                      themesChanger.getIsThemeChanged() ? Navigator.of(context).pushAndRemoveUntil(SlideRightRoute(page: remoteNotifyChanger.getShowFeed() ? FeedQuotesPage() : NewHomePage()), (Route<dynamic> route) => false) : Navigator.pop(context);
                      themesChanger.setIsThemeChanged(false);//to decide back button's func, if changed, then reload the home page
                    },
                    icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
                  ),
                ),
          body: Container(
              height: heightScreen * 1,
              width: widthScreen * 1,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(newGeneralChanger.getUsingLiveTheme() ? 'assets/themes/$selectedLiveTheme.jpg' : 'assets/themes/$selectedTheme.jpg'),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.6), BlendMode.darken),
                ),
              ),
              child: Stack(
                children: [
                  BackdropFilter(
                    filter: newGeneralChanger.getUsingLiveTheme() ? ImageFilter.blur(sigmaX: 0, sigmaY: 0) : ImageFilter.blur(sigmaX: kSigmaX, sigmaY: kSigmaY),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          generalChanger.getIsPremiumOrSubscribed() ? Container(height: heightScreen * 0.08) :  GestureDetector(
                            onTap: () {
                              functions.sendAnalyticsWithOwnName('Themes_Premium_Button');
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PremiumPayPage(
                                          directedPage: 'Themes_Premium_Button')));
                            },
                            child: Container(
                              margin: EdgeInsets.only(left: widthScreen * 0.03, right: widthScreen * 0.03, top: heightScreen*0.1, bottom: heightScreen*0.01),
                              padding: EdgeInsets.symmetric(horizontal: widthScreen * 0.06),
                              height: heightScreen * 0.12,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage('assets/realImages/premium2.png'),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    AppLocalizations.of(context).translate('upgradetopremium'),
                                    style: TextStyle(
                                      fontFamily: kDefaultFontName,
                                      fontSize: MediaQuery.of(context).size.width * 0.053,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(width: widthScreen * 0.09),
                                  Image.asset(
                                    'assets/signs2/next.png',
                                    scale: 2.5,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: heightScreen * 0.02),
                            height: generalChanger.getIsPremiumOrSubscribed() ? heightScreen * 0.55 : heightScreen * 0.44,
                            child: themeQuery.length != 0 ? GridView.builder(
                              itemCount: themeQuery.length,
                              scrollDirection: Axis.horizontal,
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  childAspectRatio: 1.6,
                                  crossAxisCount: (orientation == Orientation.portrait) ? 1 : 1),
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onDoubleTap: () async {
                                    functions.sendAnalyticsWithOwnName("Theme_Double_Click");
                                    print(themeQuery[index].themeName);
                                    functions.sendAnalyticsEventFromThemeClick(themeQuery[index].themeName);
                                    if (themeQuery[index].isPremiumTheme == 1 && themeQuery[index].isUnlockedTheme == 0) {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PremiumPayPage(
                                                    directedPage: 'Theme_Premium',
                                                  )));
                                    } else {
                                      if(themeQuery[index].isPremiumTheme == 0 && themeQuery[index].isUnlockedTheme == 0 && remoteNotifyChanger.getThemeRewardedJson().contains(generalChanger.getThemeSessionClickCount()) && themeQuery[index].themeName != themesChanger.getThemeValues()[11]) {
                                        _showInterstitialAd(false, generalChanger, newGeneralChanger);
                                        //_showAdmobInterstitialAd(false, generalChanger);
                                      }
                                      generalChanger.setThemeSessionClickCount(generalChanger.getThemeSessionClickCount() + 1);
                                      dbFunctions.updateUserThemeUnlockedStatus(themeQuery[index].themeName); //unlock new theme
                                      dbFunctions.updateUserThemeUnlockedStatus(themesChanger.getThemeName()); //lock old theme
                                      HapticFeedback.lightImpact();
                                      setThemeParam(ThemeUserData(
                                        themeName: themeQuery[index].themeName,
                                        isPremiumTheme: themeQuery[index].isPremiumTheme,
                                        a: themeQuery[index].a,
                                        r: themeQuery[index].r,
                                        g: themeQuery[index].g,
                                        b: themeQuery[index].b,
                                        xOffset1: themeQuery[index].xOffset1,
                                        yOffset1: themeQuery[index].yOffset1,
                                        blurRadius1: themeQuery[index].blurRadius1,
                                        aShadow1: themeQuery[index].aShadow1,
                                        rShadow1: themeQuery[index].rShadow1,
                                        gShadow1: themeQuery[index].gShadow1,
                                        bShadow1: themeQuery[index].bShadow1,
                                        isUnlockedTheme: themeQuery[index].isUnlockedTheme,
                                      ), true, false);
                                    }
                                  },
                                  onTap: () async {
                                    if (themeQuery[index].isPremiumTheme == 1 && themeQuery[index].isUnlockedTheme == 0) {
                                      print(themeQuery[index].themeName);
                                      functions.sendAnalyticsEventFromThemeClick(themeQuery[index].themeName);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PremiumPayPage(
                                                    directedPage: 'Theme_Premium',
                                                  )));
                                    } else {
                                      if(themeQuery[index].isPremiumTheme == 0 && themeQuery[index].isUnlockedTheme == 0 && remoteNotifyChanger.getThemeRewardedJson().contains(generalChanger.getThemeSessionClickCount()) && themeQuery[index].themeName != themesChanger.getThemeValues()[11]) {
                                        _showInterstitialAd(false, generalChanger, newGeneralChanger);
                                        //_showAdmobInterstitialAd(false, generalChanger);
                                      }
                                      generalChanger.setThemeSessionClickCount(generalChanger.getThemeSessionClickCount() + 1);
                                      dbFunctions.updateUserThemeUnlockedStatus(themeQuery[index].themeName); //unlock new theme
                                      dbFunctions.updateUserThemeUnlockedStatus(themesChanger.getThemeName()); //lock old theme
                                      HapticFeedback.lightImpact();
                                      setThemeParam(ThemeUserData(
                                        themeName: themeQuery[index].themeName,
                                        isPremiumTheme: themeQuery[index].isPremiumTheme,
                                        a: themeQuery[index].a,
                                        r: themeQuery[index].r,
                                        g: themeQuery[index].g,
                                        b: themeQuery[index].b,
                                        xOffset1: themeQuery[index].xOffset1,
                                        yOffset1: themeQuery[index].yOffset1,
                                        blurRadius1: themeQuery[index].blurRadius1,
                                        aShadow1: themeQuery[index].aShadow1,
                                        rShadow1: themeQuery[index].rShadow1,
                                        gShadow1: themeQuery[index].gShadow1,
                                        bShadow1: themeQuery[index].bShadow1,
                                        isUnlockedTheme: themeQuery[index].isUnlockedTheme,
                                      ), false, false);
                                    }
                                  },
                                  child: Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: widthScreen * 0.03,
                                        vertical: heightScreen*0.005),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: selectedTheme == themeQuery[index].themeName ? Colors.white : Colors.transparent,
                                          width: 1.0,
                                          style: BorderStyle.solid
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(5)),
                                      image: DecorationImage(
                                        image: AssetImage(
                                            'assets/themes/${themeQuery[index].themeName}.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    child: Stack(
                                      children: <Widget>[
                                        if (themeQuery[index].isPremiumTheme == 1 && themeQuery[index].isUnlockedTheme == 0)
                                          Positioned(
                                            top: 7,
                                            right: 7,
                                            child: ImageIcon(
                                              AssetImage('assets/signs2/lock.png'),
                                              size: MediaQuery.of(context).size.width * 0.04,
                                              color: Colors.white,
                                              //color: Colors.blueGrey,
                                            ),
                                          ),
                                        Center(
                                          child: Text('abcd',
                                              style: TextStyle(
                                                fontFamily: fontChanger.getFontValues()[1],
                                                fontSize: MediaQuery.of(context).size.width * double.parse(fontChanger.getFontValues()[2]),
                                                fontWeight: FontWeight.values[int.parse(fontChanger.getFontValues()[3])],
                                                color: Color.fromARGB(
                                                    themeQuery[index].a,
                                                    themeQuery[index].r,
                                                    themeQuery[index].g,
                                                    themeQuery[index].b),
                                                wordSpacing: double.parse(fontChanger.getFontValues()[5]),
                                                letterSpacing: double.parse(fontChanger.getFontValues()[6]),
                                                height: double.parse(fontChanger.getFontValues()[7]),
                                                shadows: [
                                                  Shadow(
                                                    offset: Offset(
                                                        themeQuery[index].xOffset1,
                                                        themeQuery[index].yOffset1),
                                                    blurRadius: themeQuery[index].blurRadius1,
                                                    color: Color.fromARGB(
                                                        themeQuery[index].aShadow1,
                                                        themeQuery[index].rShadow1,
                                                        themeQuery[index].gShadow1,
                                                        themeQuery[index].bShadow1),
                                                  ),
                                                ],
                                              )),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ) : CupertinoActivityIndicator(),
                          ),

                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                  height: heightScreen * 0.05,
                                margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.06),
                                padding: EdgeInsets.only(bottom: heightScreen*0.01),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      AppLocalizations.of(context).translate('sounds'),
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: kDefaultFontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: MediaQuery.of(context).size.width*0.047,
                                      ),
                                    ),
                                    SizedBox(),
                                  ],
                                ),
                              ),
                              Container(
                                height: heightScreen * 0.16,
                                margin: EdgeInsets.only(left: widthScreen*.03),
                                padding: EdgeInsets.only(bottom: heightScreen*0.03),
                                child: FutureBuilder(
                                  future: dbProvider.queryAllMusicsWithUserData(),//dbProvider.queryAllMusics(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return ListView.builder(
                                          itemCount: snapshot.data.length,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, index) {
                                            if(index == 0){
                                              return Row(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: [
                                                      GestureDetector(
                                                          onTap: () {
                                                            _onStopClick(musicChanger);
                                                          },
                                                          child: Padding(
                                                            padding: EdgeInsets.only(left: 8, right: 7, bottom: 8),
                                                            child: Container(
                                                              width: widthScreen*0.14,
                                                              height: widthScreen*0.14,
                                                              decoration: BoxDecoration(
                                                                shape: BoxShape.circle,
                                                                  image: DecorationImage(
                                                                      fit: BoxFit.fill,
                                                                      image: AssetImage(
                                                                        'assets/music_images/none.png',
                                                                      ),
                                                                  ),
                                                                border: Border.all(
                                                                    color: musicChanger.getPlayingMusic() == '' ? Colors.white : Colors.transparent,
                                                                    width: 1.0,
                                                                    style: BorderStyle.solid
                                                                ),
                                                              ),
                                                              child: Container(),
                                                            ),
                                                          )
                                                      ),
                                                      Container(
                                                        padding: EdgeInsets.symmetric(horizontal: widthScreen*0.02, vertical: heightScreen*0.005),
                                                        decoration: BoxDecoration(
                                                          color: musicChanger.getPlayingMusic() == '' ? Colors.white.withOpacity(0.15) : Colors.transparent,
                                                          borderRadius: BorderRadius.circular(5.0),
                                                        ),
                                                        child: Text(
                                                          AppLocalizations.of(context).translate('noMusic'),
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: Colors.white,
                                                            fontFamily: kDefaultFontName,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: MediaQuery.of(context).size.width*0.03,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: [
                                                      GestureDetector(
                                                          onTap: () async {
                                                            functions.sendAnalyticsEventFromMusicClick(snapshot.data[index]['musicUrl']);
                                                            connectivityResult =  await (Connectivity().checkConnectivity());
                                                            if (connectivityResult != ConnectivityResult.none) {
                                                              if(snapshot.data[index]['isUnlockedMusic'] == 1) {//it means if subscribed member
                                                                _onPlayClick(snapshot.data[index]['musicUrl']);
                                                              } else {
                                                                if(snapshot.data[index]['isPremiumMusic'] == 1) {
                                                                  Navigator.push(context, SlideLeftRoute(page: PremiumPayPage(directedPage: 'Premium_Music',),));
                                                                } else {
                                                                  if(remoteNotifyChanger.getMusicRewardedJson().contains(generalChanger.getMusicSessionClickCount()) && musicChanger.getPlayingMusic() != snapshot.data[index]['musicUrl']) {
                                                                    _showInterstitialAd(true, generalChanger, newGeneralChanger);
                                                                    //_showAdmobInterstitialAd(false, generalChanger);
                                                                  }
                                                                  if(musicChanger.getPlayingMusic() != snapshot.data[index]['musicUrl']) {
                                                                    generalChanger.setMusicSessionClickCount(generalChanger.getMusicSessionClickCount()+1);
                                                                    _onPlayClick(snapshot.data[index]['musicUrl']);
                                                                  }
                                                                }
                                                              }
                                                            } else {
                                                              showDialog(
                                                                  context: context,
                                                                  builder: (BuildContext
                                                                  context) {
                                                                    return WarningPopUp(
                                                                      text: AppLocalizations.of(context).translate('checkConnection'),);
                                                                  });
                                                            }
                                                          },
                                                          child: Padding(
                                                            padding: EdgeInsets.only(left: 7, right: 7, bottom: 8),
                                                            child: Container(
                                                                width: widthScreen*0.14,
                                                                height: widthScreen*0.14,
                                                              decoration: BoxDecoration(
                                                                shape: BoxShape.circle,
                                                                image: DecorationImage(
                                                                  fit: BoxFit.fill,
                                                                  image: AssetImage(
                                                                    'assets/music_images/${snapshot.data[index]['musicUrl']}.png',
                                                                  ),
                                                                ),
                                                                border: Border.all(
                                                                    color: musicChanger.getPlayingMusic() == snapshot.data[index]['musicUrl'] ? Colors.white : Colors.transparent,
                                                                    width: 1.0,
                                                                    style: BorderStyle.solid
                                                                ),
                                                              ),
                                                              child: (musicChanger.getPlayingMusic() == snapshot.data[index]['musicUrl'] && advancedPlayer.state != PlayerState.PLAYING) ?
                                                                Theme(data: ThemeData(cupertinoOverrideTheme: CupertinoThemeData(brightness: Brightness.dark)),
                                                                    child: CupertinoActivityIndicator()) :
                                                              generalChanger.getIsPremiumOrSubscribed() ? Container() : snapshot.data[index]['isPremiumMusic'] == 1 ? Transform.scale(
                                                                  scale: 0.38,
                                                                  child: ImageIcon(AssetImage('assets/signs2/lock.png'),
                                                                      color: Colors.white)) : Container(),
                                                            ),
                                                          )
                                                      ),
                                                      Container(
                                                        padding: EdgeInsets.symmetric(horizontal: widthScreen*0.02, vertical: heightScreen*0.005),
                                                        decoration: BoxDecoration(
                                                          color: musicChanger.getPlayingMusic() == snapshot.data[index]['musicUrl'] ? Colors.white.withOpacity(0.15) : Colors.transparent,
                                                          borderRadius: BorderRadius.circular(5.0),
                                                        ),
                                                        child: Text(
                                                          '${snapshot.data[index]['musicName']}',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            fontFamily: kDefaultFontName,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: MediaQuery.of(context).size.width*0.03,
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              );
                                            } else {
                                              return Column(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: [
                                                  GestureDetector(
                                                      onTap: () async {
                                                        functions.sendAnalyticsEventFromMusicClick(snapshot.data[index]['musicUrl']);
                                                        connectivityResult =  await (Connectivity().checkConnectivity());
                                                        if (connectivityResult != ConnectivityResult.none) {
                                                          if(snapshot.data[index]['isUnlockedMusic'] == 1) {//it means if subscribed member
                                                            _onPlayClick(snapshot.data[index]['musicUrl']);
                                                          } else {
                                                            if(snapshot.data[index]['isPremiumMusic'] == 1) {
                                                              Navigator.push(context, SlideLeftRoute(page: PremiumPayPage(directedPage: 'Premium_Music',),));
                                                            } else {
                                                              if(remoteNotifyChanger.getMusicRewardedJson().contains(generalChanger.getMusicSessionClickCount()) && musicChanger.getPlayingMusic() != snapshot.data[index]['musicUrl']) {
                                                                _showInterstitialAd(true, generalChanger, newGeneralChanger);
                                                                //_showAdmobInterstitialAd(false, generalChanger);
                                                              }
                                                              if(musicChanger.getPlayingMusic() != snapshot.data[index]['musicUrl']) {
                                                                generalChanger.setMusicSessionClickCount(generalChanger.getMusicSessionClickCount()+1);
                                                                _onPlayClick(snapshot.data[index]['musicUrl']);
                                                              }
                                                            }
                                                          }
                                                     } else {
                                                          showDialog(
                                                              context: context,
                                                              builder: (BuildContext
                                                              context) {
                                                                return WarningPopUp(
                                                                  text: AppLocalizations.of(context).translate('checkConnection'),);
                                                              });
                                                        }
                                                      },
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: 7, right: 7, bottom: 8),
                                                        child: Container(
                                                          width: widthScreen*0.14,
                                                          height: widthScreen*0.14,
                                                          decoration: BoxDecoration(
                                                            shape: BoxShape.circle,
                                                              image: DecorationImage(
                                                                fit: BoxFit.fill,
                                                                image: AssetImage(
                                                                  'assets/music_images/${snapshot.data[index]['musicUrl']}.png',
                                                                ),
                                                              ),
                                                            border: Border.all(
                                                                color: musicChanger.getPlayingMusic() == snapshot.data[index]['musicUrl'] ? Colors.white : Colors.transparent,
                                                                width: 1.0,
                                                                style: BorderStyle.solid
                                                            ),
                                                          ),
                                                          child: (musicChanger.getPlayingMusic() == snapshot.data[index]['musicUrl'] && advancedPlayer.state != PlayerState.PLAYING) ?
                                                            Theme(data: ThemeData(cupertinoOverrideTheme: CupertinoThemeData(brightness: Brightness.dark)),
                                                                child: CupertinoActivityIndicator()) :
                                                          generalChanger.getIsPremiumOrSubscribed() ? Container() : snapshot.data[index]['isPremiumMusic'] == 1 ? Transform.scale(
                                                            scale: 0.38,
                                                              child: ImageIcon(AssetImage('assets/signs2/lock.png'),
                                                                  color: Colors.white)) : Container(),
                                                        ),
                                                      )
                                                  ),
                                                  Container(
                                                    padding: EdgeInsets.symmetric(horizontal: widthScreen*0.02, vertical: heightScreen*0.005),
                                                    decoration: BoxDecoration(
                                                      color: musicChanger.getPlayingMusic() == snapshot.data[index]['musicUrl'] ? Colors.white.withOpacity(0.15) : Colors.transparent,
                                                      borderRadius: BorderRadius.circular(5.0),
                                                    ),
                                                    child: Text(
                                                      '${snapshot.data[index]['musicName']}',
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(
                                                        fontFamily: kDefaultFontName,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: MediaQuery.of(context).size.width*0.03,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              );
                                            }
                                          });
                                    } else {
                                      return Center(
                                        child: CupertinoActivityIndicator(),
                                      );
                                    }
                                  },
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    height: heightScreen * 0.045,
                                    margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.06),
                                    padding: EdgeInsets.only(bottom: heightScreen*0.01),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context).translate('fonts'),
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: kDefaultFontName,
                                            fontWeight: FontWeight.w500,
                                            fontSize: MediaQuery.of(context).size.width*0.047,
                                          ),
                                        ),
                                        SizedBox(),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: heightScreen * 0.09,
                                    margin: EdgeInsets.only(left: widthScreen*.03),
                                    padding: EdgeInsets.only(bottom: heightScreen*0.02),
                                    child: FutureBuilder(
                                        future: dbProvider.queryAllFontsWithUserData(),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            return ListView.builder(
                                                itemCount: snapshot.data.length,
                                                scrollDirection: Axis.horizontal,
                                                itemBuilder: (context, index) {
                                                  return GestureDetector(
                                                    onTap: () {
                                                      functions.sendAnalyticsEventFromFontClick(snapshot.data[index]['fontName']);
                                                      HapticFeedback.lightImpact();
                                                      if((snapshot.data[index]['isPremiumFont'] == 1)) {
                                                        if(snapshot.data[index]['isUnlockedFont'] != 1) {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => PremiumPayPage(
                                                                      directedPage: 'Fonts_Premium_Button')));
                                                        } else {
                                                          _setFontData(FontData(
                                                            fontId: snapshot.data[index]['fontId'],
                                                            fontName: snapshot.data[index]['fontName'],
                                                            fontSize: snapshot.data[index]['fontSize'],
                                                            fontWeight: snapshot.data[index]['fontWeight'],
                                                            isPremiumFont: snapshot.data[index]['isPremiumFont'],
                                                            wordSpacing: snapshot.data[index]['wordSpacing'],
                                                            letterSpacing: snapshot.data[index]['letterSpacing'],
                                                            height: snapshot.data[index]['height'],
                                                          ), fontChanger, musicChanger, false, themesChanger);
                                                        }
                                                      } else {
                                                        _setFontData(FontData(
                                                          fontId: snapshot.data[index]['fontId'],
                                                          fontName: snapshot.data[index]['fontName'],
                                                          fontSize: snapshot.data[index]['fontSize'],
                                                          fontWeight: snapshot.data[index]['fontWeight'],
                                                          isPremiumFont: snapshot.data[index]['isPremiumFont'],
                                                          wordSpacing: snapshot.data[index]['wordSpacing'],
                                                          letterSpacing: snapshot.data[index]['letterSpacing'],
                                                          height: snapshot.data[index]['height'],
                                                        ), fontChanger, musicChanger, false, themesChanger);
                                                      }
                                                    },
                                                    child: Card(
                                                            elevation: 0,
                                                            color: Colors.transparent,
                                                            child: Column(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                Column(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  children: [
                                                                    Container(
                                                                      padding: EdgeInsets.all(widthScreen*.01),
                                                                      decoration: int.parse(fontChanger.getFontValues()[0]) == snapshot.data[index]['fontId'] ?
                                                                      BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(heightScreen*0.005),
                                                                        color: Colors.white,
                                                                      ) : BoxDecoration(),
                                                                      child: Align(
                                                                        alignment: Alignment.bottomCenter,
                                                                        child: Text(
                                                                          snapshot.data[index]['displayName'],
                                                                          textAlign: TextAlign.center,
                                                                          style: TextStyle(
                                                                            fontFamily: snapshot.data[index]['fontName'],
                                                                            fontSize: MediaQuery.of(context).size.width * 0.03,
                                                                            fontWeight: FontWeight.w500,
                                                                            color: int.parse(fontChanger.getFontValues()[0]) == snapshot.data[index]['fontId'] ? Colors.black : Colors.white,
                                                                            //height: snapshot.data[index]['height'],
                                                                            letterSpacing: snapshot.data[index]['letterSpacing'],
                                                                            //wordSpacing: snapshot.data[index]['wordSpacing'],
                                                                          )
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                Container(
                                                                  height: snapshot.data[index]['isPremiumFont'] == 1 && snapshot.data[index]['isUnlockedFont'] == 0 ? heightScreen * 0.015 : 0,
                                                                  child: Align(
                                                                    alignment: Alignment.bottomCenter,
                                                                    child: ImageIcon(
                                                                      AssetImage('assets/signs2/lock.png'),
                                                                      //size: MediaQuery.of(context).size.width * 0.025,
                                                                      color: Colors.grey,
                                                                      //color: Colors.blueGrey,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                  );
                                                });
                                          } else {
                                            return Center(
                                              child: CupertinoActivityIndicator(),
                                            );
                                          }
                                        }
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  _setFontData(FontData fontData, FontChanger fontChanger, MusicChanger musicChanger, bool isComeFromRewarded, ThemeChanger themesChanger) async {
    HapticFeedback.lightImpact();
    await SharedPreferencesHelper.setFontProperties([
      '${fontData.fontId}',
      '${fontData.fontName}',
      '${fontData.fontSize}',
      '${fontData.fontWeight}',
      '${fontData.isPremiumFont}',
      '${fontData.wordSpacing}',
      '${fontData.letterSpacing}',
      '${fontData.height}',
    ]);
    List<String> fontProperties = await SharedPreferencesHelper.getFontProperties();
    setState(() {
      fontChanger.setFontValues(fontProperties);
      fontChanger.setFontId(fontData.fontId);
      themesChanger.setTextStyle(TextStyle(
        fontFamily: fontChanger.getFontValues()[1],
        fontSize: MediaQuery.of(context).size.width * double.parse(fontChanger.getFontValues()[2]),
        fontWeight: FontWeight.values[int.parse(fontChanger.getFontValues()[3])],
        color: Color.fromARGB(int.parse(themesChanger.getThemeValues()[0]), int.parse(themesChanger.getThemeValues()[1]), int.parse(themesChanger.getThemeValues()[2]), int.parse(themesChanger.getThemeValues()[3])),
        wordSpacing: double.parse(fontChanger.getFontValues()[5]),
        letterSpacing: double.parse(fontChanger.getFontValues()[6]),
        height: double.parse(fontChanger.getFontValues()[7]),
        shadows: [
          Shadow(
            offset: Offset(double.parse(themesChanger.getThemeValues()[8]), double.parse(themesChanger.getThemeValues()[9])),
            blurRadius: double.parse(themesChanger.getThemeValues()[10]),
            color: Color.fromARGB(int.parse(themesChanger.getThemeValues()[4]), int.parse(themesChanger.getThemeValues()[5]), int.parse(themesChanger.getThemeValues()[6]), int.parse(themesChanger.getThemeValues()[7])),
          ),
        ],
      ));
      themesChanger.setTextStyleAuthorName(TextStyle(
        fontFamily: fontChanger.getFontValues()[1],
        fontSize: MediaQuery.of(context).size.width * double.parse(fontChanger.getFontValues()[2]) * 0.5,
        fontWeight: FontWeight.values[int.parse(fontChanger.getFontValues()[3]) -1],
        color: Color.fromARGB(int.parse(themesChanger.getThemeValues()[0]), int.parse(themesChanger.getThemeValues()[1]), int.parse(themesChanger.getThemeValues()[2]), int.parse(themesChanger.getThemeValues()[3])),
        wordSpacing: double.parse(fontChanger.getFontValues()[5]) * 0.75,
        letterSpacing: double.parse(fontChanger.getFontValues()[6]) * 0.75,
        height: double.parse(fontChanger.getFontValues()[7]) * 0.75,
        shadows: [
          Shadow(
            offset: Offset(double.parse(themesChanger.getThemeValues()[8]), double.parse(themesChanger.getThemeValues()[9])),
            blurRadius: double.parse(themesChanger.getThemeValues()[10]),
            color: Color.fromARGB(int.parse(themesChanger.getThemeValues()[4]), int.parse(themesChanger.getThemeValues()[5]), int.parse(themesChanger.getThemeValues()[6]), int.parse(themesChanger.getThemeValues()[7])),
          ),
        ],
      ));
      themesChanger.setButtonColor(Color.fromARGB(int.parse(themesChanger.getThemeValues()[0]), int.parse(themesChanger.getThemeValues()[1]), int.parse(themesChanger.getThemeValues()[2]), int.parse(themesChanger.getThemeValues()[3])));
    });
    // if(isComeFromRewarded && Platform.isAndroid) Navigator.pop(context);
    if(isComeFromRewarded) Navigator.pop(context);
    functions.playMusic(musicChanger);
  }

  _onPlayClick(String musicName) async {
    final musicChanger = Provider.of<MusicChanger>(context, listen: false);
    HapticFeedback.lightImpact();
    advancedPlayer.setUrl('$kCloudFrontMusicUrl/$musicName.mp3');
    advancedPlayer.resume();
    advancedPlayer.setReleaseMode(ReleaseMode.LOOP);
    advancedPlayer.setPlaybackRate(1.0);
    musicChanger.setAdvancedPlayer(advancedPlayer);
    await SharedPreferencesHelper.setMusicPreference(musicName);
    musicChanger.setPlayingMusic(musicName);
    //streamController.add(0);
  }

  _onStopClick(MusicChanger musicChanger) async {
    HapticFeedback.lightImpact();
    advancedPlayer.stop();
    print(advancedPlayer.state);
    musicChanger.setAdvancedPlayer(advancedPlayer);
    await SharedPreferencesHelper.setMusicPreference('');
    musicChanger.setPlayingMusic('');
    //print(advancedPlayer.state);
  }

  setThemeParam(ThemeUserData theme, bool comeFromDoubleClick, bool comeFromLive) async {
    selectedTheme = theme.themeName;
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    final musicChanger = Provider.of<MusicChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    final fontChanger = Provider.of<FontChanger>(context, listen: false);
    final remoteNotifyChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);

    Future.delayed(Duration(milliseconds: 100), () {
      newGeneralChanger.setUsingLiveTheme(comeFromLive ? true : false);
    });


    await SharedPreferencesHelper.setIsUsingLiveTheme(comeFromLive ? true : false);

    await SharedPreferencesHelper.setThemeProperties([
      '${theme.a}',
      '${theme.r}',
      '${theme.g}',
      '${theme.b}',
      '${theme.aShadow1}',
      '${theme.rShadow1}',
      '${theme.gShadow1}',
      '${theme.bShadow1}',
      '${theme.xOffset1}',
      '${theme.yOffset1}',
      '${theme.blurRadius1}',
      '${theme.themeName}'
    ]);
    List<String> themeProperties =
        await SharedPreferencesHelper.getThemeProperties(theme.themeName);
    setState(() {
      themesChanger.setThemeValues(themeProperties);
      themesChanger.setThemeName(theme.themeName);
      themesChanger.setTextStyle(TextStyle(
        fontFamily: fontChanger.getFontValues()[1],
        fontSize: MediaQuery.of(context).size.width * double.parse(fontChanger.getFontValues()[2]),
        fontWeight: FontWeight.values[int.parse(fontChanger.getFontValues()[3])],
        color: Color.fromARGB(theme.a, theme.r, theme.g, theme.b),
        wordSpacing: double.parse(fontChanger.getFontValues()[5]),
        letterSpacing: double.parse(fontChanger.getFontValues()[6]),
        height: double.parse(fontChanger.getFontValues()[7]),
        shadows: [
          Shadow(
            offset: Offset(theme.xOffset1, theme.yOffset1),
            blurRadius: theme.blurRadius1,
            color: Color.fromARGB(
                theme.aShadow1, theme.rShadow1, theme.gShadow1, theme.bShadow1),
          ),
        ],
      ));
      themesChanger.setTextStyleAuthorName(TextStyle(
        fontFamily: fontChanger.getFontValues()[1],
        fontSize: MediaQuery.of(context).size.width * double.parse(fontChanger.getFontValues()[2]) * 0.5,
        fontWeight: FontWeight.values[int.parse(fontChanger.getFontValues()[3]) -1],
        color: Color.fromARGB(theme.a, theme.r, theme.g, theme.b),
        wordSpacing: double.parse(fontChanger.getFontValues()[5]) * 0.75,
        letterSpacing: double.parse(fontChanger.getFontValues()[6]) * 0.75,
        height: double.parse(fontChanger.getFontValues()[7]) * 0.75,
        shadows: [
          Shadow(
            offset: Offset(theme.xOffset1, theme.yOffset1),
            blurRadius: theme.blurRadius1,
            color: Color.fromARGB(
                theme.aShadow1, theme.rShadow1, theme.gShadow1, theme.bShadow1),
          ),
        ],
      ));
      themesChanger.setButtonColor(Color.fromARGB(theme.a, theme.r, theme.g, theme.b));

      themesChanger.setIsThemeChanged(true);
    });
    functions.playMusic(musicChanger);
    //streamController.add(0);
    if(comeFromDoubleClick) Navigator.of(context).pushAndRemoveUntil(SlideRightRoute(page: remoteNotifyChanger.getShowFeed() ? FeedQuotesPage() : NewHomePage()), (Route<dynamic> route) => false);
  }

  _getThemeData() async {
    final allThemes = await dbProvider.queryAllThemesWithUserData();
    allThemes.forEach((theme) {
      setState(() {
        if(theme['isPremiumTheme'] == 0) {
          themeQuery.add(
              ThemeUserData(
                  themeName: theme['themeName'],
                  isPremiumTheme: theme['isPremiumTheme'],
                  a: theme['a'],
                  r: theme['r'],
                  g: theme['g'],
                  b: theme['b'],
                  aShadow1: theme['aShadow1'],
                  rShadow1: theme['rShadow1'],
                  gShadow1: theme['gShadow1'],
                  bShadow1: theme['bShadow1'],
                  xOffset1: theme['xOffset1'],
                  yOffset1: theme['yOffset1'],
                  blurRadius1: theme['blurRadius1'],
                  isUnlockedTheme: theme['isUnlockedTheme']
              )
          );
        } else {
          themeQueryPremium.add(ThemeUserData(
              themeName: theme['themeName'],
              isPremiumTheme: theme['isPremiumTheme'],
              a: theme['a'],
              r: theme['r'],
              g: theme['g'],
              b: theme['b'],
              aShadow1: theme['aShadow1'],
              rShadow1: theme['rShadow1'],
              gShadow1: theme['gShadow1'],
              bShadow1: theme['bShadow1'],
              xOffset1: theme['xOffset1'],
              yOffset1: theme['yOffset1'],
              blurRadius1: theme['blurRadius1'],
              isUnlockedTheme: theme['isUnlockedTheme']
          )
          );
        }
      });
    });
    setState(() {
      themeQuery.insertAll(themeQuery.length, themeQueryPremium);
    });
  }

  _getLiveThemeData() async {
    final allThemes = await dbProvider.queryAllLiveThemesWithUserData();
    allThemes.forEach((theme) {
      print(theme['themeName']);
      setState(() {
        if(theme['isPremiumTheme'] == 0) {
          liveThemeQuery.add(
              ThemeUserData(
                  themeName: theme['themeName'],
                  isPremiumTheme: theme['isPremiumTheme'],
                  a: theme['a'],
                  r: theme['r'],
                  g: theme['g'],
                  b: theme['b'],
                  aShadow1: theme['aShadow1'],
                  rShadow1: theme['rShadow1'],
                  gShadow1: theme['gShadow1'],
                  bShadow1: theme['bShadow1'],
                  xOffset1: theme['xOffset1'],
                  yOffset1: theme['yOffset1'],
                  blurRadius1: theme['blurRadius1'],
                  isUnlockedTheme: theme['isUnlockedTheme']
              )
          );
        } else {
          liveThemeQueryPremium.add(ThemeUserData(
              themeName: theme['themeName'],
              isPremiumTheme: theme['isPremiumTheme'],
              a: theme['a'],
              r: theme['r'],
              g: theme['g'],
              b: theme['b'],
              aShadow1: theme['aShadow1'],
              rShadow1: theme['rShadow1'],
              gShadow1: theme['gShadow1'],
              bShadow1: theme['bShadow1'],
              xOffset1: theme['xOffset1'],
              yOffset1: theme['yOffset1'],
              blurRadius1: theme['blurRadius1'],
              isUnlockedTheme: theme['isUnlockedTheme']
          )
          );
        }
      });
    });
    setState(() {
      liveThemeQuery.insertAll(liveThemeQuery.length, liveThemeQueryPremium);
    });
  }

  _showInterstitialAd(bool isMusic, GeneralChanger generalChanger, NewGeneralChanger newGeneralChanger) {//FB Interstitial
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    if (newGeneralChanger.getIsInterstitialAdLoaded())
      FacebookInterstitialAd.showInterstitialAd();
    else {
      print("Interstial Ad not yet loaded! Change values that incremented on click");
      if(isMusic) {
        generalChanger.setMusicSessionClickCount(generalChanger.getMusicSessionClickCount() - 1);
      } else {
        generalChanger.setThemeSessionClickCount(generalChanger.getThemeSessionClickCount() - 1);
      }
      functions.loadInterstitialAd(remoteChanger, context);
    }
  }

/*
  void _showAdmobInterstitialAd(bool isMusic, GeneralChanger generalChanger) {
    print("trying to show interstitial ad...");
    if (MyStatics.interstitialAd == null) {
      print('Warning: attempt to show interstitial before loaded.');
      functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
      return;
    }
    MyStatics.interstitialAd.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (InterstitialAd ad) =>
          print('ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (InterstitialAd ad) {
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
      },
      onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
      },
    );
*//*    if(isMusic) {
      generalChanger.setMusicSessionClickCount(generalChanger.getMusicSessionClickCount() - 1);
    } else {
      generalChanger.setThemeSessionClickCount(generalChanger.getThemeSessionClickCount() - 1);
    }*//*
    MyStatics.interstitialAd.show();
    MyStatics.interstitialAd = null;
    functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
  }*/


/*  _runThemeStream(GeneralChanger generalChanger) {
    if(streamSubscription != null) {
      streamSubscription.cancel();//if running cancel it
    }
    final musicChanger = Provider.of<MusicChanger>(context, listen: false);
    Stream stream = streamController.stream;
    streamSubscription = stream.listen((value) {
      print('Value from controller: $value');
      if(value == 1) {
        functions.resumeMusic(musicChanger);
        print("Rewarded run...");
        if(_isThemeReward) {
          print("Theme session: ${generalChanger.getThemeSessionClickCount()}");
          generalChanger.setThemeSessionClickCount(generalChanger.getThemeSessionClickCount() + 1);
          print("Theme session: ${generalChanger.getThemeSessionClickCount()}");
          setThemeParam(_theme, _comeFromDoubleClick, _comeFromLive);
        } else {
          //Music rewarded
          generalChanger.setMusicSessionClickCount(generalChanger.getMusicSessionClickCount() + 1);
          _onPlayClick(_musicUrl);
        }
      }
    });
  }

  _isThemeRewarded(MusicChanger musicChanger, GeneralChanger generalChanger) {
    functions.resumeMusic(musicChanger);
    print("Rewarded run for theme...");
    print("Theme session: ${generalChanger.getThemeSessionClickCount()}");
    generalChanger.setThemeSessionClickCount(generalChanger.getThemeSessionClickCount() + 1);
    print("Theme session: ${generalChanger.getThemeSessionClickCount()}");
    setThemeParam(_theme, _comeFromDoubleClick, _comeFromLive);
  }


  _isMusicRewarded(MusicChanger musicChanger, GeneralChanger generalChanger) {
    print("Rewarded run for music...");
    functions.resumeMusic(musicChanger);
    //Music rewarded
    generalChanger.setMusicSessionClickCount(generalChanger.getMusicSessionClickCount() + 1);
    _onPlayClick(_musicUrl);
  }*/

}
