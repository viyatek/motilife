import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'dart:typed_data';
import 'package:screenshot/screenshot.dart';
import 'package:share/share.dart';
import 'dart:io';
import '../localizations.dart';
import '../prefs.dart';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart' as render;

final dbProvider = DatabaseProvider.instance;
final dbFunctions = DbFunctions();
bool barStatus = true;
bool isScreenShotProcessing = false;

class SingleQuotePage extends StatefulWidget {
  final int quoteId;
  final String appBarTitle;
  SingleQuotePage({this.quoteId, this.appBarTitle = 'Bookmarks'});
  @override
  _SingleQuotePageState createState() => _SingleQuotePageState();
}

class _SingleQuotePageState extends State<SingleQuotePage> {
  String subject = 'Subject';
  String text = 'Text Test';
  String imagePath = "";
  //File _imageFile;
  Uint8List _imageFile;
  List<String> themeProperties;
  ScreenshotController screenshotController = ScreenshotController();
  PageController _controller = PageController(
    initialPage: 0,
  );
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    super.initState();
    _controller = PageController();
    themeProperties = themesChanger.getThemeValues();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final dayNightChanger = Provider.of<DayNightChanger>(context);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    final themesChanger = Provider.of<ThemeChanger>(context);
    return Scaffold(
      key: _scaffoldKey,
      extendBodyBehindAppBar: true,
      appBar: Platform.isAndroid ? AppBar(
        centerTitle: true,
        toolbarHeight: generalChanger.getBarStatus() ? heightScreen * 0.07 : 0,
        title: Text(
          widget.appBarTitle,
          style: TextStyle(
            fontFamily: kDefaultFontName,
            fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ) : CupertinoNavigationBar(
        middle: Text(
          widget.appBarTitle,
          style: TextStyle(
            fontFamily: kDefaultFontName,
            color: Colors.white,
            fontSize: MediaQuery.of(context).size.width * 0.05,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Screenshot(
        controller: screenshotController,
        child: Container(
          height: heightScreen * 1,
          width: widthScreen * 1,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: buildGestureDetector(generalChanger, heightScreen, themesChanger, widthScreen)
        ),
      ),
    );
  }

  SafeArea buildGestureDetector(GeneralChanger barStatusChanger,
      double heightScreen, ThemeChanger themesChanger, double widthScreen) {
    final dayNightChanger = Provider.of<DayNightChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    final fontChanger = Provider.of<FontChanger>(context);
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          setState(() {
            barStatus = !barStatus;
          });
          barStatusChanger.setBarStatus(barStatus);
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          height: heightScreen * 0.85,
          child: FutureBuilder(
            future: dbProvider.querySingleQuote(widget.quoteId),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: barStatusChanger.getBarStatus()
                          ? 0
                          : heightScreen * 0.07,
                    ),
                    Container(
                      height: heightScreen * 0.05,
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(
//                                  minWidth: 300.0,
//                                  maxWidth: 300.0,
                        minHeight: heightScreen * 0.20,
                        maxHeight: heightScreen * 0.38,
                      ),
                      child: GestureDetector(
                        onLongPress: () {
                          subject = snapshot.data[0]['authorname'] != null ? "${snapshot.data[0]['quote']}\n\n${snapshot.data[0]['authorname']}" : "${snapshot.data[0]['quote']}";
                          Clipboard.setData(new ClipboardData(text: subject));
                          _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate('textCopied')),));
                        },
                        child: AutoSizeText(
                          snapshot.data[0]['quote'],
                          textAlign: TextAlign.center,
                          style: themesChanger.getTextStyle(),
                        ),
                      ),
                    ),
                    Container(
                      height: heightScreen * 0.03,
                    ),
                    snapshot.data[0]['authorname'] == null ? Container() : Container(
                      child: Text(
                        snapshot.data[0]['authorname'],
                        textAlign: TextAlign.center,
                        style: themesChanger.getTextStyleAuthorName(),
                      ),
                    ),
                    Container(
                      height: heightScreen * 0.13,
                    ),
                    isScreenShotProcessing
                        ? generalChanger.getIsPremiumOrSubscribed()
                        ? Row()
                        : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: heightScreen*0.001),
                            child: Image.asset(
                              'assets/signs2/watermark.png',
                              scale: 3,
                              color: themesChanger.getButtonColor(),
                            ),
                          ),
                          Container(
                            width: widthScreen * 0.02,
                          ),
                          Text(
                            AppLocalizations.of(context)
                                .translate('inspirationalQuotes'),
                            style: TextStyle(
                              fontFamily: 'Futura Book',
                              fontSize: MediaQuery.of(context).size.width * 0.06,
                              fontWeight: FontWeight.w500,
                              color: Color.fromARGB(int.parse(themeProperties[0]), int.parse(themeProperties[1]), int.parse(themeProperties[2]), int.parse(themeProperties[3])),
                              wordSpacing: double.parse(fontChanger.getFontValues()[5]),
                              letterSpacing: double.parse(fontChanger.getFontValues()[6]),
                              height: double.parse(fontChanger.getFontValues()[7]),
                              shadows: [
                                Shadow(
                                  offset: Offset(double.parse(themeProperties[8]), double.parse(themeProperties[9])),
                                  blurRadius: double.parse(themeProperties[10]),
                                  color: Color.fromARGB(int.parse(themeProperties[4]), int.parse(themeProperties[5]), int.parse(themeProperties[6]), int.parse(themeProperties[7])),
                                ),
                              ],
                            ),
                          ),
                        ])
                        : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                            child: ImageIcon(
                              AssetImage('assets/signs2/share.png'),
                              size: widthScreen * 0.08,
                              color: themesChanger.getButtonColor(),
                            ),
                            onTap: () {
                              if(!isScreenShotProcessing) {
                                setState(() {
                                  isScreenShotProcessing = true;
                                });
                                subject = snapshot.data[0]['authorname'] != null ? "${snapshot.data[0]['quote']}\n\n${snapshot.data[0]['authorname']}\n$kDynamicLinkIQ" : "${snapshot.data[0]['quote']}\n$kDynamicLinkIQ";
                                Future.delayed(
                                    const Duration(milliseconds: 300), () {
                                  //_takeScreenshotandShare();
                                  //_shareScreenshot();
                                  _takeScreenshot();
                                });
                                Future.delayed(
                                    const Duration(milliseconds: 600), () {
                                  setState(() {
                                    isScreenShotProcessing = false;
                                  });
                                  //Navigator.pop(context);
                                  print(
                                      'Process after screenshot: $isScreenShotProcessing');
                                });
                              }
                            }),
                        Container(
                          width: widthScreen * 0.08,
                        ),
                        GestureDetector(
                          child: ImageIcon(
                            AssetImage('assets/signs2/save.png'),
                            size: widthScreen * 0.08,
                            color: themesChanger.getButtonColor(),
                          ),
                          onTap: () async {
                            _requestPermission().catchError(
                                  (onError) {
                                print("ERROR...Cannot CANCEL Pending Notifications... $onError");
                              },
                            ).whenComplete(() {
                              setState(() {
                                isScreenShotProcessing = true;
                              });
                              Future.delayed(
                                  const Duration(milliseconds: 100), () {
                                _saveScreen();
                              });
                              Future.delayed(
                                  const Duration(milliseconds: 1000), () {
                                setState(() {
                                  isScreenShotProcessing = false;
                                });
                                //Navigator.pop(context);
                                print('Process after save image: $isScreenShotProcessing');
                              });
                            });
                          },
                        ),
                        Container(
                          width: widthScreen * 0.08,
                        ),
                        GestureDetector(
                          onTap: () {
                            HapticFeedback.lightImpact();
                            setState(() {
                              dbFunctions.updateUserQuoteStatus(
                                  snapshot.data[0]['quoteid'],
                                  snapshot.data[0]['quoteCollectedStatus'],
                                  snapshot.data[0]['isSeenStatus'],
                                  snapshot.data[0]['quoteBookmarkedStatus'] == 0
                                      ? 1
                                      : 0);
                            });
                          },
                          child: ImageIcon(
                            AssetImage(
                                snapshot.data[0]['quoteBookmarkedStatus'] == 1
                                    ? 'assets/signs2/bookmark_full.png'
                                    : 'assets/signs2/bookmark.png'),
                            size: widthScreen * 0.08,
                            color: themesChanger.getButtonColor(),
                          ),
                        ),
                      ],
                    ),
                  ],
                );
              } else {
                return Center(
                  child: CupertinoActivityIndicator(),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();

    final info = statuses[Permission.storage].toString();
    print(info);
    //_toastInfo(info);
  }

  _saveScreen() async {
    final uint8List = await screenshotController.capture();
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/image.png');
    final result = await ImageGallerySaver.saveImage(uint8List);
    print("Save Success: ${result['isSuccess']}");
    if(result['isSuccess']) {
      _toastInfo(AppLocalizations.of(context).translate('imageSaved'));
    } else {
      _toastInfo(AppLocalizations.of(context).translate('imageNotSaved'));
    }
  }

  _toastInfo(String info) {
    Fluttertoast.showToast(msg: info, toastLength: Toast.LENGTH_LONG);
  }

  void _takeScreenshot() async {
    final uint8List = await screenshotController.capture();
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/image.png');
    await file.writeAsBytes(uint8List);
    await Share.shareFiles([file.path], text: subject);
  }

/*  _takeScreenshotandShare() async {
*//*    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    generalChanger.setIsOnShareOrPurchaseProcess(true);*//*
    _imageFile = null;
    screenshotController
        .capture(delay: Duration(milliseconds: 10))
        .then((Uint8List image) async {
      setState(() {
        _imageFile = image;
      });
*//*      await Share.file('Anupam', 'screenshot.png', _imageFile, 'image/png',
          text: subject);*//*
      print('Screenshot was caught.');
    }).catchError((onError) {
      print(onError);
    });
  }*/
}
