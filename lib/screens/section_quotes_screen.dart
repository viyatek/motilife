import 'dart:async';
import 'package:facebook_audience_network/ad/ad_banner.dart';
import 'package:facebook_audience_network/ad/ad_interstitial.dart';
import 'package:facebook_audience_network/ad/ad_native.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:motilife/DbProvider.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'dart:typed_data';
import 'package:screenshot/screenshot.dart';
import 'package:share/share.dart';
import 'dart:io';
import '../localizations.dart';
import '../prefs.dart';
import 'dart:ui' as ui;
import 'package:flutter/rendering.dart' as render;

final dbProvider = DatabaseProvider.instance;
final dbFunctions = DbFunctions();
final functions = AllFunctions();
bool barStatus = true;
int itemCount = 1;
var query;
bool isScreenShotProcessing = false;

class SectionBasedWidget extends StatefulWidget {
  final String appBarTitle;
  final int sectionId;
  final bool isAllowedLiveTheme;
  SectionBasedWidget({this.appBarTitle = 'Today', this.sectionId, this.isAllowedLiveTheme});

  @override
  _SectionBasedWidgetState createState() => _SectionBasedWidgetState();
}

class _SectionBasedWidgetState extends State<SectionBasedWidget> {
  String subject = 'Subject';
  String text = 'Text Test';
  String imagePath = "";
  // File _imageFile;
  Uint8List _imageFile;
  UserQuote onScreenQuote;
  List<QuoteQuery> quoteQuery = [];
  List<String> themeProperties;
  ScreenshotController screenshotController = ScreenshotController();
  PageController _controller = PageController(
    initialPage: 0,
    viewportFraction: 2,
  );

  int _openCount = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Widget _currentAd = Container();

/*  BannerAd _anchoredBanner;
  bool _loadingAnchoredBanner = false;*/
  final functions = AllFunctions();

  _queryAllQuotesOfAnySection(int sectionId) async {
    //int count = await dbProvider.queryCountAllQuotesOfAnySectionWithUserData(sectionId);
    final allQuotes =
        await dbProvider.queryAllQuotesOfAnySectionWithUserData(sectionId);
    allQuotes.forEach((quote) {
      quoteQuery.add(QuoteQuery(
        quote: quote['quote'],
        userQuoteId: quote['quoteid'],
        authername: quote['authorname'],
        isCollected: quote['quoteCollectedStatus'],
        isBookmarked: quote['quoteBookmarkedStatus'],
        isSeen: quote['isSeenStatus'],
      ));
    });
    setState(() {
      itemCount = quoteQuery.length;
      print('item count is: $itemCount');
    });
  }

  _getPrefData() async {
    //_isSeenGif = await SharedPreferencesHelper.getIsSeenGif();
    _openCount = await SharedPreferencesHelper.getAppOpenCount();
  }

  @override
  void initState() {
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    super.initState();
    _getPrefData();
    _queryAllQuotesOfAnySection(widget.sectionId);
    themeProperties = themesChanger.getThemeValues();
    Future.delayed(Duration(milliseconds: 500), () {
      final remoteNotifyChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
      if(!generalChanger.getIsPremiumOrSubscribed()) {
        if (remoteNotifyChanger.getSectionRewardedJson().contains(generalChanger.getSectionSessionClickCount())) {
          _showFacebookInterstitialAd(remoteNotifyChanger, generalChanger);
          //_showAdmobInterstitialAd(generalChanger);
        } else {
          generalChanger.setSectionSessionClickCount(generalChanger.getSectionSessionClickCount()+1);
          print("json: ${remoteNotifyChanger.getSectionRewardedJson()}, count: ${generalChanger.getSectionSessionClickCount()}");
        }
        if(remoteChanger.getShowBannerAd()) {
          if(remoteChanger.getNativeBannerOrBanner()) {
            Platform.isAndroid ? _showNativeBannerAd(remoteChanger) : _showBannerAd(remoteChanger);
          } else {
            _showBannerAd(remoteChanger);
          }
        }
      }
    });
  }

  @override
  void dispose() {
    print("Section disposed");
    _controller.dispose();
    //MyStatics.interstitialAd.dispose();
    //_anchoredBanner?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    final themesChanger = Provider.of<ThemeChanger>(context);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
/*    if (!_loadingAnchoredBanner && remoteChanger.getShowBannerAd() && !generalChanger.getIsPremiumOrSubscribed()) {
      _loadingAnchoredBanner = true;
      _createAdmobAnchoredBanner(context);
    }*/
    return Scaffold(
      extendBodyBehindAppBar: true,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
            widget.appBarTitle,
          style: TextStyle(
            fontFamily: kDefaultFontName,
            color: Colors.white,
            fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
            fontWeight: FontWeight.w500,
          ),
        ),
        toolbarHeight: generalChanger.getBarStatus() ? heightScreen * 0.07 : 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Screenshot(
        controller: screenshotController,
        child: Container(
          height: heightScreen * 1,
          width: widthScreen * 1,
          decoration: newGeneralChanger.getUsingLiveTheme() && !isScreenShotProcessing && widget.isAllowedLiveTheme ? BoxDecoration() : BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
              Positioned(
                top: heightScreen-50,
                right: 0,//widthScreen*0.004,
                left: 0,//widthScreen*0.004,
                bottom: 0,//heightScreen*0.005,
                child: Opacity(
                    opacity: !isScreenShotProcessing ? 1 : 0,
                    child: _currentAd,
              )),
              buildGestureDetector(
                  generalChanger, heightScreen, themesChanger, widthScreen),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildGestureDetector(GeneralChanger generalChanger,
      double heightScreen, ThemeChanger themesChanger, double widthScreen) {
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    final fontChanger = Provider.of<FontChanger>(context);
    return quoteQuery.length == 0
        ? SpinKitPulse(
      itemBuilder: (_, int index) {
        return DecoratedBox(
          decoration: BoxDecoration(
              color: index.isEven ? Colors.black54 : Colors.white,
              shape: BoxShape.circle
          ),
        );
      },
      size: 120.0,
    )
        : GestureDetector(
            onTap: () {
              setState(() {
                barStatus = !barStatus;
              });
              generalChanger.setBarStatus(barStatus);
            },
            child: PageView.builder(
              controller: _controller,
              scrollDirection: Axis.vertical,
              itemCount: itemCount,
              onPageChanged: (int page) {
/*                if(remoteChanger.getNativeAdsIndexes().contains(page + 3) && !generalChanger.getIsPremiumOrSubscribed()) {
                  _nativeAdFB();
                }*/
                setState(() {
                  isScreenShotProcessing = false;
                });
                dbFunctions.updateUserQuoteStatus(
                    onScreenQuote.userQuoteId,
                    onScreenQuote.isCollected,
                    onScreenQuote.isSeen,
                    onScreenQuote.isBookmarked);
              },
              itemBuilder: (context, index) {
                onScreenQuote = UserQuote(
                  userQuoteId: quoteQuery[index].userQuoteId,
                  isCollected: quoteQuery[index].isCollected,
                  isSeen: 1,
                  isBookmarked: quoteQuery[index].isBookmarked,
                );
                _controller.addListener(
                  () {
                    if (_controller.position.userScrollDirection ==
                        ScrollDirection.reverse) {
                      if (generalChanger.getBarStatus() == true)
                        setState(() {
                          generalChanger.setBarStatus(false);
                          barStatus = false;
                        });
                    }
                    if (_controller.position.userScrollDirection ==
                        ScrollDirection.forward) {
                      if (generalChanger.getBarStatus() == false)
                        setState(() {
                          generalChanger.setBarStatus(true);
                          barStatus = true;
                        });
                    }
                  },
                );
                return SafeArea(
                  child: GestureDetector(
                    onTap: () {
                      _controller.animateToPage(index + 1, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                      if (generalChanger.getBarStatus() == true)
                        setState(() {
                          generalChanger.setBarStatus(false);
                          barStatus = false;
                        });
                    },
                    child: Container(
                      margin:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                      height: heightScreen * 0.6,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: generalChanger.getBarStatus()
                                ? 0
                                : heightScreen * 0.07,
                          ),
                          Container(
                            height: heightScreen * 0.1,
                          ),
                          ConstrainedBox(
                            constraints: BoxConstraints(
//                                  minWidth: 300.0,
//                                  maxWidth: 300.0,
                              minHeight: heightScreen * 0.1,
                              maxHeight: heightScreen * 0.35,
                            ),
                            child: GestureDetector(
                              onLongPress: () {
                                subject = quoteQuery[index].authername != null
                                    ? "${quoteQuery[index].quote}\n\n${quoteQuery[index].authername}"
                                    : "${quoteQuery[index].quote}";
                                Clipboard.setData(new ClipboardData(text: subject));
                                _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate('textCopied')),));
                              },
                              child: AutoSizeText(
                                quoteQuery[index].quote,
                                textAlign: TextAlign.center,
                                style: themesChanger.getTextStyle(),
                              ),
                            ),
                          ),
                          Container(
                            height: heightScreen * 0.03,
                          ),
                          quoteQuery[index].authername == null
                              ? Container()
                              : Container(
                            child: Text(
                              quoteQuery[index].authername,
                              textAlign: TextAlign.center,
                              style: themesChanger.getTextStyleAuthorName(),
                            ),
                          ),
                          Container(
                            height: heightScreen * 0.12,
                          ),
                          isScreenShotProcessing
                              ? generalChanger.getIsPremiumOrSubscribed()
                              ? Row()
                              : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(
                                      bottom: heightScreen * 0.001),
                                  child: Image.asset(
                                    'assets/signs2/watermark.png',
                                    scale: 3,
                                    color: themesChanger.getButtonColor(),
                                  ),
                                ),
                                Container(
                                  width: widthScreen * 0.02,
                                ),
                                Text(
                                  AppLocalizations.of(context).translate(
                                      'inspirationalQuotes'),
                                  style: TextStyle(
                                    fontFamily: 'Futura Book',
                                    fontSize: MediaQuery
                                        .of(context)
                                        .size
                                        .width * 0.06,
                                    fontWeight: FontWeight.w500,
                                    color: Color.fromARGB(
                                        int.parse(themeProperties[0]),
                                        int.parse(themeProperties[1]),
                                        int.parse(themeProperties[2]),
                                        int.parse(themeProperties[3])),
                                    wordSpacing: double.parse(
                                        fontChanger.getFontValues()[5]),
                                    letterSpacing: double.parse(
                                        fontChanger.getFontValues()[6]),
                                    height: double.parse(
                                        fontChanger.getFontValues()[7]),
                                    shadows: [
                                      Shadow(
                                        offset: Offset(
                                            double.parse(themeProperties[8]),
                                            double.parse(themeProperties[9])),
                                        blurRadius: double.parse(
                                            themeProperties[10]),
                                        color: Color.fromARGB(
                                            int.parse(themeProperties[4]),
                                            int.parse(themeProperties[5]),
                                            int.parse(themeProperties[6]),
                                            int.parse(themeProperties[7])),
                                      ),
                                    ],
                                  ),
                                ),
                              ])
                              : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                  child: ImageIcon(
                                    AssetImage('assets/signs2/share.png'),
                                    size: widthScreen * 0.08,
                                    color: themesChanger.getButtonColor(),
                                  ),
                                  onTap: () {
                                    functions.sendAnalyticsWithOwnName("quoteShareClicked");
                                    if(!isScreenShotProcessing) {
                                      setState(() {
                                        isScreenShotProcessing = true;
                                      });
                                      subject =
                                      quoteQuery[index].authername != null
                                          ? "${quoteQuery[index]
                                          .quote}\n\n${quoteQuery[index]
                                          .authername}\n$kDynamicLinkIQ"
                                          : "${quoteQuery[index].quote}\n$kDynamicLinkIQ";
                                      Future.delayed(
                                          const Duration(milliseconds: 100), () {
                                        _takeScreenshot();
                                      });
                                      Future.delayed(
                                          const Duration(milliseconds: 800), () {
                                        setState(() {
                                          isScreenShotProcessing = false;
                                        });
                                        //Navigator.pop(context);
                                        print(
                                            'Process after screenshot: $isScreenShotProcessing');
                                      });
                                    }
                                  }),
                              Container(
                                width: widthScreen * 0.08,
                              ),
                              GestureDetector(
                                child: ImageIcon(
                                  AssetImage('assets/signs2/save.png'),
                                  size: widthScreen * 0.08,
                                  color: themesChanger.getButtonColor(),
                                ),
                                onTap: () async {
                                  _requestPermission().catchError(
                                        (onError) {
                                      print("ERROR...Cannot CANCEL Pending Notifications... $onError");
                                    },
                                  ).whenComplete(() {
                                    functions.sendAnalyticsWithOwnName("quoteSaveClicked");
                                    setState(() {
                                      isScreenShotProcessing = true;
                                    });
                                    Future.delayed(
                                        const Duration(milliseconds: 300), () {
                                      _saveScreen();
                                    });
                                    Future.delayed(
                                        const Duration(milliseconds: 1000), () {
                                      setState(() {
                                        isScreenShotProcessing = false;
                                      });
                                      //Navigator.pop(context);
                                      print('Process after save image: $isScreenShotProcessing');
                                    });
                                  });
                                },
                              ),
                              Container(
                                width: widthScreen * 0.08,
                              ),
                              GestureDetector(
                                onTap: () {
                                  functions.sendAnalyticsWithOwnName("quoteBookmarkClicked");
                                  HapticFeedback.lightImpact();
                                  setState(() {
                                    quoteQuery[index] = QuoteQuery(
                                      quote: quoteQuery[index].quote,
                                      authername: quoteQuery[index].authername,
                                      userQuoteId: quoteQuery[index]
                                          .userQuoteId,
                                      isBookmarked:
                                      quoteQuery[index].isBookmarked == 1
                                          ? 0
                                          : 1,
                                      isSeen: 1,
                                      isCollected: quoteQuery[index]
                                          .isCollected,
                                    );
                                    dbFunctions.updateUserQuoteStatus(
                                        quoteQuery[index].userQuoteId,
                                        quoteQuery[index].isCollected,
                                        quoteQuery[index].isSeen,
                                        quoteQuery[index].isBookmarked);
                                  });
                                },
                                child: ImageIcon(
                                  AssetImage(onScreenQuote.isBookmarked == 1
                                      ? 'assets/signs2/bookmark_full.png'
                                      : 'assets/signs2/bookmark.png'),
                                  size: widthScreen * 0.08,
                                  color: themesChanger.getButtonColor(),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            height: heightScreen * 0.1,
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          );
  }

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();

    final info = statuses[Permission.storage].toString();
    print(info);
    //_toastInfo(info);
  }


  _saveScreen() async {
    final uint8List = await screenshotController.capture();
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/image.png');
    final result = await ImageGallerySaver.saveImage(uint8List);
    print("Save Success: ${result['isSuccess']}");
    if(result['isSuccess']) {
      _toastInfo(AppLocalizations.of(context).translate('imageSaved'));
    } else {
      _toastInfo(AppLocalizations.of(context).translate('imageNotSaved'));
    }
  }

  _toastInfo(String info) {
    Fluttertoast.showToast(msg: info, toastLength: Toast.LENGTH_LONG);
  }

  recommendThisApp() {
    final RenderBox box = context.findRenderObject() as RenderBox;

    Share.share('${AppLocalizations.of(context)
        .translate('recommend')}$kDynamicLinkIQ',
        subject: AppLocalizations.of(context).translate("inspirationalQuotes"),
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  void _takeScreenshot() async {
    final uint8List = await screenshotController.capture();
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/image.png');
    await file.writeAsBytes(uint8List);
    await Share.shareFiles([file.path], text: subject);
  }


  Widget _nativeBannerAd(RemoteNotifyChanger remoteNotifyChanger) {
    return FacebookNativeAd(
      //placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2964953543583512",
      placementId: remoteNotifyChanger.getFbNativeBannerPlacementId(),
      adType: NativeAdType.NATIVE_BANNER_AD,
      bannerAdSize: NativeBannerAdSize.HEIGHT_50,
      width: double.infinity,
      backgroundColor: Colors.blue,
      titleColor: Colors.white,
      descriptionColor: Colors.white,
      buttonColor: Colors.deepPurple,
      buttonTitleColor: Colors.white,
      buttonBorderColor: Colors.white,
      listener: (result, value) {
        print("Native Banner Ad: $result --> $value");
      },
    );
  }

  _showFacebookInterstitialAd(RemoteNotifyChanger remoteChanger, GeneralChanger generalChanger) {
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    if (newGeneralChanger.getIsInterstitialAdLoaded()) {
      generalChanger.setSectionSessionClickCount(generalChanger.getSectionSessionClickCount() + 1);
      FacebookInterstitialAd.showInterstitialAd();
    } else {
      print("Interstial Ad not yet loaded!");
      functions.loadInterstitialAd(remoteChanger, context);
    }
  }

  _showNativeBannerAd(RemoteNotifyChanger remoteNotifyChanger) {
    print("Showing native banner ad");
    setState(() {
      _currentAd = _nativeBannerAd(remoteNotifyChanger);
    });
  }


  _showBannerAd(RemoteNotifyChanger remoteNotifyChanger) {
    print("Showing banner add");
    setState(() {
      _currentAd = FacebookBannerAd(
        //placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2964944860251047", //testid
        placementId: remoteNotifyChanger.getFbBannerPlacementId(),
        bannerSize: BannerSize.STANDARD,
        listener: (result, value) {
          switch (result) {
            case BannerAdResult.ERROR:
              print("Error: $value");
              setState(() {
                _currentAd = Container();
              });
              break;
            case BannerAdResult.LOADED:
              print("Loaded: $value");
              break;
            case BannerAdResult.CLICKED:
              print("Clicked: $value");
              break;
            case BannerAdResult.LOGGING_IMPRESSION:
              print("Logging Impression: $value");
              break;
          }
        },
      );
    });
  }

/*  void _showAdmobInterstitialAd(GeneralChanger generalChanger) {
    print("trying to show interstitial ad...");
    if (MyStatics.interstitialAd == null) {
      print('Warning: attempt to show interstitial before loaded.');
      functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
      return;
    }
    MyStatics.interstitialAd.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (InterstitialAd ad) =>
          print('ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (InterstitialAd ad) {
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
      },
      onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
      },
    );
    generalChanger.setSectionSessionClickCount(generalChanger.getSectionSessionClickCount()+1);
    MyStatics.interstitialAd.show();
    MyStatics.interstitialAd = null;
    functions.createAdmobInterstitialAd(MyStatics.numInterstitialLoadAttempts);
  }

  Future<void> _createAdmobAnchoredBanner(BuildContext context) async {
    print("In _createAdmobAnchoredBanner func in section...");
    final AnchoredAdaptiveBannerAdSize size =
    await AdSize.getAnchoredAdaptiveBannerAdSize(
      Orientation.portrait,
      MediaQuery.of(context).size.width.truncate(),
    );

    if (size == null) {
      print('Unable to get height of anchored banner.');
      return;
    }

    final BannerAd banner = BannerAd(
      size: size,
      request: MyStatics.request,
      adUnitId: Platform.isAndroid ? kAdmob_Banner_ads_ad_unit_id_android : kAdmob_Banner_ads_ad_unit_id_ios,
      listener: BannerAdListener(
        onAdLoaded: (Ad ad) {
          print('$BannerAd loaded.');
          setState(() {
            _anchoredBanner = ad as BannerAd;
          });
        },
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          print('$BannerAd failedToLoad: $error');
          ad.dispose();
        },
        onAdOpened: (Ad ad) => print('$BannerAd onAdOpened.'),
        onAdClosed: (Ad ad) => print('$BannerAd onAdClosed.'),
      ),
    );
    return banner.load();
  }*/

}
