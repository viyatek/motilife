import 'dart:math';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/screens/single_quote.dart';
import '../localizations.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/ExtractedWidgets/quote_card.dart';
import 'dart:io';

final dbProvider = DatabaseProvider.instance;
final dbFunctions = DbFunctions();

class BookmarkTab extends StatefulWidget {
  final int quoteId;
  BookmarkTab({this.quoteId});

  @override
  _BookmarkTabState createState() => _BookmarkTabState();
}

class _BookmarkTabState extends State<BookmarkTab> {
  List<String> themeProperties;
  PageController _controller = PageController(
    initialPage: 0,
  );

  List<QuoteQuery> bookmarkQuery = [];
  bool showText = false;

  @override
  void initState() {
    _queryBookmarkedQuotes();
    Future.delayed(Duration(milliseconds: 200), () {
      setState(() {
        showText = true;
      });
    });
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    themeProperties = themesChanger.getThemeValues();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final orientation = MediaQuery.of(context).orientation;
    final dayAndNightChanger = Provider.of<DayNightChanger>(context);
    final themesChanger = Provider.of<ThemeChanger>(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          //titleSpacing: 0,
          title: Text(
            AppLocalizations.of(context).translate('bookmarks'),
            style: TextStyle(
              fontFamily: kDefaultFontName,
              color: Colors.white,
              fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
              fontWeight: FontWeight.w500,
            ),
          ),
          toolbarHeight: heightScreen * 0.07,
          backgroundColor: Colors.black,
          centerTitle: true,
          elevation: 0.0,
          leading: IconButton(
            icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          height: heightScreen * 1,
          width: widthScreen * 1,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: kSigmaX, sigmaY: kSigmaY),
            child: bookmarkQuery.length == 0 ? Center(
              child: showText ? Text(
                AppLocalizations.of(context).translate('bookmarkWarning'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: kDefaultFontName,
                  fontSize: MediaQuery.of(context).size.width*0.055,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ) : CupertinoActivityIndicator(),
            ) : GridView.builder(
              shrinkWrap: true,
          itemCount: bookmarkQuery.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 2.4,
                crossAxisCount:
                (orientation == Orientation.portrait) ? 1 : 1),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SingleQuotePage(
                          quoteId: bookmarkQuery[index].userQuoteId),
                    ),
                  );
                },
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: widthScreen*0.03, vertical: heightScreen*0.01),
                  padding: EdgeInsets.symmetric(horizontal: widthScreen*0.06, vertical: heightScreen*0.01),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.07),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: GridTile(
                    footer: GestureDetector(
                      onTap: () {
                        setState(() {
                          bookmarkQuery[index] = QuoteQuery(
                            quote: bookmarkQuery[index].quote,
                            authername: bookmarkQuery[index].authername,
                            userQuoteId: bookmarkQuery[index].userQuoteId,
                            isBookmarked: bookmarkQuery[index].isBookmarked == 1 ? 0 : 1,
                            isSeen: 1,
                            isCollected: bookmarkQuery[index].isCollected,
                          );
                          dbFunctions.updateUserQuoteStatus(
                              bookmarkQuery[index].userQuoteId,
                              bookmarkQuery[index].isCollected,
                              bookmarkQuery[index].isSeen,
                              bookmarkQuery[index].isBookmarked);
                        });
                      },
                      child: Card(
                        elevation: 0,
                        color: Colors.transparent,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: heightScreen*0.01),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: ImageIcon(
                              AssetImage(
                                  bookmarkQuery[index].isBookmarked == 1
                                      ? 'assets/signs2/bookmark_full.png'
                                      : 'assets/signs2/bookmark.png'),
                              size: widthScreen * 0.06,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    child: Container(
                      padding: EdgeInsets.only(right: widthScreen*.08),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ConstrainedBox(
                            constraints: BoxConstraints(
//                                  minWidth: 300.0,
//                                  maxWidth: 300.0,
                              //minHeight: heightScreen * 0.05,
                              maxHeight: heightScreen * 0.15,
                            ),
                            child: AutoSizeText(
                              '${bookmarkQuery[index].quote}',
                              style: TextStyle(
                                  fontSize: MediaQuery.of(context).size.width * 0.045,
                                  fontFamily: kDefaultFontName,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                          ),
                          if(bookmarkQuery[index].authername != null) Text(
                            bookmarkQuery[index].authername,
                            style: TextStyle(
                                fontSize: MediaQuery.of(context).size.width * 0.032,
                                fontFamily: kDefaultFontName,
                                color: Color(0xFF97969d),
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          )
          ),
        ),
      ),
    );
  }

  _queryBookmarkedQuotes() async {
    print("Querying all bookmarked quotes");
    final quotes = await dbProvider.queryAllBookmarkedQuotesOfUser();
    quotes.forEach((quote) {
      bookmarkQuery.add(
          QuoteQuery(
            quote: quote['quote'],
            userQuoteId: quote['quoteid'],
            authername: quote['authorname'],
            isCollected: quote['quoteCollectedStatus'],
            isBookmarked: quote['quoteBookmarkedStatus'],
            isSeen: quote['isSeenStatus'],
          )
      );
    });
  }
}
