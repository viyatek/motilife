import 'dart:isolate';
import 'dart:math';
import 'dart:ui';

import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart' as myRender;
import 'package:flutter/services.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:home_widget/home_widget.dart';
import 'package:home_widget/home_widget_callback_dispatcher.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/ExtractedWidgets/cupertino_page_transition.dart';
import 'package:motilife/ExtractedWidgets/feed_star_rate_us.dart';
import 'package:motilife/ExtractedWidgets/other_apps.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/Utilities/notification_functions.dart';
import 'package:motilife/screens/single_quote.dart';
import 'package:motilife/screens/topic_tab.dart';
import 'package:motilife/screens/themes_tab.dart';
import 'package:motilife/screens/setting_tab.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
//import 'package:workmanager/workmanager.dart';
import '../localizations.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/ExtractedWidgets/quote_card.dart';
import 'dart:io';

import '../main.dart';
import '../prefs.dart';


class FeedQuotesPage extends StatefulWidget {
  //final int quoteId;
  final int notifiedQuoteId;
  final bool isComeFromReminderPage;//if true, set notification
  FeedQuotesPage({this.notifiedQuoteId, this.isComeFromReminderPage});

  @override
  _FeedQuotesPageState createState() => _FeedQuotesPageState();
}

class _FeedQuotesPageState extends State<FeedQuotesPage> with WidgetsBindingObserver{
  List<String> themeProperties;
  PageController _controller = PageController(
    initialPage: 0,
  );
  final dbProvider = DatabaseProvider.instance;
  final dbFunctions = DbFunctions();
  final functions = AllFunctions();
  final notificationFunctions = NotificationHandler();

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  List<QuoteQuery> quoteQuery = [];
  List<QuoteQuery> displayingQuoteQuery = [];
  List<QuoteQuery> priorityQuoteQuery = [];
  int _displayRefreshCount = 0;
  List _displayRefreshArray = [];
  int _openCount;
  bool _isExperienced;
  bool _openWithWidgetData = true;
  bool _rateUsSeen = false;
  int itemCount = 1;
  String fbMessageBody;
  String fbMessageTitle;

  String widgetId;
  String widgetMessage;
  String widgetAuthorName;
  String widgetTheme;
  List adRequestedArray = [];
  List adRequestedArray2 = [];
  List isSeenStatusChangedArray = [];
  int queriedSectionCount = 0;
  bool isQuoteQueryReady = false;


  var connectivityResult;
  bool _isWidgetSet = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int myAdCounter = 0;
  bool _nativeAdIsLoaded = false;
  bool _nativeAdIsLoaded2 = false;
  int _sessionCountPerLaunch = 0;
  AppsflyerSdk _appsflyerSdk;
  final receiver = ReceivePort();
  int widgetCyclePeriodHour = 8;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final musicChanger = Provider.of<MusicChanger>(context, listen: false);
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    print("LifecycleWatcherState#didChangeAppLifecycleState state=${state.toString()}");
    setState(() {
      //_lastLifecyleState = state;
    });
    if(state == AppLifecycleState.paused) {
      _sessionCountPerLaunch ++;
      musicChanger.getAdvancedPlayer().pause();
    } else {
      functions.resumeMusic(musicChanger);
    }
  }

  _setWidgetData() {
    print("Widget data set");
    widgetMessage = quoteQuery[quoteQuery.length-1].quote;
    widgetId = "${quoteQuery[quoteQuery.length-1].userQuoteId}";
    widgetAuthorName = quoteQuery[quoteQuery.length-1].authername != null ? quoteQuery[quoteQuery.length-1].authername : "";
    print(widgetMessage);
    print(widgetId);
    _sendAndUpdate();
    setState(() {
      _isWidgetSet = true;
    });
  }




  @override
  void initState() {
    //Workmanager().initialize(callbackDispatcher, isInDebugMode: false);//kDebugMode
    _loadData();
    //_loadNativeAd(0);
    //_loadNativeAd2(0);
    HomeWidget.setAppGroupId('group.com.viyatek.motilife');
    Map appsFlyerOptions = { "afDevKey": kDEV_KEY_ApsFlyer,
      "afAppId": Platform.isAndroid ? kAppId_Android : kAppId_iOS,
      "isDebug": true};

    _appsflyerSdk = AppsflyerSdk(appsFlyerOptions);
    //_initAppsFlyerSdk();
    _getFirstOpenAfterNotification();
    WidgetsBinding.instance.addObserver(this);
    quoteQuery = [];
    itemCount = 0;
    _firebaseMessagingHandle();
    _queryAllSectionsAndQuotesOfUser();
    //_queryAllUserPrefsQuotes();
    _getPrefData();
    _sendFirstOpenEvent();
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    themeProperties = themesChanger.getThemeValues();
    _rateUsSeen = generalChanger.getIsSeenRateUs();
    //startSdk();
    Future.delayed(const Duration(milliseconds: 500), () {
      if (widget.isComeFromReminderPage) {
        print("Setting Notifications on home page");
        notificationFunctions.handlePendingNotifications().catchError(
              (onError) {
            print("ERROR...Cannot handle Pending Notifications... $onError");
          },
        ).whenComplete(() {
          print("Pending quotes have been updated on Home Page.");
          notificationFunctions.cancelAllNotifications().catchError(
                (onError) {
              print("ERROR...Cannot CANCEL Pending Notifications... $onError");
            },
          ).whenComplete(() async {

            if (generalChanger.getNotificationCount() != 0 && generalChanger.getIsNotifiable()) {
              receiver.listen((message) {
                notificationFunctions.setNotificationTimePeriod();
              });
              isolate = await Isolate.spawn(isolateSendPort, receiver.sendPort);
            }

          });
        });
      }
    });
    Future.delayed(const Duration(milliseconds: 300), () async {
      final musicChanger = Provider.of<MusicChanger>(context, listen: false);
      functions.playMusic(musicChanger);
    });

    Future.delayed(const Duration(milliseconds: 2000), () async {
      if (generalChanger.getOnLaunch()) _updateExpiryDateOnPref();
      //_configureLocalTimeZone();
    });
    Future.delayed(const Duration(milliseconds: 5000), () async {
/*      if(!_isWidgetSet && Platform.isIOS) _setWidgetData();
      if(!_isWidgetSet && Platform.isAndroid) _startBackgroundWidgetUpdate();*/
      if(!_isWidgetSet) _setWidgetData();
    });

/*    Future.delayed(const Duration(milliseconds: 15000), () async {
      await dbProvider.writeDefaultQuotesData();
    });*/

    print("_sessionCountPerLaunch: $_sessionCountPerLaunch");
    super.initState();
  }

  _clearCache() async {
    print("Clearing app cache");
    var appDir = (await getTemporaryDirectory()).path;
    new Directory(appDir).delete(recursive: true);
  }


  @override
  void dispose() {
    _controller.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<void> _sendData() async {
    try {
      return Future.wait([
        HomeWidget.saveWidgetData<String>('title', widgetId),
        HomeWidget.saveWidgetData<String>('message', widgetMessage),
        HomeWidget.saveWidgetData<String>('themeName', themeProperties[11]),
        HomeWidget.saveWidgetData<String>('author', widgetAuthorName),
      ]);
    } on PlatformException catch (exception) {
      debugPrint('Error Sending Data. $exception');
    }
  }

  Future<void> _updateWidget() async {
    try {
      return HomeWidget.updateWidget(
          name: 'HomeWidgetExampleProvider', iOSName: 'HomeWidgetExample');
    } on PlatformException catch (exception) {
      debugPrint('Error Updating Widget. $exception');
    }
  }

  Future<void> _sendAndUpdate() async {
    await _sendData();
    await _updateWidget();
    setState(() {
      _isWidgetSet = true;
    });
  }

  Future<void> _loadData() async {
    try {
      print("On load data");
      return Future.wait([
        quoteQuery.length >1 ? HomeWidget.getWidgetData<String>('title', defaultValue: '${quoteQuery[quoteQuery.length-1].userQuoteId}')
            .then((value) => widgetId = value) : HomeWidget.getWidgetData<String>('title', defaultValue: '2352')
            .then((value) => widgetId = value),
        quoteQuery.length >1 ? HomeWidget.getWidgetData<String>('message',
            defaultValue: '${quoteQuery[quoteQuery.length-1].quote}')
            .then((value) => widgetMessage = value) : HomeWidget.getWidgetData<String>('message',
            defaultValue: 'Live the way you want, not the way others want.')
            .then((value) => widgetMessage = value),
        quoteQuery.length >1 ? HomeWidget.getWidgetData<String>('author',
            defaultValue: '${quoteQuery[quoteQuery.length-1].authername}')
            .then((value) => widgetAuthorName = value) : HomeWidget.getWidgetData<String>('author',
            defaultValue: '')
            .then((value) => widgetAuthorName = value),
        HomeWidget.getWidgetData<String>('themeName',
            defaultValue: 'f4')
            .then((value) => widgetTheme = value),
      ]);
    } on PlatformException catch (exception) {
      debugPrint('Error Getting Data. $exception');
    }
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("In didChangeDependencies");
    _checkForWidgetLaunch();
    HomeWidget.widgetClicked.listen(_launchedFromWidget);
  }

  void _checkForWidgetLaunch() {
    HomeWidget.initiallyLaunchedFromHomeWidget().then(_launchedFromWidget);
  }

  void _launchedFromWidget(Uri uri) {
    if (uri != null) {
      // App started from HomeScreenWidget
      print("App started from HomeScreenWidget");
      _loadData();
      print(widgetId);
      if(_sessionCountPerLaunch>0) {
        print(_sessionCountPerLaunch);
        Future.delayed(Duration(milliseconds: 100), () {
          Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: FeedQuotesPage(notifiedQuoteId: int.parse(widgetId))), (Route<dynamic> route) => false);
        });
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final orientation = MediaQuery.of(context).orientation;
    final generalChanger = Provider.of<GeneralChanger>(context);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context);
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: heightScreen * 1,
          width: widthScreen * 1,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
              !isQuoteQueryReady ? Center(
                child: SpinKitPulse(
                  itemBuilder: (_, int index) {
                    return DecoratedBox(
                      decoration: BoxDecoration(
                          color: index.isEven ? Colors.white24 : Colors.black54,
                          shape: BoxShape.circle
                      ),
                    );
                  },
                  size: 120.0,
                )
              ) : Positioned(
                top: heightScreen*0.02,
                bottom: heightScreen*0.115,
                right: widthScreen*0,
                left: widthScreen*0,
                child: StaggeredGridView.countBuilder(
                  shrinkWrap: true,
                  physics: CustomScrollPhysics(),
                  itemCount: displayingQuoteQuery.length-1,
                  crossAxisCount: 1,
                  staggeredTileBuilder: (int index) =>
                  new StaggeredTile.count(1, index % remoteChanger.getNativeAdsMode() == 1 && !generalChanger.getIsPremiumOrSubscribed() ? 0.8 : 0.5),
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 4.0,
                  itemBuilder: (BuildContext context, int index) {
                    print("Index: $index displayingQuoteQuery: ${displayingQuoteQuery.length}");
                    if(index % remoteChanger.getNativeAdsMode() == 1 && !generalChanger.getIsPremiumOrSubscribed() && index > 4) {
                      //_loadNativeAd(index);
                      //_loadNativeAd2(index);
                    }
                    if(index == displayingQuoteQuery.length-5 && !_displayRefreshArray.contains(index)) {
                      print("Time to increase");
                      _displayRefreshCount ++;
                      _displayRefreshArray.add(index);
                      displayingQuoteQuery.addAll(quoteQuery.getRange(_displayRefreshCount*20, (_displayRefreshCount+1)*20));
                    }
                    if (index == remoteChanger.getRateUsPageNumber() && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount) && widget.notifiedQuoteId != 100000) {
                      return FeedStarRateUs(
                        themeProperties: themeProperties,
                        heightScreen: heightScreen,
                        widthScreen: widthScreen,
                        generalChanger: generalChanger,
                      );
                    } else if(index % remoteChanger.getNativeAdsMode() == 1 && !generalChanger.getIsPremiumOrSubscribed()) {
                      if(index.isEven && _nativeAdIsLoaded) {
                        print("ad counter even $index");
                        return Container();
/*                          FeedNativeAds(
                          nativeAd: _nativeAd,
                          marginX: widthScreen*0.03,//widthScreen*0.07,
                          marginY: 0,//heightScreen*0.68,
                        );*/
                      } else if(index.isOdd && _nativeAdIsLoaded2){
                        print("ad counter odd $index");
                        return Container();
/*                          FeedNativeAds(
                          nativeAd: _nativeAd2,
                          marginX: widthScreen*0.03,//widthScreen*0.07,
                          marginY: 0,//heightScreen*0.68,
                        );*/
                      } else {
                        if(index.isOdd) {
                          return SuggestOtherAppsForFeed(
                            appIconName: 'uf',
                            motto: 'factsMotto1',
                            directedLink: Platform.isAndroid ? kUFPlayStoreUrl : kUFAppStoreUrl,
                          );
                        } else {
                            if(Platform.isAndroid) {
                              return SuggestOtherAppsForFeed(
                                appIconName: 'uq',
                                motto: 'quoteMotto1',
                                directedLink: kQuotePlayStoreUrl,
                              );
                            } else {
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SettingCard(imageName: 'recommend', text: 'recommendThisApp', func: recommendThisApp),
                                    ],
                                  ),
                                ],
                              );
                            }
                          }
                        }
                    } else {
                      _updateQuotesSeenStatusAndCheckNotification(index, generalChanger);
                      //_updateQuotesSeenStatus(index);
                      return GestureDetector(
                        onLongPress: () {
                          var subject = quoteQuery[index].authername != null
                              ? "${quoteQuery[index].quote}\n\n${quoteQuery[index].authername}"
                              : "${quoteQuery[index].quote}";
                          Clipboard.setData(new ClipboardData(text: subject));
                          _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(AppLocalizations.of(context).translate('textCopied')),));
                        },
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SingleQuotePage(
                                quoteId: quoteQuery[index].userQuoteId,
                                appBarTitle: AppLocalizations.of(context).translate('inspirationalQuotes'),
                              ),
                            ),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: widthScreen*0.03, vertical: 0),
                          padding: EdgeInsets.symmetric(horizontal: widthScreen*0.06, vertical: heightScreen*0.01),
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.07),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: GridTile(
                            footer: GestureDetector(
                              onTap: () {
                                setState(() {
                                  quoteQuery[index] = QuoteQuery(
                                    quote: quoteQuery[index].quote,
                                    authername: quoteQuery[index].authername,
                                    userQuoteId: quoteQuery[index].userQuoteId,
                                    isBookmarked: quoteQuery[index].isBookmarked == 1 ? 0 : 1,
                                    isSeen: 1,
                                    isCollected: quoteQuery[index].isCollected,
                                  );
                                  dbFunctions.updateUserQuoteStatus(
                                      quoteQuery[index].userQuoteId,
                                      quoteQuery[index].isCollected,
                                      quoteQuery[index].isSeen,
                                      quoteQuery[index].isBookmarked);
                                });
                              },
                              child: Card(
                                elevation: 0,
                                color: Colors.transparent,
                                child: Padding(
                                  padding: EdgeInsets.only(bottom: heightScreen*0.01),
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: ImageIcon(
                                      AssetImage(
                                          quoteQuery[index].isBookmarked == 1
                                              ? 'assets/signs2/bookmark_full.png'
                                              : 'assets/signs2/bookmark.png'),
                                      size: widthScreen * 0.06,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            child: Container(
                              padding: EdgeInsets.only(right: widthScreen*.08),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ConstrainedBox(
                                    constraints: BoxConstraints(
//                                  minWidth: 300.0,
//                                  maxWidth: 300.0,
                                      //minHeight: heightScreen * 0.05,
                                      //maxHeight: heightScreen * 0.35,
                                    ),
                                    child: AutoSizeText(
                                      '${quoteQuery[index].quote}',
                                      style: TextStyle(
                                          fontSize: MediaQuery.of(context).size.width * 0.045,
                                          fontFamily: kDefaultFontName,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500
                                      ),
                                    ),
                                  ),
                                  if(quoteQuery[index].authername != null) Text(
                                    quoteQuery[index].authername,
                                    style: TextStyle(
                                        fontSize: MediaQuery.of(context).size.width * 0.038,
                                        fontFamily: kDefaultFontName,
                                        color: Colors.white,//Color(0xFF97969d),
                                        fontWeight: FontWeight.w500
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                  },
                ),
              ),

              if(!isScreenShotProcessing) Positioned(
                bottom: heightScreen*0.025,
                right: widthScreen*0.004,
                left: widthScreen*0.004,
                child: Container(
                  height: heightScreen * 0.1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      MyCupertinoPageTransition(nextScreen: TopicsTab(), imageName: 'topics', imageText: AppLocalizations.of(context).translate('topics'), isVisibleRedAlert: newGeneralChanger.getNewTopicsAlert(), buttonTag: 0, generalChanger: generalChanger, newGeneralChanger: newGeneralChanger, backgroundColor: Colors.white.withOpacity(0.07), fontSize: 0.03),
                      MyCupertinoPageTransition(nextScreen: ThemesTab(), imageName: "themes", imageText: AppLocalizations.of(context).translate('themes'), isVisibleRedAlert: newGeneralChanger.getNewThemesAlert(), buttonTag: 1, generalChanger: generalChanger, newGeneralChanger: newGeneralChanger, backgroundColor: Colors.white.withOpacity(0.07), fontSize: 0.03),
                      MyCupertinoPageTransition(nextScreen: SettingsTab(), imageName: 'settings', imageText: AppLocalizations.of(context).translate('settings'), isVisibleRedAlert: newGeneralChanger.getNewSettingsAlert(), buttonTag: 2, generalChanger: generalChanger, newGeneralChanger: newGeneralChanger, backgroundColor: Colors.white.withOpacity(0.07), fontSize: 0.03),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  
  // FUNCTIONS

  Future _onSelectNotification(String payload) async {// for launching app on notification click
    print('onSelection payload:: $payload');
    setState(() {
      _openWithWidgetData = false;
      quoteQuery = [];
      displayingQuoteQuery = [];
    });
    if(payload == "") {
      print("Payload null but app is opened by fb notification");
      if(fbMessageTitle == null) {
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  FeedQuotesPage(notifiedQuoteId: int.parse("100000"))
          ),
        );
      } else {
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  FeedQuotesPage()
          ),
        );
      }
    } else {
      return await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                FeedQuotesPage(notifiedQuoteId: int.parse(payload))
          //SingleQuotePage(quoteId: int.parse(payload), appBarTitle: AppLocalizations.of(context).translate('inspirationalQuotes'))
        ),
      );
    }
  }

  Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async {// for launching app on notification click (iOS<10)
    print('Is first OPEN onDidRECEIVE: payload: $payload');
    setState(() {
      _openWithWidgetData = false;
      quoteQuery = [];
      displayingQuoteQuery = [];
    });
    return await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              FeedQuotesPage(notifiedQuoteId: int.parse(payload))
        //SingleQuotePage(quoteId: int.parse(payload), appBarTitle: AppLocalizations.of(context).translate('inspirationalQuotes'))
      ),
    );
  }

  _getFirstOpenAfterNotification() {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid = AndroidInitializationSettings('app_icon'); //@mipmap/ic_launcher
    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _onSelectNotification);
  }

  _getPrefData() async {
    //_isSeenGif = await SharedPreferencesHelper.getIsSeenGif();
    _openCount = await SharedPreferencesHelper.getAppOpenCount();
    print("Open count isss: $_openCount");
    if(_openCount == 0) {
      print("Open count is: $_openCount");
      await SharedPreferencesHelper.setAppOpenCount(1);//when user come this page, increase open count, just for first open
      //initPlatformState();//Set os version release
    }
    _isExperienced = await SharedPreferencesHelper.getIsUserExperienceSeen();
  }

  _sendFirstOpenEvent() async {
    if (await SharedPreferencesHelper.getIsHomeScreenOpenBefore() == false) {
      print('Sending first home screen open event...');
      functions.sendAnalyticsWithOwnName('first_homeScreen_open');
      await SharedPreferencesHelper.setIsHomeScreenFirstOpen(true);
    }
  }

  _updateExpiryDateOnPref() async {
    await FlutterInappPurchase.instance.initConnection;
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult != ConnectivityResult.none) {
      if (await SharedPreferencesHelper.getIsPremiumStatus() == false)
        Platform.isAndroid
            ? await functions.getExpiryDateForAndroid()
            : await functions.getExpiryDateForIOS();
      print('EXPIRY TIME ON PREF WAS UPDATED');
    }
    generalChanger.setOnLaunch(false);
  }

  _queryAllSectionsAndQuotesOfUser() async {
    final sections = await dbProvider.queryAllUnlockedAndSelectedSections();
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    print(sections.length);
    print('Open count: $_openCount');
    if(widget.notifiedQuoteId != null && widget.notifiedQuoteId != 0 && widget.notifiedQuoteId != 100000) {//id 100000 means comes from firebase messaging
      print("id: ${widget.notifiedQuoteId} was added to first line...");
      final singleQuote = await dbProvider.querySingleQuote(widget.notifiedQuoteId);
      quoteQuery.insert(0, QuoteQuery(
        quote: singleQuote[0]['quote'],
        userQuoteId: singleQuote[0]['quoteid'],
        authername: singleQuote[0]['authorname'],
        isCollected: singleQuote[0]['quoteCollectedStatus'],
        isBookmarked: singleQuote[0]['quoteBookmarkedStatus'],
        isSeen: singleQuote[0]['isSeenStatus'],
      ));
      dbFunctions.updateUserQuoteStatus(
          singleQuote[0]['quoteid'],
          singleQuote[0]['quoteCollectedStatus'],
          1,
          singleQuote[0]['quoteBookmarkedStatus']);
      if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount)) {
        quoteQuery.insert(1, QuoteQuery(
          quote: singleQuote[0]['quote'],
          userQuoteId: singleQuote[0]['quoteid'],
          authername: singleQuote[0]['authorname'],
          isCollected: singleQuote[0]['quoteCollectedStatus'],
          isBookmarked: singleQuote[0]['quoteBookmarkedStatus'],
          isSeen: singleQuote[0]['isSeenStatus'],
        ));
      }
    } else if(widget.notifiedQuoteId == 100000 && fbMessageTitle == null) {
      quoteQuery.add(QuoteQuery(
        quote: fbMessageBody,
        userQuoteId: 100000,
        authername: "",
        isCollected: 0,
        isBookmarked: 0,
        isSeen: 0,
      ));
    } else {
      print("This page is not being opened by notification...");
      try {
        print("id: $widgetId was added to first line...");
        if(widgetId != null && widgetId != '2352') {
          final singleWidgetQuote = await dbProvider.querySingleQuote(int.parse(widgetId));
          quoteQuery.insert(0, QuoteQuery(
            quote: singleWidgetQuote[0]['quote'],
            userQuoteId: singleWidgetQuote[0]['quoteid'],
            authername: singleWidgetQuote[0]['authorname'],
            isCollected: singleWidgetQuote[0]['quoteCollectedStatus'],
            isBookmarked: singleWidgetQuote[0]['quoteBookmarkedStatus'],
            isSeen: singleWidgetQuote[0]['isSeenStatus'],
          ));
          dbFunctions.updateUserQuoteStatus(
              singleWidgetQuote[0]['quoteid'],
              singleWidgetQuote[0]['quoteCollectedStatus'],
              1,
              singleWidgetQuote[0]['quoteBookmarkedStatus']);
          if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount) && widget.notifiedQuoteId != 100000) {//if rate_us is seen on page_open, alternatively, set the first quote on the second line
            quoteQuery.insert(1, QuoteQuery(
              quote: singleWidgetQuote[0]['quote'],
              userQuoteId: singleWidgetQuote[0]['quoteid'],
              authername: singleWidgetQuote[0]['authorname'],
              isCollected: singleWidgetQuote[0]['quoteCollectedStatus'],
              isBookmarked: singleWidgetQuote[0]['quoteBookmarkedStatus'],
              isSeen: singleWidgetQuote[0]['isSeenStatus'],
            ));
          }
        }
      } catch (e) {
        print("Error on adding widget to first line $e");
      }
    }
    sections.forEach((section) async {
      final allQuotes = await dbProvider.queryAllQuotesOfAnySectionWithIsSeenData(section['sectionid']);
      allQuotes.forEach((quote) {
        if(quote['priority'] == 1) {
          var contain = priorityQuoteQuery.where((myQuote) => myQuote.userQuoteId == quote['quoteid']);
          if (contain.isEmpty) {
            priorityQuoteQuery.add(
                QuoteQuery(
                  quote: quote['quote'],
                  userQuoteId: quote['quoteid'],
                  authername: quote['authorname'],
                  isCollected: quote['quoteCollectedStatus'],
                  isBookmarked: quote['quoteBookmarkedStatus'],
                  isSeen: quote['isSeenStatus'],
                )
            );
            //print("${quote['quoteid']} added, priority length: ${priorityQuoteQuery.length}");
          } else {
            //print("${quote['quoteid']} was added before");
          }
        } else {
          var contain2 = quoteQuery.where((myQuote) => myQuote.userQuoteId == quote['quoteid']);
          if (contain2.isEmpty){
            quoteQuery.add(QuoteQuery(
              quote: quote['quote'],
              userQuoteId: quote['quoteid'],
              authername: quote['authorname'],
              isCollected: quote['quoteCollectedStatus'],
              isBookmarked: quote['quoteBookmarkedStatus'],
              isSeen: quote['isSeenStatus'],
            ));
            setState(() {
              itemCount = itemCount + 1;
            });
          } else {
            //print("${quote['quoteid']} was added before");
          }
        }
      });
      print('Other quote count is: $itemCount');
      if(priorityQuoteQuery.length > 10) shuffleMyArrayStartingFrom1(priorityQuoteQuery);
      if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount)) {
        shuffleMyArrayStartingFrom2(quoteQuery);
      } else {
        shuffleMyArrayStartingFrom1(quoteQuery);
      }
      if(quoteQuery.length>20) {
        List<QuoteQuery> permanentArray = [];
        permanentArray.addAll(quoteQuery.getRange(0, 20));
        displayingQuoteQuery = permanentArray;
      }
      print('displayingQuoteQuery count is: ${displayingQuoteQuery.length}');
      if(queriedSectionCount<sections.length-1) {
        queriedSectionCount ++;
        print("Section queried: $queriedSectionCount");
      } else {
        isQuoteQueryReady = true;
        print("Query ready");
      }
    });
    Future.delayed(const Duration(milliseconds: 3000), () async {
      if(priorityQuoteQuery.length > 0) {
        if(remoteChanger.getRateUsPageNumber() == 0 && _rateUsSeen == false && remoteChanger.getRateUsJson().contains(_openCount)) {
          quoteQuery.insertAll(2, priorityQuoteQuery);//This condition means, first page is rate us block, and second one is the notification or widget quote
        } else {
          quoteQuery.insertAll(1, priorityQuoteQuery);
        }
        if(quoteQuery.length>20) {
          List<QuoteQuery> permanentArray = [];
          permanentArray.addAll(quoteQuery.getRange(0, 20));
          displayingQuoteQuery = permanentArray;
        }
        print("PRIORITY INSERTED");
      }
    });
    Future.delayed(const Duration(milliseconds: 5000), () async {
      if (itemCount < 20) _addExtraQuote();
    });
  }

  void shuffleMyArrayStartingFrom1(List list, [int start = 2, int end]) async {
    var random = Random();
    end ??= list.length;
    var length = end - start;
    while (length > 1) {
      var pos = random.nextInt(length);
      length--;
      var tmp1 = list[start + pos];
      list[start + pos] = list[start + length];
      list[start + length] = tmp1;
    }
  }

  void shuffleMyArrayStartingFrom2(List list, [int start = 3, int end]) async {
    var random = Random();
    end ??= list.length;
    var length = end - start;
    while (length > 1) {
      var pos = random.nextInt(length);
      length--;
      var tmp1 = list[start + pos];
      list[start + pos] = list[start + length];
      list[start + length] = tmp1;
    }
  }


  _addExtraQuote() async {
    print("Adding extra quote");
    final allQuotesWithSeen =
    await dbProvider.queryAllQuotesPreferencesOfUserWithSeen();
    for (int i = 0; i < 200; i++) {
      quoteQuery.add(QuoteQuery(
        quote: allQuotesWithSeen[i]['quote'],
        userQuoteId: allQuotesWithSeen[i]['quoteid'],
        authername: allQuotesWithSeen[i]['authorname'],
        isCollected: allQuotesWithSeen[i]['quoteCollectedStatus'],
        isBookmarked: allQuotesWithSeen[i]['quoteBookmarkedStatus'],
        isSeen: allQuotesWithSeen[i]['isSeenStatus'],
      ));
      setState(() {
        itemCount = itemCount + 1;
        print('Extra quote added: new item count: $itemCount');
      });
    }
    if(quoteQuery.length>20) {
      List<QuoteQuery> permanentArray = [];
      permanentArray.addAll(quoteQuery.getRange(0, 20));
      displayingQuoteQuery = permanentArray;
    }
  }

  _toastInfo(String info) {
    Fluttertoast.showToast(msg: info, toastLength: Toast.LENGTH_LONG);
  }

  _updateQuotesSeenStatusAndCheckNotification(int quoteNumber, GeneralChanger generalChanger) async {
    if(isSeenStatusChangedArray.contains(quoteNumber)) {
      print("Quote seen status has already been changed");
    } else {
      print("Change seen status");
      isSeenStatusChangedArray.add(quoteNumber);
      var pendingNotificationRequests = await flutterLocalNotificationsPlugin.pendingNotificationRequests();
      int matchingNotificationId;
      dbFunctions.updateUserQuoteStatus(
          quoteQuery[quoteNumber].userQuoteId,
          quoteQuery[quoteNumber].isCollected,
          1,
          quoteQuery[quoteNumber].isBookmarked);
      //If quote on the screen was scheduled for reminders, then cancel that reminder...
      var contain = pendingNotificationRequests.where((pendingNotification) {
        setState(() {
          matchingNotificationId = pendingNotification.id;
        });
        return int.parse(pendingNotification.payload) == quoteQuery[quoteNumber].userQuoteId;
      });
      if(contain.isNotEmpty) {
        print("$matchingNotificationId was cancelled");
        await flutterLocalNotificationsPlugin.cancel(matchingNotificationId);
      }
    }
  }


  _updateQuotesSeenStatus(int quoteNumber) async {
    if(isSeenStatusChangedArray.contains(quoteNumber)) {
      print("Quote seen status has already been changed");
    } else {
      print("Change seen status");
      isSeenStatusChangedArray.add(quoteNumber);
      dbFunctions.updateUserQuoteStatus(
          quoteQuery[quoteNumber].userQuoteId,
          quoteQuery[quoteNumber].isCollected,
          1,
          quoteQuery[quoteNumber].isBookmarked);
    }
  }

  List<String> imagePaths = [];

  recommendThisApp() {
    final RenderBox box = context.findRenderObject() as RenderBox;

    Share.share('${AppLocalizations.of(context)
        .translate('recommend')}$kDynamicLinkIQ',
        subject: AppLocalizations.of(context).translate("inspirationalQuotes"),
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  leaveUsAReview() {
    Platform.isAndroid
        ? launch(kUrlPlayStore)
        : launch(kUrlAppStore);
  }

  _firebaseMessagingHandle() {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) async {
      print("Notification got...: $message");
      if (message != null) {
        print("Not Null");
        if(message.notification.title == null) {
          fbMessageBody = message.notification.body;
          return await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    FeedQuotesPage(notifiedQuoteId: int.parse("100000"))
            ),
          );
        } else {
          return await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    FeedQuotesPage()
            ),
          );
        }
      } else {
        print("Message is null");
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      print("Notification got");
      print(notification.title);
      print(notification.body);
      fbMessageBody = notification.body;
      fbMessageTitle = notification.title;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'app_icon',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      print('A new onMessageOpenedApp event was published!');
      if(message.notification.title != null) {
        fbMessageBody = message.notification.body;
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  FeedQuotesPage(notifiedQuoteId: int.parse("100000"))
          ),
        );
      } else {
        return await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  FeedQuotesPage()
          ),
        );
      }
    });
  }
  
}

class CustomScrollPhysics extends ScrollPhysics {
  const CustomScrollPhysics({ScrollPhysics parent}) : super(parent: parent);

  @override
  CustomScrollPhysics applyTo(ScrollPhysics ancestor) {
    return CustomScrollPhysics(parent: buildParent(ancestor));
  }

  @override
  Simulation createBallisticSimulation(
      ScrollMetrics position, double velocity) {
    final tolerance = this.tolerance;
    if ((velocity.abs() < tolerance.velocity) ||
        (velocity > 0.0 && position.pixels >= position.maxScrollExtent) ||
        (velocity < 0.0 && position.pixels <= position.minScrollExtent)) {
      return null;
    }
    return ClampingScrollSimulation(
      position: position.pixels,
      velocity: velocity,
      friction: 0.5,    // <--- HERE
      tolerance: tolerance,
    );
  }
}