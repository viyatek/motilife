import 'dart:async';
import 'dart:ui';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/Utilities/models.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/onboardingScreens/premium_pay.dart';
import 'package:motilife/screens/section_quotes_screen.dart';
import 'package:motilife/screens/single_quote.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:search_page/search_page.dart';
import 'package:simple_search_bar/simple_search_bar.dart';
import 'dart:io';
import '../localizations.dart';
import '../main.dart';
import '../prefs.dart';

final dbProvider = DatabaseProvider.instance;
final functions = AllFunctions();
final dbFunctions = DbFunctions();
List<TopicModel> topicList = [];
String rewardedSectionTitle;
int rewardedSectionId;
bool _allowLiveThemeClick = true;
var connectivityResult;

class TopicsTab extends StatefulWidget {
  @override
  _TopicsTabState createState() => _TopicsTabState();
}

class _TopicsTabState extends State<TopicsTab> {
  List<SearchQuotes> allQuotes = [];
  final AppBarController appBarController = AppBarController();
  List<String> themeProperties;
  List<Topic> topicList = [];
  List<Widget> topicStack = [];

  _queryAllQuotes() async {
    final allRows = await dbProvider.queryAllUnlockedQuotes();
    allRows.forEach((row) => allQuotes.add(SearchQuotes(row['quote'],
        row['authorname'] == null ? '' : row['authorname'], row['quoteid'])));
  }

  _queryAllTopics() async {
    final allTopics = await dbProvider.queryAllTopics();
    allTopics.forEach((topic) {
      topicList.add(
          Topic(
            topicId: topic['topicid'],
            topicName: topic['topicname'],
          )
      );
      topicStack.add(
          TopicNameAndSections(topicId: topic['topicid'], topicName: topic['topicname'])
      );
      setState(() {
        topicList;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    themeProperties = themesChanger.getThemeValues();
    _queryAllTopics();
    _queryAllQuotes();
    //if(!generalChanger.getIsPremiumOrSubscribed()) _runTopicStream(generalChanger);
    /*if (generalChanger.getIsPremiumOrSubscribed() == false && !newGeneralChanger.getIsRewardedReady()) {
      MobileAds.instance.initialize().then((InitializationStatus status) {
        print('Initialization done: ${status.adapterStatuses}');
        MobileAds.instance
            .updateRequestConfiguration(RequestConfiguration(
            tagForChildDirectedTreatment:
            TagForChildDirectedTreatment.unspecified))
            .then((void value) {
          //newGeneralChanger.setRewarded(functions.loadAdmobRewardedAd(context));
          //functions.loadMyAdmobRewardedAd(context);
        });
      });
    }*/
/*    if (!generalChanger.getIsPremiumOrSubscribed() && !newGeneralChanger.getIsInterstitialAdLoaded()) {
      _loadInterstitialAd();
    }*/
  }

  _runTopicStream(GeneralChanger generalChanger) {
    if(streamSubscription != null) {
      streamSubscription.cancel();//if running cancel it
    }
    print("Run stream...");
    final musicChanger = Provider.of<MusicChanger>(context, listen: false);
    Stream stream = streamController.stream;
    streamSubscription = stream.listen((value) {
      print('Value from controller: $value');
      if(value == 1) {
        print("Rewarded run...");
        //streamSubscription.pause();
        musicChanger.getAdvancedPlayer().resume();
        generalChanger.setSectionSessionClickCount(generalChanger.getSectionSessionClickCount() + 1);
        Navigator.pop(context);
        Navigator.push(
            context,
            SlideLeftRoute(
              page: SectionBasedWidget(
                appBarTitle: rewardedSectionTitle,
                sectionId: rewardedSectionId,
                isAllowedLiveTheme: true,//_allowLiveThemeClick,
              ),
            ));
      }
      if(value == 0) {
        print("Ad closed, so cancel stream");
        //streamSubscription.pause();
      }
    });
  }

  @override
  void dispose() {
    print("Topic Section Disposed");
    //streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final dayNightProvider = Provider.of<DayNightChanger>(context);
    final themesChanger = Provider.of<ThemeChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    String searchText;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('topics'),
          style: TextStyle(
            fontFamily: kDefaultFontName,
            color: Colors.white,
            fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
            fontWeight: FontWeight.w500,
          ),
        ),
        toolbarHeight: heightScreen * 0.07,
        backgroundColor: Colors.black,
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      /*: CupertinoNavigationBar(
        middle: Text(
          AppLocalizations.of(context).translate('topics'),
          style: TextStyle(
            fontFamily: kDefaultFontName,
            color: Colors.white,
            fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.black,
        //actionsForegroundColor: Colors.white,
        //brightness:
      ),*/
      body: Container(
        height: heightScreen * 1,
        width: widthScreen * 1,
        decoration: BoxDecoration(
          color: Colors.black,
          /*image: DecorationImage(
            image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
            fit: BoxFit.cover,
          ),*/
        ),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: kSigmaX, sigmaY: kSigmaY),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.03),
            child: ListView(
              children: [
                Container(
                  height: heightScreen * 0.01,
                ),
                GestureDetector(
                  onTap: () {
                    //appBarController.stream.add(true);
                    showSearch(
                      //query: searchText,
                      context: context,
                      delegate: SearchPage<SearchQuotes>(
                        barTheme: Theme.of(context).copyWith(
                          backgroundColor: Color(0xFF1f1f1f),
                          canvasColor: Color(0xFF1f1f1f),
                          primaryColor: Color(0xFF1f1f1f),
                          textTheme: TextTheme(
                            headline6: TextStyle(
                              color: Colors.white,
                              fontSize: MediaQuery.of(context).size.width * 0.04,
                            ),
                          ),
                          inputDecorationTheme:
                          InputDecorationTheme(
                            hintStyle: TextStyle(
                              color: Theme.of(context).primaryTextTheme.caption.color,
                              fontSize: MediaQuery.of(context).size.width * 0.04,
                            ),
                          ),
                        ),
                        items: allQuotes,
                        searchLabel: AppLocalizations.of(context).translate('search'),
                        suggestion: Container(
                          height: heightScreen * 1,
                          width: widthScreen * 1,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Center(
                            child: Text(''),
                          ),
                        ),
                        failure: Container(
                          height: heightScreen * 1,
                          width: widthScreen * 1,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: kSigmaX, sigmaY: kSigmaY),
                            child: Center(
                              child: Text(
                                AppLocalizations.of(context).translate('nothingFound'),
                                style: TextStyle(
                                  fontFamily: kDefaultFontName,
                                  fontSize: MediaQuery.of(context).size.width * 0.05,
                                  fontWeight: FontWeight.w500,
                                  color: themesChanger.getButtonColor(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        filter: (quote) => [
                          quote.quote,
                          quote.authorname,
                        ],
                        builder: (quote) => Container(
                          decoration: BoxDecoration(
                            /*image: DecorationImage(
                                            image: AssetImage('assets/themes/${themeProperties[11]}.jpg'),
                                            fit: BoxFit.cover,
                                          ),*/
                            color: Colors.black,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => SingleQuotePage(quoteId: quote.quoteId, appBarTitle: AppLocalizations.of(context).translate('searchresults'),)));
                            },
                            child: Container(
                              //elevation: 2,
                              margin: EdgeInsets.symmetric(vertical: heightScreen*0.02, horizontal: widthScreen*.03),
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.07),
                                borderRadius: BorderRadius.circular(7),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ConstrainedBox(
                                      constraints: BoxConstraints(
                                        minHeight: heightScreen * 0.05,
                                        maxHeight: heightScreen * 0.15,
                                      ),
                                      child: AutoSizeText(
                                        quote.quote,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: kDefaultFontName,
                                          fontSize: MediaQuery.of(context).size.width * 0.04,
                                          fontWeight: FontWeight.w500,
                                          color: themesChanger.getButtonColor(),
                                        ),
                                      ),
                                    ),
                                    if(quote.authorname != null) Container(
                                      margin: EdgeInsets.only(top: heightScreen*.02),
                                      child: Text(
                                        quote.authorname,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: kDefaultFontName,
                                          fontSize: MediaQuery.of(context).size.width * 0.04,
                                          fontWeight: FontWeight.w600,
                                          color: themesChanger.getButtonColor(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                  child: Container(
                    height: heightScreen * 0.06,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: SearchAppBar(
                        primary: Colors.black,
                        appBarController: appBarController,
                        statusBarColor: Colors.grey,
                        // You could load the bar with search already active
                        autoSelected: false,
                        searchHint: AppLocalizations.of(context).translate('searchHint'),
                        mainTextColor: Color(0xFF424242),
                        onChange: (String value) {
                          searchText = value;
                        },
                        //Will show when SEARCH MODE wasn't active
                        mainAppBar: AppBar(
                          automaticallyImplyLeading: false,
                          title: Row(
                            children: [
                              Container(
                                  height: heightScreen * 0.03,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        right: 10.0),
                                    child: Image.asset(
                                        'assets/signs2/search.png',
                                        scale: 2.8,
                                        color: Colors.white60),
                                  )),
                              Text(
                                AppLocalizations.of(context).translate('search'),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontFamily: kDefaultFontName,
                                  fontSize: MediaQuery.of(context).size.width * 0.04,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white60,
                                ),
                              ),
                            ],
                          ),
                          backgroundColor: Color(0xFF1f1f1f),
                          //actions: <Widget>[],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: heightScreen * 0.02,
                ),
                if(!generalChanger.getIsPremiumOrSubscribed()) GestureDetector(
                  onTap: () {
                    functions.sendAnalyticsWithOwnName('Topics_Premium_Button');
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PremiumPayPage(
                                directedPage: 'Topics_Premium_Button')));
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: widthScreen * 0.06),
                    height: heightScreen * 0.12,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            'assets/realImages/premium.png'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate('gopremium'),
                          style: TextStyle(
                            fontFamily: kDefaultFontName,
                            fontSize: MediaQuery.of(context).size.width * 0.058,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(width: widthScreen * 0.09),
                        Image.asset(
                          'assets/signs2/next.png',
                          scale: 2.5,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: heightScreen * 0.032,
                ),
                Column(
                  children: topicStack,
                ),
              ],
              shrinkWrap: true,
            ),
          ),
        ),
      ),
    );
  }

/*  void _loadInterstitialAd() {
    print("Trying to load interstitial in topic");
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    FacebookInterstitialAd.loadInterstitialAd(
      placementId: "IMG_16_9_APP_INSTALL#2312433698835503_2650502525028617",
      //placementId: remoteChanger.getFbInterstitialPlacementId(),
      listener: (result, value) {
        print(">> FAN > Interstitial Ad: $result --> $value");
        if (result == InterstitialAdResult.LOADED){
          newGeneralChanger.setIsInterstitialAdLoaded(true);
        }
        /// Once an Interstitial Ad has been dismissed and becomes invalidated,
        /// load a fresh Ad by calling this function.
        if (result == InterstitialAdResult.DISMISSED &&
            value["invalidated"] == true) {
          newGeneralChanger.setIsInterstitialAdLoaded(false);
          _loadInterstitialAd();
        }
        if(result == InterstitialAdResult.ERROR) {
          newGeneralChanger.setIsInterstitialAdLoaded(false);
        }
      },
    );
  }*/

/*  void createRewardedAd() {
    _rewardedAd ??= RewardedAd(
      adUnitId: RewardedAd.testAdUnitId,
      request: kRequest,
      listener: AdListener(
          onAdLoaded: (Ad ad) {
            print('${ad.runtimeType} loaded.');
            _rewardedReady = true;
          },
          onAdFailedToLoad: (Ad ad, LoadAdError error) {
            print('${ad.runtimeType} failed to load: $error');
            ad.dispose();
            _rewardedAd = null;
            createRewardedAd();
          },
          onAdOpened: (Ad ad) => print('${ad.runtimeType} onAdOpened.'),
          onAdClosed: (Ad ad) {
            print('${ad.runtimeType} closed.');
            ad.dispose();
            createRewardedAd();
          },
          onApplicationExit: (Ad ad) =>
              print('${ad.runtimeType} onApplicationExit.'),
          onRewardedAdUserEarnedReward: (RewardedAd ad, RewardItem reward) {
            print(
              '$RewardedAd with reward $RewardItem(${reward.amount}, ${reward.type})',
            );
          }),
    )..load();
  }*/

}

class TopicNameAndSections extends StatelessWidget {
  TopicNameAndSections({this.topicId, this.topicName});

  final int topicId;
  final String topicName;

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: heightScreen*.005),
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              topicName,
              style: TextStyle(
                fontFamily: kDefaultFontName,
                fontSize: MediaQuery.of(context).size.width * 0.054,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
          ),
        ),
        Container(
          height: heightScreen * 0.26,
          child: SectionSlider(
            topicId: topicId,
          ),
        ),
        Container(height: heightScreen*.02),
      ],
    );
  }
}

class SectionSlider extends StatefulWidget {
  final int topicId;
  final bool allowLiveTheme;
  SectionSlider({this.topicId, this.allowLiveTheme});

  @override
  _SectionSliderState createState() => _SectionSliderState();
}

class _SectionSliderState extends State<SectionSlider> {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  Timer _timer;
  int _start = 6;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final musicChanger = Provider.of<MusicChanger>(context);
    final orientation = MediaQuery.of(context).orientation;
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context);
    final generalChanger = Provider.of<GeneralChanger>(context);
    final remoteNotifyChanger = Provider.of<RemoteNotifyChanger>(context);
    return FutureBuilder(
      future: dbProvider.queryAllSectionsOfAnyTopic(widget.topicId),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return GridView.builder(
            itemCount: snapshot.data.length,
            scrollDirection: Axis.horizontal,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.53,
                crossAxisCount: (orientation == Orientation.portrait) ? 2 : 1),
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () async {
                  functions.sendAnalyticsEventFromSectionClick(snapshot.data[index]['sectionname']);
                  if (generalChanger.getIsPremiumOrSubscribed()) {
                    _startTimer();
                    Navigator.push(
                        context,
                        SlideLeftRoute(
                          page: SectionBasedWidget(
                            appBarTitle: snapshot.data[index]['sectionname'],
                            sectionId: snapshot.data[index]['sectionid'],
                            isAllowedLiveTheme: _allowLiveThemeClick,
                          ),
                        ));
                  } else {
                    if (snapshot.data[index]['isPremium'] == 1) {
                      Navigator.push(
                          context,
                          SlideLeftRoute(
                            page: PremiumPayPage(
                              directedPage: 'Section_Premium',
                            ),
                          ));
                    } else {
                      _startTimer();
                      Navigator.push(
                          context,
                          SlideLeftRoute(
                            page: SectionBasedWidget(
                              appBarTitle: snapshot.data[index]['sectionname'],
                              sectionId: snapshot.data[index]['sectionid'],
                              isAllowedLiveTheme: _allowLiveThemeClick,
                            ),
                          ));
                    }

                      /*if (remoteNotifyChanger.getSectionRewardedJson().contains(generalChanger.getSectionSessionClickCount())) {
                      var result = await newGeneralChanger.getMopupRewarded().isReady();
                      print('Is mopup Ready $result');
                      if(newGeneralChanger.getIsRewardedReady()) {
                        showDialog(context: context,
                            builder: (BuildContext context) {
                              return RewardedPopUp(
                                imageName:
                                'section_images/${snapshot.data[index]['sectionimagename']}',
                                text: AppLocalizations.of(context)
                                    .translate('openThisSection'),
                                firstOptionButtonText:
                                AppLocalizations.of(context).translate('watchVideo'),
                                secondOptionButtonText:
                                AppLocalizations.of(context).translate('upgradetopremium'),
                                firstOptionScreen: () {
                                  rewardedSectionTitle = snapshot.data[index]['sectionname'];
                                  rewardedSectionId = snapshot.data[index]['sectionid'];
                                  musicChanger.getAdvancedPlayer().stop();
                                  newGeneralChanger.getMopupRewarded().show();
                                  newGeneralChanger.setMopupRewarded(functions.loadMyMopupRewardedAd(context));

/*                                  newGeneralChanger.getRewarded().show(onUserEarnedReward: (RewardedAd ad, RewardItem rewardItem) {
                                    _topicRewarded(generalChanger, musicChanger);
                                  });*/
                                  //newGeneralChanger.setIsRewardedReady(false);
                                  //functions.loadMyAdmobRewardedAd(context);
                                  //newGeneralChanger.setRewarded(functions.loadAdmobRewardedAd(context));
                                },
                                secondOptionScreen: PremiumPayPage(
                                  directedPage: 'Section_Free',
                                ),
                              );
                            });
                      } else {
                        if(remoteNotifyChanger.getForceVideoAdsValue() == 0 && connectivityResult != ConnectivityResult.none) {
                          //newGeneralChanger.setRewarded(functions.loadAdmobRewardedAd(context));
                          //functions.loadMyAdmobRewardedAd(context);
                          newGeneralChanger.setMopupRewarded(functions.loadMyMopupRewardedAd(context));
                          print("Ad is not loaded but enjoy...");
                          _startTimer();
                          Navigator.push(
                              context,
                              SlideLeftRoute(
                                page: SectionBasedWidget(
                                  appBarTitle: snapshot.data[index]['sectionname'],
                                  sectionId: snapshot.data[index]['sectionid'],
                                  isAllowedLiveTheme: _allowLiveThemeClick,
                                ),
                              ));
                        } else {
                          print("Ad is not loaded yet you have to watch...");
                          //newGeneralChanger.setRewarded(functions.loadAdmobRewardedAd(context));
                          //functions.loadMyAdmobRewardedAd(context);
                          newGeneralChanger.setMopupRewarded(functions.loadMyMopupRewardedAd(context));
                          showDialog(
                              context: context,
                              builder: (BuildContext
                              context) {
                                return WarningPopUp(
                                  text: AppLocalizations.of(context).translate('checkConnection'),);
                              });
                        }
                      }
                    } else {
                      _startTimer();
                      print('Click count ${generalChanger.getSectionSessionClickCount()}');
                      generalChanger.setSectionSessionClickCount(
                          generalChanger.getSectionSessionClickCount() + 1);
                      Navigator.push(
                          context,
                          SlideLeftRoute(
                            page: SectionBasedWidget(
                              appBarTitle: snapshot.data[index]['sectionname'],
                              sectionId: snapshot.data[index]['sectionid'],
                              isAllowedLiveTheme: _allowLiveThemeClick,
                            ),
                          ));
                    }*/
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(
                      right: widthScreen * 0.03, top: 5, bottom: 5),
                  child: GridTile(
                    header: snapshot.data[index]['isPremium'] == 1 &&
                        snapshot.data[index]['sectionIsUnlockedStatus'] == 0
                        ? Container(
                      alignment: Alignment.bottomRight,
                      margin: EdgeInsets.only(top: 5, right: 5),
                      child: ImageIcon(
                        AssetImage('assets/signs2/lock.png'),
                        size: MediaQuery.of(context).size.width * 0.029,
                        color: Colors.white,
                      ),
                    )
                        : Container(),
                    child: Container(
                      //decoration: themeChanger.getDecorationCard(),
                      //margin: EdgeInsets.symmetric(horizontal: widthScreen*0.004),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        image: DecorationImage(
                          image: AssetImage(
                              'assets/section_images/${snapshot.data[index]['sectionimagename']}.png'),
                          fit: BoxFit.fill,
                        ),
                      ),
                      child: Container(
                        //height: heightScreen * 0.1,
                        width: widthScreen * 0.2,
                        padding: EdgeInsets.symmetric(horizontal: widthScreen * 0.02),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            margin: EdgeInsets.only(bottom: heightScreen*0.003),
                            /*decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.3),
                              borderRadius: BorderRadius.circular(8),
                            ),*/
                            padding: EdgeInsets.symmetric(horizontal: widthScreen * 0.01, vertical: heightScreen*0.003),
                            child: AutoSizeText(
                              snapshot.data[index]['sectionname'],
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: kDefaultFontName,
                                  fontSize: MediaQuery.of(context).size.width * 0.038,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          );
        } else {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        }
      },
    );
  }

  void _startTimer() {
    final newGeneralChanger = Provider.of<NewGeneralChanger>(context, listen: false);
    if(newGeneralChanger.getUsingLiveTheme()) {
      Future.delayed(Duration(milliseconds: 200), () {
        _allowLiveThemeClick = false;
      });
      setState(() {
        _start = 7;
      });
      print("Timer began");
      const oneSec = const Duration(seconds: 1);
      _timer = new Timer.periodic(
        oneSec,
            (Timer timer) {
          print("Timer: $_start");
          if (_start == 0) {
            setState(() {
              print("Live theme blocked finished...");
              _allowLiveThemeClick = true;
              timer.cancel();
            });
          } else {
            setState(() {
              _start--;
            });
          }
        },
      );
    }
  }

  _topicRewarded(GeneralChanger generalChanger, MusicChanger musicChanger) {
    print("Rewarded run...");
    //streamSubscription.pause();
    musicChanger.getAdvancedPlayer().resume();
    generalChanger.setSectionSessionClickCount(generalChanger.getSectionSessionClickCount() + 1);
    Navigator.pop(context);
    Navigator.push(
        context,
        SlideLeftRoute(
          page: SectionBasedWidget(
            appBarTitle: rewardedSectionTitle,
            sectionId: rewardedSectionId,
            isAllowedLiveTheme: true,//_allowLiveThemeClick,
          ),
        ));
  }

}

class SearchQuotes {
  final String quote, authorname;
  final int quoteId;

  SearchQuotes(this.quote, this.authorname, this.quoteId);
}