import 'dart:ui';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:connectivity/connectivity.dart';
//import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motilife/Utilities/constants.dart';
import 'package:motilife/Utilities/db_functions.dart';
import 'package:motilife/Utilities/functions.dart';
import 'package:motilife/Utilities/notification_functions.dart';
import 'package:motilife/Utilities/page_transitions.dart';
import 'package:motilife/onboardingScreens/pick_topic.dart';
import 'package:motilife/onboardingScreens/welcome_screen.dart';
import 'package:motilife/popups/warning_popup.dart';
import 'package:motilife/screens/bookmarks_page.dart';
import 'package:motilife/screens/feed_quotes_page.dart';
import 'package:motilife/screens/quotes_bubble.dart';
import 'package:share/share.dart';
import 'package:simple_search_bar/simple_search_bar.dart';
import 'package:url_launcher/url_launcher.dart';
import '../localizations.dart';
import 'package:provider/provider.dart';
import 'package:motilife/notifier_provider.dart';
import 'package:motilife/DbProvider.dart';
import 'package:motilife/screens/single_quote.dart';
import 'package:motilife/prefs.dart';
import 'dart:io';

final dbProvider = DatabaseProvider.instance;
final allFunctions = AllFunctions();

class SettingsTab extends StatefulWidget {
  @override
  _SettingsTabState createState() => _SettingsTabState();
}

class _SettingsTabState extends State<SettingsTab> {
  bool isPremium = false;
  final AppBarController appBarController = AppBarController();
  List<String> themeProperties;
  var connectivityResult;
  final functions = AllFunctions();
  final dbFunctions = DbFunctions();

  _getIsPremium() async {
    bool value = await SharedPreferencesHelper.getIsPremiumStatus();
    setState(() {
      isPremium = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _getIsPremium();
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    final themesChanger = Provider.of<ThemeChanger>(context, listen: false);
    themeProperties = themesChanger.getThemeValues();
    if (generalChanger.getIsPremiumOrSubscribed() == false) {
      //allFunctions.loadAdmobRewardedAd(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: Text(
            AppLocalizations.of(context).translate('settings'),
            style: TextStyle(
              fontFamily: kDefaultFontName,
              color: Colors.white,
              fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
              fontWeight: FontWeight.w500,
            ),
          ),
          toolbarHeight: heightScreen * 0.07,
          backgroundColor: Colors.black,
          centerTitle: true,
          elevation: 0.0,
          leading: IconButton(
            icon: ImageIcon(AssetImage('assets/signs2/back.png'), color: Colors.white, size: widthScreen*0.05),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),/* : CupertinoNavigationBar(
          middle: Text(
            AppLocalizations.of(context).translate('settings'),
            style: TextStyle(
              fontFamily: kDefaultFontName,
              color: Colors.white,
              fontSize: MediaQuery.of(context).size.width * kAppBarFontSize,
              fontWeight: FontWeight.w500,
            ),
          ),
          backgroundColor: Colors.black,
        ),*/
        body: Container(
          height: heightScreen * 1,
          width: widthScreen * 1,
          decoration: BoxDecoration(
            color: Colors.black,
            /*image: DecorationImage(
              image: AssetImage('assets/themes/${themeProperties[17]}.jpg'),
              fit: BoxFit.cover,
            ),*/
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: kSigmaY, sigmaY: kSigmaY),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: widthScreen * 0.03),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: heightScreen*0.08,
                    ),
                    GestureDetector(
                      onTap: () {
                        functions.sendAnalyticsWithOwnName('Big Bookmark Clicked');
                        HapticFeedback.lightImpact();
                        Navigator.push(context, MaterialPageRoute(builder: (context) => BookmarkTab()));
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: widthScreen * 0.06),
                        height: heightScreen * 0.12,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/realImages/bookmarknext.png'),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AppLocalizations.of(context).translate('bookmarks'),
                                  style: TextStyle(
                                    fontFamily: kDefaultFontName,
                                    fontSize: MediaQuery.of(context).size.width * 0.045,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white,
                                  ),
                                ),
                                Container(height: heightScreen*.015),
                                Text(
                                  AppLocalizations.of(context).translate('bookmarksNote'),
                                  style: TextStyle(
                                    fontFamily: kDefaultFontName,
                                    fontSize: MediaQuery.of(context).size.width * 0.04,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xFF98979e),
                                  ),
                                ),
                              ],
                            ),
                            Image.asset('assets/signs2/next.png', scale: 2.5),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: heightScreen*0.01,
                    ),
                    SettingCard(imageName: 'instagram', text: 'instagramFollow', func: navigateToInstagram),
                    SettingCardWithIcon(imageName: 'content', text: 'selectContentTypes', func: navigateToTopicPreferences, rightImageName: 'next'),
                    SettingCardWithIcon(imageName: 'reminder', text: 'setQuoteReminders', func: navigateToWelcomeScreen, rightImageName: 'next'),
                    SettingCardNotification(),
                    SettingCard(imageName: 'recommend', text: 'recommendThisApp', func: recommendThisApp),
                    SettingCard(imageName: 'rate', text: 'rateUs', func: rateUs),
                    SettingCard(imageName: 'review', text: 'leaveUsAReview', func: leaveUsAReview),
                    SettingCard(imageName: 'restore', text: 'restorePurchases', func: restorePurchases),//TODO not working
                    SettingCard(imageName: 'privacy', text: 'privacy', func: navigateToPrivacyPol),
                    SettingCard(imageName: 'terms', text: 'terms', func: navigateToTermsAndCond),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  navigateToTopicPreferences() {
    Navigator.push(context, SlideLeftRoute(page: PickTopicScreen()));
  }
  navigateToWelcomeScreen() {
    Navigator.push(context, SlideLeftRoute(page: WelcomeScreen()));
  }

  recommendThisApp() {
    //final RenderBox box = context.findRenderObject() as RenderBox;

    Share.share('${AppLocalizations.of(context).translate('recommend')}\n$kDynamicLinkIQ',
        //subject: AppLocalizations.of(context).translate('inspirationalQuotes'),
        //sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size
    );
  }
  leaveUsAReview() {
    Platform.isAndroid
        ? launch(kUrlPlayStore)
        : launch(kUrlAppStoreReview);
  }

  rateUs() {
    Platform.isAndroid
        ? launch(kUrlPlayStore)
        : launch(kUrlAppStore);
  }

  navigateToTermsAndCond() {
    launch('https://viyatek.io/terms/');
  }

  navigateToPrivacyPol() {
    launch('https://viyatek.io/privacy/');
  }

  navigateToInstagram() async{
    var url = 'https://www.instagram.com/inspirationalquotesapp/';
    if (await canLaunch(url)) {
      print("Launching instagram...");
      await launch(
        url,
        universalLinksOnly: true,
      );
    } else {
      throw 'There was a problem to open the url: $url';
    }
  }

  restorePurchases() async {
    connectivityResult =
        await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return WarningPopUp(
                text: AppLocalizations.of(context)
                    .translate('checkConnection'),);
          });
    } else {
      if (Platform.isAndroid) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return FutureBuilder(
                  future: initStoreAndroid(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return WarningPopUp(
                          text: snapshot.data == true ? AppLocalizations.of(context)
                              .translate('restoreSuccess') : AppLocalizations.of(context)
                              .translate('restoreFail'),);
                    } else {
                      return SpinKitPulse(
                        itemBuilder: (_, int index) {
                          return DecoratedBox(
                            decoration: BoxDecoration(
                                color: index.isEven ? Colors.black54 : Colors.white,
                                shape: BoxShape.circle
                            ),
                          );
                        },
                        size: 120.0,
                      );
                    }
                  });
            });
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return FutureBuilder(
                  future: initStoreInfoIOS(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return WarningPopUp(
                          text: snapshot.data == true ? AppLocalizations.of(context)
                              .translate('restoreSuccess') : AppLocalizations.of(context)
                              .translate('restoreFail'),);
                    } else {
                      return Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.white),
                          backgroundColor: Colors.black,
                        ),
                      );
                    }
                  });
            });
      }
    }
  }
  Future<bool> initStoreAndroid() async {
    List<IAPItem> _items = [];
    List<PurchasedItem> _purchases = [];
    bool isPremium = false;
    List<PurchasedItem> items = await FlutterInappPurchase.instance.getAvailablePurchases();
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    for (var item in items) {
      if (item.productId == kLifetimeMemberShipId) {
        //Restore premium purchase
        dbFunctions.updateThemesUnlockedStatus(1);
        dbFunctions.updateLiveThemesUnlockedStatus(1);
        dbFunctions.updateSectionsUnlockedStatus(1);
        dbFunctions.updateMusicsUnlockedStatus(1);
        dbFunctions.updateFontsUnlockedStatus(1);
        await SharedPreferencesHelper.setIsPremiumStatus(true);
        generalChanger.setIsPremiumOrSubscribed(true);
        print('We have restored your premium purchase. You are premium, AGAIN.');
        isPremium = true;
        return Future<bool>.value(true);
      } else {
        if (await functions.verifyPurchaseWithDate(item)) {
          _purchases.add(item);
        }
      }
    }
    if(!isPremium){
      if (_purchases.length > 0) {
        dbFunctions.updateThemesUnlockedStatus(1);
        dbFunctions.updateLiveThemesUnlockedStatus(1);
        dbFunctions.updateSectionsUnlockedStatus(1);
        dbFunctions.updateMusicsUnlockedStatus(1);
        dbFunctions.updateFontsUnlockedStatus(1);
        generalChanger.setIsPremiumOrSubscribed(true);
        return Future<bool>.value(true);
      } else {
        print('no available purchase');
        return Future<bool>.value(false);
      }
    }
  }


  Future<bool> initStoreInfoIOS() async {
    bool isPremium = false;
    List<PurchasedItem> items = await FlutterInappPurchase.instance
        .getAvailablePurchases();
    //List<PurchasedItem> items = await FlutterInappPurchase.instance.getPurchaseHistory();
    GeneralChanger generalChanger = Provider.of<GeneralChanger>(
        context, listen: false);
    for (var item in items) {
      if (item.productId == kLifetimeMemberShipId) {
        //Restore premium purchase
        dbFunctions.updateThemesUnlockedStatus(1);
        dbFunctions.updateLiveThemesUnlockedStatus(1);
        dbFunctions.updateSectionsUnlockedStatus(1);
        dbFunctions.updateMusicsUnlockedStatus(1);
        dbFunctions.updateFontsUnlockedStatus(1);
        await SharedPreferencesHelper.setIsPremiumStatus(true);
        generalChanger.setIsPremiumOrSubscribed(true);
        print('We have restored your premium purchase. You are premium, AGAIN.');
        isPremium = true;
        return Future<bool>.value(true);
      }
    }
    if(!isPremium){
      int expiryTimeMillis = await functions.getExpiryDateForIOS();
      if(expiryTimeMillis > DateTime.now().millisecondsSinceEpoch){
        print('$expiryTimeMillis > ${DateTime.now().millisecondsSinceEpoch}');
        await SharedPreferencesHelper.setExpiryTime(expiryTimeMillis);
        dbFunctions.updateThemesUnlockedStatus(1);
        dbFunctions.updateLiveThemesUnlockedStatus(1);
        dbFunctions.updateSectionsUnlockedStatus(1);
        dbFunctions.updateMusicsUnlockedStatus(1);
        dbFunctions.updateFontsUnlockedStatus(1);
        generalChanger.setIsPremiumOrSubscribed(true);
        return Future<bool>.value(true);
      } else {
        print(':((( $expiryTimeMillis < ${DateTime.now().millisecondsSinceEpoch}');
        return Future<bool>.value(false);
      }
    }
  }
}

class SettingCardWithIcon extends StatelessWidget {
  final String imageName;
  final String text;
  final Function func;
  final String rightImageName;

  SettingCardWithIcon({this.imageName, this.text, this.func, this.rightImageName});

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap:() {
        func();
        HapticFeedback.lightImpact();
      },
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Container(
          padding: EdgeInsets.only(right: widthScreen*0.03),
          child: Column(
            children: [
              SizedBox(height: heightScreen*0.02),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Image.asset(
                          'assets/signs2/$imageName.png',
                        scale: 1.65,
                      ),
                      SizedBox(width: widthScreen*0.06,),
                      Text(
                        AppLocalizations.of(context).translate(text),
                        style: TextStyle(
                          fontFamily: kDefaultFontName,
                          fontSize: MediaQuery.of(context).size.width * 0.04,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  Container(
                      padding: EdgeInsets.only(right: widthScreen * 0.03),
                      child: Image.asset('assets/signs2/$rightImageName.png', scale: 2.5)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SettingCard extends StatelessWidget {
  final String imageName;
  final String text;
  final Function func;

  SettingCard({this.imageName, this.text, this.func});

  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        func();
        HapticFeedback.lightImpact();
      },
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Column(
          children: [
            SizedBox(height: heightScreen*0.02,),
            Row(
              children: [
                Image.asset(
                  'assets/signs2/$imageName.png',
                  scale: 1.65,
                ),
                SizedBox(width: widthScreen*0.06,),
                AutoSizeText(
                  AppLocalizations.of(context).translate(text),
                  style: TextStyle(
                    fontFamily: kDefaultFontName,
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SettingCardNotification extends StatefulWidget {
  @override
  _SettingCardNotificationState createState() => _SettingCardNotificationState();
}

class _SettingCardNotificationState extends State<SettingCardNotification> {
  final notificationFunctions = NotificationHandler();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  Future _onSelectNotification(String payload) async {
    final generalChanger = Provider.of<GeneralChanger>(context, listen: false);
    final remoteChanger = Provider.of<RemoteNotifyChanger>(context, listen: false);
    print('Is first OPEN: ${generalChanger.getIsFirstOpen()}');
    if (payload != null) {
      print('notification payloads: $payload ggg');
    }
    if (generalChanger.getIsFirstOpen()) {
      generalChanger.setIsFirstOpen(false);
      if(remoteChanger.getShowFeed()) {
        return await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => FeedQuotesPage(notifiedQuoteId: int.parse(payload))));
      } else {
        return await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => NewHomePage(notifiedQuoteId: int.parse(payload))));
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    double heightScreen = MediaQuery.of(context).size.height;
    double widthScreen = MediaQuery.of(context).size.width;
    final generalChanger = Provider.of<GeneralChanger>(context);
    return GestureDetector(
      onTap: () {

      },
      child: Container(
        padding: EdgeInsets.only(right: widthScreen*0.03),
        child: Column(
          children: [
            SizedBox(height: heightScreen*0.02,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(width: widthScreen*0.012,),
                    Image.asset(
                      'assets/signs2/notification.png',
                      scale: 1.65,
                    ),
                    SizedBox(width: widthScreen*0.06,),
                    Text(
                      AppLocalizations.of(context).translate('notifications'),
                      style: TextStyle(
                        fontFamily: kDefaultFontName,
                        fontSize: MediaQuery.of(context).size.width * 0.04,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Transform.scale(
                  scale: 1.0,
                  child: generalChanger.getIsProcessing()
                      ? CupertinoActivityIndicator()
                      : Switch(
                    value: generalChanger.getIsNotifiable(),
                    onChanged: (value) async {
                      HapticFeedback.lightImpact();
                      print('Is first OPEN: ${generalChanger.getIsFirstOpen()}');
                      generalChanger.setIsProcessing(true);
                      var initializationSettingsAndroid =
                      AndroidInitializationSettings('app_icon'); //@mipmap/ic_launcher
                      var initializationSettingsIOS =
                      IOSInitializationSettings();
                      final InitializationSettings
                      initializationSettings =
                      InitializationSettings(
                          android:
                          initializationSettingsAndroid,
                          iOS: initializationSettingsIOS);
                      flutterLocalNotificationsPlugin =
                          FlutterLocalNotificationsPlugin();
                      flutterLocalNotificationsPlugin.initialize(
                          initializationSettings,
                          onSelectNotification:
                          _onSelectNotification);
                      setState(() {
                        print(value);
                      });
                      value
                          ? notificationFunctions.handlePendingNotifications().catchError(
                            (onError) {
                          print("ERROR...Cannot handle Pending Notifications...");
                        },
                      ).whenComplete(() {
                        print("Pending quotes have been updated.");
                        if(generalChanger.getNotificationCount() != 0) notificationFunctions.setNotificationTimePeriod();
                      })
                          : notificationFunctions.handlePendingNotifications().catchError(
                            (onError) {
                          print("ERROR...Cannot handle Pending Notifications...");
                        },
                      ).whenComplete(() {
                        print("Pending quotes have been updated.");
                        notificationFunctions.cancelAllNotifications();
                      });
                      await SharedPreferencesHelper
                          .setNotifiableStatus(value);
                      generalChanger.setIsNotifiable(value);
                      generalChanger.setIsProcessing(false);
                    },
                    activeTrackColor: Colors.green,
                    activeColor: Colors.white,
                    inactiveThumbColor: Colors.white,
                    inactiveTrackColor: Colors.grey,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
