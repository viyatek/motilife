import 'package:shared_preferences/shared_preferences.dart';
class SharedPreferencesHelper {

  static final String _dailyCount = "dailyNotificationCount";
  static final String _appVersion = "currentAppVersion";
  static final String _buildNumber = "buildNumber";
  static final String _isSeenIntro = "isSeenIntro";
  static final String _isNightMode = "isNightMode";
  static final String _isNotificationOpen = "isNotifiable";
  static final String _isPremium = "isPremium";
  static final String _isNotificationSet = "isNotificationSet";
  static final String _expiryTime = "expiryTime";
  static final String _themeProperties = "themeProperties";
  static final String _isSeenRateUs = "isSeenRateUs";
  static final String _isWarnedNotificationLimitIos = 'isWarnedNotificationLimitIos';
  static final String _isSeenGif = 'isSeenGif';
  static final String _isRecordDbCreatedEvent = 'isRecordDbCreatedEvent';
  static final String _isHomeScreenFirstOpen = 'isHomeScreenFirstOpen';
  static final String _isUserExperienceSeen = 'isUserExperienceSeen';
  static final String _appOpenCount = 'appOpenCount';
  static final String _reminderStartHour = 'reminderStartHour';
  static final String _reminderStartMinute = 'reminderStartMinute';
  static final String _reminderEndHour = 'reminderEndHour';
  static final String _reminderEndMinute = 'reminderEndMinute';
  static final String _musicPreference = 'musicPreference';
  static final String _fontProperties = 'fontProperties';
  static const PREFS_WIDGET_SETTINGS = 'widget_settings_';
  static final String _dbLockValue = 'dbLockValue';
  static final String _isPremiumOrSubscribe = "isPremiumOrSubscribe";
  static final String _timezone = "timezone";
  static final String _newThemesAlert = "newThemesAlert";
  static final String _newTopicsAlert = "newTopicsAlert";
  static final String _newSettingsAlert = "newSettingsAlert";
  static final String _starRate = "starRate";
  static final String _localLanguageCode = "localLanguageCode";
  static final String _isUsingLiveTheme = "isUsingLiveTheme";
  static final String _versionRelease = "versionRelease";
  static final String _firstOpenReportedToFB = "firstOpenReportedToFB";

  static setNotificationCount(int count) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_dailyCount, count);
  }

  static Future<int> getNotificationCount() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_dailyCount) ?? 5;
  }

  static setCurrentAppVersion(String version) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_appVersion, version);
  }

  static Future<String> getCurrentAppVersion() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_appVersion) ?? '1.0.0';
  }

  static setCurrentBuildNumber(String versionCode) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_buildNumber, versionCode);
  }

  static Future<String> getCurrentBuildNumber() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_buildNumber) ?? '1';
  }

  static setIntroSeenStatus(int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_isSeenIntro, value);
  }

  static Future<int> getIntroSeenStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_isSeenIntro) ?? 0;
  }

  static setNightMode(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isNightMode, value);
  }

  static Future<bool> getNightMode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isNightMode) ?? false;
  }

  static setNotifiableStatus(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isNotificationOpen, value);
  }

  static Future<bool> getNotifiableStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isNotificationOpen) ?? true;
  }

  static setThemeProperties(List<String> colorArray) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList(_themeProperties, colorArray);
  }

  static Future<List<String>> getThemeProperties(String defaultTheme) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(_themeProperties) ?? ['255','255','255','255','220','0','0','0','1.5','1.5','3.0',defaultTheme];
  }

  static setFontProperties(List<String> array) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList(_fontProperties, array);
  }

  static Future<List<String>> getFontProperties() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(_fontProperties) ?? ['1','Algerian Regular','0.09','5','0','4.0','2.0','1.35'];
  }

  static setIsPremiumStatus(bool isPremium) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isPremium, isPremium);
  }

  static Future<bool> getIsPremiumStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isPremium) ?? false;
  }

  static setNotificationSetStatus(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isNotificationSet, value);
  }

  static Future<bool> getNotificationSetStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isNotificationSet) ?? false;
  }

  static setExpiryTime(int expiry) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_expiryTime, expiry);
  }

  static Future<int> getExpiryTime() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_expiryTime) ?? 1603107459537;
  }

  static setIsSeenRateUs(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isSeenRateUs, value);
  }

  static Future<bool> getIsSeenRateUs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isSeenRateUs) ?? false;
  }

  static setIsWarnedNotificationLimitIos(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isWarnedNotificationLimitIos, value);
  }

  static Future<bool> getIsWarnedNotificationLimitIos() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isWarnedNotificationLimitIos) ?? false;
  }

  static setIsSeenGif(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isSeenGif, value);
  }

  static Future<bool> getIsSeenGif() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isSeenGif) ?? false;
  }

  static setIsRecordDbCreatedEvent(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isRecordDbCreatedEvent, value);
  }

  static Future<bool> getIsRecordDbCreatedEvent() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isRecordDbCreatedEvent) ?? false;
  }

  static setIsHomeScreenFirstOpen(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isHomeScreenFirstOpen, value);
  }

  static Future<bool> getIsHomeScreenOpenBefore() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isHomeScreenFirstOpen) ?? false;
  }

  static setIsUserExperienceSeen(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isUserExperienceSeen, value);
  }

  static Future<bool> getIsUserExperienceSeen() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isUserExperienceSeen) ?? false;
  }

  static setAppOpenCount(int count) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_appOpenCount, count);
  }

  static Future<int> getAppOpenCount() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_appOpenCount) ?? 0;
  }

  static setReminderStartHour(int time) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_reminderStartHour, time);
  }

  static Future<int> getReminderStartHour() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_reminderStartHour) ?? 9;
  }

  static setReminderStartMinute(int time) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_reminderStartMinute, time);
  }

  static Future<int> getReminderStartMinute() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_reminderStartMinute) ?? 0;
  }

  static setReminderEndHour(int time) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_reminderEndHour, time);
  }

  static Future<int> getReminderEndHour() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_reminderEndHour) ?? 21;
  }

  static setReminderEndMinute(int time) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_reminderEndMinute, time);
  }

  static Future<int> getReminderEndMinute() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_reminderEndMinute) ?? 0;
  }

  static setMusicPreference(String musicName) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_musicPreference, musicName);
  }

  static Future<String> getMusicPreference() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_musicPreference) ?? '';
  }

  static setWidgetSettings(String settings) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREFS_WIDGET_SETTINGS, settings);
  }

  static Future<String> getWidgetSettings() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREFS_WIDGET_SETTINGS) ?? '4278190335';
  }


  static setDbLockValue(int time) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_dbLockValue, time);
  }

  static Future<int> getDbLockValue() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_dbLockValue) ?? 2;
  }


  static setIsPremiumOrSubscribedStatus(bool isPremium) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isPremiumOrSubscribe, isPremium);
  }

  static Future<bool> getIsPremiumOrSubscribedStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isPremiumOrSubscribe) ?? false;
  }

  static setTimezone(String timezone) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_timezone,timezone);
  }

  static Future<String> getTimeZone() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_timezone) ?? 'America/Detroit';
  }

  static setNewThemesAlert(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_newThemesAlert, value);
  }

  static Future<bool> getNewThemesAlert() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_newThemesAlert) ?? true;
  }

  static setNewTopicsAlert(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_newTopicsAlert, value);
  }

  static Future<bool> getNewTopicsAlert() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_newTopicsAlert) ?? true;
  }

  static setNewSettingsAlert(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_newSettingsAlert, value);
  }

  static Future<bool> getNewSettingsAlert() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_newSettingsAlert) ?? true;
  }

  static setStarRate(int count) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_starRate, count);
  }

  static Future<int> getStarRate() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_starRate) ?? 0;
  }

  static setLocalLanguageCode(String timezone) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_localLanguageCode,timezone);
  }

  static Future<String> getLocalLanguageCode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_localLanguageCode) ?? 'en';
  }

  static setIsUsingLiveTheme(bool isUsingLiveTheme) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isUsingLiveTheme, isUsingLiveTheme);
  }

  static Future<bool> getIsUsingLiveTheme() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isUsingLiveTheme) ?? false;
  }

  static setVersionRelease(int count) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(_versionRelease, count);
  }

  static Future<int> getVersionRelease() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_versionRelease) ?? 9;
  }

  static Future<bool> getFirstOpenReportedToFB() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_firstOpenReportedToFB) ?? false;
  }

  static setFirstOpenReportedToFB(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_firstOpenReportedToFB, value);
  }

}