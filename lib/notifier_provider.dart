import 'dart:ui';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

import 'DbProvider.dart';



class DayNightChanger with ChangeNotifier {
  ThemeData _themeData;
  Brightness _brightness;
  Color _appBarColor;
  Color _textColor;
  Decoration _decorationCard;
  Decoration _decorationButton;
  Color _searchBarColor;
  Color _quoteColor;
  bool _isNightMode;

  DayNightChanger(
    this._themeData,
    this._brightness,
    this._appBarColor,
    this._textColor,
    this._decorationCard,
    this._decorationButton,
    this._searchBarColor,
    this._quoteColor,
      this._isNightMode,
  );

  getTheme() => _themeData;
  getBrightness() => _brightness;
  getAppBarColor() => _appBarColor;
  getTextColor() => _textColor;
  getDecorationCard() => _decorationCard;
  getDecorationButton() => _decorationButton;
  getSearchBarColor() => _searchBarColor;
  getQuoteColor() => _quoteColor;
  getIsNightMode()=> _isNightMode;

  setTheme(ThemeData theme) {
    _themeData = theme;
    notifyListeners();
  }

  setBrightness(Brightness brightness) {
    _brightness = brightness;
    notifyListeners();
  }

  setAppBarColor(Color colour) {
    _appBarColor = colour;
    notifyListeners();
  }

  setTextColor(Color colour) {
    _textColor = colour;
    notifyListeners();
  }

  setDecorationCard(Decoration decoration) {
    _decorationCard = decoration;
    notifyListeners();
  }

  setDecorationButton(Decoration decoration) {
    _decorationButton = decoration;
    notifyListeners();
  }

  setSearchBarColor(Color colour) {
    _searchBarColor = colour;
    notifyListeners();
  }

  setQuoteColor(Color colour) {
    _quoteColor = colour;
    notifyListeners();
  }

  setIsNightMode(bool isNightMode) {
    _isNightMode = isNightMode;
    notifyListeners();
  }
}

class GeneralChanger with ChangeNotifier {
  bool _showBar;
  bool _isNotifiable;
  int _notificationCount;
  bool _isPremiumOrSubscribed;
  int _sessionTopicClickCount;
  int _sessionMusicClickCount;
  int _sessionThemeClickCount;
  bool _isProcessing;
  bool _isFirstOpen;
  String _timeZone;
  bool _isNotificationSet;
  String _bestOfferPrice;
  bool _isSeenRateUs;
  List<QuoteQuery> _quoteQuery;
  bool _isOnLaunch;
  bool _isShuffled;
  bool _didChangedIconStatus;
  int _starRate;
  bool _wasDirectedToPremium;
  GeneralChanger(this._showBar, this._isNotifiable, this._notificationCount, this._isPremiumOrSubscribed, this._sessionTopicClickCount, this._sessionMusicClickCount, this._sessionThemeClickCount, this._isProcessing, this._isFirstOpen, this._timeZone, this._isNotificationSet, this._bestOfferPrice, this._isSeenRateUs, this._quoteQuery, this._isOnLaunch, this._isShuffled, this._didChangedIconStatus, this._starRate, this._wasDirectedToPremium);

  getBarStatus() => _showBar;
  getIsNotifiable() => _isNotifiable;
  getNotificationCount() => _notificationCount;
  getIsPremiumOrSubscribed() => _isPremiumOrSubscribed;
  getSectionSessionClickCount() => _sessionTopicClickCount;
  getMusicSessionClickCount() => _sessionMusicClickCount;
  getThemeSessionClickCount() => _sessionThemeClickCount;
  getIsProcessing() => _isProcessing;
  getIsFirstOpen() => _isFirstOpen;
  getTimeZone() => _timeZone;
  getIsNotificationSet() => _isNotificationSet;
  getBestOfferPrice() => _bestOfferPrice;
  getIsSeenRateUs() => _isSeenRateUs;
  getQuoteQuery() => _quoteQuery;
  getOnLaunch() => _isOnLaunch;
  getIsShuffled() => _isShuffled;
  getDidChangedIconStatus() => _didChangedIconStatus;
  getStarRate() => _starRate;
  getWasDirectedToPremium() => _wasDirectedToPremium;

  setBarStatus(bool status) {
    _showBar = status;
    notifyListeners();
  }
  setIsNotifiable(bool isNotifiable){
    _isNotifiable = isNotifiable;
    notifyListeners();
  }
  setNotificationCount(int count) {
    _notificationCount = count;
    notifyListeners();
  }
  setIsPremiumOrSubscribed(bool value){
    _isPremiumOrSubscribed = value;
    notifyListeners();
  }
  setSectionSessionClickCount(int count) {
    _sessionTopicClickCount = count;
    notifyListeners();
  }
  setMusicSessionClickCount(int count) {
    _sessionMusicClickCount = count;
    notifyListeners();
  }
  setThemeSessionClickCount(int count) {
    _sessionThemeClickCount = count;
    notifyListeners();
  }
  setIsProcessing(bool status) {
    _isProcessing = status;
    notifyListeners();
  }
  setIsFirstOpen(bool value) {
    _isFirstOpen = value;
    notifyListeners();
  }
  setTimeZone(String timeZone) {
    _timeZone = timeZone;
    notifyListeners();
  }
  setIsNotificationSet(bool value) {
    _isNotificationSet = value;
    notifyListeners();
  }
  setBestOfferPrice(String price) {
    _bestOfferPrice = price;
    notifyListeners();
  }
  setIsSeenRateUs(bool value) {
    _isSeenRateUs = value;
    notifyListeners();
  }
  setQuoteQuery(List<QuoteQuery> query) {
    _quoteQuery = query;
    notifyListeners();
  }
  setOnLaunch(bool value) {
    _isOnLaunch = value;
    notifyListeners();
  }
  setIsShuffled(bool value) {
    _isShuffled = value;
    notifyListeners();
  }

  setDidChangedIconStatus(bool value) {
    _didChangedIconStatus = value;
    notifyListeners();
  }

  setStarRate(int value) {
    _starRate = value;
    notifyListeners();
  }
  setWasDirectedToPremium(bool value) {
    _wasDirectedToPremium = value;
    notifyListeners();
  }
}

class ThemeChanger with ChangeNotifier {
  String _themeName;
  TextStyle _textStyle;
  TextStyle _textStyleAuthorName;
  Color _buttonColor;
  List<String> _themeValues;
  bool _isThemeChanged;
  ThemeChanger(this._themeName, this._textStyle, this._textStyleAuthorName, this._buttonColor, this._themeValues, this._isThemeChanged);

  getThemeName() => _themeName;
  getTextStyle() => _textStyle;
  getTextStyleAuthorName() => _textStyleAuthorName;
  getButtonColor() => _buttonColor;
  getThemeValues() => _themeValues;
  getIsThemeChanged() => _isThemeChanged;

  setThemeName(String name) {
    _themeName = name;
    notifyListeners();
  }

  setTextStyle(TextStyle style) {
    _textStyle = style;
    notifyListeners();
  }

  setTextStyleAuthorName(TextStyle styleAuthorName) {
    _textStyleAuthorName = styleAuthorName;
    notifyListeners();
  }

  setButtonColor(Color colour) {
    _buttonColor = colour;
    notifyListeners();
  }

  setThemeValues(List<String> array) {
    _themeValues = array;
    notifyListeners();
  }
  setIsThemeChanged(bool value) {
    _isThemeChanged = value;
    notifyListeners();
  }

}

class MusicChanger with ChangeNotifier {
  AudioPlayer _advancedPlayer;
  String _playingMusic;
  MusicChanger(this._advancedPlayer, this._playingMusic);

  getAdvancedPlayer() => _advancedPlayer;
  getPlayingMusic() => _playingMusic;

  setAdvancedPlayer(AudioPlayer player) {
    _advancedPlayer = player;
    notifyListeners();
  }
  setPlayingMusic(String value) {
    _playingMusic = value;
    notifyListeners();
  }
}

class RemoteNotifyChanger with ChangeNotifier {
  int _forceVideoAds;
/*  String _admobRewardedAdUnitId;
  String _iosAppId;*/
  List<int> _themeRewardedJson;
  List<int> _sectionRewardedJson;
  List<int> _musicRewardedJson;
  int _rateUsPageNumber;
  List<int> _nativeAdsIndexes;
  String _defaultTheme;
  int _freeTrialOpen;
  int _showPremiumPage;
  List<int> _rateUsJson;
  int _showLiveThemes;
  bool _showFeed;
  int _nativeAdsMode;
  String _fbNativePlacementId;
  String _fbInterstitialPlacementId;
  String _fbBannerPlacementId;
  String _fbNativeBannerPlacementId;
  bool _showBannerAd;
  bool _showBannerAdForIOS;
  List<int> _showPremiumOnLaunchArray;
  bool _nativeBannerOrBanner;
  RemoteNotifyChanger(this._forceVideoAds, this._themeRewardedJson, this._sectionRewardedJson, this._musicRewardedJson, this._rateUsPageNumber, this._nativeAdsIndexes, this._defaultTheme, this._freeTrialOpen, this._showPremiumPage, this._rateUsJson, this._showLiveThemes, this._showFeed, this._nativeAdsMode, this._fbBannerPlacementId, this._fbNativePlacementId, this._fbInterstitialPlacementId, this._fbNativeBannerPlacementId, this._showBannerAd, this._showBannerAdForIOS, this._showPremiumOnLaunchArray, this._nativeBannerOrBanner);


  getForceVideoAdsValue() => _forceVideoAds;
/*  getAdmobRewardedAdUnitId() => _admobRewardedAdUnitId;
  getAdmobAppId()=> _iosAppId;*/
  getThemeRewardedJson() => _themeRewardedJson;
  getSectionRewardedJson() => _sectionRewardedJson;
  getMusicRewardedJson() => _musicRewardedJson;
  getRateUsPageNumber() => _rateUsPageNumber;
  getNativeAdsIndexes() => _nativeAdsIndexes;
  getDefaultTheme() => _defaultTheme;
  getFreeTrialOpen() => _freeTrialOpen;
  getShowPremiumPage() => _showPremiumPage;
  getRateUsJson() => _rateUsJson;
  getShowLiveThemes() => _showLiveThemes;
  getShowFeed() => _showFeed;
  getNativeAdsMode() => _nativeAdsMode;
  getFbNativePlacementId() => _fbNativePlacementId;
  getFbInterstitialPlacementId() => _fbInterstitialPlacementId;
  getFbNativeBannerPlacementId() => _fbNativeBannerPlacementId;
  getFbBannerPlacementId() => _fbBannerPlacementId;
  getShowBannerAd() => _showBannerAd;
  getShowBannerAdForIOS() => _showBannerAdForIOS;
  getShowPremiumOnLaunchArray() => _showPremiumOnLaunchArray;
  getNativeBannerOrBanner() => _nativeBannerOrBanner;
  
  setForceVideoAdsValue(int value) {
    _forceVideoAds = value;
    notifyListeners();
  }
  
/*  setAdmobRewardedAdUnitId(String adUnit) {
    _admobRewardedAdUnitId = adUnit;
    notifyListeners();
  }
  
  setAdmobAppId(String adUnit) {
    _iosAppId = adUnit;
    notifyListeners();
  }*/

  setThemeRewardedJson(List<int> array) {
    _themeRewardedJson = array;
    notifyListeners();
  }

  setSectionRewardedJson(List<int> array) {
    _sectionRewardedJson = array;
    notifyListeners();
  }

  setMusicRewardedJson(List<int> array) {
    _musicRewardedJson = array;
    notifyListeners();
  }

  setRateUsPageNumber(int number) {
    _rateUsPageNumber = number;
    notifyListeners();
  }

  setNativeAdsIndexes(List<int> array) {
    _nativeAdsIndexes = array;
    notifyListeners();
  }

  setDefaultTheme(String value) {
    _defaultTheme = value;
    notifyListeners();
  }

  setFreeTrialOpen(int value) {
    _freeTrialOpen = value;
    notifyListeners();
  }

  setShowPremiumPage(int value) {
    _showPremiumPage = value;
    notifyListeners();
  }
  setRateUsJson(List<int> array) {
    _rateUsJson = array;
    notifyListeners();
  }

  setShowLiveThemes(int value) {
    _showLiveThemes = value;
    notifyListeners();
  }

  setShowFeed(bool value) {
    _showFeed = value;
    notifyListeners();
  }

  setNativeAdsMode(int value) {
    _nativeAdsMode = value;
    notifyListeners();
  }

  setFbNativePlacementId(String value) {
    _fbNativePlacementId = value;
    notifyListeners();
  }

  setFbInterstitialPlacementId(String value) {
    _fbInterstitialPlacementId = value;
    notifyListeners();
  }

  setFbBannerPlacementId(String value) {
    _fbBannerPlacementId = value;
    notifyListeners();
  }

  setFbNativeBannerPlacementId(String value) {
    _fbNativeBannerPlacementId = value;
    notifyListeners();
  }

  setShowBannerAd(bool value) {
    _showBannerAd = value;
    notifyListeners();
  }

  setShowBannerAdForIOS(bool value) {
    _showBannerAdForIOS = value;
    notifyListeners();
  }

  setShowPremiumOnLaunchArray(List<int> value) {
    _showPremiumOnLaunchArray = value;
    notifyListeners();
  }

  setNativeBannerOrBanner(bool value) {
    _nativeBannerOrBanner = value;
    notifyListeners();
  }
}


class EventChanger with ChangeNotifier {
  bool _isScrolledOnce;
  bool _isScrolledForth;
  EventChanger(this._isScrolledOnce, this._isScrolledForth);

  getIsScrolled() => _isScrolledOnce;
  getIsScrolledForth() => _isScrolledForth;

  setIsScrolledOnce(bool value) {
    _isScrolledOnce = value;
    notifyListeners();
  }
  setIsScrolledForth(bool value) {
    _isScrolledForth = value;
    notifyListeners();
  }
}


class FontChanger with ChangeNotifier {
  List<String> _fontValues;
  FontChanger(this._fontValues);

  getFontValues() => _fontValues;

  setFontId(int value) {
    notifyListeners();
  }

  setFontValues(List<String> array) {
    _fontValues = array;
    notifyListeners();
  }
}

class NewGeneralChanger with ChangeNotifier {
  bool _newTopicsAlert;
  bool _newThemesAlert;
  bool _newSettingsAlert;
  bool _usingLiveTheme;
  bool _liveTabSelected;
  bool _isFirstRewardedOnSession;
  //MoPubRewardedVideoAd _mopupRewardedAd;
/*  FacebookNativeAd _fbNativeAd;
  FacebookInterstitialAd _fbInterstitialAd;*/
  bool _isInterstitialAdLoaded;

  NewGeneralChanger(this._newTopicsAlert, this._newThemesAlert, this._newSettingsAlert, this._usingLiveTheme, this._liveTabSelected, this._isFirstRewardedOnSession, this._isInterstitialAdLoaded);
  getNewTopicsAlert() => _newTopicsAlert;
  getNewThemesAlert() => _newThemesAlert;
  getNewSettingsAlert() => _newSettingsAlert;
  getUsingLiveTheme() => _usingLiveTheme;
  getLiveTabSelected() => _liveTabSelected;
  getIsFirstRewardedOnSession() => _isFirstRewardedOnSession;
/*  getMopupRewarded() => _mopupRewardedAd;
  getFbNativeAd() => _fbNativeAd;
  getFbInterstitialAd() => _fbInterstitialAd;*/
  getIsInterstitialAdLoaded() => _isInterstitialAdLoaded;


  setNewTopicsAlert(bool value) {
    _newTopicsAlert = value;
    notifyListeners();
  }
  setNewThemesAlert(bool value) {
    _newThemesAlert = value;
    notifyListeners();
  }
  setNewSettingsAlert(bool value) {
    _newSettingsAlert = value;
    notifyListeners();
  }
  setUsingLiveTheme(bool value) {
    _usingLiveTheme = value;
    notifyListeners();
  }
  setLiveTabSelected(bool value) {
    _liveTabSelected = value;
    notifyListeners();
  }

  setIsFirstRewardedOnSession(bool value) {
    _isFirstRewardedOnSession = value;
    notifyListeners();
  }

/*  setMopupRewarded(MoPubRewardedVideoAd ad) {
    _mopupRewardedAd = ad;
    notifyListeners();
  }

  setFbNativeAd(FacebookNativeAd ad) {
    _fbNativeAd = ad;
    notifyListeners();
  }

  setFbInterstitialAd(FacebookInterstitialAd ad) {
    _fbInterstitialAd = ad;
    notifyListeners();
  }*/

  setIsInterstitialAdLoaded(bool value) {
    _isInterstitialAdLoaded = value;
    notifyListeners();
  }

}