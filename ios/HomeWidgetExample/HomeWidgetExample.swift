//
//  HomeWidgetExample.swift
//  HomeWidgetExample
//
//  Created by Ömer Karaca on 1.05.2021.
//

import WidgetKit
import SwiftUI

private let widgetGroupId = "group.com.viyatek.motilife"

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> ExampleEntry {
        ExampleEntry(date: Date(), title: "2352", message: "Live the way you want, not the way others want", author: "", themeName: "f9")
    }
    
    func getSnapshot(in context: Context, completion: @escaping (ExampleEntry) -> ()) {
        let data = UserDefaults.init(suiteName:widgetGroupId)
        let entry = ExampleEntry(date: Date(), title: data?.string(forKey: "title") ?? "2352", message: data?.string(forKey: "message") ?? "Live the way you want, not the way others want.", author: data?.string(forKey: "author") ?? "", themeName: data?.string(forKey: "themeName") ?? "f9")
        completion(entry)
    }
    
    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        getSnapshot(in: context) { (entry) in
            let timeline = Timeline(entries: [entry], policy: .atEnd)
            completion(timeline)
        }
    }
}

struct ExampleEntry: TimelineEntry {
    let date: Date
    let title: String
    let message: String
    let author: String
    let themeName: String
}

struct HomeWidgetExampleEntryView : View {
    var entry: Provider.Entry
    let data = UserDefaults.init(suiteName:widgetGroupId)
    
    var body: some View {
        ZStack {
            
            Image(entry.themeName)
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)

            VStack.init(alignment: .center, spacing: 5, content: {
                        Text(entry.message).multilineTextAlignment(.center)
                            .font(.system(size: 16)).foregroundColor(Color.white)
                            .widgetURL(URL(string: "homeWidgetExample://message?message=\(entry.message)&homeWidget"))
                        Text(entry.author).bold().font(.system(size: 11)).foregroundColor(Color.white).widgetURL(URL(string: "homeWidgetExample://message?message=\(entry.author)&homeWidget"))
            }
            ).padding(EdgeInsets(top: 3, leading: 3, bottom: 3, trailing: 3))
        }
        
        
//        Color.black
//                .ignoresSafeArea() // Ignore just for the color
//            .overlay(VStack.init(alignment: .center, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
//                    Text(entry.message)
//                        .font(.body).foregroundColor(Color.white)
//                        .widgetURL(URL(string: "homeWidgetExample://message?message=\(entry.message)&homeWidget"))
//                    Text(entry.author).bold().font(.footnote).foregroundColor(Color.white).widgetURL(URL(string: "homeWidgetExample://message?message=\(entry.author)&homeWidget"))
//        }
//        ).background(
//            Image(entry.themeName)
//                .scaledToFill()
//                            .edgesIgnoringSafeArea(.all)
//        ))
        
        
        
    }
}

@main
struct HomeWidgetExample: Widget {
    let kind: String = "HomeWidgetExample"
    
    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            HomeWidgetExampleEntryView(entry: entry)
        }
        .configurationDisplayName("Quotes")
        .description("Inspirational Positive Quotes.")
    }
}

struct HomeWidgetExample_Previews: PreviewProvider {
    static var previews: some View {
        HomeWidgetExampleEntryView(entry: ExampleEntry(date: Date(), title: "915", message: "Love all, trust a few, do wrong to none.", author: "William Shakespeare", themeName: "f9"))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
