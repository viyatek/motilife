INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (1, 'motivation', 'Motivation');
INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (2, 'inspiration', 'Inspiration');
INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (3, 'hardtimes', 'Hard Times');
INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (4, 'selfcare', 'Self-Care');
INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (5, 'healthfitness', 'Health & Fitness');
INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (6, 'love', 'Love');
INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (7, 'faith', 'Faith');
INSERT INTO topic_table (topicid, topicimagename, topicname) VALUES (8, 'peace', 'Peace');


INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (1,'selfdiscipline','Self Discipline',1,4,5,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (2,'positivity','Positivity',1,4,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (3,'happiness','Happiness',1,4,6,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (4,'success','Success',1,5,2,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (5,'focus','Focus',1,8,5,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (6,'divorce','Divorce',3,NULL,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (7,'regret','Regret',3,NULL,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (8,'sadness','Sadness',3,NULL,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (9,'pain','Pain',3,5,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (10,'breakup','Break Up',3,NULL,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (11,'depression','Depression',3,NULL,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (12,'money','Money',1,8,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (13,'sport','Sport',5,4,1,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (14,'freedom','Freedom',7,8,2,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (15,'hope','Hope',7,8,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (16,'patience','Patience',7,8,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (17,'time','Time',1,NULL,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (18,'art','Art',8,NULL,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (19,'family','Family',8,6,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (20,'trust','Trust',8,6,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (21,'change','Change',1,4,5,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (22,'entrepreneur','Entrepreneur',2,3,1,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (23,'education','Education',4,NULL,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (24,'creativity','Creativity',2,NULL,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (25,'wisdom','Wisdom',4,8,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (26,'courage','Courage',4,1,3,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (27,'dream','Dream',6,7,1,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (28,'ego','Ego',4,8,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (29,'dating','Dating',6,8,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (30,'loveyourself','Love Yourself',4,6,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (31,'kindness','Kindness',6,8,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (32,'passion','Passion',1,2,7,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (33,'marriage','Marriage',6,7,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (34,'friendship','Friendship',6,8,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (35,'travel','Travel',5,8,1,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (36,'health','Health',5,1,8,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (37,'hardwork','Hard Work',1,4,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (38,'goals','Goals',1,2,4,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (39,'cooperation','Cooperation',8,NULL,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (40,'leadership','Leadership',2,1,4,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (41,'appreciation','Appreciation',8,7,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (42,'love','Love',6,8,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (43,'stress','Stress',3,NULL,NULL,0);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (44,'bestrong','Be Strong',1,3,NULL,1);
INSERT INTO "section_table" ("sectionid","sectionimagename","sectionname","topicid1","topicid2","topicid3","isPremium") VALUES (45,'respect','Respect',4,7,8,1);



INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('d2',0,255,255,255,255,2.0,2.0,4.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('d1',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f1',0,255,255,255,255,1.5,1.5,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f2',1,255,255,255,255,2.0,2.0,4.0,230,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f3',1,255,255,255,255,1.5,1.5,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f4',1,255,255,255,255,1.5,1.5,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f5',0,255,255,255,255,1.5,1.5,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f6',1,255,255,255,255,2.0,2.0,4.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f7',1,255,255,255,255,1.5,1.5,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f8',0,255,255,255,255,1.5,1.5,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f9',1,255,255,255,255,1.0,1.0,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f10',1,255,255,255,255,1.0,1.0,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f11',0,255,255,255,255,1.0,1.0,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f12',1,255,255,255,255,2.0,2.0,4.0,230,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f13',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('f14',1,255,255,255,255,1.0,1.0,3.0,200,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('g1',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('g2',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('g3',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('g4',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('g5',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('g6',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('g7',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h1',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h2',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h3',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h4',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h5',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h6',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h7',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h8',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('h9',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('i1',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('i2',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('i3',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('i4',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('i5',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('i6',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);



INSERT INTO "my_live_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('live1',0,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_live_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('live2',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_live_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('live5',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_live_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('live7',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);
INSERT INTO "my_live_theme_table" ("themeName","isPremiumTheme","a","r","g","b","xOffset1","yOffset1","blurRadius1","aShadow1","rShadow1","gShadow1","bShadow1") VALUES ('live8',1,255,255,255,255,1.5,1.5,3.0,220,0,0,0);



INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (1,'Algerian Regular','Algerian',0.09,4,0,4.0,2.0,1.35);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (2,'Northwood High','Northwood',0.11,4,0,2.5,1.2,1.25);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (3,'Hanken Grotesk','Hanken Grotesk',0.1,4,0,3.5,1.5,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (4,'Script MT Bold','Script',0.11,3,0,3.5,1.3,1.1);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (5,'VintageITC TT','Vintage',0.1,5,0,2.5,1.2,1.25);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (6,'Hey-Jack','Hey-Jack',0.09,4,0,3.7,1.5,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (7,'Copperplate Gothic Bold','Copperplate',0.1,4,0,3.0,1.0,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (8,'Monterey Regular FLF','Monterey',0.11,4,1,2.5,1.0,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (9,'Black Chancery','Chancery',0.1,5,1,3.5,1.5,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (10,'PG Isadora Cyr Pro Regular','PG Isadora',0.1,4,1,3.5,1.5,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (11,'AuntieLee Light','AuntieLee',0.09,6,1,4.0,1.8,1.4);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (12,'Trajan Pro Regular','Trajan',0.077,4,1,3.0,1.4,1.4);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (13,'Boldace Brush','Boldace',0.09,4,1,3.7,1.5,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (14,'Chameron','Chameron',0.085,4,1,3.2,1.3,1.4);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (15,'Romantica','Romantica',0.1,4,1,3.5,1.4,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (16,'Carnevalee','Carnevalee',0.1,4,1,3.5,1.4,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (17,'Futura Book','Futura',0.1,4,1,3.5,1.4,1.3);
INSERT INTO "font_table" ("fontId","fontName","displayName","fontSize","fontWeight","isPremiumFont","wordSpacing","letterSpacing","height") VALUES (18,'Red-Rock','Red-Rock',0.09,4,1,3.7,1.5,1.3);



INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (1, 'Forest', 'forest', 0);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (2, 'City', 'city', 0);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (3, 'Saturn', 'saturn', 0);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (4, 'Night', 'night', 0);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (5, 'Piano', 'piano', 0);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (6, 'Submarine', 'submarine', 0);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (7, 'Jungle', 'jungle', 0);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (8, 'Whisper', 'whisper', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (9, 'Fire', 'fire', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (10, 'Ambient', 'ambient', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (11, 'Desert', 'desert', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (12, 'Stream', 'stream', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (13, 'Nature', 'nature', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (14, 'Rain', 'rain', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (15, 'Wind', 'wind', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (16, 'Birds', 'birds', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (17, 'Mood', 'mood', 1);
INSERT INTO my_music_table (musicId, musicName, musicUrl, isPremiumMusic) VALUES (18, 'Meadow', 'meadow', 1);
