
-- Table: quotes_table
DROP TABLE IF EXISTS quotes_table;
CREATE TABLE quotes_table(
quoteid INTEGER PRIMARY KEY,
priority INTEGER,
quote TEXT,
sectionid1 INTEGER,
sectionid2 INTEGER,
sectionid3 INTEGER,
authorname TEXT
);


CREATE TABLE IF NOT EXISTS user_quote_table (
userQuoteId INTEGER PRIMARY KEY,
isSeenStatus INTEGER,
quoteBookmarkedStatus INTEGER,
quoteCollectedStatus INTEGER
);

-- Table: section_table
DROP TABLE IF EXISTS section_table;
CREATE TABLE section_table (sectionid INTEGER PRIMARY KEY, sectionimagename TEXT, sectionname TEXT, topicid1 INTEGER, topicid2 INTEGER, topicid3 INTEGER, isPremium INTEGER);

-- Table: topic_table
DROP TABLE IF EXISTS topic_table;
CREATE TABLE topic_table(
topicid INTEGER PRIMARY KEY,
topicimagename TEXT,
topicname TEXT
);

DROP TABLE IF EXISTS my_theme_table;
CREATE TABLE my_theme_table (
	themeName TEXT PRIMARY KEY,
	isPremiumTheme INTEGER,
	a INTEGER,
	r INTEGER,
	g INTEGER,
	b INTEGER,
	xOffset1 BLOB,
	yOffset1 BLOB,
	blurRadius1 BLOB,
	aShadow1 INTEGER,
	rShadow1 INTEGER,
	gShadow1 INTEGER,
	bShadow1 INTEGER
);

DROP TABLE IF EXISTS my_live_theme_table;
CREATE TABLE my_live_theme_table (
	themeName TEXT PRIMARY KEY,
	isPremiumTheme INTEGER,
	a INTEGER,
	r INTEGER,
	g INTEGER,
	b INTEGER,
	xOffset1 BLOB,
	yOffset1 BLOB,
	blurRadius1 BLOB,
	aShadow1 INTEGER,
	rShadow1 INTEGER,
	gShadow1 INTEGER,
	bShadow1 INTEGER
);


DROP TABLE IF EXISTS my_music_table;
CREATE TABLE my_music_table (
    musicId INTEGER PRIMARY KEY,
	musicName TEXT,
	musicUrl TEXT,
	isPremiumMusic INTEGER
);


DROP TABLE IF EXISTS font_table;
CREATE TABLE font_table (
    fontId INTEGER PRIMARY KEY,
	fontName TEXT,
	displayName TEXT,
	fontSize REAL,
	fontWeight INTEGER,
	isPremiumFont INTEGER,
	wordSpacing BLOB,
	letterSpacing BLOB,
	height BLOB
);

CREATE UNIQUE INDEX "quotes_index" ON "quotes_table" (
	"quoteid"	ASC
);

CREATE UNIQUE INDEX "section_index" ON "section_table" (
	"sectionid"	ASC
);

CREATE UNIQUE INDEX "topic_index" ON "topic_table" (
	"topicid" ASC
);

CREATE UNIQUE INDEX "theme_index" ON "my_theme_table" (
	"themeName"	ASC
);

CREATE UNIQUE INDEX "live_theme_index" ON "my_live_theme_table" (
	"themeName"	ASC
);

CREATE UNIQUE INDEX "music_index" ON "my_music_table" (
	"musicId"	ASC
);

CREATE UNIQUE INDEX "font_index" ON "font_table" (
	"fontId"	ASC
);
